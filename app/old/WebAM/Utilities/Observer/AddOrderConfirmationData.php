<?php

namespace WebAM\Utilities\Observer;

use Magento\Framework\Event\ObserverInterface;

class AddOrderConfirmationData implements ObserverInterface
{
    /**
     * @var \Magento\Cms\Block\BlockFactory
     */
    private $cmsBlockFactory;

    /**
     * @param \Magento\Cms\Block\BlockFactory $cmsBlockFactory
     */
    public function __construct(\Magento\Cms\Block\BlockFactory $cmsBlockFactory)
    {
        $this->cmsBlockFactory = $cmsBlockFactory;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getTransport()->getOrder();
        $extraPaymentInfo = "";

        if ($order->getPayment()->getMethod() == 'banktransfer') {
            $cmsBlock = $this->cmsBlockFactory->create();
            $cmsBlock->setBlockId("banktransfer_extra_info");
            $extraPaymentInfo = $cmsBlock->toHtml();
        }

        /** @var \Magento\Framework\App\Action\Action $controller */
        $transport = $observer->getTransport();
        $transport['extra_payment_info'] = $extraPaymentInfo;
    }
}
