<?php
declare(strict_types=1);

namespace Leanpay\Payment\Model\Method;

class LeanpayInstallment extends Leanpay
{
    const CODE = 'leanpay_installment';
}
