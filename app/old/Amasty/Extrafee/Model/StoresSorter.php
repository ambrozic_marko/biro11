<?php

namespace Amasty\Extrafee\Model;

use Magento\Store\Api\Data\StoreInterface;

class StoresSorter
{
    /**
     * @param array|StoreInterface[] $stores
     * @return array
     */
    public function getStoresSortedBySortOrder($stores)
    {
        if (is_array($stores)) {
            usort($stores, function ($storeA, $storeB) {
                if ($storeA->getSortOrder() == $storeB->getSortOrder()) {
                    return $storeA->getId() < $storeB->getId() ? -1 : 1;
                }
                return ($storeA->getSortOrder() < $storeB->getSortOrder()) ? -1 : 1;
            });
        }

        return $stores;
    }
}
