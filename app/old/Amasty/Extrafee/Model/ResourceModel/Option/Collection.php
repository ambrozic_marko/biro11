<?php

namespace Amasty\Extrafee\Model\ResourceModel\Option;

use Amasty\Extrafee\Model\Option as OptionEntity;
use Amasty\Extrafee\Model\ResourceModel\Option;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init(OptionEntity::class, Option::class);
    }
}
