<?php

namespace Amasty\Extrafee\Model;

use Magento\Framework\Model\AbstractModel;

class Option extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\Option::class);
    }
}
