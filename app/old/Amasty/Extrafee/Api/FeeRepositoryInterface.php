<?php

namespace Amasty\Extrafee\Api;

use Amasty\Extrafee\Api\Data\FeeInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface FeeRepositoryInterface
{
    /**
     * Save
     *
     * @param \Amasty\Extrafee\Api\Data\FeeInterface $fee
     * @param string[] $options
     * @return \Amasty\Extrafee\Api\Data\FeeInterface
     */
    public function save(FeeInterface $fee, $options);

    /**
     * Get by id
     *
     * @param int $feeId
     * @return \Amasty\Extrafee\Api\Data\FeeInterface
     */
    public function getById($feeId);

    /**
     * Delete
     *
     * @param \Amasty\Extrafee\Api\Data\FeeInterface $fee
     * @return bool true on success
     */
    public function delete(FeeInterface $fee);

    /**
     * Delete by id
     *
     * @param int $feeId
     * @return bool true on success
     */
    public function deleteById($feeId);

    /**
     * @param SearchCriteriaInterface|null $searchCriteria
     * @return \Amasty\Extrafee\Api\Data\FeeSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null);

    /**
     * @param int $optionId
     * @return FeeInterface
     */
    public function getByOptionId($optionId);

    /**
     * @return FeeInterface
     */
    public function create();
}
