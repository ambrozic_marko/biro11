<?php

namespace Amasty\Extrafee\Api;

use Magento\Checkout\Api\Data\TotalsInformationInterface;

interface GuestFeesInformationManagementInterface
{
    /**
     * @param string $cartId
     * @param TotalsInformationInterface $addressInformation
     *
     * @return \Amasty\Extrafee\Api\Data\FeesManagerInterface
     */
    public function collect(
        $cartId,
        TotalsInformationInterface $addressInformation
    );
}
