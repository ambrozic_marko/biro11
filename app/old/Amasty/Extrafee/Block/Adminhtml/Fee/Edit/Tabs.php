<?php

namespace Amasty\Extrafee\Block\Adminhtml\Fee\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('amasty_extrafee_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Fee Information'));
    }
}
