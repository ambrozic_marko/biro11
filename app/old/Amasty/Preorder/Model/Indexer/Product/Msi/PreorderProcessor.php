<?php

declare(strict_types=1);

namespace Amasty\Preorder\Model\Indexer\Product\Msi;

use Magento\Framework\Indexer\AbstractProcessor;

class PreorderProcessor extends AbstractProcessor
{
    const INDEXER_ID = 'amasty_preorder_product_preorder_msi';
}
