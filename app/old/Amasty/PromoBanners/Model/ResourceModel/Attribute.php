<?php

namespace Amasty\PromoBanners\Model\ResourceModel;

class Attribute extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('amasty_banner_attribute', 'id');
    }
}
