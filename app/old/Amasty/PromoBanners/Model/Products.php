<?php

namespace Amasty\PromoBanners\Model;

use Magento\Framework\Model\AbstractModel;

class Products extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Amasty\PromoBanners\Model\ResourceModel\Products');
    }
}
