<?php

namespace Amasty\PromoBanners\Model\Source;

use Magento\Framework\Option\ArrayInterface;

class Active implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            '0' => __('Inactive'),
            '1' => __('Active'),
        ];
    }
}
