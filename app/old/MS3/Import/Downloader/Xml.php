<?php

namespace MS3\Import\Downloader;

class Xml
{
    const XML_FILE_URL_CONFIG_PATH = "metaimport_integration/#/xml_url";
    const XML_FILE_URL_CONFIG_PATH_REPLACE_CHARACTER = "#";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Framework\HTTP\ClientFactory
     */
    private $httpClientFactory;
    /**
     * @var \Magento\Framework\Xml\Parser
     */
    private $xmlParser;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\HTTP\ClientFactory $httpClientFactory,
        \Magento\Framework\Xml\Parser $xmlParser
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->httpClientFactory = $httpClientFactory;
        $this->xmlParser = $xmlParser;
    }

    public function getXml($type)
    {
        $url = $this->scopeConfig->getValue(
            str_replace(
                self::XML_FILE_URL_CONFIG_PATH_REPLACE_CHARACTER,
                $type,
                self::XML_FILE_URL_CONFIG_PATH
            )
        );

        $httpClient = $this->httpClientFactory->create();
        $httpClient->setOption(CURLOPT_SSL_VERIFYPEER, 0);
        $httpClient->get($url);
        return $this->xmlParser->loadXml($httpClient->getBody());
    }

    public function getXmlAsArray($type)
    {
        return $this->getXml($type)->xmlToArray();
    }
}
