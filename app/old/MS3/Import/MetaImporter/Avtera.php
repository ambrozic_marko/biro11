<?php

namespace MS3\Import\MetaImporter;

class Avtera extends ImportAbstract
{
    const IMPORT_TYPE = 'avtera';
    const DATA_WRAPPERS = ["podjetje", "_value", "izdelki", "izdelek"];
    const IMPORTER_ENABLED_XML_PATH = 'metaimport_integration/avtera/enable';
    const RAISE_PERCENT_XML_PATH = 'metaimport_integration/avtera/raise_by_percentage';

    /**
     * @var \MS3\Import\Mapper\Avtera
     */
    private $avteraMapper;

    public function __construct(
        \MS3\Import\Downloader\Xml $xmlDownloader,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \MS3\Import\Api\Data\MetaImportInterfaceFactory $metaImportInterfaceFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MS3\Import\Mapper\Avtera $avteraMapper,
        \Magento\Framework\Filter\FilterManager $filter,
        \MS3\Import\Model\MetaImportHistory $history
    ) {
        $this->avteraMapper = $avteraMapper;
        parent::__construct($xmlDownloader, $metaImportRepositoryInterface, $metaImportInterfaceFactory, $jsonSerializer, $searchCriteriaBuilder, $debugLogger, $scopeConfig, $filter, $history);
    }

    public function mapData($data)
    {
        $mappedData = [];
        foreach ($data as $datum) {
            $mappedData[] = $this->avteraMapper->mapIntegrationProductToMagentoProduct($datum);
        }

        return $mappedData;
    }

}
