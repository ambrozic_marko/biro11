<?php

namespace MS3\Import\MetaImporter;

class Also extends ImportAbstract
{
    const IMPORT_TYPE = 'also';
    const DATA_WRAPPERS = ["xmlData", "_value", "product"];
    const IMPORTER_ENABLED_XML_PATH = 'metaimport_integration/also/enable';
    const RAISE_PERCENT_XML_PATH = 'metaimport_integration/also/raise_by_percentage';

    /**
     * @var \MS3\Import\Mapper\Also
     */
    private $alsoMapper;

    public function __construct(
        \MS3\Import\Downloader\Xml $xmlDownloader,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \MS3\Import\Api\Data\MetaImportInterfaceFactory $metaImportInterfaceFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MS3\Import\Mapper\Also $alsoMapper,
        \Magento\Framework\Filter\FilterManager $filter,
        \MS3\Import\Model\MetaImportHistory $history
    ) {
        $this->alsoMapper = $alsoMapper;
        parent::__construct($xmlDownloader, $metaImportRepositoryInterface, $metaImportInterfaceFactory, $jsonSerializer, $searchCriteriaBuilder, $debugLogger, $scopeConfig, $filter, $history);
    }

    public function mapData($data)
    {
     //   print_r('test1');
        $mappedData = [];
     //   print_r($data);
    //    print_r('test2');
        foreach ($data as $datum) {
            $mappedData[] = $this->alsoMapper->mapIntegrationProductToMagentoProductAlso($datum);
        }
       // print_r($mappedData);
      //  print_r('test2');
      //  die();
        return $mappedData;
    }
}
