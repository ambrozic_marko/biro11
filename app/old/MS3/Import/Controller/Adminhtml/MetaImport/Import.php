<?php

namespace MS3\Import\Controller\Adminhtml\MetaImport;

use Magento\Framework\Exception\LocalizedException;

class Import extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    private $metaImportRepositoryInterface;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;
    /**
     * @var \MS3\Import\Importer\Product
     */
    private $productImporter;
    /**
     * @var \MS3\Import\Logger\Debug
     */
    private $debugLogger;

    private $filter;

    private $collectionFactory;

    /**
     * Constructor
     *
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \MS3\Import\Importer\Product $productImporter
     * @param \MS3\Import\Logger\Debug $debugLogger
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \MS3\Import\Model\ResourceModel\MetaImport\CollectionFactory $collectionFactory
     */
    public function __construct(
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \MS3\Import\Importer\Product $productImporter,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \MS3\Import\Model\ResourceModel\MetaImport\CollectionFactory $collectionFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->metaImportRepositoryInterface = $metaImportRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->jsonSerializer = $jsonSerializer;
        $this->productImporter = $productImporter;
        $this->debugLogger = $debugLogger;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $metaImportItemImported = 0;
        $metaImportItemImportedError = 0;
        /** @var \MS3\Import\Model\MetaImport $metaImportItem */
        foreach ($collection->getItems() as $metaImportItem) {
            try {
                $dataModel = $metaImportItem->getDataModel();
                $dataModel->setImported(true);
                $this->metaImportRepositoryInterface->save($dataModel);
                $metaImportItemImported++;
            } catch (LocalizedException $exception) {
                $metaImportItemImportedError++;
            }
        }

        if ($metaImportItemImported) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been set for import.', $metaImportItemImported)
            );
        }

        if ($metaImportItemImportedError) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been set for import. Please see server logs for more details.',
                    $metaImportItemImportedError
                )
            );
        }

        $this->_redirect("*/*/index");
        return $resultPage;
    }
}
