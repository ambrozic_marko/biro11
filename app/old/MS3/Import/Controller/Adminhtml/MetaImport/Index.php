<?php

namespace MS3\Import\Controller\Adminhtml\MetaImport;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    /**
     * @var \MS3\Import\Model\ErrorFlag
     */
    private $errorFlag;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \MS3\Import\Model\ErrorFlag $errorFlag
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \MS3\Import\Model\ErrorFlag $errorFlag
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $messageManager;
        $this->errorFlag = $errorFlag;
        $this->errorFlag->loadSelf();
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__("MetaImport"));

        if ($this->errorFlag->getState() == 1) {
            //$this->messageManager->addErrorMessage($this->errorFlag->getFlagData()["error"]);
        }

        return $resultPage;
    }
}
