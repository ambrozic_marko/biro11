<?php

namespace MS3\Import\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;


class Actions extends Column
{
    /** Url path */
    const URL_PATH_EDIT = 'your/url/edit';
    const URL_PATH_DELETE = 'your/url/delete';


    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['metaimport_id'])) {
                    $item[$name]['view'] = [
                        'href' => $this->urlBuilder->getUrl('ms3_import/metaimport/view', ['id' => $item['metaimport_id']]),
                        'label' => __('View')
                    ];
                    $item[$name]['import'] = [
                        'href' => $this->urlBuilder->getUrl('ms3_import/metaimport/importImmediate', ['id' => $item['metaimport_id']]),
                        'label' => __('Import Immediately'),
                        'confirm' => [
                            "title" => __("Are you sure?"),
                            "message" => __("Are you sure you want to import this row immediately?")
                        ]
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl('ms3_import/metaimport/delete', ['id' => $item['metaimport_id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            "title" => __("Are you sure?"),
                            "message" => __("Are you sure you want to delete this row?")
                        ]
                    ];
                }
            }
        }
        return $dataSource;
    }
}
