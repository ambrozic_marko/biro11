<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MS3\Import\Api\Data;

interface Ms3ImportMetaimportHistorySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get ms3_import_metaimport_history list.
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface[]
     */
    public function getItems();

    /**
     * Set sku list.
     * @param \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

