<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MS3\Import\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface Ms3ImportMetaimportHistoryRepositoryInterface
{

    /**
     * Save ms3_import_metaimport_history
     * @param \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface $ms3ImportMetaimportHistory
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface $ms3ImportMetaimportHistory
    );

    /**
     * Retrieve ms3_import_metaimport_history
     * @param string $ms3ImportMetaimportHistoryId
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($ms3ImportMetaimportHistoryId);

    /**
     * Retrieve ms3_import_metaimport_history matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistorySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete ms3_import_metaimport_history
     * @param \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface $ms3ImportMetaimportHistory
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface $ms3ImportMetaimportHistory
    );

    /**
     * Delete ms3_import_metaimport_history by ID
     * @param string $ms3ImportMetaimportHistoryId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($ms3ImportMetaimportHistoryId);
}

