<?php

namespace MS3\Import\Mapper;

class Biromat extends MapperAbstract
{
    const MAPPING_ARRAY_CONFIG_PATH = 'metaimport_integration/biromat/field_mapping';
    const ROOT_CATEGORY = 'Biromat';

    public function mapField(&$magentoProduct, $magentoKey, $value)
    {
        parent::mapField($magentoProduct, $magentoKey, $value);

        if ($magentoKey == "is_in_stock") {
            $value = $this->flattenField($value);
            $magentoProduct["qty"] = $this->mapStockStatusText($value) ? 999 : 0;
            $magentoProduct["is_in_stock"] = $this->mapStockStatusText($value) ? 1 : 0;
        }
    }

    private function mapStockStatusText($value)
    {
        switch (trim($value)) {
            case "na zalogi":
                return true;
            default:
                return false;
        }
    }
}
