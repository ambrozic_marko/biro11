<?php

namespace MS3\Import\Model;

use Magento\Framework\Exception\LocalizedException;

class MetaImportHistory
{
    /**
     * @var \MS3\Import\Api\Ms3ImportMetaimportHistoryRepositoryInterface
     */
    private $metaImportHistoryRepositoryInterface;
    /**
     * @var \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterfaceFactory
     */
    private $metaImportHistoryInterfaceFactory;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    private $stockRegistryInterface;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var \Magento\Framework\App\State
     */
    private $appState;
    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    private $metaImportRepository;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;

    public $supplier;

    /**
     * MetaImportHistory constructor.
     * @param \MS3\Import\Api\Ms3ImportMetaimportHistoryRepositoryInterface $metaImportHistoryRepositoryInterface
     * @param \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterfaceFactory $metaImportHistoryInterfaceFactory
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistryInterface
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\App\State $appState
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepository
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     */
    public function __construct(
        \MS3\Import\Api\Ms3ImportMetaimportHistoryRepositoryInterface   $metaImportHistoryRepositoryInterface,
        \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterfaceFactory $metaImportHistoryInterfaceFactory,
        \Magento\Framework\Api\SearchCriteriaBuilder                    $searchCriteriaBuilder,
        \Magento\CatalogInventory\Api\StockRegistryInterface            $stockRegistryInterface,
        \Magento\Catalog\Api\ProductRepositoryInterface                 $productRepository,
        \Magento\Framework\App\State                                    $appState,
        \MS3\Import\Api\MetaImportRepositoryInterface                   $metaImportRepository,
        \Magento\Framework\Serialize\Serializer\Json                    $jsonSerializer
    )
    {
        $this->metaImportHistoryRepositoryInterface = $metaImportHistoryRepositoryInterface;
        $this->metaImportHistoryInterfaceFactory = $metaImportHistoryInterfaceFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->stockRegistryInterface = $stockRegistryInterface;
        $this->stockRegistryInterface = $stockRegistryInterface;
        $this->productRepository = $productRepository;
        $this->appState = $appState;
        $this->metaImportRepository = $metaImportRepository;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @param $xmlProducts
     */
    public function handleHistory($xmlProducts)
    {
        try {
            if (!$appState->getAreaCode()) {
                $appState->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
            }
        } catch (LocalizedException $e) {
        }
        $currentSkus = [];
        foreach ($xmlProducts as $xmlProduct) {
            $currentSkus[] = $xmlProduct["sku"];
            echo "XML SKU: " . $xmlProduct["sku"] . PHP_EOL;
        }

        $oldSkus = [];
        $r = $this->oldSkusExists();
        foreach ($r as $item) {
            $oldSkus[] = $item->getSku();
            echo "OLD SKU: " . $item->getSku() . PHP_EOL;

        }

        foreach ($xmlProducts as $xmlProduct) {
            echo "INSERT OR UPDATE: " . $xmlProduct["sku"] . PHP_EOL;

            $this->insertOrUpdate($xmlProduct["sku"], 1);
        }

        echo "STARTED ARRAY DIFF" . PHP_EOL;
        //$missingSkus = array_diff($oldSkus, $currentSkus);

        $missingSkus = [];
        $intersection = array_intersect($oldSkus, $currentSkus);
        foreach ($oldSkus as $keya => $valuea) {
            if (!isset($intersection[$keya])) {
                $missingSkus[$keya] = $valuea;
            }
        }

        echo "END OF ARRAY DIFF" . PHP_EOL;
        $this->checkMissingSkus($missingSkus);
    }

    /**
     * @param $skus
     */
    public function checkMissingSkus($skus)
    {
        foreach ($skus as $sku) {
            // first check if other in_stock suppliers exist and skip disabling if so
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter("main_table.sku", $sku, "eq")
                ->addFilter("main_table.imported", 1)
                ->addFilter("main_table.supplier", $this->supplier, "neq")
                ->create();

            $skip = false;
            $list = $this->metaImportRepository->getList($searchCriteria);
            if ($list->getTotalCount() > 1) {
                foreach ($list->getItems() as $item) {
                    $jsonData = $this->jsonSerializer->unserialize($item->getJsonData());
                    if ($jsonData["is_in_stock"] == 1) {
                        $skip = true;
                    }
                }
            }

            if ($skip) {
                continue;
            }

            try {
                $stockData = $this->getStockData($sku);
                if (!$stockData["backorders"]) {
                    $this->disableProduct($sku);
                    $this->insertOrUpdate($sku, 0);
                }
            } catch (\Exception $e) {

            }

        }
    }

    /**
     * @param $sku
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function insertOrUpdate($sku, $status = 1)
    {
        $metaImportModel = $this->historyExists($sku);
        if (!$metaImportModel) {
            $metaImportModel = $this->metaImportHistoryInterfaceFactory->create();
            $metaImportModel->setTimestamp(date("Y-m-d H:i:s"));
        }

        $metaImportModel->setSku($sku);
        $metaImportModel->setImported($status);
        $metaImportModel->setSource($this->supplier);

        $this->metaImportHistoryRepositoryInterface->save($metaImportModel);
    }

    /**
     * @param $sku
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function historyExists($sku, $status = 1)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter("sku", $sku, "eq")
            ->addFilter("source", $this->supplier, "eq")
            ->create();

        $metaImportHistoryItems = $this->metaImportHistoryRepositoryInterface->getList($searchCriteria);

        if ($metaImportHistoryItems->getTotalCount() > 0) {
            return $metaImportHistoryItems->getItems()[0];
        }
    }

    /**
     * @return array|\MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function oldSkusExists()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter("source", $this->supplier, "eq")
            ->create();

        $metaImportHistoryItems = $this->metaImportHistoryRepositoryInterface->getList($searchCriteria);

        if ($metaImportHistoryItems->getTotalCount() > 0) {
            return $metaImportHistoryItems->getItems();
        }
        return [];

    }

    /**
     * @param $sku
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function disableProduct($sku)
    {
        $product = $this->productRepository->get($sku);
        $product->setStatus(0);
        $this->productRepository->save($product);

        $this->metaImportRepository->disableStatus($sku);
    }

    /**
     * @param $sku
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockData($sku)
    {
        $stockItem = $this->stockRegistryInterface->getStockItemBySku(
            $sku,
            1
        );

        $stockItemData = [
            "manage_stock" => $stockItem->getManageStock(),
            "backorders" => $stockItem->getBackorders()
        ];

        return $stockItemData;
    }

}
