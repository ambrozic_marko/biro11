<?php


namespace MS3\Import\Model\ResourceModel;

class MetaImport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ms3_import_metaimport', 'metaimport_id');
    }
}
