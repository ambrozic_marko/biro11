<?php

namespace MS3\Import\Model\ResourceModel;

class Product extends \Magento\Catalog\Model\ResourceModel\Product
{
    /*
     * Direct database query to avoid full product load
     */
    public function getPriceUpdateLockFlag($sku)
    {
        $updateLockSql = $this->getConnection()
            ->select()
            ->from(["cpe" => "catalog_product_entity"], [])
            ->joinInner(["cpei" => "catalog_product_entity_int"], "cpe.entity_id = cpei.entity_id", ["cpei.value"])
            ->where("cpei.attribute_id = 149")
            ->where("cpei.store_id = 0")
            ->where("cpe.sku = ?", $sku);

        try {
            return (bool)$this->getConnection()->fetchOne($updateLockSql);
        } catch (\Zend_Db_Statement_Exception $e) {
            echo $e->__toString();
        }
    }

    /*
     * Direct database query to avoid full product load
     */
    public function getPriceImportLockFlag($sku)
    {
        $importLockSql = $this->getConnection()
            ->select()
            ->from(["cpe" => "catalog_product_entity"], [])
            ->joinInner(["cpei" => "catalog_product_entity_int"], "cpe.entity_id = cpei.entity_id", ["cpei.value"])
            ->where("cpei.attribute_id = 148")
            ->where("cpei.store_id = 0")
            ->where("cpe.sku = ?", $sku);

        try {
            return (bool)$this->getConnection()->fetchOne($importLockSql);
        } catch (\Zend_Db_Statement_Exception $e) {
            echo $e->__toString();
        }
    }

}
