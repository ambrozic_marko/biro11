<?php

namespace MS3\Import\Model\ImportExport\Import;

class Uploader extends \Magento\CatalogImportExport\Model\Import\Uploader
{

    /**
     * All mime types.
     *
     * @var array
     */
    protected $_allowedMimeTypes = [
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'gif' => 'image/gif',
        'png' => 'image/png',
        'ashx' => 'image/jpeg' // added for biromat compatibility
    ];

}
