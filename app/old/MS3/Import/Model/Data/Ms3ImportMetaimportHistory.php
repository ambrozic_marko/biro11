<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MS3\Import\Model\Data;

use MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface;

class Ms3ImportMetaimportHistory extends \Magento\Framework\Api\AbstractExtensibleObject implements Ms3ImportMetaimportHistoryInterface
{

    /**
     * Get ms3_import_metaimport_history_id
     * @return string|null
     */
    public function getMs3ImportMetaimportHistoryId()
    {
        return $this->_get(self::MS3_IMPORT_METAIMPORT_HISTORY_ID);
    }

    /**
     * Set ms3_import_metaimport_history_id
     * @param string $ms3ImportMetaimportHistoryId
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setMs3ImportMetaimportHistoryId($ms3ImportMetaimportHistoryId)
    {
        return $this->setData(self::MS3_IMPORT_METAIMPORT_HISTORY_ID, $ms3ImportMetaimportHistoryId);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get source
     * @return string|null
     */
    public function getSource()
    {
        return $this->_get(self::SOURCE);
    }

    /**
     * Set source
     * @param string $source
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setSource($source)
    {
        return $this->setData(self::SOURCE, $source);
    }

    /**
     * Get imported
     * @return string|null
     */
    public function getImported()
    {
        return $this->_get(self::IMPORTED);
    }

    /**
     * Set imported
     * @param string $imported
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setImported($imported)
    {
        return $this->setData(self::IMPORTED, $imported);
    }

    /**
     * Get timestamp
     * @return string|null
     */
    public function getTimestamp()
    {
        return $this->_get(self::TIMESTAMP);
    }

    /**
     * Set timestamp
     * @param string $timestamp
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setTimestamp($timestamp)
    {
        return $this->setData(self::TIMESTAMP, $timestamp);
    }
}

