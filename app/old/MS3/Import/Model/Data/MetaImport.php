<?php


namespace MS3\Import\Model\Data;

use MS3\Import\Api\Data\MetaImportInterface;

class MetaImport extends \Magento\Framework\Api\AbstractExtensibleObject implements MetaImportInterface
{

    /**
     * Get metaimport_id
     * @return string|null
     */
    public function getMetaimportId()
    {
        return $this->_get(self::METAIMPORT_ID);
    }

    /**
     * Set metaimport_id
     * @param string $metaimportId
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setMetaimportId($metaimportId)
    {
        return $this->setData(self::METAIMPORT_ID, $metaimportId);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MS3\Import\Api\Data\MetaImportExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \MS3\Import\Api\Data\MetaImportExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MS3\Import\Api\Data\MetaImportExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get price
     * @return string|null
     */
    public function getPrice()
    {
        return $this->_get(self::PRICE);
    }

    /**
     * Set price
     * @param string $price
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setPrice($price)
    {
        return $this->setData(self::PRICE, $price);
    }

    /**
     * Get data
     * @return string|null
     */
    public function getJsonData()
    {
        return $this->_get(self::JSON_DATA);
    }

    /**
     * Set data
     * @param string $data
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setJsonData($data)
    {
        return $this->setData(self::JSON_DATA, $data);
    }

    /**
     * Get Raise Percent
     * @return float|null
     */
    public function getRaisePercent()
    {
        return $this->_get(self::RAISE_PERCENT);
    }

    /**
     * Set Raise Percent
     * @param float $percent
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setRaisePercent($percent)
    {
        return $this->setData(self::RAISE_PERCENT, $percent);

    }

    /**
     * Get Raise Amount
     * @return float|null
     */
    public function getRaiseAmount()
    {
        return $this->_get(self::RAISE_AMOUNT);
    }

    /**
     * Set Raise Amount
     * @param float $amount
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setRaiseAmount($amount)
    {
        return $this->setData(self::RAISE_AMOUNT, $amount);
    }

    /**
     * Get Imported
     * @return boolean|null
     */
    public function getImported()
    {
        return $this->_get(self::IMPORTED);
    }

    /**
     * Set Imported
     * @param boolean $imported
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setImported($imported)
    {
        return $this->setData(self::IMPORTED, $imported);
    }

    /**
     * @param string $supplier
     * @return MetaImportInterface|MetaImport
     */
    public function setSupplier($supplier)
    {
        return $this->setData(self::SUPPLIER, $supplier);
    }

    /**
     * @return mixed|string|null
     */
    public function getSupplier()
    {
        return $this->_get(self::SUPPLIER);
    }

    /**
     * @return mixed|string|null
     */
    public function getCost()
    {
        return $this->_get(self::COST);
    }

    /**
     * @param $cost
     * @return MetaImportInterface|MetaImport
     */
    public function setCost($cost)
    {
        return $this->setData(self::COST, $cost);
    }

    /**
     * @return mixed|string|null
     */
    public function getMargin()
    {
        return $this->_get(self::MARGIN);
    }

    /**
     * @param $margin
     * @return MetaImportInterface|MetaImport
     */
    public function setMargin($margin)
    {
        return $this->setData(self::MARGIN, $margin);
    }
}
