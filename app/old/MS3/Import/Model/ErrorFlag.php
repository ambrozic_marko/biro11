<?php

namespace MS3\Import\Model;

class ErrorFlag extends \Magento\Framework\Flag
{
    /**
     * Flag code
     *
     * @var string
     */
    protected $_flagCode = 'ms3_metaimport_error';
}
