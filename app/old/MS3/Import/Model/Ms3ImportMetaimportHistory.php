<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MS3\Import\Model;

use MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface;
use MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Ms3ImportMetaimportHistory extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'ms3_import_ms3_import_metaimport_history';
    protected $dataObjectHelper;

    protected $ms3_import_metaimport_historyDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Ms3ImportMetaimportHistoryInterfaceFactory $ms3_import_metaimport_historyDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \MS3\Import\Model\ResourceModel\Ms3ImportMetaimportHistory $resource
     * @param \MS3\Import\Model\ResourceModel\Ms3ImportMetaimportHistory\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        Ms3ImportMetaimportHistoryInterfaceFactory $ms3_import_metaimport_historyDataFactory,
        DataObjectHelper $dataObjectHelper,
        \MS3\Import\Model\ResourceModel\Ms3ImportMetaimportHistory $resource,
        \MS3\Import\Model\ResourceModel\Ms3ImportMetaimportHistory\Collection $resourceCollection,
        array $data = []
    ) {
        $this->ms3_import_metaimport_historyDataFactory = $ms3_import_metaimport_historyDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve ms3_import_metaimport_history model with ms3_import_metaimport_history data
     * @return Ms3ImportMetaimportHistoryInterface
     */
    public function getDataModel()
    {
        $ms3_import_metaimport_historyData = $this->getData();
        
        $ms3_import_metaimport_historyDataObject = $this->ms3_import_metaimport_historyDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $ms3_import_metaimport_historyDataObject,
            $ms3_import_metaimport_historyData,
            Ms3ImportMetaimportHistoryInterface::class
        );
        
        return $ms3_import_metaimport_historyDataObject;
    }
}

