<?php

namespace MS3\Import\Cron;

use MS3\Import\Model\Category\Attribute\Source\Mode;

class CategoryMapper
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $categoryCollectionFactory;
    /**
     * @var \Magento\Catalog\Api\CategoryLinkManagementInterface
     */
    private $categoryLinkManagement;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
    }

    public function execute()
    {
        $importOnlyCategories = $this->categoryCollectionFactory->create()
            ->addAttributeToFilter("display_mode", ["eq" => Mode::IMPORT_MODE_ONLY])
            ->addAttributeToSelect("*");

        foreach ($importOnlyCategories as $importOnlyCategory) {
            if ($importOnlyCategory->getMappingCategory() > 1) {
                $productSkusToMap = $importOnlyCategory->getProductCollection()->getColumnValues("sku");
                foreach ($productSkusToMap as $productSkuToMap) {
                    try {
                        $this->categoryLinkManagement->assignProductToCategories($productSkuToMap, [$importOnlyCategory->getMappingCategory()]);
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        }
    }
}
