<?php

namespace MS3\Import\Cron;

use Magento\Framework\Exception\LocalizedException;
use MS3\Import\Api\Data\MetaImportInterface;

class ProductImport
{
    const CHUNK_SIZE = 100; // If import magically fails try modifying this value

    /**
     * @var \MS3\Import\Importer\Product
     */
    private $productImporter;
    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    private $metaImportRepositoryInterface;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;
    /**
     * @var \MS3\Import\Logger\Debug
     */
    private $debugLogger;
    /**
     * @var \Magento\Framework\App\State
     */
    private $appState;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    private $filter;
    /**
     * @var \MS3\Import\Model\ErrorFlag
     */
    private $errorFlag;

    /**
     * ProductImport constructor.
     * @param \MS3\Import\Importer\Product $productImporter
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \MS3\Import\Logger\Debug $debugLogger
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \MS3\Import\Importer\Product $productImporter,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Framework\App\State $appState,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Filter\FilterManager $filter,
        \MS3\Import\Model\ErrorFlag $errorFlag
    ) {
        try {
            if (!$appState->getAreaCode()) {
                $appState->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
            }
        } catch (LocalizedException $e) {
        }

        $this->productImporter = $productImporter;
        $this->metaImportRepositoryInterface = $metaImportRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->jsonSerializer = $jsonSerializer;
        $this->debugLogger = $debugLogger;
        $this->resourceConnection = $resourceConnection;
        $this->filter = $filter;
        $this->errorFlag = $errorFlag;
    }

    public function execute()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter("main_table.imported", 1, "eq")
            ->create();

        $metaImportItems = $this->metaImportRepositoryInterface->getList($searchCriteria);
        $importData = $this->metaImportRepositoryInterface->processMetadata($metaImportItems->getItems());

        if ($importData) {
                $this->debugLogger->debug(count($importData));
                $totalCount = count($importData);
                $i = 0;
                foreach (array_chunk($importData, self::CHUNK_SIZE) as $chunk) {
                    try {

                        $this->productImporter->import($chunk);
                        $this->updateMetaImportHistoryTimestamps($chunk);
                        $i++;
                        echo "Processed " . ($i * self::CHUNK_SIZE) . " out of $totalCount" . PHP_EOL;
                        $this->debugLogger->debug("Processed " . ($i * self::CHUNK_SIZE) . " out of $totalCount");
                    } catch (\PDOException $pdoE) {
                        $this->debugLogger->debug($pdoE->__toString());
                        $this->debugLogger->debug(print_r($chunk, true));
                        echo $pdoE->__toString();
                        print_r($chunk);
                        $this->errorFlag->loadSelf();
                        $this->errorFlag->setState(1);
                        $this->errorFlag->setFlagData(["error" => $pdoE->__toString()]);
                        die();
                    }
                    catch (\Exception $e) {
                        $this->debugLogger->debug($e->__toString());
                        $this->debugLogger->debug(print_r($chunk, true));
                        echo $e->__toString();
                        print_r($chunk);
                        $this->errorFlag->setFlagData(["error" => $e->__toString()]);
                        die();
                    }
                }
        } else {
            $this->debugLogger->debug("Nothing to import.");
        }
    }

    public function updateMetaImportHistoryTimestamps($chunk)
    {
        $skus = array_map(function($item) { return $item["sku"]; }, $chunk);
        foreach ($skus as $sku) {
            $this->resourceConnection->getConnection()->query(
                "UPDATE ms3_import_ms3_import_metaimport_history SET timestamp = NOW() WHERE sku = :sku",
                ['sku' => $sku]);
        }
    }

    public function fixUrlKeys($imported = false)
    {
        if ($imported) {
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter("main_table.imported", 1, "eq")
                ->create();
        } else {
            $searchCriteria = $this->searchCriteriaBuilder
                ->create();
        }

        $metaImportItems = $this->metaImportRepositoryInterface->getList($searchCriteria);

        foreach ($metaImportItems->getItems() as $metaImportItem)
        {
            echo "Processing ".$metaImportItem->getSku().PHP_EOL;
            $jsonData = json_decode($metaImportItem->getJsonData(), true);
            if (!isset($jsonData["url_key"])) {
                $jsonData["url_key"] = $this->filter->translitUrl($jsonData[MetaImportInterface::NAME] . "-" . $jsonData[MetaImportInterface::SKU]) . "-" . uniqid();
                echo "Setting url key: " . $jsonData["url_key"] . PHP_EOL;
                $metaImportItem->setJsonData(json_encode($jsonData));
                $this->metaImportRepositoryInterface->save($metaImportItem);
            }
        }
    }
}
