<?php

namespace MS3\Import\Cron;

class ProductMetaImport
{
    /**
     * @var array
     */
    private $importers;

    public function __construct(
        \MS3\Import\MetaImporter\Altera $alteraImporter,
        \MS3\Import\MetaImporter\Biromat $biromatImporter,
        \MS3\Import\MetaImporter\Pigo $pigoImporter,
        \MS3\Import\MetaImporter\Avtera $avteraImporter,
        \MS3\Import\MetaImporter\Diss $dissImporter,
        \MS3\Import\MetaImporter\Also $alsoImporter
    ) {
        $this->importers = [
            $alteraImporter,
            $biromatImporter,
            $pigoImporter,
            $avteraImporter,
            $dissImporter,
            $alsoImporter
        ];
    }

    public function execute()
    {
        /** @var \MS3\Import\MetaImporter\ImportInterface $importer */
        foreach ($this->importers as $importer) {
            $importer->import();
        }
    }
}
