<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_CategoriesColumnToProductGrid
 */
namespace Cynoinfotech\CategoriesColumnToProductGrid\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder;
use Magento\Framework\UrlInterface;

/**
 * Class PageActions
 */
class Category extends Column
{

    /**
     * @var string
     */
    private $editUrl;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\Catalog\Model\CategoryFactory $_categoryloader,
        array $components = [],
        array $data = []
    ) {
        $this->_productloader = $_productloader;
        $this->_categoryloader = $_categoryloader;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $fieldName = $this->getData('name');
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $productId=$item['entity_id'];
                $product = $this->_productloader->create()->load($productId);
                $cats = $product->getCategoryIds();
                $categories=[];
                if (!empty($cats)) {
                    foreach ($cats as $cat) {
                        $category = $this->_categoryloader->create()->load($cat);
                        $categories[]=$category->getName();
                    }
                }
                $item[$fieldName]=implode(',', $categories);
            }
        }
        return $dataSource;
    }
}
