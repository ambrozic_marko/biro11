<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_CategoriesColumnToProductGrid
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cynoinfotech_CategoriesColumnToProductGrid',
    __DIR__
);
