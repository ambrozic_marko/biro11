########## 30-09-2020 
module setup_version="1.0.3"
composer.json "version": "1.0.3",

########## 19-08-2020 
module setup_version="1.0.2"
composer.json "version": "1.0.2",

########## 17-12-2019 
module setup_version="1.0.1"
composer.json "version": "1.0.1",

########## 05-06-2019  
module setup_version="1.0.0"
composer.json "version": "1.0.0",