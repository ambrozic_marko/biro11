<?php
/**
 * @author CynoInfotech Team
 * @package Cynoinfotech_CategoriesColumnToProductGrid
 */
namespace Cynoinfotech\CategoriesColumnToProductGrid\Model\Category;
 
class Categorylist implements \Magento\Framework\Option\ArrayInterface
{
		public function __construct(
			\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
			\Magento\Catalog\Block\Adminhtml\Category\Tree $adminCategoryTree
		) {
			$this->_categoryCollectionFactory = $collectionFactory;
			$this->_adminCategoryTree = $adminCategoryTree;
		}
		
		
		public  function getElement($path = '') {
				
				$parentName = '';
				$rootCats = array(1);	 
				$catTree = explode("/", $path);
				array_pop($catTree);	 
				if($catTree && (count($catTree) > count($rootCats))){
					foreach ($catTree as $catId){
						if(!in_array($catId, $rootCats)){						
							$categoryName = '-';
							$parentName .= $categoryName .'-';
						}
					}
				}
				return $parentName;					
		}
		
		public function getPrintTree($tree) {
			
				$ret = [];
				
				foreach ($tree as $i => $t) {
					
					$dash = $this->getElement($t['path']);					
					$value = $t['id'];
					$text = substr($t['text'], 0, strpos(__($t['text']), "(ID:" ));					
					$label = $dash.' '.$text;					
					$ret[] = ['value' => $value, 'label' => $label];
					
					if (isset($t['children'])) {					
						$childArray=$this->getPrintTree($t['children']);
						$ret=array_merge($ret, $childArray);
					}					
				}				
				return $ret;				
			}
		
		
		public function toOptionArray($addEmpty = true)
		{
			$options = [];
			$tree = $this->_adminCategoryTree->getTree();
			$collection = $this->getPrintTree($tree);			
			 foreach ($collection as $category) {			 
				$options[] = ['label' => $category['label'], 'value' => $category['value']];
			 }
			return $options;			
		}    
}
