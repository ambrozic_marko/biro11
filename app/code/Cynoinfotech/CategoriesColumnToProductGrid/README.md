#### ADMIN CATEGORY FILTER FOR PRODUCT GRID MAGENTO 2

docs : http://docs.cynoinfotech.com/categoryfilterforproductgrid/
By: CynoInfotech Team
Platform: Magento 2 Community Edition
Email: info@cynoinfotech.com

####1 - Installation Guide

 * Download extension zip file from the account
 * Unzip the file
 * root of your Magento 2 installation and merge app folder {Magento Root}
 * folder structure --> app/code/Cynoinfotech/CategoriesColumnToProductGrid

 
####2 -  Enable Extension :  
  
  After this run below commands from Magento 2 root directory to install module.
  
 * Run :cd < your Magento install dir >
 * php bin/magento cache:disable
 * php bin/magento module:enable Cynoinfotech_CategoriesColumnToProductGrid
 * php bin/magento setup:upgrade
 * php bin/magento setup:static-content:deploy
 * rm -rf var/cache var/generation var/di var/cache /generated



/**********************************************************/

Extension Features:

- Expand product grid to add category column
- Filter products according to the category
- Expand product grid in Magento2 by admin
- Quick and save time to find categories for any product

Extension Information : 

Magento 2 Admin Category Filter for Product Grid Extension allows add category column in product grid to expand magento2 admin. Using Admin Category Filter for Product Grid help to find that product belongs which categories. Category Filter for Product Grid helps to filter products of particular a category in Magento2

/***************************************************/

#### To request support:

Feel free to contact us via email: info@cynoinfotech.com

www.cynoinfotech.com