<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace WebAM\Promo\Rewrite\Amasty\Promo\Block;

class Items extends \Amasty\Promo\Block\Items
{
    private $productRepository;
    private $store;
    private $catalogHelper;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Amasty\Promo\Helper\Data $promoHelper, \Magento\Catalog\Helper\Image $helperImage, \Magento\Framework\Url\Helper\Data $urlHelper, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \Magento\Catalog\Block\Product\View $productView, \Magento\Framework\Registry $registry, \Magento\Store\Model\Store $store, \Magento\Catalog\Helper\Data $catalogHelper, \Magento\Framework\Json\EncoderInterface $jsonEncoder, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, \Amasty\Promo\Model\Config $modelConfig, \Magento\Framework\App\ProductMetadataInterface $productMetadata, \Magento\Framework\Json\DecoderInterface $jsonDecoder, array $data = [])
    {
        parent::__construct($context, $promoHelper, $helperImage, $urlHelper, $productRepository, $productView, $registry, $store, $catalogHelper, $jsonEncoder, $priceCurrency, $modelConfig, $productMetadata, $jsonDecoder, $data);
        $this->productRepository = $productRepository;
        $this->store = $store;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductPrice(\Magento\Catalog\Model\Product $product)
    {
        $product = $this->productRepository->getById($product->getId());
        $price = $product->getPrice() * $this->store->getCurrentCurrencyRate();

        $price = $this->catalogHelper->getTaxPrice($product, $price, true);

        return $price;
    }
}

