<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace WebAM\Utilities\Plugin\Magento\Sales\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\App\AreaList;
use \Magento\Framework\App\State;
use \Magento\Framework\App\Area;

class EmailSenderHandler
{
    /**
     * @var ScopeConfigInterface
     */
    private $globalConfig;

    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @var State
     */
    private $appState;

    /**
     * EmailSenderHandler constructor.
     * @param ScopeConfigInterface $globalConfig
     * @param AreaList $areaList
     * @param State $appState
     */
    public function __construct(
        ScopeConfigInterface $globalConfig,
        AreaList $areaList,
        State $appState
    ){
        $this->globalConfig = $globalConfig;
        $this->areaList = $areaList;
        $this->appState = $appState;
    }

    /**
     * Initializes the translation if emails get send async by cron
     * This is a core bug and can be removed when this is solved:
     * https://github.com/magento/magento2/issues/7426
     */
    public function beforeSendEmails()
    {
        $area = $this->areaList->getArea($this->appState->getAreaCode());
        $area->load(Area::PART_TRANSLATE);
    }
}

