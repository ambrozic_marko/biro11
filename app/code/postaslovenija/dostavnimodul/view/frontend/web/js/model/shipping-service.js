define([
    'ko',
    'Magento_Checkout/js/model/checkout-data-resolver',
	'mage/utils/wrapper',
	'jquery',
	'Magento_Checkout/js/model/quote',
	'Magento_Checkout/js/checkout-data'
], function (ko, checkoutDataResolver, wrapper, $, quote, checkoutData) {
    'use strict';
	
	return function (target) { 
	   
	   var setShippingRates=wrapper.wrap(target.setShippingRates, function (setShippingRates) {
            setShippingRates();
			
			var checkExist = setInterval(function() {
			if ($('.loading-mask').length==0) {
				var availableMethods = document.querySelectorAll('[id^="label_carrier"]');
			console.log(availableMethods);
			PrikazGumba();
				clearInterval(checkExist);
				}
				}, 200);
				
				
				
				//test
				
				
			   
		function openFB(DostavnaMetoda) {
			//debugger;
		    if (document.getElementById("psjQuery") && document.getElementById("psHTML") && document.getElementById("psStyle")) {
		        psjQuery.fancybox({
		            type: 'inline',
		            href: '.psfancybox',
		            maxWidth: 1500,
		            width: 1500,
		            modal: true,
		            padding: 0,
		            marging: 0,
		            autoSize: false,
		            margin: [0, 0, 0, 0],
		            Right: '0px'
		        });
		        //psjQuery.fancybox.helpers.overlay.open({parent: 'html'});
		        //debugger;

		        document.body.style.overflow = 'hidden';
		    } else {
				
				
				
				var FormData = {
					Naslov1 : quote.shippingAddress().street[0],
					Naslov2 : quote.shippingAddress().street[1],
					Mesto : quote.shippingAddress().city,
					PostnaStevilka : quote.shippingAddress().postcode,
					Token : window.checkoutConfig.ps.token,
					Apikey : window.checkoutConfig.ps.apikey,
					Guid : window.checkoutConfig.ps.guid,
					availableMethods : window.checkoutConfig.ps.nacinidostave
				}
				
				for (let x in FormData) {
					if(!FormData[x])
					{
						FormData[x]="";
					}
				}
				
				 var Naslov = FormData['Naslov1'] + FormData['Naslov2'];
		        //test url
		        //https://komunikator.scorpio-it.si:444/api/PSMPmodal/show?token=det&guid=b&adress=TumovaUlica27&postcode=3000&city=Celje

		        //testni url -- localhost
		        //var URL = 'http://localhost:49536/api/PSMPmodal/show?token='+FormData['Token']+'&guid='+FormData['Guid']+'&naslov=' + Naslov + '&PostnaStevilka=' + FormData['PostnaStevilka'] + '&Mesto=' + FormData['Mesto'] + '&apikey=' + FormData['Apikey'];

		        //testni url -- virtualka
		        //var URL='http://185.49.2.85:89/api/PSMPmodal/show?token=<?php print_r ($token); ?>&guid=<?php print_r ($guid); ?>&naslov='+Naslov+'&PostnaStevilka='+PostnaStevilka+'&Mesto='+Mesto+'&apikey='+Apikey;

		        //test na SSL - virtualka
		        //var URL = 'https://komunikator.scorpio-it.si/api/PSMPmodal/show?token='+FormData['Token']+'&guid='+FormData['Guid']+'&naslov=' + Naslov + '&PostnaStevilka=' + FormData['PostnaStevilka'] + '&Mesto=' + FormData['Mesto'] + '&apikey=' + FormData['Apikey'];
				
				//azure -- produkcija
		        var URL = 'https://ec-komunikator.appservice.azuremb.posta.si/api/PSMPmodal/show?token='+FormData['Token']+'&guid='+FormData['Guid']+'&naslov=' + Naslov + '&PostnaStevilka=' + FormData['PostnaStevilka'] + '&Mesto=' + FormData['Mesto'] + '&apikey=' + FormData['Apikey'];
				
		        jQuery.ajax({
		            type: 'GET',
		            url: URL,
		            dataType: "text",
		            crossDomain: true,
		            success: function (response) {

		                if (response.includes("PSNAPAKA")) {
		                    console.log(response);
		                } else {
		                    var DynamicScript = document.createElement("script");
		                    DynamicScript.id = "psjQuery";
		                    DynamicScript.type = "text/javascript";

		                    var ScriptGM = document.createElement("script");
		                    ScriptGM.id = "psGoogleMaps";
		                    ScriptGM.type = "text/javascript";

		                    //pridobim api key iz nastavitev in ga zapišem v datoteko
		                    response = response.replace("vstaviApiKey", FormData['Apikey']);

		                    //razdelim html,css in js v različne podstringe
		                    var JSText = response.split('--js/start--').pop().split('--js/end--')[0];
		                    var HTMLText = response.split('--html/start--').pop().split('--html/end--')[0];
		                    var CSSText = response.split('--css/start--').pop().split('--css/end--')[0];
		                    var Podatki = response.split('--podatki/start--').pop().split('--podatki/end--')[0];

		                    var PrevzemnaMesta = Podatki.split('--prevzemnaMestaStart--').pop().split('--prevzemnaMestaEnd--')[0];
		                    var VrstePošiljk = Podatki.split('--vrstePosiljkStart--').pop().split('--vrstePosiljkEnd--')[0];
		                    var PoštneŠtevilke = Podatki.split('--poštneŠtevilkeStart--').pop().split('--poštneŠtevilkeEnd--')[0];
		                    var Države = Podatki.split('--državeStart--').pop().split('--državeEnd--')[0];
		                    var DodatneStoritve = Podatki.split('--dodatneStoritveStart--').pop().split('--dodatneStoritveEnd--')[0];
		                    var Guid = Podatki.split('--guid/start--').pop().split('--guid/end--')[0];
		                    var Token = Podatki.split('--token/start--').pop().split('--token/end--')[0];
		                    //var Apikey=Podatki.split('--apikey/start--').pop().split('--apikey/end--')[0];
		                    var Lokacija = Podatki.split('--lokacija/start--').pop().split('--lokacija/end--')[0];
		                    var GoogleMaps = response.split('--maps/start--').pop().split('--maps/end--')[0];
							

		                    //napolnem js
		                    if (!document.getElementById("psjQuery")) {
		                        try {
		                            DynamicScript.appendChild(document.createTextNode(JSText));
		                            document.body.appendChild(DynamicScript);
		                        } catch (e) {
		                            DynamicScript.text = response;
		                            document.body.appendChild(DynamicScript);
		                        }
		                    }

		                    //napolnem google maps
		                    if (!document.getElementById("psGoogleMaps")) {
		                        try {
		                            ScriptGM.appendChild(document.createTextNode(GoogleMaps));
		                            document.body.appendChild(ScriptGM);
		                        } catch (e) {
		                            ScriptGM.text = response;
		                            document.body.appendChild(ScriptGM);
		                        }
		                    }

		                    //napolnem html
		                    var div = document.querySelector("#psHTML");
		                    if (div != null) {
		                        div.innerHTML = HTMLText;
		                    } else {
		                        var html = document.createElement('psHTML');
		                        html.id = "psHTML";
		                        html.innerHTML = HTMLText;
		                        document.body.appendChild(html);
		                    }

		                    //napolnem css
		                    var style = document.createElement('style');
		                    style.id = "psStyle"
		                        style.innerHTML = CSSText;
		                    document.head.appendChild(style);

		                    psjQuery.fancybox({
		                        type: 'inline',
		                        href: '.psfancybox',
		                        maxWidth: 1500,
		                        width: 1500,
		                        modal: true,
		                        padding: 0,
		                        marging: 0,
		                        autoSize: false,
		                        margin: [0, 0, 0, 0],
		                        Right: '0px'
		                    });
		                    //psjQuery.fancybox.helpers.overlay.close({parent: 'body'});
		                    //psjQuery.fancybox.helpers.overlay.open({parent: 'body'},{closeClick : false});
		                    //debugger;

		                    document.body.style.overflow = 'hidden';

		                    ajaxPosta.pslokacija(Lokacija);
							
							/*
		                    for (index = 0; index < availableMethods.length; ++index) {
		                        if (availableMethods[index].nextElementSibling.innerText.toUpperCase().replace(/\s/g, '') == selectedMethod.toUpperCase()) {
		                            availableMethods[index].checked = true;
		                            jQuery(document.body).trigger('update_checkout');
		                        }
		                    }
							*/

		                }
		            },
		            error: function (xhr) {
		                console.log(xhr);
		            }
		        })
		    }
		}
		
		
			   
			   
		function PrikazGumba() {
			var selectedMethods=window.checkoutConfig.ps.nacinidostave;
			
			if(selectedMethods == '' || selectedMethods == null)
			{
				return;
			}
			 for (var indx = 0; indx < selectedMethods.length; ++indx) {
				 
				 var availableMethods = document.querySelectorAll('[id^="label_carrier"]');
				 var show = false;
				 
				 for (var index = 0; index < availableMethods.length; ++index) {
					 if(availableMethods[index].innerText.toUpperCase().replace(/\s/g,'').includes(selectedMethods[indx].toUpperCase().replace(/\s/g,''))){
						 //var selectedCountry=document.getElementsByName("country_id");
						 //selectedCountry=$(selectedCountry).find(":selected").text();
						 
						 var selectedCountry=quote.shippingAddress().countryId;
						 
						 if(selectedCountry)
						 {
							 if(selectedCountry.toUpperCase().includes('SLOVENIJA') || selectedCountry.toUpperCase().includes('SLOVENIA') || selectedCountry.toUpperCase().includes('SI')){
								 show = true;
							 }
						 }
						 else
						 {
							 show = true;
						 }
						 
						 
						 if(show){
							 var celotenShipping=document.getElementsByClassName("table-checkout-shipping-method");
							 if(!celotenShipping[0].innerHTML.includes("openFB('" + availableMethods[index].innerText.toUpperCase().replace(/\s/g,'') + "')"))
							 {
								 var table=document.getElementsByClassName("table-checkout-shipping-method");
								 var element=table[0].tBodies;
								 var Rows=element[0].children;
								 for (var indexR = 0; indexR < Rows.length; ++indexR) {
									 if(Rows[indexR].innerText.toUpperCase().replace(/\s/g,'').includes(availableMethods[index].innerText.toUpperCase().replace(/\s/g,'')) && !Rows[indexR].innerHTML.includes("PS-uredi-dostavo-v3.svg"))
									 {
										 var cell=Rows[indexR].insertCell(-1);
										 cell.innerHTML="<img src='https://ecomercestore.blob.azuremb.posta.si/images/PS-uredi-dostavo-v3.svg' class='psImg' data-dostavnametoda='"+availableMethods[index].innerText.toUpperCase().replace(/\s/g,'')+"' style='cursor: pointer; width: 150px'>";
										 //dodajanje onclick handlerja
										 $(document).on("click",".psImg", function() {
											 var element=document.getElementsByClassName("psImg");
											 var DostavnaMetoda=element[0].dataset.dostavnametoda;
											 openFB(DostavnaMetoda);
										 });
									 }
									 
								 }
								 console.log(Rows);
							 }
							 
						 }
					 }						 
				 }
			 }
		}
				
				//konec test
				
				
        });
		
		target.setShippingRates=setShippingRates;
		return target;
    };
});