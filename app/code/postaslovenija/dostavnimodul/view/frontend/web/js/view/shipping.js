define(
    [
        'jquery',
        'underscore',
        'Magento_Ui/js/form/form',
        'ko',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-address/form-popup-state',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Ui/js/modal/modal',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/checkout-data',
        'uiRegistry',
        'mage/translate',
        'Magento_Checkout/js/model/shipping-rate-service'
    ],function (
        $,
        _,
        Component,
        ko,
        customer,
        addressList,
        addressConverter,
        quote,
        createShippingAddress,
        selectShippingAddress,
        shippingRatesValidator,
        formPopUpState,
        shippingService,
        selectShippingMethodAction,
        rateRegistry,
        setShippingInformationAction,
        stepNavigator,
        modal,
        checkoutDataResolver,
        checkoutData,
        registry,
        $t) {
    'use strict';
	
	

    var mixin = {

        selectShippingMethod: function (shippingMethod) {
            console.log("method overriden");
            selectShippingMethodAction(shippingMethod);
            checkoutData.setSelectedShippingRate(shippingMethod.carrier_code + '_' + shippingMethod.method_code);

            return true;
        },
		saveNewAddressPS: function (addressData) {
           

            //this.source.set('params.invalid', false);
            //this.triggerShippingDataValidateEvent();
			
                // if user clicked the checkbox, its value is true or false. Need to convert.
				/*
               var addressData = {
                    city: quote.shippingAddress().city,
                    company: "Scorpio",
                    country_id: "SI",
                    firstname: "Matic",
                    lastname: "Bracic",
                    postcode: quote.shippingAddress().postcode,
                    region: "",
                    region_id: undefined ,
                    save_in_address_book: 0 ,
                    street: {
                        0: 'test JS',
                        1: '',
                        2: ''
                    },
                    telephone: "031111222"
                }
				*/
				
				

                // New address must be selected as a shipping address
                var newShippingAddress = createShippingAddress(addressData);
                selectShippingAddress(newShippingAddress);
                checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressData));
                //this.getPopUp().closeModal();
                //this.isNewAddressAdded(true);
            
        }
		
    };
	window.saveNewAddressPS = mixin.saveNewAddressPS;

    return function (target) { // target == Result that Magento_Ui/.../default returns.
    return target.extend(mixin); // new result that all other modules receive 
};
});