<?php

namespace postaslovenija\dostavnimodul\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Psr\Log\LoggerInterface;

/**
 * Class SampleConfigProvider
 */
class SampleConfigProvider implements ConfigProviderInterface
{

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
	 protected $_logger;
	public function __construct(LoggerInterface $logger, ScopeConfigInterface $scopeConfig)
	{
          $this->_scopeConfig = $scopeConfig;
		  $this->_logger = $logger;
	}
    public function getConfig()
    {
		$this->_logger->debug("NOTR V GETCONFIG");
		$nacinidostave=$this->_scopeConfig->getValue('postaslovenija/general/psnacinidostave', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$apikey=$this->_scopeConfig->getValue('postaslovenija/general/psapikey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$token=$this->_scopeConfig->getValue('postaslovenija/general/pstoken', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$guid=$this->_scopeConfig->getValue('postaslovenija/general/psguid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		
        $nacinidostave=explode(";",$nacinidostave);
        return [
            'ps' => [
                'nacinidostave' => $nacinidostave,
				'apikey' => $apikey,
				'token' => $token,
				'guid' => $guid,
            ],
        ];
    }
}