<?php

namespace postaslovenija\dostavnimodul\Block\Adminhtml\Order\View\Tab;

class CustomTab extends \Magento\Backend\Block\Template implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
   //protected $_template = 'order/view/tab/customtab.phtml';
   protected $_template = 'postaslovenija_dostavnimodul::customtab.phtml';
   /**
    * @var \Magento\Framework\Registry
    */
   private $_coreRegistry;
   
   /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
   protected $scopeConfig;

   /**
    * View constructor.
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Framework\Registry $registry
    * @param array $data
    */
   public function __construct(
       \Magento\Backend\Block\Template\Context $context,
       \Magento\Framework\Registry $registry,
	   \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
       array $data = []
   ) {
       $this->_coreRegistry = $registry;
	   $this->scopeConfig = $scopeConfig;
       parent::__construct($context, $data);
   }

   /**
    * Retrieve order model instance
    * 
    * @return \Magento\Sales\Model\Order
    */
   public function getOrder()
   {
       return $this->_coreRegistry->registry('current_order');
   }
   /**
    * Retrieve order model instance
    *
    * @return int
    *Get current id order
    */
   public function getOrderId()
   {
       return $this->getOrder()->getEntityId();
   }

   /**
    * Retrieve order increment id
    *
    * @return string
    */
   public function getOrderIncrementId()
   {
       return $this->getOrder()->getIncrementId();
   }
   /**
    * {@inheritdoc}
    */
   public function getTabLabel()
   {
       return __('Pošta Slovenije');
   }

   /**
    * {@inheritdoc}
    */
   public function getTabTitle()
   {
       return __('Pošta Slovenije');
   }

   /**
    * {@inheritdoc}
    */
   public function canShowTab()
   {
       return true;
   }

   /**
    * {@inheritdoc}
    */
   public function isHidden()
   {
       return false;
   }

   public function getPSForm()
   {
   	   $order=$this->getOrder();
	   $datum=$order->getCreatedAt();
	   $token=$this->scopeConfig->getValue('postaslovenija/general/pstoken', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
       $DostavneMetodeNastavitve=$this->scopeConfig->getValue('postaslovenija/general/psnacinidostave', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
       $weight_unit=$this->scopeConfig->getValue('general/locale/weight_unit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

       //test - localhost
	   //$urlPridobiStatus="http://localhost:49536/api/PSMPorder/GetStatus?token=". $token ."&idnarocila=". $order->getIncrementId() . "&datumnarocila=" . $datum;

       //test - localhost
       $urlPridobiStatus="https://ec-komunikator.appservice.azuremb.posta.si/api/PSMPorder/GetStatus?token=". $token ."&idnarocila=". $order->getIncrementId() . "&datumnarocila=" . $datum;

	   $total_weight=$order->getWeight();
       
       $DostavneMetodeNastavitve = explode(";", $DostavneMetodeNastavitve);
       $methodNames = array();
        foreach ($DostavneMetodeNastavitve as $value) {
            array_push($methodNames, strtoupper(str_replace(' ', '', $value)));    
        }

       $IzbranaDostavnaMetoda=$order->getShippingDescription();
       $IzbranaDostavnaMetoda= substr($IzbranaDostavnaMetoda, 0, strpos($IzbranaDostavnaMetoda, "-"));


       if (in_array( strtoupper(str_replace(' ', '', $IzbranaDostavnaMetoda)), $methodNames)) {

       if($weight_unit=="lbs")
       {
           $total_weight=$total_weight * 453.59237;
       }
       else
       {
           $total_weight=$total_weight * 1000;
       }
       $total_weight = (int)$total_weight;

       $custLastName= $order->getCustomerLastname();
       $custFirstName= $order->getCustomerFirstname();
       $billingaddress=$order->getBillingAddress();
       $billingcompany=$billingaddress->getCompany();
       $billingemail=$billingaddress->getEmail();
       $billingphone=$billingaddress->getTelephone();
       $shippingaddress=$order->getShippingAddress();

       //pridobivanje ime in priimek - dodan popravek za PayPal Express prazen ime in priimek
       $custFirstName= $order->getCustomerFirstname();
       $custLastName= $order->getCustomerLastname();

       if (empty($custFirstName)) {
        $custFirstName=$billingaddress->getFirstname();
        if(empty($custFirstName))
        {
            $custFirstName=$shippingaddress->getFirstname();
        }
       }

       if (empty($custLastName)) {
        $custLastName=$billingaddress->getLastname();
        if(empty($custLastName))
        {
            $custLastName=$shippingaddress->getLastname();
        }
       }

       //

       $shippingcity=$shippingaddress->getCity();
       $shippingstreet1=$shippingaddress->getStreetLine(1);
       $shippingstreet2=$shippingaddress->getStreetLine(2);
       $shippingpostcode=$shippingaddress->getPostcode();
       $shippingcountry=$shippingaddress->getCountryId();

       $PaymentMethodTitle = $order->getPayment()->getAdditionalInformation("method_title");
       if($PaymentMethodTitle=="Cash On Delivery")
       {
         $COD=1;
       }
       else
       {
         $COD=0;
       }
       $opomba="";

       $total=$order->getGrandTotal();
       $znesek=$order->getSubtotal();

       if(preg_match("/\bBS|PS\b/i", $shippingstreet1))
       {
          $Naslov=$shippingstreet2;
          $DodatenNaziv=$shippingstreet1;
      }

      else if(preg_match("/\bPošta\b/i", $shippingstreet1) && !preg_match("/\bDirect4me\b/i", $shippingstreet1))
      {
          $Naslov=$shippingstreet2;
          $DodatenNaziv="Poštno ležeče";
      }

      else if(preg_match("/\bDirect4me\b/i", $shippingstreet1))
      {
          $Naslov=$shippingstreet2;
          $DodatenNaziv=$shippingstreet1;
      }

      else
      {
          $Naslov=$shippingstreet1;
          $DodatenNaziv="";
          if(!empty($billingcompany)){
             $DodatenNaziv=$billingcompany;
         }
       }

	   $PSform='<script id="testSkript" type="application/javascript"  onerror="document.getElementById(\'tekst\').innerHTML=\'<p>NAPAKA PRI POVEZOVANJU S POSTO SLOVENIJE<p/>\';" src="'. $urlPridobiStatus .'"data-Naziv1="'.$custFirstName . " " . $custLastName.'"data-Naziv2="'.$DodatenNaziv.'"data-Naslov="'.$Naslov.'"data-Posta="'.$shippingcity.'"data-PostnaSt="'.$shippingpostcode.'"data-Drzava="'.$shippingcountry.'"data-Email="'.$billingemail.'"data-Telefon="'.$billingphone.'"data-Teza="'.$total_weight.'"data-Odkupnina="'.$total.'"data-Vrednost="'.$znesek.'"data-CashOnDelivery= "'.$COD.'"data-Opomba="'.$opomba. '"async></script>';
	   return $PSform;
   }
   else
   {
    return 'Ta dostavna metoda ('. $IzbranaDostavnaMetoda .') ni vnesena v nastavitvih Pošte Slovenije polje "Omogočeni načini dostave": ';
   }
}
   
}