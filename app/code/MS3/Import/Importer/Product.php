<?php

namespace MS3\Import\Importer;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Type;
use MS3\Import\Api\Data\MetaImportInterface;

class Product
{
    const ATTRIBUTE_SET_CODE = "Vsi atributi";
    const BASE_WEBSITE_CODE = "base";
    const PUBLIC_MEDIA_CATALOG_PATH = "/pub/media/catalog/product";

    /**
     * @var \FireGento\FastSimpleImport\Model\Importer
     */
    private $importer;
    /**
     * @var \MS3\Import\Model\ResourceModel\Product
     */
    private $productResource;
    /**
     * @var \MS3\Import\Logger\Debug
     */
    private $debugLogger;
    /**
     * @var \Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface
     */
    private $attributeMediaGalleryManagement;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $productModel;
    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    private $directoryList;
    /**
     * @var \MS3\Import\Model\ErrorFlag
     */
    private $errorFlag;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Product constructor.
     * @param \FireGento\FastSimpleImport\Model\Importer $importer
     * @param \MS3\Import\Model\ResourceModel\Product $productResource
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface $attributeMediaGalleryManagement
     * @param \MS3\Import\Logger\Debug $debugLogger
     * @param \Magento\Catalog\Model\Product $productModel
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \MS3\Import\Model\ErrorFlag $errorFlag
     */
    public function __construct(
        \FireGento\FastSimpleImport\Model\Importer $importer,
        \MS3\Import\Model\ResourceModel\Product $productResource,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface $attributeMediaGalleryManagement,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \MS3\Import\Model\ErrorFlag $errorFlag
    )
    {
        $this->importer = $importer;
        $this->productResource = $productResource;
        $this->debugLogger = $debugLogger;
        $this->attributeMediaGalleryManagement = $attributeMediaGalleryManagement;
        $this->productModel = $productModel;
        $this->directoryList = $directoryList;
        $this->errorFlag = $errorFlag;
        $this->productRepository = $productRepository;
    }

    public function import($productsData)
    {
        $productsData = $this->addConfigurationValues($productsData);
        try {
            $productsData = $this->filterImageFields($productsData);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $nse) {
        }

        $validationResult = true;

        try {
            $this->importer->setAllowedErrorCount(99);
            $validationResult = $validationResult && $this->importer->processImport($productsData);
            if (!$validationResult) {
                return new \Exception("Validation failed!");
            }
        } catch (\Exception $e) {
            $this->debugLogger->debug($this->importer->getErrorMessages());
            $this->debugLogger->debug($this->importer->getLogTrace());
            $this->debugLogger->debug($e->__toString());
            $this->debugLogger->debug(print_r($productsData, true));

            $this->errorFlag->loadSelf();
            $this->errorFlag->setState(1);
            $this->errorFlag->setFlagData(["error" => $e->__toString()]);
            $this->errorFlag->save();

            echo $e->getMessage() . PHP_EOL;
            print_r($productsData);
            throw $e;
        }

        $this->debugLogger->debug($this->importer->getLogTrace());
        $this->debugLogger->debug($this->importer->getErrorMessages());
        $this->debugLogger->debug(print_r($productsData, true));

    }

    public function addConfigurationValues($productsData)
    {
        foreach ($productsData as &$productData) {
            echo "Processing SKU: " . $productData[MetaImportInterface::SKU].PHP_EOL;
            $productData[MetaImportInterface::SKU] = trim($productData[MetaImportInterface::SKU]);
            $productId = $this->productModel->getIdBySku($productData[MetaImportInterface::SKU]);
            if (!$productId) {
                $productData["attribute_set_code"] = self::ATTRIBUTE_SET_CODE;
                $productData["product_type"] = Type::TYPE_SIMPLE;
            } else {
                unset($productData["url_key"]);
                try {
                    $product = $this->productRepository->getById($productId);
                    $stockItem = $product->getExtensionAttributes()->getStockItem();
                    if (in_array($stockItem->getBackorders(), [1, 2])) {
                        unset($productData["is_in_stock"]);
                        unset($productData["qty"]);
                    }
                } catch (\Exception $e) {
                    $this->debugLogger->debug($e->__toString());
                }
            }

            $productData["product_websites"] = self::BASE_WEBSITE_CODE;

            $priceUpdateFlag = $this->productResource->getPriceUpdateLockFlag($productData[MetaImportInterface::SKU]);
            $priceImportFlag = $this->productResource->getPriceImportLockFlag($productData[MetaImportInterface::SKU]);

            if (abs($productData["raise_percent"]) > 0 && !$priceUpdateFlag) {
                $productData["price"] = $productData["price"] * (1 + ($productData["raise_percent"] / 100));
            }

            if (abs($productData["raise_amount"]) > 0 && !$priceUpdateFlag) {
                $productData["price"] += $productData["raise_amount"];
            }

            if ($priceImportFlag) {
                unset($productData["price"]);
            }
        }

        return $productsData;
    }

    public function filterImageFields($productsData)
    {
        foreach ($productsData as &$productsDatum) {
            $sku = $productsDatum["sku"];

            $galleryList = $this->attributeMediaGalleryManagement->getList($sku);

            $numberOfImages = 0;
            $typesSet = [];
            foreach ($galleryList as $galleryItem) {
                $typesSet = array_merge($typesSet, $galleryItem->getTypes());
                $numberOfImages++;
            }

            if (in_array("thumbnail", $typesSet)) {
                unset($productsDatum["thumbnail"]);
            }

            if (in_array("small_image", $typesSet)) {
                unset($productsDatum["small_image"]);
            }

            if (in_array("image", $typesSet)) {
                unset($productsDatum["image"]);
            }

            if ($numberOfImages > 3) {
                unset($productsDatum["additional_images"]);
            }

            try {
                $this->cleanDuplicateImages($galleryList, $sku);
            } catch (\Exception $e) {

            }
        }

        return $productsData;
    }

    public function cleanDuplicateImages($galleryList, $sku)
    {
        $hashArray = [];
        $deletionList = [];

        /** @var \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface $galleryItem */
        foreach (array_reverse($galleryList) as $galleryItem)
        {
            $md5Hash = md5(file_get_contents($this->directoryList->getRoot() . self::PUBLIC_MEDIA_CATALOG_PATH . $galleryItem->getFile()));
            if (in_array($md5Hash, $hashArray))
            {
                $deletionList[] = $galleryItem;
            } else {
                $hashArray[] = $md5Hash;
            }
        }

        foreach ($deletionList as $deletionItem)
        {
            if ($deletionItem->getTypes()) {
                continue;
            }
            $this->attributeMediaGalleryManagement->remove($sku, $deletionItem->getId());
        }
    }
}
