<?php
namespace MS3\Import\Controller\Adminhtml\MetaImport;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use MS3\Import\Api\Data\MetaImportInterface;

class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MS3_Import::MetaImport_inlineEdit';

    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    protected $metaimportRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaimportRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaimportRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->metaimportRepository = $metaimportRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $metaimportId) {
                    /** @var MetaImportInterface $metaImport */
                    $metaImport = $this->metaimportRepository->getById($metaimportId);
                    try {
                        $percentage = isset($postItems[$metaimportId][MetaImportInterface::RAISE_PERCENT]) ?
                            $postItems[$metaimportId][MetaImportInterface::RAISE_PERCENT] : null;

                        $amount = isset($postItems[$metaimportId][MetaImportInterface::RAISE_AMOUNT]) ?
                            $postItems[$metaimportId][MetaImportInterface::RAISE_AMOUNT] : null;

                        $metaImport->setRaisePercent($percentage);
                        $metaImport->setRaiseAmount($amount);

                        $this->metaimportRepository->save($metaImport);
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithMetaimportId(
                            $metaImport,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add metaimport title to error message
     *
     * @param MetaImportInterface $metaImport
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithMetaimportId(MetaImportInterface $metaImport, $errorText)
    {
        return '[Metaimport ID: ' . $metaImport->getId() . '] ' . $errorText;
    }
}
