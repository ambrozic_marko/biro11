<?php

namespace MS3\Import\Controller\Adminhtml\MetaImport;

use Magento\Framework\Exception\LocalizedException;

class Selected extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    private $metaImportRepositoryInterface;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;
    /**
     * @var \MS3\Import\Importer\Product
     */
    private $productImporter;
    /**
     * @var \MS3\Import\Logger\Debug
     */
    private $debugLogger;

    private $filter;

    private $collectionFactory;

    /**
     * Constructor
     *
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \MS3\Import\Importer\Product $productImporter
     * @param \MS3\Import\Logger\Debug $debugLogger
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \MS3\Import\Model\ResourceModel\MetaImport\CollectionFactory $collectionFactory
     */
    public function __construct(
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \MS3\Import\Importer\Product $productImporter,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \MS3\Import\Model\ResourceModel\MetaImport\CollectionFactory $collectionFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->metaImportRepositoryInterface = $metaImportRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->jsonSerializer = $jsonSerializer;
        $this->productImporter = $productImporter;
        $this->debugLogger = $debugLogger;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
		$resultPage = $this->resultPageFactory->create();
		$collection = $this->filter->getCollection($this->collectionFactory->create());
        $metaImportItemImported = 0;
        $metaImportItemImportedError = 0;
		$metaImportItemsList = null;
		
		ini_set('memory_limit', '8192M');
		ini_set('max_execution_time', '-1');
		ini_set('max_input_time', '-1');
		
		foreach ($collection->getItems() as $metaImportItem) {
            try {
				$dataModel = $metaImportItem->getDataModel();

				if ($dataModel->getMetaimportId()) {
					$searchCriteria = $this->searchCriteriaBuilder
					->addFilter("metaimport_id", $dataModel->getMetaimportId(), "eq")
					->create();
					
					$metaImportItems = $this->metaImportRepositoryInterface->getList($searchCriteria);
					$importData = $this->metaImportRepositoryInterface->processMetadata($metaImportItems->getItems());

					if ($metaImportItemImported == 0){
						$metaImportItemsList = $metaImportItemsList . $dataModel->getSku();
					} else {
						$metaImportItemsList = $metaImportItemsList . ", ". $dataModel->getSku();
					}
					
					if ($importData) {
					try {
						$this->productImporter->import($importData);
					} catch (\Exception $e) {
						$this->messageManager->addErrorMessage($e->__toString());
					}
					} else {
						$this->messageManager->addErrorMessage(__("Nothing to import."));
					}
					$dataModel->setImported(true);
					$this->metaImportRepositoryInterface->save($dataModel);			
				}
                $metaImportItemImported++;
            } catch (LocalizedException $exception) {
				$this->messageManager->addErrorMessage($exception);
                $metaImportItemImportedError++;
            }
        }



        if ($metaImportItemImported) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been immediately imported (%2).', $metaImportItemImported, $metaImportItemsList)
            );
        }

        if ($metaImportItemImportedError) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been immediately imported. Please see server logs for more details.',
                    $metaImportItemImportedError
                )
            );
        }

        $this->_redirect("*/*/index");
        return $resultPage;
     //   $resultPage = $this->resultPageFactory->create();

     //   $metaimportId = $this->getRequest()->getParam('id', false);
     //   if ($metaimportId) {
     //       $searchCriteria = $this->searchCriteriaBuilder
     //           ->addFilter("metaimport_id", $metaimportId, "eq")
     //           ->create();

     //       $metaImportItems = $this->metaImportRepositoryInterface->getList($searchCriteria);
     //       $importData = $this->metaImportRepositoryInterface->processMetadata($metaImportItems->getItems());

     //       if ($importData) {
     //           try {
     //               $this->debugLogger->debug(count($importData));
     //               $this->productImporter->import($importData);

     //           } catch (\Exception $e) {
     //               $this->debugLogger->debug($e->__toString());
     //           }
     //       } else {
     //           $this->debugLogger->debug("Nothing to import.");
     //       }
     //   }

     //   $this->_redirect("*/*/index");
     //   return $resultPage;
	// return null;
    }
	
 //   private  function is_array_empty($arr){
 //       if(is_array($arr)){     
 //           foreach($arr as $key => $value){
 //               if(!empty($value) || $value != NULL || $value != ""){
 //                   return true;
  //                  break;//stop the process we have seen that at least 1 of the array has value so its not empty
  //              }
  //          }
  //          return false;
  //      }
  //  }	

}
