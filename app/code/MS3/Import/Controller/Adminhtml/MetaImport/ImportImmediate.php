<?php
namespace MS3\Import\Controller\Adminhtml\MetaImport;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class ImportImmediate extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MS3_Import::MetaImport_importImmediate';

    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    protected $metaimportRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    private $searchCriteriaBuilder;

    private $metaImportRepositoryInterface;

    private $jsonSerializer;

    private $debugLogger;

    private $productImporter;

    /**
     * @param Context $context
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaimportRepository
     * @param JsonFactory $jsonFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \MS3\Import\Logger\Debug $debugLogger
     * @param \MS3\Import\Importer\Product $productImporter
     */
    public function __construct(
        Context $context,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaimportRepository,
        JsonFactory $jsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \MS3\Import\Logger\Debug $debugLogger,
        \MS3\Import\Importer\Product $productImporter
    ) {
        parent::__construct($context);
        $this->metaimportRepository = $metaimportRepository;
        $this->jsonFactory = $jsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $messageManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->metaImportRepositoryInterface = $metaImportRepositoryInterface;
        $this->jsonSerializer = $jsonSerializer;
        $this->debugLogger = $debugLogger;
        $this->productImporter = $productImporter;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        $metaimportId = $this->getRequest()->getParam('id', false);
        if ($metaimportId) {
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter("metaimport_id", $metaimportId, "eq")
                ->create();

            $metaImportItems = $this->metaImportRepositoryInterface->getList($searchCriteria);
            $importData = $this->metaImportRepositoryInterface->processMetadata($metaImportItems->getItems());

            if ($importData) {
                try {
                    $this->debugLogger->debug(count($importData));
                    $this->productImporter->import($importData);

                } catch (\Exception $e) {
                    $this->debugLogger->debug($e->__toString());
                }
            } else {
                $this->debugLogger->debug("Nothing to import.");
            }
        }

        $this->_redirect("*/*/index");
        return $resultPage;
    }

}
