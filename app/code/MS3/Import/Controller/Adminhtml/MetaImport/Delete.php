<?php
namespace MS3\Import\Controller\Adminhtml\MetaImport;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'MS3_Import::MetaImport_delete';

    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    protected $metaimportRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Context $context
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaimportRepository
     * @param JsonFactory $jsonFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        Context $context,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaimportRepository,
        JsonFactory $jsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->metaimportRepository = $metaimportRepository;
        $this->jsonFactory = $jsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        $metaimportId = $this->getRequest()->getParam('id', false);
        if ($metaimportId) {
            try {
                $this->metaimportRepository->deleteById($metaimportId);
                $this->messageManager->addSuccessMessage("Item with id $metaimportId deleted successfully.");
                $this->_redirect("*/*/index");
            } catch (\Exception $e) {
                $this->messageManager->addErrorMEssage($e->getMessage());
            }
        }

        $this->_redirect("*/*/index");
        return $resultPage;
    }

}
