<?php

namespace MS3\Import\Ui\DataProvider\Product\Form\Modifier;

class AdvancedInventory
{
    public function afterModifyMeta(\Magento\InventoryCatalogAdminUi\Ui\DataProvider\Product\Form\Modifier\AdvancedInventory $subject,
                                    $result
    )
    {

        $config['children']['backorders']['arguments']['data']['config']['label'] = __("Own Stock");

        $meta['advanced_inventory_modal'] = [
            'children' => [
                'stock_data' => [
                    'children' => [
                        'container_backorders' => $config,
                    ],
                ],
            ],
        ];

        return array_merge($result, $meta);
    }

}
