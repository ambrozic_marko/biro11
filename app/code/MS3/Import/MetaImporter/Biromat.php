<?php

namespace MS3\Import\MetaImporter;

class Biromat extends ImportAbstract
{
    const IMPORT_TYPE = 'biromat';
    const DATA_WRAPPERS = ["podjetje", "_value", "izdelki", "izdelek"];
    const IMPORTER_ENABLED_XML_PATH = 'metaimport_integration/biromat/enable';
    const RAISE_PERCENT_XML_PATH = 'metaimport_integration/biromat/raise_by_percentage';

    /**
     * @var \MS3\Import\Mapper\Biromat
     */
    private $biromatMapper;

    public function __construct(
        \MS3\Import\Downloader\Xml $xmlDownloader,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \MS3\Import\Api\Data\MetaImportInterfaceFactory $metaImportInterfaceFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MS3\Import\Mapper\Biromat $biromatMapper,
        \Magento\Framework\Filter\FilterManager $filter,
        \MS3\Import\Model\MetaImportHistory $history
    ) {
        $this->biromatMapper = $biromatMapper;
        parent::__construct($xmlDownloader, $metaImportRepositoryInterface, $metaImportInterfaceFactory, $jsonSerializer, $searchCriteriaBuilder, $debugLogger, $scopeConfig, $filter, $history);
    }

    public function mapData($data)
    {
        $mappedData = [];
        foreach ($data as $datum) {
            $mappedData[] = $this->biromatMapper->mapIntegrationProductToMagentoProduct($datum);
        }
        return $mappedData;
    }
}
