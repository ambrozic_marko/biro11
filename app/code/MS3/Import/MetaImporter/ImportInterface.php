<?php

namespace MS3\Import\MetaImporter;

interface ImportInterface
{

    public function import();

}
