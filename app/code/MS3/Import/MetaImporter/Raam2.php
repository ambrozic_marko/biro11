<?php

namespace MS3\Import\MetaImporter;

class Raam2 extends ImportAbstract
{
    const IMPORT_TYPE = 'raam2';
    const DATA_WRAPPERS = ["podjetje", "_value", "izdelki", "izdelek"];
    const IMPORTER_ENABLED_XML_PATH = 'metaimport_integration/raam2/enable';
    const RAISE_PERCENT_XML_PATH = 'metaimport_integration/raam2/raise_by_percentage';

    /**
     * @var \MS3\Import\Mapper\Raam2
     */
    private $raam2Mapper;

    public function __construct(
        \MS3\Import\Downloader\Xml $xmlDownloader,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \MS3\Import\Api\Data\MetaImportInterfaceFactory $metaImportInterfaceFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MS3\Import\Mapper\Raam2 $raam2Mapper,
        \Magento\Framework\Filter\FilterManager $filter,
        \MS3\Import\Model\MetaImportHistory $history
    ) {
        $this->raam2Mapper = $raam2Mapper;
        parent::__construct($xmlDownloader, $metaImportRepositoryInterface, $metaImportInterfaceFactory, $jsonSerializer, $searchCriteriaBuilder, $debugLogger, $scopeConfig, $filter, $history);
    }

    public function mapData($data)
    {
        $mappedData = [];
        foreach ($data as $datum) {
            $mappedData[] = $this->raam2Mapper->mapIntegrationProductToMagentoProduct($datum);
        }
        return $mappedData;
    }

}
