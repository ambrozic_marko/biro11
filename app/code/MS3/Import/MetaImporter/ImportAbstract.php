<?php

namespace MS3\Import\MetaImporter;

use Magento\Framework\Exception\LocalizedException;
use MS3\Import\Api\Data\MetaImportInterface;

abstract class ImportAbstract implements ImportInterface
{
    const IMPORT_TYPE = "_";
    const DATA_WRAPPERS = [];
    const IMPORTER_ENABLED_XML_PATH = 'metaimport_integration/_/enable';
    const RAISE_PERCENT_XML_PATH = 'metaimport_integration/_/raise_by_percentage';
    /**
     * @var \MS3\Import\Downloader\Xml $xmlDownloader
     */
    private $xmlDownloader;
    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface
     */
    private $metaImportRepositoryInterface;
    /**
     * @var \MS3\Import\Api\Data\MetaImportInterfaceFactory
     */
    private $metaImportInterfaceFactory;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \MS3\Import\Logger\Debug
     */
    private $debugLogger;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Framework\Filter\FilterManager
     */
    private $filter;

    /**
     * @var \MS3\Import\Model\MetaImportHistory
     */
    private $history;

    /**
     * @param \MS3\Import\Downloader\Xml $xmlDownloader
     * @param \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface
     * @param \MS3\Import\Api\Data\MetaImportInterfaceFactory $metaImportInterfaceFactory
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \MS3\Import\Logger\Debug $debugLogger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filter\FilterManager $filter
     * @param \MS3\Import\Model\MetaImportHistory $history
     */
    public function __construct(
        \MS3\Import\Downloader\Xml $xmlDownloader,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \MS3\Import\Api\Data\MetaImportInterfaceFactory $metaImportInterfaceFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filter\FilterManager $filter,
        \MS3\Import\Model\MetaImportHistory $history
    )
    {
        $this->xmlDownloader = $xmlDownloader;
        $this->metaImportRepositoryInterface = $metaImportRepositoryInterface;
        $this->metaImportInterfaceFactory = $metaImportInterfaceFactory;
        $this->jsonSerializer = $jsonSerializer;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->debugLogger = $debugLogger;
        $this->scopeConfig = $scopeConfig;
        $this->filter = $filter;
        $this->history = $history;
    }

    public function import()
    {

		print_r("Uvoz profila: ". static::IMPORT_TYPE. "\n");

        if (!$this->scopeConfig->getValue(static::IMPORTER_ENABLED_XML_PATH)) {
            return;
        }
       // print_r ($this->scopeConfig->getValue(static::IMPORTER_ENABLED_XML_PATH));
        $productData = $this->getProductData();
        
//        $recCount1 = count($productData);
//        print_r ("Št. elementov: " . $recCount1 . "\n");
        
      //  $xml = simplexml_load_string(curl_exec($ch), 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);
            
        try {
            $mappedData = $this->mapData($productData);
            
            // print_r ($mappedData);

//            $recCount = count($mappedData);
//            print_r ("Št. mapped elementov: " . $recCount . "\n");
//            $icount = 1; 
            
            foreach ($mappedData as $productDatum) {
//                print_r ("Izdelek " . $icount . "/" . $recCount . "\n");
                $productDatum[MetaImportInterface::SKU] = trim($productDatum[MetaImportInterface::SKU]);
                //echo "PROCESSING SKU: " . $productDatum[MetaImportInterface::SKU] . PHP_EOL;

                $metaImportModel = $this->findExistingMetaImportItem($productDatum[MetaImportInterface::SKU], static::IMPORT_TYPE);
                if (!$metaImportModel) {
                    echo "NEW PRODUCT: " . $productDatum[MetaImportInterface::SKU]. "\n";
                    $metaImportModel = $this->metaImportInterfaceFactory->create();
                    $metaImportModel->setImported(false);
                    if (isset($productDatum["name"])) {
                        $productDatum["url_key"] = $this->filter->translitUrl($productDatum[MetaImportInterface::NAME] . "-" . $productDatum[MetaImportInterface::SKU]) . "-" . uniqid();
                    }

                    $raiseByPercent = $this->scopeConfig->getValue(static::RAISE_PERCENT_XML_PATH);
                    if ($raiseByPercent !== false) {
                        $metaImportModel->setRaisePercent($raiseByPercent);
                    }
                } else {
                    // Set existing url key to json_data
                    $jsonData = $this->jsonSerializer->unserialize($metaImportModel->getJsonData());
                    if (isset($jsonData["url_key"])) {
                        $productDatum["url_key"] = $jsonData["url_key"];
                    }
                }

                $metaImportModel->setName($productDatum[MetaImportInterface::NAME]);
                $metaImportModel->setSku($productDatum[MetaImportInterface::SKU]);
                $metaImportModel->setPrice($productDatum[MetaImportInterface::PRICE]);
                $metaImportModel->setCost($productDatum[MetaImportInterface::COST]);
                $metaImportModel->setJsonData($this->jsonSerializer->serialize($productDatum));
                $metaImportModel->setSupplier(static::IMPORT_TYPE);

                $this->metaImportRepositoryInterface->save($metaImportModel);
                
//                $icount++;
            }
            
            $this->flagNonExistingImports($mappedData, static::IMPORT_TYPE);

            $this->history->supplier = static::IMPORT_TYPE;
            $this->history->handleHistory($mappedData);
            
        } catch (\Exception $e) {
            $this->debugLogger->debug($e->__toString());
        }
    }

    public function findExistingMetaImportItem($sku, $supplier)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter("main_table.sku", $sku)
            ->addFilter("main_table.supplier", $supplier)
            ->create();
        try {
            $matchingItems = $this->metaImportRepositoryInterface->getList($searchCriteria);
            if ($matchingItems->getTotalCount() > 0) {
                return $matchingItems->getItems()[0];
            }
        } catch (LocalizedException $e) {
            $this->debugLogger->debug($e->__toString());
        }
        return null;
    }

    public function getProductData()
    {
        $data = $this->xmlDownloader->getXmlAsArray(static::IMPORT_TYPE);

      //  $xml = simplexml_load_string(curl_exec($ch), 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);
        return $this->extractDataLines($data);
    }

    public function extractDataLines($data)
    {
        // Remove xml wrappers
        foreach (static::DATA_WRAPPERS as $wrapper) {
            $data = $data[$wrapper];
        }

        return array_map(function ($item) {
            if (isset($item["_value"])) {
                return $item["_value"];
            }
            return $item;
        }, $data);
    }

    public function mapData($data)
    {
        return $data;
    }

    public function flagNonExistingImports($mappedData, $supplier) {
        echo "Marking those for removal.\n";
        echo "Supplier: " . $supplier . "\n";
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter("main_table.supplier", $supplier)
            ->create();
        try {
            $matchingItems = $this->metaImportRepositoryInterface->getList($searchCriteria);
            if ($matchingItems->getTotalCount() > 0) {
                echo "Removing from sync:\n";
                foreach($matchingItems->getItems() as $importItem) {
                    
                    $exists = false;
                    foreach($mappedData as $xmlItem) {
                        if(trim($importItem->getSku()) == trim($xmlItem['sku'])) {
                            $exists = true;
                            break;
                        }
                    }
                    if (!$exists) {
                        // Doesn't exist
                        echo $importItem->getSku() . "\n";
                        $importItem->setImported(0);
                        $this->metaImportRepositoryInterface->save($importItem);
                    };
                }
            }
        } catch (LocalizedException $e) {
            $this->debugLogger->debug($e->__toString());
        }
    }

}
