<?php

namespace MS3\Import\MetaImporter;

class Altera extends ImportAbstract
{
    const IMPORT_TYPE = 'altera';
    const DATA_WRAPPERS = ["izdelki", "izdelek"];
    const IMPORTER_ENABLED_XML_PATH = 'metaimport_integration/altera/enable';
    const RAISE_PERCENT_XML_PATH = 'metaimport_integration/altera/raise_by_percentage';

    /**
     * @var \MS3\Import\Mapper\Altera
     */
    private $alteraMapper;

    public function __construct(
        \MS3\Import\Downloader\Xml $xmlDownloader,
        \MS3\Import\Api\MetaImportRepositoryInterface $metaImportRepositoryInterface,
        \MS3\Import\Api\Data\MetaImportInterfaceFactory $metaImportInterfaceFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \MS3\Import\Logger\Debug $debugLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MS3\Import\Mapper\Altera $alteraMapper,
        \Magento\Framework\Filter\FilterManager $filter,
        \MS3\Import\Model\MetaImportHistory $history
    ) {
        $this->alteraMapper = $alteraMapper;
        parent::__construct($xmlDownloader, $metaImportRepositoryInterface, $metaImportInterfaceFactory, $jsonSerializer, $searchCriteriaBuilder, $debugLogger, $scopeConfig, $filter, $history);
    }

    public function mapData($data)
    {
        $mappedData = [];
        foreach ($data as $datum) {
            $mappedData[] = $this->alteraMapper->mapIntegrationProductToMagentoProduct($datum);
        }

        return $mappedData;
    }

}
