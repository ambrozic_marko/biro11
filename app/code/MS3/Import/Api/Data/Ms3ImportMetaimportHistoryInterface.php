<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MS3\Import\Api\Data;

interface Ms3ImportMetaimportHistoryInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const TIMESTAMP = 'timestamp';
    const SOURCE = 'source';
    const SKU = 'sku';
    const IMPORTED = 'imported';
    const MS3_IMPORT_METAIMPORT_HISTORY_ID = 'ms3_import_metaimport_history_id';

    /**
     * Get ms3_import_metaimport_history_id
     * @return string|null
     */
    public function getMs3ImportMetaimportHistoryId();

    /**
     * Set ms3_import_metaimport_history_id
     * @param string $ms3ImportMetaimportHistoryId
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setMs3ImportMetaimportHistoryId($ms3ImportMetaimportHistoryId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setSku($sku);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryExtensionInterface $extensionAttributes
    );

    /**
     * Get source
     * @return string|null
     */
    public function getSource();

    /**
     * Set source
     * @param string $source
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setSource($source);

    /**
     * Get imported
     * @return string|null
     */
    public function getImported();

    /**
     * Set imported
     * @param string $imported
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setImported($imported);

    /**
     * Get timestamp
     * @return string|null
     */
    public function getTimestamp();

    /**
     * Set timestamp
     * @param string $timestamp
     * @return \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface
     */
    public function setTimestamp($timestamp);
}

