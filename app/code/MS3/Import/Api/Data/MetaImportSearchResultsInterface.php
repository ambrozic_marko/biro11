<?php


namespace MS3\Import\Api\Data;

interface MetaImportSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get MetaImport list.
     * @return \MS3\Import\Api\Data\MetaImportInterface[]
     */
    public function getItems();

    /**
     * Set sku list.
     * @param \MS3\Import\Api\Data\MetaImportInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
