<?php


namespace MS3\Import\Api\Data;

interface MetaImportInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const SKU = 'sku';
    const METAIMPORT_ID = 'metaimport_id';
    const PRICE = 'price';
    const COST = 'cost';
    const MARGIN = 'margin';
    const NAME = 'name';
    const JSON_DATA = 'json_data';
    const RAISE_PERCENT = 'raise_percent';
    const RAISE_AMOUNT = 'raise_amount';
    const IMPORTED = 'imported';
    const SUPPLIER = 'supplier';

    /**
     * Get metaimport_id
     * @return string|null
     */
    public function getMetaimportId();

    /**
     * Set metaimport_id
     * @param string $metaimportId
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setMetaimportId($metaimportId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setSku($sku);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \MS3\Import\Api\Data\MetaImportExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \MS3\Import\Api\Data\MetaImportExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \MS3\Import\Api\Data\MetaImportExtensionInterface $extensionAttributes
    );

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setName($name);

    /**
     * Get price
     * @return string|null
     */
    public function getPrice();

    /**
     * Set price
     * @param string $price
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setPrice($price);

    /**
     * Get cost
     * @return string|null
     */
    public function getCost();

    /**
     * Set cost
     * @param string cost
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setCost($cost);

    /**
     * Get margin
     * @return string|null
     */
    public function getMargin();

    /**
     * Set margin
     * @param string margin
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setMargin($margin);

    /**
     * Get data
     * @return string|null
     */
    public function getJsonData();

    /**
     * Set data
     * @param string $data
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setJsonData($data);

    /**
     * Get Raise Percent
     * @return float|null
     */
    public function getRaisePercent();

    /**
     * Set Raise Percent
     * @param float $percent
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setRaisePercent($percent);

    /**
     * Get Raise Amount
     * @return float|null
     */
    public function getRaiseAmount();

    /**
     * Set Raise Amount
     * @param float $amount
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setRaiseAmount($amount);

    /**
     * Get Imported
     * @return boolean|null
     */
    public function getImported();

    /**
     * Set Imported
     * @param boolean $imported
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setImported($imported);

    /**
     * Set Supplier
     * @param string $supplier
     * @return \MS3\Import\Api\Data\MetaImportInterface
     */
    public function setSupplier($supplier);

    /**
     * Get Supplier
     * @return string|null
     */
    public function getSupplier();


}
