<?php


namespace MS3\Import\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface MetaImportRepositoryInterface
{

    /**
     * Save MetaImport
     * @param \MS3\Import\Api\Data\MetaImportInterface $metaImport
     * @return \MS3\Import\Api\Data\MetaImportInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MS3\Import\Api\Data\MetaImportInterface $metaImport
    );

    /**
     * Retrieve MetaImport
     * @param string $metaimportId
     * @return \MS3\Import\Api\Data\MetaImportInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($metaimportId);

    /**
     * Retrieve MetaImport matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MS3\Import\Api\Data\MetaImportSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete MetaImport
     * @param \MS3\Import\Api\Data\MetaImportInterface $metaImport
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MS3\Import\Api\Data\MetaImportInterface $metaImport
    );

    /**
     * Delete MetaImport by ID
     * @param string $metaimportId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($metaimportId);
}
