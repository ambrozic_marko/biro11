<?php

namespace MS3\Import\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    /**
     * @var \Magento\Eav\Api\AttributeManagementInterface
     */
    private $attributeManagement;
    /**
     * @var \Magento\Catalog\Model\Config
     */
    private $config;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param \Magento\Eav\Api\AttributeManagementInterface $attributeManagement
     * @param \Magento\Catalog\Model\Config $config
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Api\AttributeManagementInterface $attributeManagement,
        \Magento\Catalog\Model\Config $config
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeManagement = $attributeManagement;
        $this->config = $config;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '2.0.0', "<=")) {

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'mapping_category',
                [
                    'type' => 'varchar',
                    'label' => 'Mapping Category',
                    'input' => 'select',
                    'sort_order' => 333,
                    'global' => 1,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => null,
                    'group' => 'Display Settings',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend'
                ]
            );
        }

        if (version_compare($context->getVersion(), '2.0.2', "<=")){
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'supplier',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Supplier',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );

            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
            foreach ($attributeSetIds as $attributeSetId) {
                if ($attributeSetId) {
                    $group_id = $this->config->getAttributeGroupId($attributeSetId, 'General');
                    $this->attributeManagement->assign(
                        'catalog_product',
                        $attributeSetId,
                        $group_id,
                        'supplier',
                        999
                    );
                }
            }
        }

        if (version_compare($context->getVersion(), '2.0.3', "<=")){
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'additional_details',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Additional Details',
                    'input' => 'textarea',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );

            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
            foreach ($attributeSetIds as $attributeSetId) {
                if ($attributeSetId) {
                    $group_id = $this->config->getAttributeGroupId($attributeSetId, 'Content');
                    $this->attributeManagement->assign(
                        'catalog_product',
                        $attributeSetId,
                        $group_id,
                        'additional_details',
                        999
                    );
                }
            }
        }
    }
}
