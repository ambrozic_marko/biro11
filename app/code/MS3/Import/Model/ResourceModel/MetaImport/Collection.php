<?php


namespace MS3\Import\Model\ResourceModel\MetaImport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = "metaimport_id";

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MS3\Import\Model\MetaImport::class,
            \MS3\Import\Model\ResourceModel\MetaImport::class
        );
    }
}
