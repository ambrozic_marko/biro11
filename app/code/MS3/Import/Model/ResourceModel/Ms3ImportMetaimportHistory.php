<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MS3\Import\Model\ResourceModel;

class Ms3ImportMetaimportHistory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ms3_import_ms3_import_metaimport_history', 'ms3_import_metaimport_history_id');
    }
}

