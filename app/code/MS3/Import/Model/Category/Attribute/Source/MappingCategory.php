<?php

namespace MS3\Import\Model\Category\Attribute\Source;

use Magento\Framework\Data\OptionSourceInterface;

class MappingCategory implements OptionSourceInterface
{

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $categoryCollectionFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray()
    {
        $options = [];
        $allCategories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addOrderField("path");

        foreach ($allCategories as $category) {
            $options[] =
                [
                    'label' => str_repeat("==", $category->getLevel())
                        . ($category->getLevel() ? "> " : "")
                        . $category->getName(),
                    'value' => $category->getId()
                ];
        }

        return $options;
    }
}
