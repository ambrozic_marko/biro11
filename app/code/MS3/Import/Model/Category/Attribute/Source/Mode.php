<?php

namespace MS3\Import\Model\Category\Attribute\Source;

class Mode extends \Magento\Catalog\Model\Category\Attribute\Source\Mode
{
    const IMPORT_MODE_ONLY = 'import_only';
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            parent::getAllOptions();
            $this->_options[] = ['value' => self::IMPORT_MODE_ONLY, 'label' => __('Import only')];
        }
        return $this->_options;
    }
}
