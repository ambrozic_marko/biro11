<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MS3\Import\Model;

use MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterfaceFactory;
use MS3\Import\Api\Data\Ms3ImportMetaimportHistorySearchResultsInterfaceFactory;
use MS3\Import\Api\Ms3ImportMetaimportHistoryRepositoryInterface;
use MS3\Import\Model\ResourceModel\Ms3ImportMetaimportHistory as ResourceMs3ImportMetaimportHistory;
use MS3\Import\Model\ResourceModel\Ms3ImportMetaimportHistory\CollectionFactory as Ms3ImportMetaimportHistoryCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class Ms3ImportMetaimportHistoryRepository implements Ms3ImportMetaimportHistoryRepositoryInterface
{

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $ms3ImportMetaimportHistoryCollectionFactory;

    protected $dataMs3ImportMetaimportHistoryFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $ms3ImportMetaimportHistoryFactory;

    private $storeManager;


    /**
     * @param ResourceMs3ImportMetaimportHistory $resource
     * @param Ms3ImportMetaimportHistoryFactory $ms3ImportMetaimportHistoryFactory
     * @param Ms3ImportMetaimportHistoryInterfaceFactory $dataMs3ImportMetaimportHistoryFactory
     * @param Ms3ImportMetaimportHistoryCollectionFactory $ms3ImportMetaimportHistoryCollectionFactory
     * @param Ms3ImportMetaimportHistorySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMs3ImportMetaimportHistory $resource,
        Ms3ImportMetaimportHistoryFactory $ms3ImportMetaimportHistoryFactory,
        Ms3ImportMetaimportHistoryInterfaceFactory $dataMs3ImportMetaimportHistoryFactory,
        Ms3ImportMetaimportHistoryCollectionFactory $ms3ImportMetaimportHistoryCollectionFactory,
        Ms3ImportMetaimportHistorySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->ms3ImportMetaimportHistoryFactory = $ms3ImportMetaimportHistoryFactory;
        $this->ms3ImportMetaimportHistoryCollectionFactory = $ms3ImportMetaimportHistoryCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMs3ImportMetaimportHistoryFactory = $dataMs3ImportMetaimportHistoryFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface $ms3ImportMetaimportHistory
    ) {
        /* if (empty($ms3ImportMetaimportHistory->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $ms3ImportMetaimportHistory->setStoreId($storeId);
        } */
        
        $ms3ImportMetaimportHistoryData = $this->extensibleDataObjectConverter->toNestedArray(
            $ms3ImportMetaimportHistory,
            [],
            \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface::class
        );
        
        $ms3ImportMetaimportHistoryModel = $this->ms3ImportMetaimportHistoryFactory->create()->setData($ms3ImportMetaimportHistoryData);
        
        try {
            $this->resource->save($ms3ImportMetaimportHistoryModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the ms3ImportMetaimportHistory: %1',
                $exception->getMessage()
            ));
        }
        return $ms3ImportMetaimportHistoryModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($ms3ImportMetaimportHistoryId)
    {
        $ms3ImportMetaimportHistory = $this->ms3ImportMetaimportHistoryFactory->create();
        $this->resource->load($ms3ImportMetaimportHistory, $ms3ImportMetaimportHistoryId);
        if (!$ms3ImportMetaimportHistory->getId()) {
            throw new NoSuchEntityException(__('ms3_import_metaimport_history with id "%1" does not exist.', $ms3ImportMetaimportHistoryId));
        }
        return $ms3ImportMetaimportHistory->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->ms3ImportMetaimportHistoryCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MS3\Import\Api\Data\Ms3ImportMetaimportHistoryInterface $ms3ImportMetaimportHistory
    ) {
        try {
            $ms3ImportMetaimportHistoryModel = $this->ms3ImportMetaimportHistoryFactory->create();
            $this->resource->load($ms3ImportMetaimportHistoryModel, $ms3ImportMetaimportHistory->getMs3ImportMetaimportHistoryId());
            $this->resource->delete($ms3ImportMetaimportHistoryModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the ms3_import_metaimport_history: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($ms3ImportMetaimportHistoryId)
    {
        return $this->delete($this->get($ms3ImportMetaimportHistoryId));
    }
}

