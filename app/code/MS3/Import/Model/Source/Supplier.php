<?php

namespace MS3\Import\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Supplier implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ["value" => "altera", "label" => "altera"],
            ["value" => "avtera", "label" => "avtera"],
            ["value" => "biromat", "label" => "biromat"],
            ["value" => "diss", "label" => "diss"],
            ["value" => "pigo", "label" => "pigo"],
            ["value" => "also", "label" => "also"],
            ["value" => "raam2", "label" => "raam2"],
        ];
    }
}
