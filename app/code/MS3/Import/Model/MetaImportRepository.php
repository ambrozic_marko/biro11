<?php

namespace MS3\Import\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use MS3\Import\Api\Data\MetaImportInterfaceFactory;
use MS3\Import\Api\Data\MetaImportSearchResultsInterfaceFactory;
use MS3\Import\Api\MetaImportRepositoryInterface;
use MS3\Import\Model\ResourceModel\MetaImport as ResourceMetaImport;
use MS3\Import\Model\ResourceModel\MetaImport\CollectionFactory as MetaImportCollectionFactory;

class MetaImportRepository implements MetaImportRepositoryInterface
{
    const SUPPLIER_DESCRIPTION_MAPPING = [
        'diss' => 'avtera'
    ];

    protected $dataMetaImportFactory;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;
    protected $metaImportCollectionFactory;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    protected $resource;

    protected $searchResultsFactory;

    protected $metaImportFactory;

    protected $dataObjectProcessor;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;

    /**
     * @param ResourceMetaImport $resource
     * @param MetaImportFactory $metaImportFactory
     * @param MetaImportInterfaceFactory $dataMetaImportFactory
     * @param MetaImportCollectionFactory $metaImportCollectionFactory
     * @param MetaImportSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMetaImport $resource,
        MetaImportFactory $metaImportFactory,
        MetaImportInterfaceFactory $dataMetaImportFactory,
        MetaImportCollectionFactory $metaImportCollectionFactory,
        MetaImportSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
    ) {
        $this->resource = $resource;
        $this->metaImportFactory = $metaImportFactory;
        $this->metaImportCollectionFactory = $metaImportCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMetaImportFactory = $dataMetaImportFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MS3\Import\Api\Data\MetaImportInterface $metaImport
    ) {
        $metaImportData = $this->extensibleDataObjectConverter->toNestedArray(
            $metaImport,
            [],
            \MS3\Import\Api\Data\MetaImportInterface::class
        );

        $metaImportModel = $this->metaImportFactory->create()->setData($metaImportData);

        try {
            $this->resource->save($metaImportModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the metaImport: %1',
                $exception->getMessage()
            ));
        }
        return $metaImportModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($metaImportId)
    {
        $metaImport = $this->metaImportFactory->create();
        $this->resource->load($metaImport, $metaImportId);
        if (!$metaImport->getId()) {
            throw new NoSuchEntityException(__('MetaImport with id "%1" does not exist.', $metaImportId));
        }
        return $metaImport->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->metaImportCollectionFactory->create();
        $collection->getSelect()->joinLeft(
            ["HIST" => "ms3_import_ms3_import_metaimport_history"],
            "HIST.sku = main_table.sku AND HIST.source = main_table.supplier",
            []
        );
        $collection->getSelect()->order("MAX(HIST.timestamp)");
        $collection->getSelect()->group(["main_table.sku", "main_table.supplier"]);

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \MS3\Import\Api\Data\MetaImportInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MS3\Import\Api\Data\MetaImportInterface $metaImport
    ) {
        try {
            $metaImportModel = $this->metaImportFactory->create();
            $this->resource->load($metaImportModel, $metaImport->getMetaimportId());
            $this->resource->delete($metaImportModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the MetaImport: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($metaImportId)
    {
        return $this->delete($this->getById($metaImportId));
    }

    public function processMetadata(array $items)
    {
        $importData = [];
        foreach ($items as $metaImportItem) {
            $additionalData = [
                "raise_percent" => floatval($metaImportItem->getRaisePercent()),
                "raise_amount" => floatval($metaImportItem->getRaiseAmount()),
                "supplier" => $metaImportItem->getSupplier()
            ];
            try {
                $importData[] = array_merge(
                    $this->jsonSerializer->unserialize($metaImportItem->getJsonData()),
                    $additionalData
                );
            } catch (\Exception $e) {
                echo $e->__toString() . PHP_EOL;
            }

        }

        return $this->processDescriptions($this->processDuplicateItems($importData));
    }

    public function processDescriptions($items)
    {
        foreach ($items as &$item) {
            if (array_key_exists($item["supplier"], self::SUPPLIER_DESCRIPTION_MAPPING)) {
                /** @var \MS3\Import\Model\ResourceModel\MetaImport\Collection $collection */
                $collection = $this->metaImportCollectionFactory->create();
                $collection->addFieldToFilter('sku', ["eq" => $item["sku"]]);
                $collection->addFieldToFilter('supplier', ["eq" => self::SUPPLIER_DESCRIPTION_MAPPING[$item["supplier"]]]);

                $descriptionItem = $collection->getFirstItem();
                if ($descriptionItem && $descriptionItem->getSku()) {
                    try {
                        $jsonData = $this->jsonSerializer->unserialize($descriptionItem->getJsonData());
                        if (isset($jsonData["description"]) && $jsonData["description"]) {
                            $item["description"] = $jsonData["description"];
                        }
                    } catch (\Exception $e) {
                        echo $e->__toString() . PHP_EOL;
                    }
                }
            }
        }

        return $items;
    }

    public function processDuplicateItems(array $items)
    {
        $skuGroupedProducts = [];
        $finalItems = [];
        foreach ($items as $item) {
            $skuGroupedProducts[$item["sku"]][] = $item;
        }

        foreach ($skuGroupedProducts as $sku => $items) {
            if (count($items) > 1) {
                $minItem = $this->findMinimalItem($items);

                // If no in stock products, skip stock check and look for minimal
                if (!$minItem) {
                    $finalItems[] = $this->findMinimalItem($items, false);
                } else {
                    $finalItems[] = $minItem;
                }
            } else {
                $finalItems[] = $items[0];
            }
        }

        return $finalItems;
    }

    public function disableStatus($sku)
    {
        $collection = $this->metaImportCollectionFactory->create();
        $collection->addFieldToFilter('sku', ["eq" => $sku]);

        /** @var MetaImport $item */
        foreach ($collection as $item) {
            $jsonData = json_decode($item->getJsonData(), true);
            $jsonData["status"] = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
            $item->setJsonData(json_encode($jsonData));
            $item->save();
        }
    }

    private function findMinimalItem($items, $checkStock = true)
    {
        $minValue = PHP_FLOAT_MAX;
        $minItem = false;
        foreach ($items as $item)
        {
            if (
                (($checkStock
                && isset($item["is_in_stock"])
                && $item["is_in_stock"] == 1)
                || !$checkStock)
                && $item["cost"] < $minValue) {
                $minItem = $item;
                $minValue = $item["cost"];
            }
        }

        return $minItem;
    }

}
