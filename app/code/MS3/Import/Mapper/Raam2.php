<?php

namespace MS3\Import\Mapper;

class Raam2 extends MapperAbstract
{
    const MAPPING_ARRAY_CONFIG_PATH = 'metaimport_integration/raam2/field_mapping';
    const COMPLEX_MAPPING_ARRAY_CONFIG_PATH = 'metaimport_integration/raam2/complex_field_mapping';
    const ROOT_CATEGORY = 'Raam2';

    protected $complexFields = ["dodatneLastnosti"];
    protected $complexMapperArray = [];

    public function mapField(&$magentoProduct, $magentoKey, $value)
    {
        parent::mapField($magentoProduct, $magentoKey, $value);

        if ($magentoKey == "qty") {
            $value = $this->flattenField($value);
            $magentoProduct["qty"] = is_numeric($value) ? $value : 0;
            $magentoProduct["is_in_stock"] = is_numeric($value) && $value > 0 ? 1 : 0;
        }
    }
}
