<?php

namespace MS3\Import\Mapper;

class Avtera extends MapperAbstract
{
    const MAPPING_ARRAY_CONFIG_PATH = 'metaimport_integration/avtera/field_mapping';
    const COMPLEX_MAPPING_ARRAY_CONFIG_PATH = 'metaimport_integration/avtera/complex_field_mapping';
    const ROOT_CATEGORY = 'Avtera';

    protected $complexFields = ["dodatneLastnosti"];
    protected $complexMapperArray = [];

    public function mapField(&$magentoProduct, $magentoKey, $value)
    {
        parent::mapField($magentoProduct, $magentoKey, $value);

        if ($magentoKey == "qty") {
            $value = $this->flattenField($value);
            $magentoProduct["qty"] = is_numeric($value) ? $value : 0;
            $magentoProduct["is_in_stock"] = is_numeric($value) && $value > 0 ? 1 : 0;
        }
    }
}
