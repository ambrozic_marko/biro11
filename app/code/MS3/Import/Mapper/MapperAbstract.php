<?php

namespace MS3\Import\Mapper;

use FireGento\FastSimpleImport\Helper\Config;

abstract class MapperAbstract
{
    protected $mapperArray = [];
    protected $complexFields = [];
    protected $complexMapperArray = [];

    const MAPPING_ARRAY_CONFIG_PATH = '';
    const INTEGRATION_FIELD_KEY = 'integration_field';
    const MAGENTO_FIELD_KEY = 'magento_field';
    const ROOT_CATEGORY = 'MetaImport';

    const SUPPLIER_HAS_STOCK_LABEL = 'Na zalogi pri dobavitelju';
    const SUPPLIER_NO_STOCK_LABEL = 'Ni zaloge pri dobavitelju';

    const IN_STOCK_LABELS_ARRAY = ['na zalogi', 'Na Zalogi'];

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerializer;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var \MS3\Import\Api\Ms3ImportMetaimportHistoryRepositoryInterface
     */
    private $historyRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * MapperAbstract constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \MS3\Import\Api\Ms3ImportMetaimportHistoryRepositoryInterface $historyRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \MS3\Import\Api\Ms3ImportMetaimportHistoryRepositoryInterface $historyRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->jsonSerializer = $jsonSerializer;
        $this->productRepository = $productRepository;
        $this->historyRepository = $historyRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getMapperArray()
    {
        if (!$this->mapperArray) {
            $serializedConfigValue = $this->scopeConfig->getValue(static::MAPPING_ARRAY_CONFIG_PATH);
            $configuredArray = $this->jsonSerializer->unserialize($serializedConfigValue);

            foreach ($configuredArray as $mapDefinition) {
                $this->mapperArray[$mapDefinition[static::MAGENTO_FIELD_KEY]] = $mapDefinition[static::INTEGRATION_FIELD_KEY];
            }
        }

        return $this->mapperArray;
    }

    public function mapIntegrationProductToMagentoProduct(array $integrationProduct)
    {
        $magentoProduct = [];
       // echo "1";
       // var_dump($integrationProduct); 
       // if (($integrationProduct['VendorItemNo.'] == null) && ($integrationProduct['izdelekID'] == null)){
        if (!array_key_exists('VendorItemNo.',$integrationProduct) &&  !array_key_exists('izdelekID',$integrationProduct)){  
            return null;
        } else {
            if (array_key_exists('VendorItemNo.',$integrationProduct)){
                print_r($integrationProduct['VendorItemNo.'] . ' - > ');

            } elseif (array_key_exists('izdelekID',$integrationProduct)){
                print_r($integrationProduct['izdelekID'] . ' - > ');
            }   
        }
        foreach ($this->getMapperArray() as $magentoKey => $integrationKey) {
            if (isset($integrationProduct[$integrationKey])) {
                $this->mapField($magentoProduct, $magentoKey, $integrationProduct[$integrationKey]);
            }
        }

        if ($this->hasOwnStock($magentoProduct["sku"])) {
            $magentoProduct["is_in_stock"] = 1;
        }

        if ($this->existsInHistory($magentoProduct["sku"])) {
            $magentoProduct["status"] = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED;
        }

        foreach ($this->complexFields as $field) {
            if (array_key_exists($field, $integrationProduct) && is_array($integrationProduct[$field])) {
                foreach (array_pop($integrationProduct[$field]) as $complexField => $complexValue) {
                    $flattenedComplexField = $this->flattenComplexField($complexValue);
                    if ($flattenedComplexField && array_key_exists($flattenedComplexField["label"], $this->getComplexMapperArray())) {
                        $magentoProduct[$this->mapComplexFieldToMagentoField($flattenedComplexField["label"])] = trim($flattenedComplexField["value"]);
                    }
                }
            }
        }

        return $magentoProduct;
    }

    public function mapField(&$magentoProduct, $magentoKey, $value)
    {
        $flatValue = $this->flattenField($value);
        if (is_array($flatValue) || strlen(trim($flatValue)) > 0) {
            $magentoProduct[$magentoKey] = $flatValue;
        }

        /* Prefix with root category */
        if ($magentoKey == "categories" && isset($magentoProduct[$magentoKey])) {
            $magentoProduct[$magentoKey] =
                static::ROOT_CATEGORY
                . $this->scopeConfig->getValue(Config::XML_PATH_CATEGORY_PATH_SEPERATOR)
                . str_replace(",", ";", $magentoProduct[$magentoKey]) // Replaces commans with semicolons to prevent splitting into separate categories
                . " - " . static::ROOT_CATEGORY; // Added root category name at the end to prevent url duplicates
        }

        /* Map gallery to additional images */
        if ($magentoKey == "gallery" && is_array($value)) {
            //Remove image base image

            try {

                $error = false;

                if(is_array($value)){   
                    //var_dump($value);
                    foreach($value as $key => &$value1){
                        if(!empty($value1) || $value1 != NULL || $value1 != ""){
                          //  return true;
                           // echo 'OK' . $key . " -> " . $value1. "\n";
                           // break;//stop the process we have seen that at least 1 of the array has value so its not empty
                        } else {
                            //echo 'ERROR';
                            //die();
                            $error = true;
                        }
                    }
                }
            
                if ($error == false){
                    if (($key = array_search( $magentoProduct["image"], $value)) !== false) {
                        unset($value[$key]);
                    }

                    $magentoProduct["additional_images"] = implode(",", $value);
                    unset($magentoProduct["gallery"]);
                }


            } catch(Exception $e) {
                echo 'Message: ' .$e->getMessage();

            }
        }

        $this->supplierStock($magentoProduct, $magentoKey, $flatValue);
        $this->additionalDetails($magentoProduct, $magentoKey, $flatValue);
    }

    public function flattenField($value)
    {
        if (is_array($value) && array_key_exists("_value", $value)) {
            $value = $this->flattenField($value["_value"]);

            if (strcasecmp($value, "ne") === 0) {
                $value = "No";
            } else if(strcasecmp($value, "da") === 0){
                $value = "Yes";
            }
        }

        return $value;
    }

    public function hasOwnStock($sku)
    {
        try {
            $product = $this->productRepository->get($sku);
            $backorderStatus = $product->getExtensionAttributes()->getStockItem()->getBackorders();
            return $backorderStatus > 0;
        } catch(\Exception $e)
        {
            return false;
        }
    }

    public function supplierStock(&$magentoProduct, $magentoKey, $value)
    {
        if ($magentoKey == 'zaloga_pri_dobavitelju') {
            $magentoProduct[$magentoKey] = (is_numeric($value) && $value > 0) || in_array($value, self::IN_STOCK_LABELS_ARRAY) ?
                self::SUPPLIER_HAS_STOCK_LABEL
                : self::SUPPLIER_NO_STOCK_LABEL;
        }
    }

    public function existsInHistory($sku)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter("sku", $sku)->create();
        $list = $this->historyRepository->getList($searchCriteria);

        echo "Product $sku exists in history?: " . ($list->getTotalCount() > 0) . PHP_EOL;
        return $list->getTotalCount() > 0;
    }

    public function additionalDetails(&$magentoProduct, $magentoKey, $value)
    {
        if ($magentoKey == 'additional_details') {
            if (isset($value["lastnost"])) {
                $detailArray = $value["lastnost"];
                $result = [];
                $magentoProduct[$magentoKey] = "<ul>";
                foreach ($detailArray as $item)
                {
                    if(isset($item["_attribute"]["naziv"])) {
                        $row = "<li>" . $item["_attribute"]["naziv"] . ": " . $result[] = $item["_value"] . "</li>"; ;
                        $magentoProduct[$magentoKey] .= $row;
                    }
                }
                $magentoProduct[$magentoKey] .= "</ul>";
            }
        }
    }

    public function flattenComplexField($complexField)
    {
        if (is_array($complexField) && isset($complexField["_attribute"]["naziv"]) && isset($complexField["_value"])) {
            $value = $complexField["_value"];
            if (strcasecmp($value, "ne") === 0) {
                $value = "No";
            } else if(strcasecmp($value, "da") === 0){
                $value = "Yes";
            }
            return ["label" => $complexField["_attribute"]["naziv"], "value" => $value];
        }
        return false;
    }

    public function getComplexMapperArray()
    {
        if (!$this->complexMapperArray) {
            $serializedConfigValue = $this->scopeConfig->getValue(static::COMPLEX_MAPPING_ARRAY_CONFIG_PATH);
            $configuredArray = $this->jsonSerializer->unserialize($serializedConfigValue);

            foreach ($configuredArray as $mapDefinition) {
                $this->complexMapperArray[$mapDefinition[static::INTEGRATION_FIELD_KEY]] = $mapDefinition[static::MAGENTO_FIELD_KEY];
            }
        }

        return $this->complexMapperArray;
    }

    public function mapComplexFieldToMagentoField($key)
    {
        return $this->getComplexMapperArray()[$key];
    }

    public function mapIntegrationProductToMagentoProductAlso(array $integrationProduct)
    {
        $magentoProduct = [];
        foreach ($this->getMapperArray() as $magentoKey => $integrationKey) {

			$str_arr = explode ("/", $integrationKey);
      
			$value = $integrationProduct;
        
			foreach ($str_arr as $arrKey) {
				$value = $value[$arrKey];
			}


			if (is_array($value) && ($magentoKey != "gallery")) {
                $value = $value['_value'];
            }

            if (isset($value)) {
                $this->mapField($magentoProduct, $magentoKey, $value);
            }
        }

        if ($this->hasOwnStock($magentoProduct["sku"])) {
            $magentoProduct["is_in_stock"] = 1;
        }

        if ($this->existsInHistory($magentoProduct["sku"])) {
            $magentoProduct["status"] = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED;
        }

		foreach ($this->complexFields as $field) {
            if (array_key_exists($field, $integrationProduct) && is_array($integrationProduct[$field])) {
                $specificationValues = array();
                $attrValues = array();
                if (array_key_exists('specification', $integrationProduct[$field])) {
                    foreach ($integrationProduct[$field]['specification'] as $specificationValue) {
                        $temp['_value'] = $specificationValue['_value'];
                        $temp['_attribute']['naziv'] = $specificationValue['_attribute']['name'];
                        $attrValues[] = $temp;
                    }
                }
                $specificationValues['lastnost'] = $attrValues;               
                foreach (array_pop($specificationValues) as $complexField => $complexValue) {
                    $flattenedComplexField = $this->flattenComplexField($complexValue);
                    if ($flattenedComplexField && array_key_exists($flattenedComplexField["label"], $this->getComplexMapperArray())) {
                        $magentoProduct[$this->mapComplexFieldToMagentoField($flattenedComplexField["label"])] = trim($flattenedComplexField["value"]);
                    }
                }
            }
        }

        return $magentoProduct;
    }
}
