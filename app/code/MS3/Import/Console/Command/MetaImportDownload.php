<?php
namespace MS3\Import\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SomeCommand
 */
class MetaImportDownload extends Command
{
    protected $importers;

    /*public function __construct(
        \MS3\Import\MetaImporter\Altera $alteraImporter,
        \MS3\Import\MetaImporter\Biromat $biromatImporter,
        \MS3\Import\MetaImporter\Pigo $pigoImporter,
        \MS3\Import\MetaImporter\Avtera $avteraImporter,
        \MS3\Import\MetaImporter\Diss $dissImporter,
        \MS3\Import\MetaImporter\Also $alsoImporter
    ) {
        $this->importers = [
            $alteraImporter,
            $biromatImporter,
            $pigoImporter,
            $avteraImporter,
            $dissImporter,
            $alsoImporter
        ];
        parent::__construct();
    }*/

    protected function configure()
    {
        $this->setName('metaImporter:download')->setDescription('Run MetaImport Download Manually');    
        parent::configure();
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       // $alteraImporter = $objectManager->get('\MS3\Import\MetaImporter\Altera');
       // $biromatImporter = $objectManager->get('\MS3\Import\MetaImporter\Biromat');
       // $pigoImporter = $objectManager->get('\MS3\Import\MetaImporter\Pigo');
        //$avteraImporter = $objectManager->get('\MS3\Import\MetaImporter\Avtera');
       // $raam2Importer = $objectManager->get('\MS3\Import\MetaImporter\Raam2');
		$alsoImporter = $objectManager->get('\MS3\Import\MetaImporter\Also');
        //$dissImporter = $objectManager->get('\MS3\Import\MetaImporter\Diss');
        

        $importers = [
       //     $alteraImporter,
       //     $biromatImporter,
       //     $pigoImporter,
        //    $avteraImporter//,
       //     $raam2Importer,
            $alsoImporter//,
       //     $dissImporter
        ];
            foreach ($importers as $importer) {
				try {
					$importer->import();
					print_r('Uvoz zaključen');
				} catch (Exception $e) {
					return $e;
				}     
            }

         //   $output->writeln($e->getMessage());

    }
   
}
