<?php

namespace MS3\Import\Block\Adminhtml\MetaImport;

use Magento\Backend\Block\Template;
use Magento\Framework\Exception\LocalizedException;

class View extends Template
{
    /**
     * @var \MS3\Import\Api\MetaImportRepositoryInterface
     */
    private $metaimportRepository;

    public function __construct(
        \MS3\Import\Api\MetaImportRepositoryInterface $metaimportRepository,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->metaimportRepository = $metaimportRepository;
    }

    public function getPrettyJson()
    {
        $metaImportId = $this->getRequest()->getParam("id", false);
        if ($metaImportId) {
            $metaImport = $this->metaimportRepository->getById($metaImportId);
            return json_encode(json_decode($metaImport->getJsonData()), JSON_PRETTY_PRINT);
        }

        return __("No Data Found");
    }
}
