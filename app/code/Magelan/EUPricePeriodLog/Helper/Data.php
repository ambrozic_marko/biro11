<?php

namespace Magelan\EUPricePeriodLog\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

use \Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\Helper\Context as Context;

use Magento\Store\Api\StoreRepositoryInterface as StoresRepository;
use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magelan\EUPricePeriodLog\Model\ResourceModel\ProductPriceLog\CollectionFactory as ProductPriceLogCollection;
use Magelan\EUPricePeriodLog\Model\ProductPriceLogFactory as ProductPriceLogFactory;

use Magento\Catalog\Helper\Data as TaxHelper;

class Data extends AbstractHelper
{
    
    protected $_productStatus;
    protected $_storesRepository;

    protected $_productCollectionFactory;
    protected $_priceHistoryCollectionFactory;
    protected $_productPriceLogFactory;

    protected $_date;

    protected $_taxHelper;

    protected $_logger;
    
    public function __construct(
        Context $context,
        ProductCollection $productCollectionFactory,
        ProductPriceLogCollection $priceHistoryCollectionFactory,
        ProductPriceLogFactory $productPriceLogFactory,
        StoresRepository $storesRepository,
        ProductStatus $productStatus,
        TimezoneInterface $date,
        TaxHelper $taxHelper
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_priceHistoryCollectionFactory = $priceHistoryCollectionFactory;
        $this->_productPriceLogFactory = $productPriceLogFactory;
        $this->_storesRepository = $storesRepository;
        $this->_productStatus = $productStatus;
        $this->_date = $date;
        $this->_taxHelper = $taxHelper;
        
        parent::__construct($context);
        
        $logger = new \Zend_Log();
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/eu-pricechange.cron.log');
        $logger->addWriter($writer);
        $this->_logger = $logger;
    }

    public function createProductPriceLog() {
        $this->_logger->info("Creating price log.");
        $stores = $this->_storesRepository->getList();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$taxCalculation = $objectManager->create('\Magento\Tax\Api\TaxCalculationInterface');
		$scopeConfig = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');  
		$taxSetting = $scopeConfig->getValue('tax/calculation/price_includes_tax');

        $includeTax = null;
		if ($taxSetting) {
			$includeTax = false;
		} else {
			$includeTax = true;
		}

        foreach ($stores as $store) {
            
            $this->_logger->info('Setting price log for Store: ' . $store->getId());
            $collection = $this->getProductCollectionForStore($store->getId());
            $now = $this->getCurrentDate();
            
            foreach($collection as $product) {
                
                //$this->_logger->info("Product SKU: " . $product->getSKU(). " Store: " . $store->getId() . " SP: " . $product->getSpecialPrice() . " NP: " . $product->getPrice());
                
                $currentPrice = null;
                if($product->getTypeId() ==  'simple') {
                    $currentPrice = $this->_taxHelper->getTaxPrice($product, $product->getPrice(), $includeTax);
                    if($product->getSpecialPrice() && $product->getSpecialPrice() < $currentPrice) 
                        $currentPrice = $this->_taxHelper->getTaxPrice($product, $product->getSpecialPrice(), $includeTax);

                    if($product->getFinalPrice() && $product->getFinalPrice() < $currentPrice) 
                        $currentPrice = $this->_taxHelper->getTaxPrice($product, $product->getFinalPrice(), $includeTax);
                }

                if($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                    $regularPrice = $this->_taxHelper->getTaxPrice($product, $product->getPriceInfo()->getPrice('regular_price'), $includeTax);
                    $currentPrice = $this->_taxHelper->getTaxPrice($product, $regularPrice->getMinRegularAmount()->getValue(), $includeTax);
                }

                $lastPriceLog = $this->getLastHistoricPrice($product->getId(), $store->getId());
                if($currentPrice != null) {
                    $hours = 999;
                    if($lastPriceLog != null) {
                            $this->_logger->info("T: " . $product->getTypeId() . " Product SKU: " . $product->getSKU(). " Store: " . $store->getId() . " SP: " . $lastPriceLog->getPrice() . " NP: " . $currentPrice);
                            $diff = strtotime($lastPriceLog->getCreatedAt()) - strtotime($now);
                            $hours = abs($diff / (60 * 60));
                    }
                    if($lastPriceLog == null || $hours >= 24 || $lastPriceLog->getPrice() <> $currentPrice) {
                        $this->savePriceLog([
                            'store_id' => $store->getId(),
                            'product_id' => $product->getId(),
                            'price' => $currentPrice,
                            'created_at' => $now
                        ]);
                    }
                }
            }

        }
        $this->_logger->info("Price log finished.");
    }

    public function get30DaysPriceMovementsFor($productId, $storeId) {
        $fromDate = date('Y-m-d', strtotime('today - 30 days'));
        $collection = $this->_priceHistoryCollectionFactory->create()
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('price')
            ->addFieldToFilter('product_id',['eq'=> $productId])
            ->addFieldToFilter('store_id',['eq'=> $storeId])
            ->addFieldToFilter('created_at',['gt'=> $fromDate]);
        return $collection;
    }

    public function getProductCollectionForStore($storeId) {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addStoreFilter($storeId); 
        $collection->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()]);
        return $collection;
    }

    public function savePriceLog($data) {
        $priceLog = $this->_productPriceLogFactory->create();
        $priceLog->addData($data);
        $priceLog->save();
    }

    public function getCurrentDate() {
        return $this->_date->date()->format('d-m-Y H:i:s');
    }

    private function getLastHistoricPrice($productId, $storeId) {
        $collection = $this->_priceHistoryCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter('product_id',['eq'=> $productId])
            ->addFieldToFilter('store_id',['eq'=> $storeId])
            ->getSelect()->order('created_at DESC')->limit(1);
        if($collection->count()>0) return $collection->getFirstItem();
        return null;
    }

}