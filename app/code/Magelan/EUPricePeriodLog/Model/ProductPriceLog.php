<?php
namespace Magelan\EUPricePeriodLog\Model;

use Magento\Framework\Model\AbstractModel;

class ProductPriceLog extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magelan\EUPricePeriodLog\Model\ResourceModel\ProductPriceLog');
    }
}