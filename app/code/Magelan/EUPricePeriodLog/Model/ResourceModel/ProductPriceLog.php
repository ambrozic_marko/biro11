<?php
namespace Magelan\EUPricePeriodLog\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ProductPriceLog extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magelan_productpricelog', 'id');
    }
}