<?php
namespace Magelan\EUPricePeriodLog\Model\ResourceModel\ProductPriceLog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magelan\EUPricePeriodLog\Model\ProductPriceLog',
            'Magelan\EUPricePeriodLog\Model\ResourceModel\ProductPriceLog'
        );
    }
}