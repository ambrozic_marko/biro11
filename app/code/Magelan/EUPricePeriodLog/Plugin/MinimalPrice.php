<?php
namespace Magelan\EUPricePeriodLog\Plugin;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use \Magelan\EUPricePeriodLog\Helper\Data;

class MinimalPrice
{
    
    protected $_helper;
    protected $_priceCurrency;

    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        Data $helper
    ) {
        $this->_helper = $helper;
        $this->_priceCurrency = $priceCurrency;
    }

    public function afterGetProductPrice(
        \Magento\Catalog\Block\Product\ListProduct $subject,
        $result,
        \Magento\Catalog\Model\Product $_product
    ) {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $currentStoreId = $storeManager->getStore()->getStoreId();
        $productId = $_product->getId();

        $collection = $this->_helper->get30DaysPriceMovementsFor($productId, $currentStoreId);
        $minPrice = null;
        foreach ($collection as $log) {
            if($minPrice==null || $minPrice > $log->getPrice())
                $minPrice = $log->getPrice();
        }
        return $result . "<div class=\"minimal_price_in_30\">" . __('Lowest price in 30 days') . " " . $this->_priceCurrency->convertAndFormat($minPrice) . "</div>";
    }
}