<?php
namespace Magelan\EUPricePeriodLog\Cron;

use \Magento\Backend\Block\Template\Context;
use \Magelan\EUPricePeriodLog\Helper\Data;

class Process extends \Magento\Framework\View\Element\Template {

    protected $_helper;

    public function __construct(
            Context $context,
            Data $helper
            ) {
        
        $this->_helper = $helper;
        parent::__construct($context);
    }

	public function execute()
	{
        $this->_helper->createProductPriceLog();
    }
    
}