<?php
namespace Magelan\EUPricePeriodLog\Block;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use \Magelan\EUPricePeriodLog\Helper\Data;

class DisplayPrices extends \Magento\Framework\View\Element\Template
{

    protected $_helper;
    protected $_priceCurrency;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context, 
        PriceCurrencyInterface $priceCurrency,
        Data $helper
    ) {
        parent::__construct($context);
        $this->_helper = $helper;
        $this->_priceCurrency = $priceCurrency;
    }

    public function getProductPriceLog($productId, $storeId) {
        $collection = $this->_helper->get30DaysPriceMovementsFor($productId, $storeId);
        return $collection;
    }

    public function getPriceValues($collection) {
        $values = [];
        foreach ($collection as $log) {
            $values[] = $log->getPrice();
        }
        return $values;
    }

    public function getTimeValues($collection) {
        $dates = [];
        foreach ($collection as $log) {
            $dates[] = date('d', strtotime($log->getCreatedAt()));
        }
        return $dates;
    }

    public function getMinPriceValue($collection) {
        $minPrice = null;
        foreach ($collection as $log) {
            if($minPrice==null || $minPrice > $log->getPrice())
                $minPrice = $log->getPrice();
        }
        return $minPrice;
    }

    public function getFormatedPrice($amount)
    {
        return $this->_priceCurrency->convertAndFormat($amount);
    }

}