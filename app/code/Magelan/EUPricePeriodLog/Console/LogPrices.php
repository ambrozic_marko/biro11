<?php
namespace Magelan\EUPricePeriodLog\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LogPrices extends Command
{

    protected $_helper;

    public function __construct(
        \Magelan\EUPricePeriodLog\Helper\Data $helper
    )
    {
        parent::__construct();
        $this->_helper = $helper;
    }

    protected function configure()
    {
       $this->setName('magelan:logger:log-prices');
       $this->setDescription('Log prices for 30 days history by EU directive');
       parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $output->writeln("Loggin prices.");
       $this->_helper->createProductPriceLog();
       $output->writeln("Loggin finished.");
    }

}