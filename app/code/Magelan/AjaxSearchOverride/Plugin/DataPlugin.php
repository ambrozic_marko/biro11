<?php
declare(strict_types=1);

namespace Magelan\AjaxSearchOverride\Plugin;

class DataPlugin
{
    public function afterIsTaxIncludingToPrice(\Swissup\Ajaxsearch\Helper\Data $data = null, $result)
    {
        return true;
    }
}