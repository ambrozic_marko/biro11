<?php
namespace Magelan\Gls\Helper;

use Magento\Framework\App\ObjectManager;
use Magento\Backend\Model\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Allowed hash keys
     *
     * @var array
     */
    protected $_allowedHashKeys = ['ship_id', 'order_id', 'track_id'];

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var UrlInterface|null
     */
    private $url;
    protected $assetRepo;
    protected $request;


    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface|null $url
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        StoreManagerInterface $storeManager,
        UrlInterface $url = null,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_storeManager = $storeManager;
        $this->url = $url ?: ObjectManager::getInstance()->get(UrlInterface::class);
        $this->assetRepo = $assetRepo;
        $this->fileFactory = $fileFactory;
        $this->request = $request;
        parent::__construct($context);
    }
    
    public function getViewFileUrl(){
        $params = array('_secure' => $this->request->isSecure());
        $url = $this->assetRepo->getUrlWithParams('Magelan_Gls::images/PS-logo-crn.png', $params);
        
        return $url;
    }
    public function getConfigValue($configField)
    {
    	return $this->scopeConfig->getValue($configField,
    			\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    
    }
    /**
     * Decode url hash
     *
     * @param  string $hash
     * @return array
     */
    public function decodeTrackingHash($hash)
    {
        $hash = explode(':', $this->urlDecoder->decode($hash));
        if (count($hash) == 3 && in_array($hash[0], $this->_allowedHashKeys)) {
            return ['key' => $hash[0], 'id' => (int)$hash[1], 'hash' => $hash[2]];
        }
        return [];
    }

    /**
     * Retrieve tracking url with params
     *
     * @param  string $key
     * @param  \Magento\Sales\Model\Order
     * |\Magento\Sales\Model\Order\Shipment|\Magento\Sales\Model\Order\Shipment\Track $model
     * @param  string $method Optional - method of a model to get id
     * @return string
     */
    protected function _getTrackingUrl($key, $model, $method = 'getId')
    {
        $urlPart = "{$key}:{$model->{$method}()}:{$model->getProtectCode()}";
        $params = [
            '_scope' => $model->getStoreId(),
            '_nosid' => true,
            '_direct' => 'gls/tracking/popup',
            '_query' => ['isAjax' =>'true', 'track' => $model->getTrackNumber(), 'shipment_id' => $model->getParentId(), 'key' => $this->urlEncoder->encode($urlPart)]
        ];

        return $this->url->getUrl('', $params);
    }

    /**
     * Shipping tracking popup URL getter
     *
     * @param \Magento\Sales\Model\AbstractModel $model
     * @return string
     */
    public function getTrackingPopupUrlBySalesModel($model)
    {
    	 
       if ($model instanceof \Magento\Sales\Model\Order) {
            return $this->_getTrackingUrl('order_id', $model);
        } elseif ($model instanceof \Magento\Sales\Model\Order\Shipment) {
            return $this->_getTrackingUrl('ship_id', $model);
        } elseif ($model instanceof \Magento\Sales\Model\Order\Shipment\Track) {
        	return $this->_getTrackingUrl('track_id', $model, 'getEntityId');
        }
        return '';
    }
    
    
    
    
    /**
     * Retrieve tracking url with params
     *
     * @param  string $key
     * @param  \Magento\Sales\Model\Order
     * |\Magento\Sales\Model\Order\Shipment|\Magento\Sales\Model\Order\Shipment\Track $model
     * @param  string $method Optional - method of a model to get id
     * @return string
     */
    protected function _getTrackingUrl1($key, $model, $method = 'getId', $postNum = null)
    {
    	$urlPart = "{$key}:{$model->{$method}()}:{$model->getProtectCode()}";
    	$params = [
    			'_scope' => $model->getStoreId(),
    			'_nosid' => true,
    			'_direct' => 'gls/tracking/popup1',
    			'_query' => ['isAjax' =>'true', 'track' => $model->getTrackNumber().'0'.$postNum,/*'postNum' => $postNum,*/ 'key' => $this->urlEncoder->encode($urlPart)]
    	];
    
    	return $this->url->getUrl('', $params);
    	}
    
    	/**
    	 * Shipping tracking popup URL getter
    	 *
    	 * @param \Magento\Sales\Model\AbstractModel $model
    	 * @return string
    	 */
    	public function getTrackingPopupUrlBySalesModel1($model, $postNum)
    	{
            
    		if ($model instanceof \Magento\Sales\Model\Order) {
    			return $this->_getTrackingUrl1('order_id', $model, $postNum);
    		} elseif ($model instanceof \Magento\Sales\Model\Order\Shipment) {
    			return $this->_getTrackingUrl1('ship_id', $model, $postNum);
    		} elseif ($model instanceof \Magento\Sales\Model\Order\Shipment\Track) {
    			return $this->_getTrackingUrl1('track_id', $model, 'getEntityId', $postNum);
    		}
    		return '';
        }
        
    public function printPacketLabels($username,$password,$url,$method,$parcels,$isXmlFormat)
    {	
        $request = $this->getRequestString($username,$password,$parcels,$isXmlFormat,$method);
        $response = $this->getResponse($url,$method,$request,$isXmlFormat);

        if($response == true && count(json_decode($response)->PrintLabelsErrorList) == 0 && count(json_decode($response)->Labels) > 0)
        {		                
            return implode(array_map('chr', json_decode($response)->Labels));
            
        } 
        return false;
            
    }
    public function deleteLabels($username,$password,$url,$method,$parcels,$isXmlFormat)
    {	
        $request = $this->getRequestString($username,$password,$parcels,$isXmlFormat,$method);
        $response = $this->getResponse($url,$method,$request,$isXmlFormat);

        if($response == true && count(json_decode($response)->DeleteLabelsErrorList) == 0 && count(json_decode($response)->SuccessfullyDeletedList) > 0)
        {		                
            return true;
        } 
        return false;
            
    }
    public function massPrintLabels($username,$password,$url,$method,$parcels,$isXmlFormat)
    {	
        //Test request:
        $request = $this->getRequestString($username,$password,$parcels,$isXmlFormat,$method);

        $response = $this->getResponse($url,$method,$request,$isXmlFormat);

        if($isXmlFormat == false)
        {
            if($response == true && count(json_decode($response)->PrintLabelsErrorList) == 0 && count(json_decode($response)->Labels) > 0)
            {	
                return $response;
            }else{
                return $response;
            }
        }
        else
        {		
            if($response != "" && strpos($response,"ErrorCode") == false)
            {
                $labelsNodeName = "<Labels>";
                $labels = strpos($response, $labelsNodeName);
                if($labels != false)
                {
                    $firstPosition = ($labels + strlen($labelsNodeName));
                    $lastPosition = strpos($response,"</Labels>");
                                    
                    //Label(s) saving:
                    file_put_contents('php_rest_client_'.$method.'.pdf', base64_decode(substr($response, $firstPosition, ($lastPosition - $firstPosition))));
                }
            }
        }
    }
    public function printLabels($username,$password,$url,$method,$parcels,$isXmlFormat)
    {	
        //Test request:
        $request = $this->getRequestString($username,$password,$parcels,$isXmlFormat,$method);

        $response = $this->getResponse($url,$method,$request,$isXmlFormat);

        if($isXmlFormat == false)
        {
            if($response == true && count(json_decode($response)->PrintLabelsErrorList) == 0 && count(json_decode($response)->Labels) > 0)
            {		
                //Label(s) saving:
                $pdf = implode(array_map('chr', json_decode($response)->Labels));
                
                file_put_contents('pub/media/gls-'.json_decode($response)->PrintLabelsInfoList[0]->ParcelNumber.'.pdf', $pdf);
                
                /*$fileName = 'gls-'.json_decode($response)->PrintLabelsInfoList[0]->ParcelNumber.'.pdf';
                $fileFactory->create(
                        $fileName,
                        $pdf,
                        \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
                        'application/pdf'
                        );
                */
                return json_decode($response)->PrintLabelsInfoList[0];
            }
        }
        else
        {		
            if($response != "" && strpos($response,"ErrorCode") == false)
            {
                $labelsNodeName = "<Labels>";
                $labels = strpos($response, $labelsNodeName);
                if($labels != false)
                {
                    $firstPosition = ($labels + strlen($labelsNodeName));
                    $lastPosition = strpos($response,"</Labels>");
                                    
                    //Label(s) saving:
                    file_put_contents('php_rest_client_'.$method.'.pdf', base64_decode(substr($response, $firstPosition, ($lastPosition - $firstPosition))));
                }
            }
        }
    }
    /* UTIL */
    public function getRequestString($username,$password,$dataList,$isXmlFormat,$requestObjectName)
    {
        $result = "";
        
        if($isXmlFormat == false)
        {		
            switch ($requestObjectName) {
                case "PrintLabels":
                    $result = '{"Username":"'.$username.'","Password":'.$password.',"ParcelList":'.$dataList.',"PrintPosition":1,"ShowPrintDialog":0}';
                    break;
                case "PrepareLabels":
                    $result = '{"Username":"'.$username.'","Password":'.$password.',"ParcelList":'.$dataList.'}';
                    break;
                case "GetPrintedLabels":
                    $result = '{"Username":"'.$username.'","Password":'.$password.',"ParcelIdList":'.json_encode($dataList).',"PrintPosition":1,"ShowPrintDialog":0}';
                    break;
                case "DeleteLabels":
                    $result = '{"Username":"'.$username.'","Password":'.$password.',"ParcelIdList":'.$dataList.'}';;
                    break;
            } 
        }
        else
        {		
            $additionalParameters = "";
            if(in_array($requestObjectName, array("PrintLabels", "GetPrintedLabels")) == true)
            {
                $additionalParameters = "<PrintPosition>1</PrintPosition><ShowPrintDialog>false</ShowPrintDialog>";
            }

            $result =  '<'.$requestObjectName.'Request
            xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.LabelOperations"
            xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                <ClientNumberList
                xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.Common"
                xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
                <Password
                    xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.Common">'.$password.'</Password>
                <Username
                    xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.Common">'.$username.'</Username>
                '.$dataList.$additionalParameters.'
            </'.$requestObjectName.'Request>';
            
            $toBeReplaced = array("\r", "\n");
            $result = str_replace($toBeReplaced, "", $result);
        }
        
        return $result;
    }

    function getResponse($url,$method,$request,$isXmlFormat)
    {	
        //Service calling:
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_URL, $url.$method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 600);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        if($isXmlFormat == false)
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                            
                'Content-Length: ' . strlen($request))                                                                       
            );
        }
        else
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: text/xml',                                                                            
                'Content-Length: ' . strlen($request))                                                                       
            );
        }
        $response = curl_exec($curl);	
        /*print_r($request);
        print_r(json_decode($response));
        print_r(curl_getinfo($curl));
        die();*/
        if(curl_getinfo($curl)["http_code"] == "401")
        {
            die("Unauthorized");
        }
        if(curl_getinfo($curl)["http_code"] != "200")
        {
            die("Processing error.");
        }
        if ($response === false) 
        {
            die('curl_error:"' . curl_error($curl) . '";curl_errno:' . curl_errno($curl));
        }
        curl_close($curl);
        
        return $response;
    }

    function getParcelList($pickupDate, $username, $password, $url, $method, $isXmlFormat)
{
	//Test request:
	if($isXmlFormat == false)
	{
		$pickupDateFrom="\/Date(".(strtotime($pickupDate) * 1000).")\/";
		$pickupDateTo="\/Date(".(strtotime($pickupDate) * 1000).")\/";
		$request = '{"Username":"'.$username.'","Password":'.$password.',"PickupDateFrom":"'.$pickupDate.'","PickupDateTo":"'.$pickupDate.'","PrintDateFrom":null,"PrintDateTo":null}';
	}
	else
	{
		$request = '<GetParcelListRequest
					xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.ParcelOperations"
					xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
						<ClientNumberList
						xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.Common"
						xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
						<Password
							xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.Common">'.$password.'</Password>
						<Username
							xmlns="http://schemas.datacontract.org/2004/07/GLS.MyGLS.ServiceData.APIDTOs.Common">'.$username.'</Username>
						<PickupDateFrom>2020-04-16T00:00:00</PickupDateFrom>
						<PickupDateTo>2020-04-16T00:00:00</PickupDateTo>
						<PrintDateFrom i:nil="true"/>
						<PrintDateTo i:nil="true"/>
					</GetParcelListRequest>';
	}

	$response = $this->getResponse($url,$method,$request,$isXmlFormat);

	if($isXmlFormat == false)
	{
		var_dump(count(json_decode($response)->GetParcelListErrors));
		var_dump(count(json_decode($response)->PrintDataInfoList));
	}
	else
	{
		if($response != "")
		{
			die("GetParcelList: OK");
			
			//die($response);
			
			/*
			//https://www.php.net/manual/en/function.xml-parse-into-struct.php
			$p = xml_parser_create();
			xml_parse_into_struct($p, $response, $vals, $index);
			xml_parser_free($p);
			
			foreach ($vals as $val) 
			{
				var_dump($val);
			}
			*/
		}
	}
}
}

  
