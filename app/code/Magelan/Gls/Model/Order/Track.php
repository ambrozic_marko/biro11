<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magelan\Gls\Model\Order;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\App\Bootstrap;

/**
 * @method int getParentId()
 * @method float getWeight()
 * @method float getQty()
 * @method int getOrderId()
 * @method string getDescription()
 * @method string getTitle()
 * @method string getCarrierCode()
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method \Magento\Sales\Api\Data\ShipmentTrackExtensionInterface getExtensionAttributes()
 */
class Track extends \Magento\Shipping\Model\Order\Track
{
  
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
     * @param \Magento\Shipping\Model\CarrierFactory $carrierFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
	protected $scopeConfig;
	protected $helper;
	
	public function __construct(
			\Magento\Framework\Model\Context $context,
			\Magento\Framework\Registry $registry,
			\Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
			AttributeValueFactory $customAttributeFactory,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository,
			\Magento\Shipping\Model\CarrierFactory $carrierFactory,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magelan\Gls\Helper\Data $helper
				) {
				parent::__construct(
						$context,
						$registry,
						$extensionFactory,
						$customAttributeFactory,
						$storeManager,
						$shipmentRepository,
						$carrierFactory
						);
				$this->scopeConfig = $scopeConfig;
				$this->helper = $helper;
	}
    /**
     * Retrieve detail for shipment track
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getNumberDetail()
    {
    	
    //	echo $this->getCarrierCode();
        $carrierInstance = $this->_carrierFactory->create("GLS");
       
        $url = $this->scopeConfig->getValue('carriers/gls/carrier_print_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
       	$username = $this->scopeConfig->getValue('carriers/gls/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
       	$password = $this->scopeConfig->getValue('carriers/gls/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
       	
       	
       	header('Content-disposition: attachment; filename='.$this->getTrackNumber().".pdf");
       	header('Content-type: application/octet-stream');
       	//header('Content-Length: '.filesize($file));
       	header("Pragma: no-cache");
       	header("Expires: 0");
       	echo $this->helper->wsClientPostRaw($url, [
       			'username'=>$username,
       			'password'=>$password,
       			'parcels'=> $this->getTrackNumber() . "|"
       	]);
       	die();
        

       	if (!$carrierInstance) {
       		//	print_r($this->getTrackNumber());
       		$custom = [];
       		$custom['title'] = $this->getTitle();
       		$custom['number'] = $this->getTrackNumber();
       		return $custom;
       	} else {
       		$carrierInstance->setStore($this->getStore());
       	}
       	
        

        $trackingInfo = $carrierInstance->getTrackingInfo($this->getNumber());
        if (!$trackingInfo) {
            return __('No detail for number "%1"', $this->getNumber());
        }

        return $trackingInfo;
    }
}
