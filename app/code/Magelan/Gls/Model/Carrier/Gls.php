<?php

namespace Magelan\Gls\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Config;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Psr\Log\LoggerInterface;

/**
 * @category   Magelan
 * @package    Magelan_gls
 */
class Gls extends AbstractCarrier implements CarrierInterface
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'gls';
    
    /**
     * Whether this carrier has fixed rates calculation
     *
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @var \Magento\Shipping\Model\Tracking\ResultFactory
     */
    protected $_trackFactory;
    /**
     * @var \Magento\Shipping\Model\Tracking\Result\StatusFactory
     */
    protected $_trackStatusFactory;
    //protected $cart;
    
    protected $scopeConfig;
    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
		\Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        //\Magento\Checkout\Model\Cart $cart,
    	array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_trackFactory = $trackFactory;
        $this->_trackStatusFactory = $trackStatusFactory;
        $this->scopeConfig = $scopeConfig;
        //$this->cart = $cart;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }


    /**
     * Generates list of allowed carrier`s shipping methods
     * Displays on cart price rules page
     *
     * @return array
     * @api
     */
    public function getAllowedMethods()
    {
        return [$this->getCarrierCode() => __($this->getConfigData('name'))];
    }
    
    public function isTrackingAvailable() {
    	return true;
    }

    /**
     * Collect and get rates for storefront
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param RateRequest $request
     * @return DataObject|bool|null
     * @api
     */
    public function collectRates(RateRequest $request)
    {
        /**
         * Make sure that Shipping method is enabled
         */
        if (!$this->isActive()) {
            return false;
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        $shippingPrice = $this->getConfigData('price');

        $method = $this->_rateMethodFactory->create();

        /**
         * Set carrier's method data
         */
        $method->setCarrier($this->getCarrierCode());
        $method->setCarrierTitle($this->getConfigData('title'));

        /**
         * Displayed as shipping method under Carrier
         */
        $method->setMethod($this->getCarrierCode());
        $method->setMethodTitle($this->getConfigData('name'));

        $i = 0;
        //$items = $this->cart->getQuote()->getItemsCollection();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 

        // retrieve quote items array
        $items = $cart->getQuote()->getAllItems();
        $itemsVisible = $cart->getQuote()->getAllVisibleItems();
        foreach($itemsVisible as $item) {
            $product = $item->getProduct();
            $productId = $product->getId();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
            $attr = $product->getCustomAttribute("nizja_postnina");
            if($attr != NULL){
                $postnina = $attr->getValue();
                if($postnina == 1){
                    $i = 1;
                }    
            }     
        }
        $cartAmount = $request->getBaseSubtotalInclTax();
        
		$maxAmount = $this->scopeConfig->getValue('carriers/gls/orderAmount', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        if($maxAmount > $cartAmount){
			if($i == 1){
                $method->setPrice($this->getConfigData('lowerPrice'));
                $method->setCost($this->getConfigData('lowerPrice'));
            }else{
                $method->setPrice($shippingPrice);
                $method->setCost($shippingPrice);
            }	
    	}else{
			$method->setPrice('0.00');
            $method->setCost('0.00');
        }
        
        $result->append($method);

        return $result;
    }

    
    public function getTrackingInfo($tracking, $postcode = null)
    {
    	$result = $this->getTracking($tracking, $postcode);
    
    	if ($result instanceof \Magento\Shipping\Model\Tracking\Result) {
    		if ($trackings = $result->getAllTrackings()) {
    			return $trackings[0];
    		}
    	} elseif (is_string($result) && !empty($result)) {
    		return $result;
    	}
    
    	return false;
    }
    
    public function getTracking($trackings, $postcode = null)
    {
    	$this->setTrackingReqeust();
    
    	if (!is_array($trackings)) {
    		$trackings = array($trackings);
    	}
    	$this->_getCgiTracking($trackings, $postcode);
    
    	return $this->_result;
    }
    
    protected function setTrackingReqeust()
    {
    	$r = new \Magento\Framework\DataObject();
    
    	$userId = $this->getConfigData('userid');
    	$r->setUserId($userId);
    
    	$this->_rawTrackRequest = $r;
    }
    
    /** Popup window to tracker **/
    protected function _getCgiTracking($trackings, $postcode = null)
    {
		
    	$this->_result = $this->_trackFactory->create();
    
    	$defaults = $this->getDefaults();
    	foreach ($trackings as $tracking) {
    		$status = $this->_trackStatusFactory->create();    
    		$status->setCarrier('Tracker');
    		$status->setCarrierTitle($this->getConfigData('title'));
    		$status->setTracking($tracking);
    		$status->setPopup(1);
    		$manualUrl = $this->getConfigData('url');
    		$taggedUrl = 'https://gls-group.eu/SI/sl/sledenje-posiljki/';//#TRACKNUM#';
    		//$fullUrl = str_replace("#TRACKNUM#", $tracking, $taggedUrl);
    		$status->setUrl($taggedUrl);
    		$this->_result->append($status);
    	}
    }

}