<?php


namespace Magelan\Gls\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Services implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => "XS", 'label' => __("XS")],
            ['value' => "TGS", 'label' => __("TGS")],
            ['value' => "T12", 'label' => __("T12")],
            ['value' => "T10", 'label' => __("T10")],
            ['value' => "T09", 'label' => __("T09")],
            ['value' => "SZL", 'label' => __("SZL")],
            ['value' => "SRS", 'label' => __("SRS")],
            ['value' => "SM2", 'label' => __("SM2")],
            ['value' => "SM1", 'label' => __("SM1")],
            //['value' => "SDS", 'label' => __("SDS")],
            //['value' => "ADR", 'label' => __("ADR")],
            ['value' => "SBS", 'label' => __("SBS")],
            ['value' => "PSS", 'label' => __("PSS")],
            ['value' => "PRS", 'label' => __("PRS")],
            ['value' => "MCC", 'label' => __("MCC")],
            ['value' => "INS", 'label' => __("INS")],
            ['value' => "FDS", 'label' => __("FDS")],
            ['value' => "FSS", 'label' => __("FSS")],
            //['value' => "DPV", 'label' => __("DPV")],
            ['value' => "DDS", 'label' => __("DDS")],
            ['value' => "CS1", 'label' => __("CS1")],
            ['value' => "AOS", 'label' => __("AOS")],
            ['value' => "24H", 'label' => __("24H")]
        ];
    }
}