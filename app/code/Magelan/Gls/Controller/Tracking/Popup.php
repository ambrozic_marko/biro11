<?php
/**
 *
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Magelan\Gls\Controller\Tracking;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
//use \Zend\Barcode\Barcode;

require_once(BP.'/vendor/autoload.php');

class Popup extends \Magento\Framework\App\Action\Action
{
	protected $fileFactory;
	protected $_coreRegistry;
	protected $_shippingInfoFactory;
	protected $_orderFactory;
	protected $helper;
	protected $scopeConfig;
	

	/**
	 * @param \Magento\Framework\App\Action\Context $context
	 * @param \Magento\Framework\Registry $coreRegistry
	 * @param \Magento\Shipping\Model\InfoFactory $shippingInfoFactory
	 * @param \Magento\Sales\Model\OrderFactory $orderFactory
	 */
	public function __construct(
			\Magento\Framework\App\Action\Context $context,
			\Magelan\Gls\Helper\Data $helper,
			\Magento\Framework\Registry $coreRegistry,
			\Magento\Shipping\Model\InfoFactory $shippingInfoFactory,
			\Magento\Sales\Model\OrderFactory $orderFactory,
			FileFactory $fileFactory,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
			) {
				$this->helper = $helper;
				$this->_coreRegistry = $coreRegistry;
				$this->_shippingInfoFactory = $shippingInfoFactory;
				$this->_orderFactory = $orderFactory;
				$this->fileFactory = $fileFactory;
				$this->scopeConfig = $scopeConfig;
				parent::__construct($context);
	}

	
	public function execute()
	{
		$trackId = $this->getRequest()->getParam('track');
		//$shipment = $track->getShipment(true);
		$shipmentId = $this->getRequest()->getParam('shipment_id');
		$bulk = $this->getRequest()->getParam('bulk');
		
		$username = $this->scopeConfig->getValue('carriers/gls/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$pwd = $this->scopeConfig->getValue('carriers/gls/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$password = "[".implode(',',unpack('C*', hash('sha512', $pwd, true)))."]";
		$url = $this->scopeConfig->getValue('carriers/gls/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$url .= "json/";
		
		if($trackId){
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
			$rootPath  =  $directory->getRoot();
			$filename = 'gls-'.$trackId;
			$destination = $rootPath . '/pub/media/'.$filename.'.pdf';
			if(file_exists($destination)){			
				header('Content-Transfer-Encoding: binary');
				header('Accept-Ranges: bytes');  
				header('Content-Encoding: none');
				header('Content-Type: application/pdf');  
				//header('Content-Disposition: attachment; filename='.$filename.'.pdf'); 
				header("Content-Disposition: inline; filename=" . $filename . ".pdf");  
				readfile($destination); 
			}else{
				$response = [
					'message' => __('You have generated this label en masse or label does not exist')
				];
				//if (is_array($response)) {
				//	$response = $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($response);
				//	$this->getResponse()->representJson($response);
				//} else {
					$this->getResponse()->setBody('<p style="color:red;"><strong><font size="5">'.$response["message"].'</font></strong></p>');
				//}
			}
		}
	}

}