<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magelan\Gls\Controller\Adminhtml\Track;

ini_set('memory_limit', '4096M');

use Magelan\Gls\Helper\Data;
use Magento\Backend\App\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AddTrack extends \Magento\Shipping\Controller\Adminhtml\Order\Shipment\AddTrack
{

	protected $helper;
	protected $request;
	protected $scopeConfig;

	public function __construct(
			Action\Context $context,
			\Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader,
			\Magento\Framework\App\Request\Http $request,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			Data $helper
			) {
				//$this->shipmentLoader = $shipmentLoader;
				$this->helper = $helper;
				$this->request = $request;
				$this->scopeConfig = $scopeConfig;
				parent::__construct($context, $shipmentLoader);
	}
	

    /**
     * Add new tracking number action
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
    	try {
    		$url = $this->scopeConfig->getValue('carriers/gls/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$username = $this->scopeConfig->getValue('carriers/gls/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$pwd = $this->scopeConfig->getValue('carriers/gls/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$clientNumber = $this->scopeConfig->getValue('carriers/gls/clientNumber', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$fromName = $this->scopeConfig->getValue('carriers/gls/fromName', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$fromAddress = $this->scopeConfig->getValue('carriers/gls/address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$fromAddressNumber = $this->scopeConfig->getValue('carriers/gls/addressNumber', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$fromAddressInfo = $this->scopeConfig->getValue('carriers/gls/addressInfo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$fromCity = $this->scopeConfig->getValue('carriers/gls/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$fromPcode = $this->scopeConfig->getValue('carriers/gls/pCode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$fromPhone = $this->scopeConfig->getValue('carriers/gls/phone', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$fromEmail = $this->scopeConfig->getValue('carriers/gls/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$pickdate = $this->scopeConfig->getValue('carriers/gls/date_from', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    		$services = $this->scopeConfig->getValue('carriers/gls/services', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$cashOnDeliveryName = $this->scopeConfig->getValue('carriers/gls/cashOnDeliveryName', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$pieces = explode(",", $services);

			$bulk = 1;
			
			$carrier = 'gls';

			$shipmentId = $this->request->getParam('shipment_id');
			$shipment = $this->_objectManager->create('\Magento\Sales\Model\Order\Shipment')->load($shipmentId);
		
			$orderId = $shipment->getOrder()->getIncrementId();
			$order = $this->_objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
			
			//$amount = $order->getGrandTotal();
				
			$weight = $this->getRequest()->getPost('weight');
			
			if (empty($weight)) {
				$orderItems = $order->getAllVisibleItems();
				$weight = 0;
				foreach($orderItems as $item) {
					$weight += ($item->getWeight() * $item->getQtyOrdered());
				}    			 
			}
			
			$note = $this->getRequest()->getPost('note');
			if (empty($note)) {
				$note = 'Pazljivo!';			 			 
			}
			$payment = $order->getPayment();
			$method = $payment->getMethodInstance();
			$methodTitle = $method->getTitle();
			
			$ex = explode(' ',$order->getShippingAddress()->getData()['street']);
			$last = end($ex);
			$streetName = str_replace($last,'',$order->getShippingAddress()->getData()['street']);

			
			$streetNumber = preg_replace('/[^0-9]/', '', $last);
			$streetApartment = preg_replace("/[^A-Za-z]+/", "", $last);

			if($streetNumber == null){
				$lastN = str_replace($last, "",$order->getShippingAddress()->getData()['street']);
				$ex = explode(' ',$lastN);
				$lastL = end($ex);
				
				$streetName = str_replace($lastL, "",$streetName);
				$streetNumber = preg_replace('/[^0-9]/', '', $lastN);
				$streetName = str_replace($streetNumber, "",$streetName);
			}
			
			if ( preg_match('/([^\d]+)\s?(.+)/i', $fromAddress, $result) )
			{
				$fromStreetName = $result[1];
				$fromStreetNumber = $result[2];
			}
			$password = "[".implode(',',unpack('C*', hash('sha512', $pwd, true)))."]";
			
			$pickupDate = "\/Date(".(strtotime(date("Y-m-d"). " " .$pickdate) * 1000).")\/";
			
			//$pickupDate = "\/Date(".(strtotime("2020-03-20 00:00:00") * 1000).")\/";
		
			$stop_date = date('Y-m-d', strtotime(date("Y-m-d") . ' +1 day'));

			$dayOfDelivery = "\/Date(".(strtotime($stop_date. " " .$pickdate) * 1000).")\/";
			
			$phone = $order->getShippingAddress()->getData()['telephone'];
			if($phone[0] == '0' || $phone[1] == '0' || $phone[0] == '+'){
				if($phone[0] != '+' && $phone[1] != '0' ){
					$phone = substr($phone, 1, strlen($phone)-1);
				}else if($phone[0] == '0' && $phone[1] == '0'){
					$phone = substr($phone, 5, strlen($phone)-1);
				}
			}
			if($fromPhone[0] == '0' || $fromPhone[1] == '0' || $fromPhone[0] == '+'){
				if($fromPhone[0] != '+' && $fromPhone[1] != '0' ){
					$fromPhone = substr($fromPhone, 1, strlen($fromPhone)-1);
				}else if($fromPhone[0] == '0' && $fromPhone[1] == '0'){
					$fromPhone = substr($fromPhone, 5, strlen($fromPhone)-1);
				}
			}
			$amount = 0;
			if($methodTitle == $cashOnDeliveryName){
				$amount = round($order->getGrandTotal(), 2);
			}else{
				$amount = 0;
			}
			//'.$phone.'     '.$fromPhone.'
			$additionalServices = '';
			foreach($pieces as $service){
				if($service == 'CS1' || $service == 'FSS' || $service == 'SM2' || $service == 'SM2'){
					$additionalServices .= '{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"Value": "'.$phone.'"
						}
					}';
				}else if($service == 'AOS'){
					$additionalServices .='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"Value": "'.$streetName.$streetNumber.'"
						}
					},';
				}else if($service == 'DDS'){
					$additionalServices .='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"Value": "'.$dayOfDelivery.'"
						}
					},';
				}else /*if($service == 'DPV'){
					$params['ServiceList'][] ='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"StringValue": "'.$phone.'",
							"DecimalValue": '.$phone.'
						}
					}';
				}else */if($service == 'FDS'){
					$additionalServices .= '{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"Value": "'.$order->getShippingAddress()->getData()['email'].'"
						}
					},';
				}else if($service == 'INS'){
					$additionalServices .='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"Value": '.$amount.'
						}
					},';
				}else /*if($service == 'SDS'){
					$params['ServiceList'][] ='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"TimeFrom": "'.$pickupDate.'",
							"TimeTo": "'.$pickupDate.'"
						}
					}';
				}else */if($service == 'SM1'){
					$additionalServices .='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"Value": "'.$phone.'|#ParcelNr#-'.$note.'"
						}
					},';
				}else if($service == 'SZL'){
					$additionalServices .='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"Value": "'.$orderId.'"
						}
					},';
				}else /*if($service == 'ADR'){
					$params['ServiceList'][] ='{
						"Code": "'.$service.'",
						"'.$service.'Parameter": {
							"AdrItemType": '.$service.', 
							"AmountUnit": '.$service.', 
							"InnerCount": '.$service.', 
							"PackSize": '.$bulk.', 
							"UnNumber": '.$service.'
						}
					}';
				}else*/{
					$additionalServices .='{
						"Code": "'.$service.'"
					},';
				}
			}



			$params = '[{
					"ClientNumber": "'.$clientNumber.'",
					"ClientReference": "'.$orderId.'",
					"CODAmount": '.$amount.',
					"CODReference": "COD REFERENCE",
					"Content": "CONTENT",
					"Count": '.$bulk.',
					"DeliveryAddress": {
						"City": "'.$order->getShippingAddress()->getData()['city'].'",
						"ContactEmail": "'.$order->getShippingAddress()->getData()['email'].'",
						"ContactName": "'.$order->getShippingAddress()->getData()['firstname'].' '.$order->getShippingAddress()->getData()['lastname'].'",
						"ContactPhone": "'.$phone.'",
						"CountryIsoCode": "'.$order->getShippingAddress()->getData()['country_id'].'",
						"HouseNumber": "'.$streetNumber.'",
						"Name": "'.$order->getShippingAddress()->getData()['firstname'].' '.$order->getShippingAddress()->getData()['lastname'].'",
						"Street": "'.rtrim($streetName).'",
						"ZipCode": "'.$order->getShippingAddress()->getData()['postcode'].'",
						"HouseNumberInfo": "'.$streetApartment.'"
					},
					"PickupAddress": {
						"City": "'.$fromCity.'",
						"ContactEmail": "'.$fromEmail.'",
						"ContactName": "'.$fromName.'",
						"ContactPhone": "'.$fromPhone.'",
						"CountryIsoCode": "SI",
						"HouseNumber": "'.$fromAddressNumber.'",
						"Name":  "'.$fromName.'",
						"Street": "'.$fromAddress.'",
						"ZipCode": "'.$fromPcode.'",
						"HouseNumberInfo": "'.$fromAddressInfo.'"
					},
					"PickupDate": "'.$pickupDate.'",
					"ServiceList": ['.$additionalServices.']
				}]';
			
			$serviceName = "ParcelService";
			$isXmlFormat = false;
			$url .= "json/";
			
			$trackNumber = $this->helper->printLabels($username,$password,str_replace("SERVICE_NAME",$serviceName,$url),"PrintLabels",$params,$isXmlFormat);
			
			$this->shipmentLoader->setOrderId($orderId);
    		$this->shipmentLoader->setShipmentId($shipmentId);
    		$this->shipmentLoader->setShipment($this->getRequest()->getParam('shipment'));
    		$this->shipmentLoader->setTracking($trackNumber->ParcelNumber);
    		$shipment = $this->shipmentLoader->load();
    		
    		if ($shipment) {
                $track = $this->_objectManager->create(\Magento\Sales\Model\Order\Shipment\Track::class)
                	->setNumber($trackNumber->ParcelNumber)
                	->setCarrierCode($carrier)
					->setTitle($bulk)
					->setWeight($weight)					
					->setDescription($trackNumber->ParcelId);
                
                $shipment->addTrack($track)->save();

                $response = $this->_view->getLayout()
				->createBlock('Magelan\Gls\Block\Order\Tracking\View')
				->setTemplate('Magelan_Gls::order/tracking/view.phtml')
				->toHtml();
				// Send email
				$this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
				->notify($shipment);
            } else {
                $response = [
                    'error' => true,
                    'message' => __('We can\'t initialize shipment for adding tracking number.'),
                ];
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $response = ['error' => true, 'message' => $e->getMessage()];
        } catch (\Exception $e) {
            $response = ['error' => true, 'message' => __('Cannot add tracking number.')];
        }
        if (is_array($response)) {
            $response = $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($response);
            $this->getResponse()->representJson($response);
        } else {
            $this->getResponse()->setBody($response);
		}
    }
}
