<?php
/**
 *
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/
namespace Magelan\Gls\Controller\Adminhtml\Track;

use Magento\Backend\App\Action;
use Magelan\Gls\Helper\Data;

class RemoveTrack extends \Magento\Backend\App\Action
{

	const ADMIN_RESOURCE = 'Magento_Sales::shipment';
	protected $shipmentLoader;
	protected $scopeConfig;
	protected $helper;

	public function __construct(
			Action\Context $context,
			\Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			Data $helper
			) {
				$this->helper = $helper;
				$this->shipmentLoader = $shipmentLoader;
				$this->scopeConfig = $scopeConfig;
				parent::__construct($context);
	}
	/**
	 * Remove tracking number from shipment
	 *
	 * @return void
	 */
	public function execute()
	{
		 
		$trackNum = $this->getRequest()->getParam('track_number');
		$username = $this->scopeConfig->getValue('carriers/gls/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$pwd = $this->scopeConfig->getValue('carriers/gls/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$password = "[".implode(',',unpack('C*', hash('sha512', $pwd, true)))."]";
		$bulk = $this->getRequest()->getParam('bulk');
		
		for($i = 0; $i < $bulk + 1; $i++){
			$newArray[$i] = $trackNum + $i;
		}

		$url = $this->scopeConfig->getValue('carriers/gls/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    
		if($trackNum){
                
			$params = '['.$trackNum.']';
		
		$serviceName = "DeleteLabels";
		$isXmlFormat = false;
		$url .= "json/";
		$deleteResponse = $this->helper->deleteLabels($username,$password,str_replace("SERVICE_NAME","ParcelService",$url),$serviceName,$params,$isXmlFormat);
		
		//https://api.mygls.si/DeleteLabels.svc/json/DeleteLabels
                
		if($deleteResponse == false){
			$response = [
				'error' => true,
				'message' => __('Parcel can not be deleted!')
			];
			if (is_array($response)) {
				$response = $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($response);
				$this->getResponse()->representJson($response);
			} else {
				$this->getResponse()->setBody($response);
			}
        } else{
            

			 $trackId = $this->getRequest()->getParam('track_id');
			 /** @var \Magento\Sales\Model\Order\Shipment\Track $track */
			 $track = $this->_objectManager->create(\Magento\Sales\Model\Order\Shipment\Track::class)->load($trackId);
			  
			 if ($track->getId()) {
			 	try {
			 		$this->shipmentLoader->setOrderId($this->getRequest()->getParam('order_id'));
			 		$this->shipmentLoader->setShipmentId($this->getRequest()->getParam('shipment_id'));
			 		$this->shipmentLoader->setShipment($this->getRequest()->getParam('shipment'));
			 		$this->shipmentLoader->setTracking($this->getRequest()->getParam('tracking'));
			 		$shipment = $this->shipmentLoader->load();
			 		if ($shipment) {
			 			$track->delete();

			 			$this->_view->loadLayout();
			 			$this->_view->getPage()->getConfig()->getTitle()->prepend(__('Shipments'));
			 			$response = $this->_view->getLayout()
			 			->createBlock('Magelan\Gls\Block\Order\Tracking\View')
			 			->setTemplate('Magelan_Gls::order/tracking/view.phtml')
			 			->toHtml();
			 				
			 		} else {
			 			$response = [
			 					'error' => true,
			 					'message' => __('We can\'t initialize shipment for delete tracking number.'),
			 			];
			 		}
			 	} catch (\Exception $e) {
			 		$response = ['error' => true, 'message' => __('We can\'t delete tracking number.')];
			 	}
			 } else {
			 	$response = [
			 			'error' => true,
			 			'message' => __('We can\'t load track with retrieving identifier right now.')
			 	];
			 }

			 if (is_array($response)) {
			 	$response = $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($response);
			 	$this->getResponse()->representJson($response);
			 } else {
			 	$this->getResponse()->setBody($response);
			 }
			  
			}
		}
	}
}
