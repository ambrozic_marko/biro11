<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magelan\Gls\Controller\Adminhtml\Order;

ini_set('memory_limit', '4096M');

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\App\Config\ScopeConfigInterface;

use Magelan\Gls\Helper\Data;

class GenerateGlsLabels extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
	protected $collectionFactory;
	protected $fileFactory;
	protected $date;
	protected $scopeConfig;
	protected $_configInterface;
	/**
	 * Pdforders constructor.
	 *
	 * @param \Magento\Backend\App\Action\Context                        $context
	 * @param \Magento\Ui\Component\MassAction\Filter                    $filter
	 * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
	 * @param \Magento\Framework\App\Response\Http\FileFactory           $fileFactory
	 * @param \Magento\Framework\Stdlib\DateTime\DateTime                $date
	 */
	public function __construct(
			\Magento\Backend\App\Action\Context $context,
			\Magento\Ui\Component\MassAction\Filter $filter,
			\Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
			\Magento\Framework\App\Response\Http\FileFactory $fileFactory,
			//\Fooman\PrintOrderPdf\Model\Pdf\OrderFactory $orderPdfFactory,
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Framework\Stdlib\DateTime\DateTime $date,
			\Magento\Directory\Model\CountryFactory $countryFactory,
			\Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface,
			Data $helper
			//\Magento\Framework\App\Config\Storage\WriterInterface $configWriter

        	) {
				$this->collectionFactory = $collectionFactory;
				$this->fileFactory = $fileFactory;
				//$this->orderPdfFactory = $orderPdfFactory;
				$this->scopeConfig = $scopeConfig;
				$this->date = $date;
				$this->countryFactory = $countryFactory;
				//$this->_configWriter = $configWriter;
				$this->_configInterface = $configInterface;
				$this->helper = $helper;
				parent::__construct($context, $filter);
	}

	/**
	 * @return bool
	 */
	protected function _isAllowed()
	{
		return $this->_authorization->isAllowed('Magento_Sales::sales_order');
	}

	/**
	 * Print selected orders
	 *
	 * @param AbstractCollection $collection
	 *
	 * @return \Magento\Backend\Model\View\Result\Redirect
	 */
	protected function massAction(/*AbstractCollection $collection*/$orders = [])
	{
		$url = $this->scopeConfig->getValue('carriers/gls/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		//$url = 'https://andreja.requestcatcher.com/ParcelService.svc/';
		$username = $this->scopeConfig->getValue('carriers/gls/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$pwd = $this->scopeConfig->getValue('carriers/gls/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$password = "[".implode(',',unpack('C*', hash('sha512', $pwd, true)))."]";
		$clientNumber = $this->scopeConfig->getValue('carriers/gls/clientNumber', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromName = $this->scopeConfig->getValue('carriers/gls/fromName', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromAddress = $this->scopeConfig->getValue('carriers/gls/address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromAddressNumber = $this->scopeConfig->getValue('carriers/gls/addressNumber', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromAddressInfo = $this->scopeConfig->getValue('carriers/gls/addressInfo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromCity = $this->scopeConfig->getValue('carriers/gls/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromPcode = $this->scopeConfig->getValue('carriers/gls/pCode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromPhone = $this->scopeConfig->getValue('carriers/gls/phone', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$fromEmail = $this->scopeConfig->getValue('carriers/gls/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$pickdate = $this->scopeConfig->getValue('carriers/gls/date_from', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$services = $this->scopeConfig->getValue('carriers/gls/services', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$pieces = explode(",", $services);
		$cashOnDeliveryName = $this->scopeConfig->getValue('carriers/gls/cashOnDeliveryName', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$carrier = 'gls';
		$bulk = 1;
		$url .= "json/";
				
		$countOrder = 1;
		$params = '[';		
		foreach ($orders as $order) {

			//check if shipment exist
			if ($order->getShipmentsCollection()->count() == 0) {

				$orderId = $order->getIncrementId();		
				$order = $this->_objectManager->create('Magento\Sales\Model\Order')
				->loadByAttribute('increment_id', $orderId);
				
				try {
					
					$payment = $order->getPayment();
					$method = $payment->getMethodInstance();
					$methodTitle = $method->getTitle();
					
					$ex = explode(' ',$order->getShippingAddress()->getData()['street']);
					$last = end($ex);
					$streetName = str_replace($last,'',$order->getShippingAddress()->getData()['street']);

					
					$streetNumber = preg_replace('/[^0-9]/', '', $last);
					$streetApartment = preg_replace("/[^A-Za-z]+/", "", $last);

					if($streetNumber == null){
						$lastN = str_replace($last, "",$order->getShippingAddress()->getData()['street']);
						$ex = explode(' ',$lastN);
						$lastL = end($ex);
						
						$streetName = str_replace($lastL, "",$streetName);
						$streetNumber = preg_replace('/[^0-9]/', '', $lastN);
						$streetName = str_replace($streetNumber, "",$streetName);
					}
					
					if ( preg_match('/([^\d]+)\s?(.+)/i', $fromAddress, $result) )
					{
						$fromStreetName = $result[1];
						$fromStreetNumber = $result[2];
					}
					$pickupDate = "\/Date(".(strtotime(date("Y-m-d"). " " .$pickdate) * 1000).")\/";
					
					//$pickupDate = "\/Date(".(strtotime("2020-03-20 00:00:00") * 1000).")\/";
				
					$stop_date = date('Y-m-d', strtotime(date("Y-m-d") . ' +1 day'));

					$dayOfDelivery = "\/Date(".(strtotime($stop_date. " " .$pickdate) * 1000).")\/";
					
					$phone = $order->getShippingAddress()->getData()['telephone'];
					if($phone[0] == '0' || $phone[1] == '0' || $phone[0] == '+'){
						if($phone[0] != '+' && $phone[1] != '0' ){
							$phone = substr($phone, 1, strlen($phone)-1);
						}else if($phone[0] == '0' && $phone[1] == '0'){
							$phone = substr($phone, 5, strlen($phone)-1);
						}
					}
					if($fromPhone[0] == '0' || $fromPhone[1] == '0' || $fromPhone[0] == '+'){
						if($fromPhone[0] != '+' && $fromPhone[1] != '0' ){
							$fromPhone = substr($fromPhone, 1, strlen($fromPhone)-1);
						}else if($fromPhone[0] == '0' && $fromPhone[1] == '0'){
							$fromPhone = substr($fromPhone, 5, strlen($fromPhone)-1);
						}
					}
					$amount = 0;
					if($methodTitle == $cashOnDeliveryName){
						$amount = round($order->getGrandTotal(), 2);
					}else{
						$amount = 0;
					}
					//'.$phone.'     '.$fromPhone.'
					$additionalServices = '';
					$countAdditionalData = 1;
					foreach($pieces as $service){
						if($service == 'CS1' || $service == 'FSS' || $service == 'SM2' || $service == 'SM2'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .= '{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"Value": "'.$phone.'"
								}
							}';
						}else if($service == 'AOS'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"Value": "'.$streetName.$streetNumber.'"
								}
							}';
						}else if($service == 'DDS'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"Value": "'.$dayOfDelivery.'"
								}
							}';
						}else /*if($service == 'DPV'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"StringValue": "'.$phone.'",
									"DecimalValue": '.$phone.'
								}
							}';
						}else */if($service == 'FDS'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .= '{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"Value": "'.$order->getShippingAddress()->getData()['email'].'"
								}
							}';
						}else if($service == 'INS'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"Value": '.$amount.'
								}
							}';
						}else /*if($service == 'SDS'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"TimeFrom": "'.$pickupDate.'",
									"TimeTo": "'.$pickupDate.'"
								}
							}';
						}else */if($service == 'SM1'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"Value": "'.$phone.'|#ParcelNr#-"
								}
							}';
						}else if($service == 'SZL'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"Value": "'.$orderId.'"
								}
							}';
						}else /*if($service == 'ADR'){
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'",
								"'.$service.'Parameter": {
									"AdrItemType": '.$service.', 
									"AmountUnit": '.$service.', 
									"InnerCount": '.$service.', 
									"PackSize": '.$bulk.', 
									"UnNumber": '.$service.'
								}
							}';
						}else*/{
							if($countAdditionalData > 1)
								$additionalServices .= ',';
							$additionalServices .='{
								"Code": "'.$service.'"
							}';
						}
						$countAdditionalData++;
					}


					if($countOrder > 1)
						$params .= ',';
					$params .= '{
							"ClientNumber": "'.$clientNumber.'",
							"ClientReference": "'.$orderId.'",
							"CODAmount": '.$amount.',
							"CODReference": "'.$orderId.'",
							"Content": "CONTENT",
							"Count": 1,
							"DeliveryAddress": {
								"City": "'.$order->getShippingAddress()->getData()['city'].'",
								"ContactEmail": "'.$order->getShippingAddress()->getData()['email'].'",
								"ContactName": "'.$order->getShippingAddress()->getData()['firstname'].' '.$order->getShippingAddress()->getData()['lastname'].'",
								"ContactPhone": "'.$phone.'",
								"CountryIsoCode": "'.$order->getShippingAddress()->getData()['country_id'].'",
								"HouseNumber": "'.$streetNumber.'",
								"Name": "'.$order->getShippingAddress()->getData()['firstname'].' '.$order->getShippingAddress()->getData()['lastname'].'",
								"Street": "'.rtrim($streetName).'",
								"ZipCode": "'.$order->getShippingAddress()->getData()['postcode'].'",
								"HouseNumberInfo": "'.$streetApartment.'"
							},
							"PickupAddress": {
								"City": "'.$fromCity.'",
								"ContactEmail": "'.$fromEmail.'",
								"ContactName": "'.$fromName.'",
								"ContactPhone": "'.$fromPhone.'",
								"CountryIsoCode": "SI",
								"HouseNumber": "'.$fromAddressNumber.'",
								"Name":  "'.$fromName.'",
								"Street": "'.$fromAddress.'",
								"ZipCode": "'.$fromPcode.'",
								"HouseNumberInfo": "'.$fromAddressInfo.'"
							},
							"PickupDate": "'.$pickupDate.'",
							"ServiceList": ['.$additionalServices.']
						}';
					$countOrder++;
				} catch (\Exception $e) {
					throw new \Magento\Framework\Exception\LocalizedException(
							__($e->getMessage())
							);
				}
			}
		}
		
		$params .= ']';

	//	$this->messageManager->addError($params);
		
	//	die();
		

		try{
			$serviceName = "ParcelService";
			$isXmlFormat = false;
			/*print_r($url);*/
			if($countOrder == 1){
				throw new \Magento\Framework\Exception\LocalizedException(
					__("Shipments have already been generated, please process them individually or manually.")
					);
			}
			$response = $this->helper->massPrintLabels($username,$password,str_replace("SERVICE_NAME",$serviceName,$url),"PrintLabels",$params,$isXmlFormat);
			if(count(json_decode($response)->PrintLabelsErrorList) == 0){
				$trackNumbers = json_decode($response)->PrintLabelsInfoList;
				//Label(s) saving:
				$pdf = implode(array_map('chr', json_decode($response)->Labels));
								
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
				$rootPath  =  $directory->getRoot();
				$filename = $rootPath . '/pub/media/gls-'.json_decode($response)->PrintLabelsInfoList[0]->ParcelNumber.'.pdf';
										
				file_put_contents($filename, $pdf);

				foreach ($orders as $order) {

					if ($order->getShipmentsCollection()->count() == 0) {
						$orderId = $order->getIncrementId();		
						$order = $this->_objectManager->create('Magento\Sales\Model\Order')
						->loadByAttribute('increment_id', $orderId);
						
						if (! $order->canShip()) {
							throw new \Magento\Framework\Exception\LocalizedException(
									__('You can\'t create an shipment.')
									);
						}
						
						// Initialize the order shipment object
						$convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
						$shipment = $convertOrder->toShipment($order);
						
						$weight = 0;
						// Loop through order items
						foreach ($order->getAllItems() as $orderItem) {
							// Check if order item has qty to ship or is virtual
							if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
								continue;
							}
							$weight += ($orderItem->getWeight() * $orderItem->getQtyOrdered());

							$qtyShipped = $orderItem->getQtyToShip();
						
							// Create shipment item with qty
							$shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
						
							// Add shipment item to shipment
							$shipment->addItem($shipmentItem);
						}
						// Register shipment
						$shipment->register();
						
						$shipment->getOrder()->setIsInProcess(true);
						$shipment->getExtensionAttributes()->setSourceCode('default');

						// Save created shipment and order
						$shipment->save();
				
						//print_r($orderId);
						//die();
						$shipment->getOrder()->save();
						foreach($trackNumbers as $trackNumber){
							if($trackNumber->ClientReference == $orderId){
								if ($shipment) {
									$track = $this->_objectManager->create(\Magento\Sales\Model\Order\Shipment\Track::class)
										->setNumber($trackNumber->ParcelNumber)
										->setWeight($weight)
										->setCarrierCode($carrier)
										->setTitle($bulk)
										->setDescription($trackNumber->ParcelId);
									
									$shipment->addTrack($track)->save();
									// Send email
									$this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
									->notify($shipment);
								} else {
									$this->messageManager->addError(__('We can\'t initialize shipment for adding tracking number.'));					
								}
							}
						}
					}
				}

				$this->messageManager->addSuccess(__('%1 order(s) GLS shipment created successfully.', $countOrder - 1));
		
				$fileName = 'gls-'.json_decode($response)->PrintLabelsInfoList[0]->ParcelNumber.'.pdf';
				$this->fileFactory->create(
						$fileName,
						$pdf,
						\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
						'application/pdf'
						);
				$this->_redirect($this->_redirect->getRefererUrl());

			}else{
				$this->messageManager->addError(__('We can\'t initialize shipment for adding tracking number. Check your customer data.').'-'. json_decode($response)->PrintLabelsErrorList[0]->ClientReferenceList[0] .' - '. json_decode($response)->PrintLabelsErrorList[0]->ErrorDescription);
		
				$this->_redirect($this->_redirect->getRefererUrl());
				//throw new \Magento\Framework\Exception\LocalizedException(__(json_decode($response)->PrintLabelsErrorList[0]->ErrorDescription));
			}
		} catch (\Exception $e) {
			$this->messageManager->addError($e->getMessage());
			/*throw new \Magento\Framework\Exception\LocalizedException(
					__($e->getMessage())
					);*/
			$this->_redirect($this->_redirect->getRefererUrl());
		}
	}
}
