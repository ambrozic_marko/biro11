<?php

namespace Magelan\Gls\Block\Order\Tracking;

/**
 * Shipment tracking control form
 *
 * @api
 * @since 100.0.2
 */
class View extends \Magento\Shipping\Block\Adminhtml\Order\Tracking {
	
	/**
	 * @var \Magento\Shipping\Model\CarrierFactory
	 */
	protected $_carrierFactory;

	
	/**
	 * @var \Magento\Framework\HTTP\Client\Curl
	 *
	 */
	protected $curl;

	/**
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Magento\Shipping\Model\Config $shippingConfig
	 * @param \Magento\Framework\Registry $registry
	 * @param \Magento\Shipping\Model\CarrierFactory $carrierFactory
	 * @param array $data
	 * @param \Magento\Framework\HTTP\Client\Curl				 $curl
	 */
	public function __construct(
			\Magento\Backend\Block\Template\Context $context,
			\Magento\Shipping\Model\Config $shippingConfig,
			\Magento\Framework\Registry $registry,
			\Magento\Shipping\Model\CarrierFactory $carrierFactory,
			\Magento\Framework\HTTP\Client\Curl $curl,
			array $data = []
			) {
				parent::__construct($context, $shippingConfig, $registry, $data);
				$this->_carrierFactory = $carrierFactory;
				$this->curl = $curl;

	}

	/**
	 * Prepares layout of block
	 *
	 * @return void
	 */
	protected function _prepareLayout()
	{
		$onclick = "submitAndReloadArea($('shipment_tracking_info').parentNode, '" . $this->getSubmitUrl() . "')";
		$this->addChild(
				'save_button',
				\Magento\Backend\Block\Widget\Button::class,
				['label' => __('Add'), 'class' => 'save', 'onclick' => $onclick]
				);
		$onclick1 = "submitAndReloadArea($('shipment_tracking_info').parentNode, '" . $this->getGLSSubmitUrl() . "')";
		$this->addChild(
				'save_button_gls',
				\Magento\Backend\Block\Widget\Button::class,
				['label' => __('Add Gls'), 'class' => 'save', 'onclick' => $onclick1]
				);
	}
	
	
	/*public function actionDelete()
	{
		$this->curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
		$message = "wrong answer";
		echo "<script type='text/javascript'>alert('$message');</script>";
		return null;
	}*/

	/**
	 * Retrieve save url
	 *
	 * @return string
	 */
	public function getSubmitUrl()
	{
		return $this->getUrl('admin/order_shipment/addTrack/', ['shipment_id' => $this->getShipment()->getId()]);
	}

	public function getGLSSubmitUrl()
	{
		return $this->getUrl('gls/track/addTrack/', ['shipment_id' => $this->getShipment()->getId()]);
	}
	
	public function getPrintUrl($track)
	{
		return $this->getUrl('gls/track/printAction/', ['shipment_id' => $this->getShipment()->getId(), 'track_number' => substr($track->getTrackNumber(), 14, 14)]);
	}
	
	public function getTrackingPopupUrlBySalesModel($track)
	{
		return $this->getUrl('gls/track/printAction/', ['shipment_id' => $this->getShipment()->getId(), 'track_number' => $track->getTrackNumber()]);
	}

	/**
	 * Retrieve save button html
	 *
	 * @return string
	 */
	public function getSaveButtonHtml()
	{
		return $this->getChildHtml('save_button');
	}

	public function getGLSSaveButtonHtml()
	{
		return $this->getChildHtml('save_button_gls');
	}
	
	public function getGLSExpressSaveButtonHtml()
	{
		return $this->getChildHtml('save_button_gls_express');
	}
	
	public function getGLSInternacionalSaveButtonHtml()
	{
		return $this->getChildHtml('save_button_gls_international');
	}
	/**
	 * Retrieve remove url
	 *
	 * @param \Magento\Sales\Model\Order\Shipment\Track $track
	 * @return string
	 */
	public function getRemoveUrl($track)
	{
		return $this->getUrl(
				'admin/order_shipment/removeTrack/',
				['shipment_id' => $this->getShipment()->getId(), 'track_id' => $track->getId()]
				);
	}
	/**
	 * Retrieve remove url
	 *
	 * @param \Magento\Sales\Model\Order\Shipment\Track $track
	 * @return string
	 */
	public function getGLSRemoveUrl($track)
	{
		return $this->getUrl('gls/track/removeTrack/', ['shipment_id' => $this->getShipment()->getId(), 'track_number' => substr($track->getDescription(), 0, 14), 'track_id' => $track->getId()]);
	}
	

	/**
	 * @param string $code
	 * @return \Magento\Framework\Phrase|string|bool
	 */
	public function getCarrierTitle($code)
	{
		$carrier = $this->_carrierFactory->create($code);
		if ($carrier) {
			return $carrier->getConfigData('title');
		} else {
			return __('Custom Value');
		}
		return false;
	}
}
