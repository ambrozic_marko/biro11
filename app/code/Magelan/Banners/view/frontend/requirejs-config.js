var config = {
    map: { 
    	'*': {           
            'owlcarousel': "Magelan_Banners/js/jquery.owlcarousel"
        }
    },   
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};