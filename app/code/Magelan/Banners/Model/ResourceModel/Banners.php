<?php


namespace Magelan\Banners\Model\ResourceModel;

class Banners extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
           
    protected function _construct()
    {
        $this->_init('magelan_banners', 'banners_id');
    }
}
