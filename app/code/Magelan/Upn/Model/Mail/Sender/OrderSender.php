<?php

namespace Magelan\Upn\Model\Mail\Sender;


use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\ObjectManager;

class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender
{
    /**
     * @var \Fooman\EmailAttachments\Model\AttachmentContainerInterface
     */
    protected $templateContainer;
    protected $_designerhelper;
    protected $upnHelper;
    protected $transportBuilder;

    public function __construct(
        \Magento\Sales\Model\Order\Email\Container\Template $templateContainer,
        //\Magelan\Upn\Helper\Upn $upnHelper,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        TransportBuilder $transportBuilder
    ) {
        $this->templateContainer = $templateContainer;
        $this->transportBuilder = $transportBuilder;
        //$this->upnHelper = $upnHelper;
        parent::__construct(
            $this->templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $orderResource,
            $globalConfig,
            $eventManager
        );
      /*  $this->attachmentContainer = $attachmentContainer;*/
    }

    public function send(\Magento\Sales\Model\Order $order, $forceSyncMode = false)
    {
        $items = $order->getAllVisibleItems();
        $IncrementId = $order->getIncrementId();
        $imageData = array();
        $pdfData = array();
        
        $this->logger->info("Sending UPN over mail.");

        if ( "upn" == $order->getPayment()->getMethodInstance()->getCode()){
            $helper = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magelan\Upn\Helper\Upn');//$helperFactory->get('\Magelan\Upn\Helper\Upn');
            $helper->getUpnOrder($IncrementId);
            $fileName = 'upn-'.$IncrementId.'.pdf';
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
            $rootPath = $directory->getRoot();
            $this->logger->info($rootPath . '/var/' . $fileName);
            if (file_exists($rootPath . '/var/' . $fileName))
                $this->transportBuilder->addAttachment(file_get_contents($rootPath . '/var/' . $fileName), $fileName, 'application/pdf');
        }

        return parent::send($order, $forceSyncMode);
    }

    
}