<?php

namespace Magelan\Upn\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Helper\AbstractHelper;

//use BaconQrCode\Renderer\ImageRenderer;
//use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
//use BaconQrCode\Renderer\RendererStyle\RendererStyle;
//use BaconQrCode\Writer;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;

class Upn extends AbstractHelper
{
    protected $session;
    protected $quote;
    protected $quoteManagement;
    protected $orderSender;
    protected $storeManager;
    protected $pageFactory = null;

    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $session,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        $this->session = $session;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->storeManager = $storeManager;
        $this->assetRepo = $assetRepo;
        $this->request = $request;
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }

    public function getConfigValue($configField)
    {
    	return $this->scopeConfig->getValue($configField,
    			\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    
    }
    
    public function getViewFileUrl(){
        $params = array('_secure' => $this->request->isSecure());
        $url = $this->assetRepo->getUrlWithParams('Magelan_Upn::images/upn.jpg', $params);
        return $url;
    }

    public function cancelCurrentOrder($comment)
    {
        $order = $this->session->getLastRealOrder();
        if ($order->getId() && $order->getState() != Order::STATE_CANCELED) {
            $order->registerCancellation($comment)->save();
            return true;
        }
        return false;
    }

    public function restoreQuote()
    {
        return $this->session->restoreQuote();
    }

    public function getUrl($route, $params = [])
    {
        return $this->_getUrl($route, $params);
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl();
    }

    public function getUpnButton()
    {
        return $this->getUpnOrder($this->session->getLastRealOrderId());
    }

    public function getUpnOrder($orderId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('\Magento\Sales\Model\Order')
                                ->loadByIncrementId($orderId);
        $amount = $order->getGrandTotal();
		
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
        $storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $storeID       = $storeManager->getStore()->getStoreId(); 
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

        $refNum = 'SI00';//.sprintf("%'02d", $storeID);
        //$paymentType = 'OTHR';

		//Lastnik računa
		$account_holder = $this->getConfigValue('payment/upn/bank_accounts/account_holder');
		$postcode = $this->getConfigValue('payment/upn/bank_accounts/zip');
		$cityCompany = $this->getConfigValue('payment/upn/bank_accounts/city');
		$address = $this->getConfigValue('payment/upn/bank_accounts/address');
		$account_number = $this->getConfigValue('payment/upn/bank_accounts/account_number');
		$bank_name = $this->getConfigValue('payment/upn/bank_accounts/bank_name');
		$paymentType = $this->getConfigValue('payment/upn/bank_accounts/bic');
		$contract = $this->getConfigValue('payment/upn/bank_accounts/contract');
        
        
        $date_order=date_create($order->getCreatedAt());
		$date_order->settime(0,0);
		$xdays = $this->getConfigValue('payment/upn/paywithinxdays');	
		date_add($date_order, date_interval_create_from_date_string("+".$xdays." days"));
		$date = $date_order->format('d.m.Y'); 

		//Naslovnik
		$company = $order->getBillingAddress()->getData()['company'];
		$name1 = $order->getBillingAddress()->getData()['firstname'].' '.$order->getBillingAddress()->getData()['lastname'];
		
		if (strlen($company) > 4){
			$name1 = $company;	
		}
		
    	$pcode = $order->getBillingAddress()->getData()['postcode'];
    	$city = $order->getBillingAddress()->getData()['city'];
    	$street = $order->getBillingAddress()->getData()['street'];
        $phone = $order->getBillingAddress()->getData()['telephone'];
        


        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
        $page = $pdf->pages[0]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        //$font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
        //$page->setFont(Zend_Pdf_Font::fontWithPath('fonts/times.ttf'), 13); 
        //$font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_TIMES);
        $font = \Zend_Pdf_Font::fontWithPath($directory->getRoot() . '/lib/customfonts/times.ttf');
        $style->setFont($font,13);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $x = 30;
        $pageTopalign = 850; 
        $this->y = 850 - 100; 
        
        $style->setFont($font,10);
        $page->setStyle($style);

        //$pathImage = $this->_checkoutgetViewFileUrl();
		//echo $pathImage;
	    //$image = \Zend_Pdf_Image::imageWithPath($pathImage);
	    $upn_jpg = $this->assetRepo->createAsset("Magelan_Upn::images/upn.jpg", [
            'area' => 'frontend'
        ]);
		$image = \Zend_Pdf_Image::imageWithPath($upn_jpg->getSourceFile());
		$page->drawImage($image, 35, 530, 550, 800);

        //echo mb_detect_encoding($name1);
        //iconv("UTF-8","ISO-8859-2", $name1);

        //print_r( $name1);
        //die();

        $page->drawText(__(iconv("UTF-8","UTF-16BE", $name1)), 48, 773, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $street)), 48, 762, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $pcode)), 48, 751, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $city)), 78, 751, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", "Naročilo št.: " . $orderId)), 48, 729, 'UTF-16BE');
        
        $page->drawText(__("***".number_format($amount, 2, ',', '.')), 80, 696, 'UTF-8');
       
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $account_number)), 48, 673, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $refNum .' '. $orderId)), 48, 651, 'UTF-16BE');
        
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $account_holder)), 48, 629, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $address)), 48, 618, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $postcode)), 48, 607, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $cityCompany)), 80, 607, 'UTF-16BE');
        

        $style->setFont($font,12);
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $name1)), 300, 730, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $street)), 300, 717, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $pcode)), 300, 703, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $city)), 330, 703, 'UTF-16BE');
        
        $page->drawText(__("***".number_format($amount, 2, ',', '.')), 320, 680, 'UTF-8');
        
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $paymentType)), 194, 656, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", "Naročilo št.: " . $orderId)), 237, 656, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $date)), 471, 656, 'UTF-16BE');
        
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $account_number)), 195, 632, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $refNum)), 195, 610, 'UTF-16BE');
        
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $orderId)), 235, 610, 'UTF-16BE');
        
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $account_holder)), 195, 588, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $address)), 195, 575, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $postcode)), 195, 561, 'UTF-16BE');
        $page->drawText(__(iconv("UTF-8","UTF-16BE", $cityCompany)), 225, 561, 'UTF-16BE');

		$qrText = $this->getQRText($order);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $rootPath = $directory->getRoot();
        $tmp_file = $rootPath . "/" . 'var/qrcode.png';
		
		$qrCode = new QrCode($this->getQRText($order));
        $qrCode->setSize(400);
        $qrCode->setMargin(-35);
        $qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevelHigh());
        $qrCode->setForegroundColor(new Color(0, 0, 0, 0));
        $qrCode->setBackgroundColor(new Color(255, 255, 255, 0));
        $qrCode->setEncoding(new Encoding('ISO-8859-2'));

        $writer = new PngWriter();
        $pngData = $writer->write($qrCode);
		$pngData->saveToFile($tmp_file);
        
        $image_barcode = \Zend_Pdf_Image::imageWithPath($tmp_file);
        
        $page->drawImage($image_barcode, 194, 680, 286, 780);

        $fileName = 'upn-'. $orderId . '.pdf';

        $this->fileFactory->create(
           $fileName,
           $pdf->render(),
           \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, 
           'application/pdf'
        );
    }

    public function getQRText($order)
    {

		$date_order=date_create($order->getCreatedAt());
		$date_order->settime(0,0);
		$xdays = $this->getConfigValue('payment/upn/paywithinxdays');	
		date_add($date_order, date_interval_create_from_date_string("+".$xdays." days"));
		$date = $date_order->format('d.m.Y'); 

		$company = $order->getBillingAddress()->getData()['company'];
		
        $string = 'UPNQR'.chr(10);
        $string .= chr(10);
        $string .= chr(10);
        $string .= chr(10);
        $string .= chr(10);
		if (strlen($company) > 4){
			$string .= substr($order->getBillingAddress()->getData()['company'],0,33).chr(10);			
		} else {		
			$string .= substr($order->getBillingAddress()->getData()['firstname'].' '.$order->getBillingAddress()->getData()['lastname'],0,33).chr(10);
		}
        $string .= substr($order->getBillingAddress()->getData()['street'],0,33).chr(10);
        $string .= substr($order->getBillingAddress()->getData()['postcode'].' '.$order->getBillingAddress()->getData()['city'],0,33).chr(10);
        $string .= str_pad(number_format($order->getGrandTotal(), 2, "", ""), 11, "0", STR_PAD_LEFT).chr(10);
        $string .= chr(10);
        $string .= chr(10);
        $string .= substr("OTHR",0,4).chr(10);
        $string .= substr("Naročilo".' '.$order->getIncrementId(),0,42).chr(10);
	$string .= substr($date,0,10).chr(10);
	$accountNumber = str_replace(' ', '', $this->getConfigValue('payment/upn/bank_accounts/account_number'));
	$string .= substr($accountNumber.chr(10),0,34);
        $string .= substr('SI00'.$order->getIncrementId(),0,26).chr(10);
        $string .= substr($this->getConfigValue('payment/upn/bank_accounts/account_holder'),0,33).chr(10);
        $string .= substr($this->getConfigValue('payment/upn/bank_accounts/address'),0,33).chr(10);
        $string .= substr($this->getConfigValue('payment/upn/bank_accounts/zip').' '.$this->getConfigValue('payment/upn/bank_accounts/city'),0,33);

        $count = strlen($string);
        $string .= chr(10).str_pad($count, 3, "0", STR_PAD_LEFT).chr(10);

        $count = strlen($string);
        $string .= str_pad("", 411 - $count, " ", STR_PAD_LEFT);

        return $string;
    }
}
