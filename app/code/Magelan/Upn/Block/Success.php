<?php
namespace Magelan\Upn\Block;

class Success extends \Magento\Framework\View\Element\Template
{

    private $checkoutSession;
    private $cartManagement;
    private $order;
    private $orderSender;
    private $cart;
    private $redirectFactory;
    private $databaseFactory;
    private $storeManager;
    private $request;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session\Proxy $checkoutSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magelan\Upn\Helper\Upn $helper,
        \Magento\Framework\App\Request\Http $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->checkoutSession = $checkoutSession;
        $this->cartManagement = $cartManagement;
        $this->order = $order;
        $this->orderSender = $orderSender;
        $this->cart = $cart;
        $this->redirectFactory = $redirectFactory;
        $this->storeManager = $helper;
        $this->request = $request;
    }

    public function getOrderById($id)
    {
        return $this->order->load($id);
    }

    public function getOrderPaymentTitle(){
        return "";//$this->getLastOrder()->getPayment()->getMethodInstance()->getTitle();
    }

    public function getOrderPaymentCode(){
        return $this->getLastOrder()->getPayment()->getMethodInstance()->getCode();
    }

    public function getUpnUrl(){
        return $this->getBaseUrl() . 'upn/order/upn';
    }
    public function getXmlUrl(){
        return $this->getBaseUrl() . 'upn/order/xml';
    }
    public function getLastOrder()
    {
        $order = $this->order->loadByIncrementId($this->checkoutSession->getLastRealOrderId());
        return $order;
    }

    public function placeOrder($quoteId)
    {
        $this->cartManagement->placeOrder($quoteId);
    }

    public function sendOrder($order)
    {
        $this->orderSender->send($order);
    }

    public function restoreQuote()
    {
        return $this->checkoutSession->restoreQuote();
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getBaseUrl();
    }

    public function getQuote()
    {
        return $this->checkoutSession->getQuote();
    }

    
}
