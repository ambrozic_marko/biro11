<?php
/**
 * Upn payments module for Magento 2
 */

use \Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(ComponentRegistrar::MODULE,'Magelan_Upn',__DIR__);
