<?php
 
namespace Magelan\Upn\Controller\Order;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

class Upn extends \Magelan\Upn\Controller\CfAbstract
{

    public function execute()
    {
        
        $this->_checkoutHelper->getUpnButton();
        
    }
}
