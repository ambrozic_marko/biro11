<?php
 
namespace Magelan\Upn\Controller\Order;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;

class Xml extends \Magelan\Upn\Controller\CfAbstract
{

    public function execute()
    {
        $orderId = $this->_checkoutSession->getLastRealOrderId();
    
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('\Magento\Sales\Model\Order')
                                ->load($orderId);
        $amount = $order->getGrandTotal();
		

		//Lastnik računa
		$account_holder = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/account_holder');
		$postcode = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/zip');
		$cityCompany = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/city');
		$address = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/address');
		$account_number = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/account_number');
		$bank_name = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/bank_name');
		$bic = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/bic');
		$contract = $this->_checkoutHelper->getConfigValue('payment/upn/bank_accounts/contract');
        
        
        $xdays = $this->_checkoutHelper->getConfigValue('payment/upn/paywithinxdays');
		$plusFive = strtotime( $order->getCreatedAt().'+'.$xdays.' weekday' );
        $date = date( 'd.m.Y', $plusFive );
		//Naslovnik
		$name1 = $order->getBillingAddress()->getData()['firstname'].' '.$order->getBillingAddress()->getData()['lastname'];
    	$pcode = $order->getBillingAddress()->getData()['postcode'];
    	$city = $order->getBillingAddress()->getData()['city'];
    	$street = $order->getBillingAddress()->getData()['street'];
        $phone = $order->getBillingAddress()->getData()['telephone'];
        

        $paymentdate = strtotime($order->getCreatedAt()) + 86400;

        $xml = '<?xml version="1.0" encoding="utf-8"?>
                <Document xsi:schemaLocation="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 ./pain.001.001.03.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03">
                    <CstmrCdtTrfInitn>
                        <GrpHdr>
                            <MsgId>'.substr(str_replace(' ', '-', $order->getCreatedAt()).'/'.$order->getIncrementId(), 0, 35).'</MsgId>
                            <CreDtTm>'.substr($order->getCreatedAt(),0, 10).'T'.substr($order->getCreatedAt(),11, 8).'.00</CreDtTm>
                            <NbOfTxs>1</NbOfTxs>
                            <InitgPty>
                                <Nm>'.substr($account_holder, 0, 70).'</Nm>
                                <Id>
                                    <OrgId>
                                        <Othr>
                                            <Id></Id>
                                            <SchmeNm>
                                                <Cd>TXID</Cd>
                                            </SchmeNm>
                                        </Othr>
                                    </OrgId>
                                </Id>
                            </InitgPty>
                        </GrpHdr>
                        <PmtInf>
                            <PmtInfId>'.$order->getIncrementId().'</PmtInfId>
                            <PmtMtd>TRF</PmtMtd>
                            <PmtTpInf>
                                <InstrPrty>NORM</InstrPrty>
                                <SvcLvl>
                                    <Cd>SEPA</Cd>
                                </SvcLvl>
                                <LclInstrm>
                                    <Prtry>SEPA</Prtry>
                                </LclInstrm>
                            </PmtTpInf>
                            <ReqdExctnDt>'.date('Y-m-d', $paymentdate).'</ReqdExctnDt>
                            <Dbtr>
                                <Nm>'.substr($account_holder, 0, 70).'</Nm>
                                <PstlAdr>
                                    <Ctry></Ctry>
                                    <AdrLine>'.$address.'</AdrLine>
                                    <AdrLine>'.$postcode.' '.$cityCompany.'</AdrLine>
                                </PstlAdr>
                            </Dbtr>
                            <DbtrAcct>
                                <Id>
                                    <IBAN></IBAN>
                                </Id>
                            </DbtrAcct>
                            <DbtrAgt>
                                <FinInstnId>
                                    <BIC></BIC>
                                </FinInstnId>
                            </DbtrAgt>
                            <CdtTrfTxInf>
                                <PmtId>
                                    <InstrId>'.$order->getIncrementId().'</InstrId>
                                    <EndToEndId>SI00254000</EndToEndId>
                                </PmtId>
                                <Amt>
                                    <InstdAmt Ccy="EUR">'.number_format($order->getGrandTotal(), 2, '.', '').'</InstdAmt>
                                </Amt>
                                <ChrgBr>SLEV</ChrgBr>
                                <CdtrAgt>
                                    <FinInstnId>
                                        <BIC>BAKOSI2X</BIC>
                                    </FinInstnId>
                                </CdtrAgt>
                                <Cdtr>
                                    <Nm>'.substr($name1, 0, 70).'</Nm>
                                    <PstlAdr>
                                        <Ctry></Ctry>
                                        <AdrLine>'.$street.'</AdrLine>
                                        <AdrLine>'.$pcode.' '.$city.'</AdrLine>
                                    </PstlAdr>
                                </Cdtr>
                                <CdtrAcct>
                                    <Id>
                                        <IBAN>'.str_replace(' ', '', $account_number).'</IBAN>
                                    </Id>
                                </CdtrAcct>
                                <Purp>
                                    <Cd>TAXS</Cd>
                                </Purp>
                                <RmtInf>
                                    <Strd>
                                        <CdtrRefInf>
                                            <Tp>
                                                <CdOrPrtry>
                                                    <Cd>SCOR</Cd>
                                                </CdOrPrtry>
                                            </Tp>
                                            <Ref>SI'.sprintf("%'02d", $order->getStoreId()).$order->getIncrementId().'</Ref>
                                        </CdtrRefInf>
                                        <AddtlRmtInf>Order '.$order->getIncrementId().'</AddtlRmtInf>
                                    </Strd>
                                </RmtInf>
                            </CdtTrfTxInf>
                        </PmtInf>
                    </CstmrCdtTrfInitn>
                </Document>';

        
        $file = 'uvoz.xml';
        file_put_contents($file, $xml);
    }
}
