<?php
namespace Magelan\Upn\Controller\Standard;

class Request extends \Magelan\Upn\Controller\CfAbstract {

    public function execute() {
       
    	
		$this->messageManager->addSuccess(__('Your order was successful'));
		$returnUrl = $this->getCheckoutHelper()->getUrl('checkout/onepage/success');
        $this->getResponse()->setRedirect($returnUrl);
    	
    }
    	
}
    	 