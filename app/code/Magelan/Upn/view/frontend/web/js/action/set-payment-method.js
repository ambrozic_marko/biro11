define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/customer-data',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magelan_Upn/js/form/form-builder',
        'Magento_Ui/js/modal/alert'
    ],
    function ($, quote, customerData,customer, fullScreenLoader, formBuilder,alert) {
        'use strict';

        return function (messageContainer) {

            var serviceUrl,
                email,
                form;

            if (!customer.isLoggedIn()) {
                email = quote.guestEmail;
            } else {
                email = customer.customerData.email;
            }


            
            //serviceUrl = window.checkoutConfig.payment.upn.redirectUrl+'?email='+email;
            fullScreenLoader.startLoader();
            
            $.ajax({
                url: '/mag2.dev/upn/standard/redirect/'+'?email='+email,//serviceUrl,
                type: 'post',
                context: this,
                data: {isAjax: 1},
                dataType: 'json',
                success: function (response) {
                	if ($.type(response) === 'object' && !$.isEmptyObject(response)) {
                        $('#activa_payment_form').remove();
                        form = formBuilder.build(
                            {
                                action: response.url,
                                fields: response.fields
                            }
                        );
                        customerData.invalidate(['cart']);
                        form.submit();
                    } else {
                        fullScreenLoader.stopLoader();
                        alert({
                            content: $.mage.__('Sorry, something went wrong. Please try again.')
                        });
                    }
                },
                error: function (response) {
                	console.log(response);
                    fullScreenLoader.stopLoader();
                    alert({
                        content: $.mage.__('Sorry, something went wrong. Please try again later.0000')
                    });
                }
            });
        };
    }
);


