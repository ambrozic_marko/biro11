define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
],function(Component,renderList){
    'use strict';
    renderList.push({
        type : 'upn',
        component : 'Magelan_Upn/js/view/payment/method-renderer/upn-method'
    });

    return Component.extend({});
})
