define(
    [
        'Magento_Checkout/js/view/payment/default',
        'Magelan_Upn/js/action/set-payment-method',
        'mage/url'
    ],
    function(Component,setPaymentMethod, url){
    'use strict';

    return Component.extend({
        defaults:{
            'template':'Magelan_Upn/payment/upn'
        },
        redirectAfterPlaceOrder: false,
        
        /*afterPlaceOrder: function () {
            setPaymentMethod();    
        }*/
        afterPlaceOrder: function () {
        	window.location.replace(url.build('upn/standard/request/'));
        },

    })
});
