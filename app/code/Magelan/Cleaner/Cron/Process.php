<?php
namespace Magelan\Cleaner\Cron;

use Psr\Log\LoggerInterface;

class Process {

    protected $_helper;
    protected $_logger;

    public function __construct(
            \Magelan\Cleaner\Helper\Data $helper,
            LoggerInterface $logger
            ) {
        
        $this->_helper = $helper;
        $this->_logger = $logger;
    }

	public function execute()
	{
        $this->_logger->info('Cleaner Cron started');
        $this->_helper->clean();
        $this->_logger->info('Cleaner Cron finished');
    }
    
}