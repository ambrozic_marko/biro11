<?php

namespace Magelan\Cleaner\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context as Context;

use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use \MS3\Import\Model\ResourceModel\MetaImport\CollectionFactory;
use \Magento\CatalogInventory\Api\StockRegistryInterface;
use \Magento\Framework\App\State;
use \Magento\Framework\Mail\Template\TransportBuilder;
use \Magento\Store\Model\StoreManagerInterface;


class Data extends AbstractHelper
{
    
    protected $_productRepository;
    protected $_logger;
    protected $_searchCriteriaBuilder;
    protected $_collectionFactory;
    protected $_stockRegistry;
    protected $_state;
    protected $_transportBuilder;
    protected $_storeManager;

    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionFactory $collectionFactory,
        StockRegistryInterface $stockRegistry,
        State $state
        //namespace MS3\Import\Model\ResourceModel\MetaImport;
        //class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
    )
    {
        $this->_productRepository = $productRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_collectionFactory = $collectionFactory;
        $this->_stockRegistry = $stockRegistry;
        $this->_state = $state;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
        $logger = new \Zend_Log();
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/cleaner.cron.log');
        $logger->addWriter($writer);
        $this->_logger = $logger;
    }

    public function clean() {
        
        $this->_logger->info("Cleaning product catalogue.");

        try {
            $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or AREA_FRONTEND
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // Area code is already set
        }

        $imports = $this->_getCollection();
        $importStatus = [];
        foreach($imports as $importedItem) {
            if(!array_key_exists($importedItem->getSku(), $importStatus)) {
                $importStatus[$importedItem->getSku()] = $importedItem->getImported();
            } else {
                if($importedItem->getImported() == 1)
                    $importStatus[$importedItem->getSku()] = 1;
            }   
        }
        
        $body = "Following SKUs were disabled:\n";
        foreach ($importStatus as $sku => $status) {
            try {
                $product = $this->_productRepository->get($sku, false, 0);
                if ($status == 0) {
                    if($product->getStatus() == \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED) {
                        $allowBelowZero = $this->_isAllowQtyBelowZero($product->getId());
                        $preorderAllowed = $this->_isPreorderAllowed($product->getId());
                        if (!$allowBelowZero && !$preorderAllowed) {
                            echo 'Disabling SKU: ' . $sku . " ";
                            $body .= 'Disabling SKU: ' . $sku . " ";
                            foreach($imports as $import) { 
                                if($sku == $import->getSku()) {
                                    echo "[" . $import->getSupplier() . ":" . $import->getImported() . "]";
                                    $body .= "[" . $import->getSupplier() . ":" . $import->getImported() . "]";
                                }
                            }
                            echo "[AllowQtyBelowZero:" . $allowBelowZero . "]";
                            $body .= "[AllowQtyBelowZero:" . $allowBelowZero . "]";
                            echo "[PreorderAllowed:" . $preorderAllowed . "]";
                            $body .= "[PreorderAllowed:" . $preorderAllowed . "]";
                            echo "\n";
                            $body .= "\n";
                            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
                            $this->_productRepository->save($product);
                        }
                    }
                }
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                // Handle the exception if the product does not exist
                //echo $e;
            } catch (\Exception $e) {
                // Handle other exceptions
                //echo $e;
            }
            
        }

        $this->sendEmail("biro@biro.si", "biro@biro.si", "Sync Report", $body);

        /*$stores = $this->_storesRepository->getList();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$taxCalculation = $objectManager->create('\Magento\Tax\Api\TaxCalculationInterface');
		$scopeConfig = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');  
		$taxSetting = $scopeConfig->getValue('tax/calculation/price_includes_tax');*/

        $this->_logger->info("Catalogue clean.");
    }

    private function _checkIfInImport($sku, $imports) {
        foreach ($imports as $imp) {
            if($imp->getSku() == $sku) {
                return true;
            }
        }
        return false;
    }

    public function _getCollection()
    {
        return $this->_collectionFactory->create();
    }

    public function _isAllowQtyBelowZero($productId) {
        $stockItem = $this->_stockRegistry->getStockItem($productId);
        $backorderStatus = $stockItem->getBackorders();
        if ($backorderStatus == \Magento\CatalogInventory\Model\Stock::BACKORDERS_NO) {
            return 0;
        } else {
            return 1;
        }
    }

    public function _isPreorderAllowed($productId) {
        $stockItem = $this->_stockRegistry->getStockItem($productId);
        $backorderStatus = $stockItem->getBackorders();
        if ($backorderStatus == \Magento\CatalogInventory\Model\Stock::BACKORDERS_NO) {
            return 0;
        } else {
            return 1;
        }
    }

    public function sendEmail($recipientEmail, $senderEmail, $subject, $body)
    {
        try {
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier('email_template')
                ->setTemplateOptions(['area' => 'frontend', 'store' => $this->_storeManager->getStore()->getId()])
                ->setTemplateVars([])
                ->setFromByScope($senderEmail)
                ->addTo($recipientEmail)
                ->getTransport();

            $transport->getMessage()->setSubject($subject);
            $transport->getMessage()->setBodyText($body);
            $transport->sendMessage();
        } catch (\Exception $e) {
            // Handle the exception as needed
            echo $e;
        }
    }

}