<?php
namespace Magelan\Cleaner\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Clean extends Command
{

    protected $_helper;

    public function __construct(
        \Magelan\Cleaner\Helper\Data $helper
    )
    {
        parent::__construct();
        $this->_helper = $helper;
    }

    protected function configure()
    {
       $this->setName('magelan:sync:clean');
       $this->setDescription('Disables products that are not in import any more');
       parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $output->writeln("Started cleaning product catalogue.");
       $this->_helper->clean();
       $output->writeln("Catalogue clean.");
    }

}