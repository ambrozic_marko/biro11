<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/terms
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2021 Landofcoder (https://www.landofcoder.com/)
 * @license    https://landofcoder.com/terms
 */

namespace Lof\SmsNotification\Helper;

use Exception;
use Magento\Framework\Stdlib\DateTime\DateTime as CoreDateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\App\Helper\Context;

/**
 * Class DateTimeHelper
 * @package Lof\SmsNotification\Helper
 */
class DateTimeHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var CoreDateTime
     */
    protected $_dateTime;

    /**
     * @var TimezoneInterface
     */
    protected $_timezoneInterface;

    /**
     * @param Context $context
     * @param CoreDateTime $dateTime
     * @param TimezoneInterface $timezoneInterface
     */
    public function __construct(
        Context $context,
        CoreDateTime $dateTime,
        TimezoneInterface $timezoneInterface
    ) {
        $this->_dateTime = $dateTime;
        $this->_timezoneInterface = $timezoneInterface;
        parent::__construct($context);
    }

    /**
     * Get Date time
     * 
     * @return CoreDateTime
     */
    public function getDateTime()
    {
        return $this->_dateTime;
    }

    /**
     * Get Timezone date time
     * 
     * @param string
     * @return string|int|null
     */
    public function getTimezoneDateTime($dateTime = "today")
    {
        if($dateTime === "today" || !$dateTime){
            $dateTime = $this->_dateTime->gmtDate();
        }
        
        $today = $this->_timezoneInterface
            ->date(
                new \DateTime($dateTime)
            )->format('Y-m-d H:i:s');
        return $today;
    }

    /**
     * Get timezone name
     * 
     * @return string
     */
    public function getTimezoneName()
    {
        return $this->_timezoneInterface->getConfigTimezone(\Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }

    /**
     * Verify date time delay
     * 
     * $timeType: seconds, days, minutes, hours
     * 
     * 
     * @param string currentDateTime
     * @param int $delaySeconds
     * @param string $timeType
     * @return bool|int
     */
    public function verifyDelayTime($updatedAt, $delaySeconds = 5, $timeType = "seconds")
    {
        $timeTypeName = in_array($timeType, ["seconds","days","minutes","hours"])?$timeType:"seconds";
        $today = $this->getTimezoneDateTime();
        $currentTime = strtotime($today);
        $updatedAt = $this->getTimezoneDateTime($updatedAt);
        $nextTime = strtotime($updatedAt . ' + ' . (int)$delaySeconds . ' '.$timeTypeName);
        $difference = $nextTime - $currentTime;
        if ($difference > 0) {
            return true;
        }
        return false;
    }
}