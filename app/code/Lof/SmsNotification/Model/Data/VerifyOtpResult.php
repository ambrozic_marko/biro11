<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2021 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Model\Data;

use Lof\SmsNotification\Api\Data\VerifyOtpResultInterface;

class VerifyOtpResult extends \Magento\Framework\Api\AbstractExtensibleObject implements VerifyOtpResultInterface
{
    /**
     * {@inheritdoc}
     */
    public function getVerified()
    {
        return $this->_get(self::VERIFIED);
    }

    /**
     * {@inheritdoc}
     */
    public function setVerified($verified)
    {
        return $this->setData(self::VERIFIED, $verified);
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone()
    {
        return $this->_get(self::PHONE);
    }

    /**
     * {@inheritdoc}
     */
    public function setPhone($phone)
    {
        return $this->setData(self::PHONE, $phone);
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage() 
    {
        return $this->_get(self::MESSAGE);
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * {@inheritdoc}
     */
    public function setExtensionAttributes(
        \Lof\SmsNotification\Api\Data\VerifyOtpResultExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

