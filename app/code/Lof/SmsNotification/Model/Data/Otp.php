<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2021 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Model\Data;

use Lof\SmsNotification\Api\Data\OtpInterface;

class Otp extends \Magento\Framework\Api\AbstractExtensibleObject implements OtpInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOtpId()
    {
        return $this->_get(self::OTP_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setOtpId($otp_id)
    {
        return $this->setData(self::OTP_ID, $otp_id);
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId($customer_id)
    {
        return $this->setData(self::CUSTOMER_ID, $customer_id);
    }

    /**
     * {@inheritdoc}
     */
    public function getDigitCode()
    {
        return $this->_get(self::DIGIT_CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setDigitCode($digit_code)
    {
        return $this->setData(self::DIGIT_CODE, $digit_code);
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone() 
    {
        return $this->_get(self::PHONE);
    }

    /**
     * {@inheritdoc}
     */
    public function setPhone($phone)
    {
        return $this->setData(self::PHONE, $phone);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryCode()
    {
        return $this->_get(self::COUNTRY_CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setCountryCode($country_code)
    {
        return $this->setData(self::COUNTRY_CODE, $country_code);
    }

    /**
     * {@inheritdoc}
     */
    public function getResend()
    {
        return $this->_get(self::RESEND);
    }

    /**
     * {@inheritdoc}
     */
    public function setResend($resend)
    {
        return $this->setData(self::RESEND, $resend);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($created_at)
    {
        return $this->setData(self::CREATED_AT, $created_at);
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt($updated_at)
    {
        return $this->setData(self::UPDATED_AT, $updated_at);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * {@inheritdoc}
     */
    public function setExtensionAttributes(
        \Lof\SmsNotification\Api\Data\OtpExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

