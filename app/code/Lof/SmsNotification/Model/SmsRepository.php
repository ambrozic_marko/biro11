<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2021 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 **/

namespace Lof\SmsNotification\Model;

use Lof\SmsNotification\Api\SmsRepositoryInterface;
use Lof\SmsNotification\Api\Data\OtpInterfaceFactory;
use Lof\SmsNotification\Api\Data\OtpInterface;
use Lof\SmsNotification\Api\Data\VerifyOtpResultInterfaceFactory;
use Lof\SmsNotification\Api\Data\VerifyOtpResultInterface;
use Lof\SmsNotification\Api\Data\PhoneInterfaceFactory;
use Lof\SmsNotification\Api\Data\PhoneInterface;
use Lof\SmsNotification\Helper\DateTimeHelper;
use Lof\SmsNotification\Helper\Data;
use Lof\SmsNotification\Setup\InstallData;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Class SmsRepository
 * @package Lof\SmsNotification\Model
 */
class SmsRepository implements SmsRepositoryInterface
{
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var DateTimeHelper
     */
    protected $helperDateTime;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var OtpInterfaceFactory
     */
    protected $otpInterfaceFactory;

    /**
     * @var VerifyOtpResultInterfaceFactory
     */
    protected $otpVerifyResultInterfaceFactory;

    /**
     * @var PhoneInterfaceFactory
     */
    protected $phoneInterfaceFactory;

    /**
     * @var OtpFactory
     */
    protected $otpModelFactory;

    /**
     * @var PhoneFactory
     */
    protected $phoneModelFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManagerInterface;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var SendSms
     */
    protected $sendSms;

    /**
     * @var Smslog
     */
    protected $smsLog;

    /**
     * Otp Repository constructor.
     *
     * @param Date $helperData
     * @param SerializerInterface $serializer
     * @param DataObjectHelper $dataObjectHelper
     * @param OtpInterfaceFactory $otpInterfaceFactory
     * @param PhoneInterfaceFactory $phoneInterfaceFactory
     * @param VerifyOtpResultInterfaceFactory $otpVerifyResultInterfaceFactory
     * @param OtpFactory $otpModelFactory
     * @param PhoneFactory $phoneModelFactory
     * @param StoreManagerInterface $storeManagerInterface
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param SendSms $sendSms
     * @param Smslog $smsLog
     */
    public function __construct(
        Data $helperData,
        SerializerInterface $serializer,
        DataObjectHelper $dataObjectHelper,
        OtpInterfaceFactory $otpInterfaceFactory,
        PhoneInterfaceFactory $phoneInterfaceFactory,
        VerifyOtpResultInterfaceFactory $otpVerifyResultInterfaceFactory,
        OtpFactory $otpModelFactory,
        PhoneFactory $phoneModelFactory,
        StoreManagerInterface $storeManagerInterface,
        CustomerRepositoryInterface $customerRepositoryInterface,
        SendSms $sendSms,
        Smslog $smsLog
    ) {
        $this->helperData = $helperData;
        $this->helperDateTime = $helperData->getDateTimeHelper();
        $this->serializer = $serializer;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->otpInterfaceFactory = $otpInterfaceFactory;
        $this->otpVerifyResultInterfaceFactory = $otpVerifyResultInterfaceFactory;
        $this->otpModelFactory = $otpModelFactory;
        $this->phoneModelFactory = $phoneModelFactory;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->phoneInterfaceFactory = $phoneInterfaceFactory;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->sendSms = $sendSms;
        $this->smsLog = $smsLog;
    }

    /**
     * {@inheritdoc}
     */
    public function get($customerId) 
    {
        if (!$customerId) {
            throw new NoSuchEntityException(__('Customer Id is required.'));
        }
        $result = null;
        $phoneData = $this->phoneModelFactory->create()->getCollection()
                            ->addFieldToFilter("customer_id", (int)$customerId)
                            ->getFirstItem();
        if ($phoneData) {
            $result = $this->getPhoneResultsDataModel($phoneData->getData());
        }
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getById($phoneId) 
    {
        $result = null;
        $phoneData = $this->phoneModelFactory->create()->load((int)$phoneId);
        if ($phoneData) {
            $result = $this->getPhoneResultsDataModel($phoneData->getData());
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function saveCustomerPhone($customerId, string $phone, string $country_code, $verify_otp = true) 
    {
        if (!$customerId) {
            throw new NoSuchEntityException(__('Customer is required.'));
        }

        if (!$phone) {
            throw new NoSuchEntityException(__('Phone Number is required.'));
        }

        $phoneData = $this->phoneModelFactory->create();
        $phoneData->load($customerId, 'customer_id');
        $oldPhoneNumber = $phoneData->getPhone();

        $phoneData->setPhone($phone)
                 ->setCountryCode($country_code)
                 ->setCustomerId($customerId);

        $needVerify = false;
        if (!$oldPhoneNumber || ($phone !== $oldPhoneNumber)) {
            $phoneData->setVerified(0);
            $needVerify = true;
        }

        try {
            $phoneData->save();
            $customer = $this->customerRepositoryInterface->getById($customerId);
            $customer->setCustomAttribute(InstallData::PHONE_NUMBER, $phone);
            $this->customerRepositoryInterface->save($customer);
            if ($verify_otp && $needVerify) {
                $otpData = $this->getOtpResultsDataModel([
                    "phone" => $phoneData->getPhone(),
                    "country_code" => $phoneData->getCountryCode(),
                    "customer_id" => $phoneData->getCustomerId()
                ]);
                $this->sendOtp($otpData);
            }
            return $this->getPhoneResultsDataModel($phoneData->getData());
        }catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new NoSuchEntityException(__('We can not process the action at now, please try again!'));
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getOtp($customerId, string $phone) 
    {
        if (!$phone) {
            throw new NoSuchEntityException(__('Phone Number is required.'));
        }
        $result = null;
        $otpData = $this->otpModelFactory->create()->getCollection()
                            ->addFieldToFilter("phone", $phone)
                            ->getFirstItem();
        if ($otpData) {
            $result = $this->getOtpResultsDataModel($otpData->getData());
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone(string $phone) 
    {
        if (!$phone) {
            return null;
        }
        $result = null;
        $phoneData = $this->phoneModelFactory->create()->getCollection()
                            ->addFieldToFilter("phone", $phone)
                            ->getFirstItem();
        if ($phoneData) {
            $result = $this->getPhoneResultsDataModel($phoneData->getData());
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function sendOtp(\Lof\SmsNotification\Api\Data\OtpInterface $otpData, $isLogin = false) 
    {
        if (!$otpData->getPhone()) {
            throw new NoSuchEntityException(__('Phone Number is required.'));
        }
        $length = $this->helperData->getConfig('sms_otp/opt_length');
        $format = $this->helperData->getConfig('sms_otp/opt_format');
        $digitCode = $this->helperData->randomString($length, $format);
        $otpData->setDigitCode($digitCode);

        if ($this->smsLog->isBlacklist($otpData->getPhone())) {
            throw new NoSuchEntityException(__('Phone Number is in Blacklist.'));
        }

        if (!$isLogin) {
            $collection = $this->phoneModelFactory->create()->getCollection()
                                ->addFieldToFilter('phone', $otpData->getPhone());
            if ($collection->getSize() > 0) {
                throw new NoSuchEntityException(__('Phone number already exists, please select another one.'));
            }
        }
        $otpModel = $this->otpModelFactory->create()->load($otpData->getPhone(), 'phone');
        if ($otpModel->getId()) {
            $otpModel->setDigitCode($digitCode)
                    ->setResend(1);
        } else {
            $otpModel->setPhone($otpData->getPhone())
                    ->setDigitCode($digitCode)
                    ->setCountryCode($otpData->getCountryCode());
        }

        try {
            $otpModel->save();

            //Verify delay time before send otp sms message
            $numberSecondsValid = $this->helperData->getConfig('sms_otp/opt_resend');
            $numberSecondsValid = $numberSecondsValid?(int)$numberSecondsValid:30;
            if ($this->helperDateTime->verifyDelayTime($otpModel->getUpdatedAt(), $numberSecondsValid)) {
                if($this->helperData->getConfig('sms_otp/opt_message')) {
                    $message = $this->helperData->getSmsOtp($digitCode);
                } else {
                    $message = 'Your OTP code to active here: '.$digitCode;
                }
                $send_to = 'customer';
                $this->sendSms->send($otpModel->getPhone(),$message,$send_to);
            }
            return $this->getOtpResultsDataModel($otpModel->getData());
        }catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new NoSuchEntityException(__('We can not process the action at now, please try again!'));
        }
        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function verifyOtp(string $phone, string $otp, $isLogin = false)
    {
        if (!$phone || !$otp) {
            throw new NoSuchEntityException(__('Phone Number and Otp number is required.'));
        }

        try {
            $model = $this->otpModelFactory->create();
            $model->load($phone, 'phone');
            $responseData = [];
            if ($model->getId()) {
                $phoneData = $this->getPhone($phone);
                if ($isLogin && (!$phoneData || ($phoneData && !$phoneData->getVerified()))) {
                    throw new NoSuchEntityException(__('The phone is not exists or was not verified before!'));
                }
                $responseData['phone'] = $phone;
                $numberSecondsValid = $this->helperData->getConfig('sms_otp/opt_resend');
                $numberSecondsValid = $numberSecondsValid?(int)$numberSecondsValid:30;
                $isExpired = !$this->helperDateTime->verifyDelayTime($model->getUpdatedAt(), $numberSecondsValid);
                if($otp == $model->getData('digit_code') && !$isExpired) {
                    $responseData['message'] = __('Verified OTP sucessfully.');
                    $responseData['verified'] = true;
                    if (!$isLogin) {
                        //Save phone is verified
                        if ($phoneData && !$phoneData->getVerified()) {
                            $phoneModel = $this->phoneModelFactory->create()->load((int)$phoneData->getPhoneId());
                            $phoneModel->setVerified(1);
                            $phoneModel->save();
                        }
                    }
                } elseif ($otp == $model->getData('digit_code') && $isExpired) {
                    $responseData['message'] = __('The OTP code is expired. Please try to send new OTP code again.');
                    $responseData['verified'] = false;
                } else {
                    $responseData['message'] = __('The OTP code is not valid.');
                    $responseData['verified'] = false;
                }
                return $this->getOtpVerifyResultsDataModel($responseData);
            }
        }catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new NoSuchEntityException(__('We can not verify OTP at now, please try again!'));
        }
        return 0;
    }

    /**
     * Get shipping rate result data model
     * 
     * @param array
     * @return Object
     */
    public function getOtpResultsDataModel($arrayData = [])
    {
        $itemDataObject = $this->otpInterfaceFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $itemDataObject,
            $arrayData,
            OtpInterface::class
        );
        
        return $itemDataObject;
    }

    /**
     * Get shipping rate result data model
     * 
     * @param array
     * @return Object
     */
    public function getPhoneResultsDataModel($arrayData = [])
    {
        $itemDataObject = $this->phoneInterfaceFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $itemDataObject,
            $arrayData,
            PhoneInterface::class
        );
        
        return $itemDataObject;
    }

    /**
     * Get shipping rate result data model
     * 
     * @param array
     * @return Object
     */
    public function getOtpVerifyResultsDataModel($arrayData = [])
    {
        $itemDataObject = $this->otpVerifyResultInterfaceFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $itemDataObject,
            $arrayData,
            VerifyOtpResultInterface::class
        );
        
        return $itemDataObject;
    }

}