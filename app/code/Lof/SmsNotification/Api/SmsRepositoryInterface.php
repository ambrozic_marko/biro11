<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2021 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Api;

/**
 * @api
 */
interface SmsRepositoryInterface
{
    /**
     * Get Sms Phone
     *
     * @param string|int $customerId
     * @return \Lof\SmsNotification\Api\Data\PhoneInterface|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($customerId);

    /**
     * Get Sms Phone by Phone Number
     *
     * @param string $phone
     * @return \Lof\SmsNotification\Api\Data\PhoneInterface|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPhone(string $phone);

    /**
     * Get Sms Phone by Id
     *
     * @param int $phoneId
     * @return \Lof\SmsNotification\Api\Data\PhoneInterface|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($phoneId);

    /**
     * Save Sms Customer Phone
     *
     * @param int|string $customerId
     * @param string $phone
     * @param string $country_code
     * @param bool|int $verify_otp
     * @return \Lof\SmsNotification\Api\Data\PhoneInterface|int|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function saveCustomerPhone($customerId, string $phone, string $country_code, $verify_otp = true);

    /**
     * Get Sms Otp
     *
     * @param string|int $customerId
     * @param string $phone
     * @return \Lof\SmsNotification\Api\Data\OtpInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getOtp($customerId, string $phone);

    /**
     * Send Sms Otp
     *
     * @param \Lof\SmsNotification\Api\Data\OtpInterface $otpData
     * @param bool|int $isLogin
     * @return \Lof\SmsNotification\Api\Data\OtpInterface|string|int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function sendOtp(\Lof\SmsNotification\Api\Data\OtpInterface $otpData, $isLogin = false);

    /**
     * Verify Sms Otp
     *
     * @param string $phone
     * @param string $otp
     * @param bool|int $isLogin
     * @return \Lof\SmsNotification\Api\Data\VerifyOtpResultInterface|string|int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function verifyOtp(string $phone, string $otp, $isLogin = false);
}
