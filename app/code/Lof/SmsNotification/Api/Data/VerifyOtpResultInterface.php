<?php
/**
 * Copyright © Landofcoder All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Lof\SmsNotification\Api\Data;

interface VerifyOtpResultInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const VERIFIED = 'verified';
    const MESSAGE = 'message';
    const PHONE = 'phone';

    /**
     * Get verified
     * @return int|bool|null
     */
    public function getVerified();

    /**
     * Set verified
     * @param int|bool $verified
     * @return \Lof\SmsNotification\VerifyOtpResultInterface
     */
    public function setVerified($verified);

    /**
     * Get phone
     * @return string|null
     */
    public function getPhone();

    /**
     * Set phone
     * @param string $phone
     * @return \Lof\SmsNotification\VerifyOtpResultInterface
     */
    public function setPhone($phone);

    /**
     * Get message
     * @return string|null
     */
    public function getMessage();

    /**
     * Set message
     * @param string $message
     * @return \Lof\SmsNotification\VerifyOtpResultInterface
     */
    public function setMessage($message);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Lof\SmsNotification\Api\Data\VerifyOtpResultExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Lof\SmsNotification\Api\Data\VerifyOtpResultExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Lof\SmsNotification\Api\Data\VerifyOtpResultExtensionInterface $extensionAttributes
    );
}