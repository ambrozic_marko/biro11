<?php
/**
 * Copyright © Landofcoder All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Lof\SmsNotification\Api\Data;

interface PhoneInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const PHONE_ID = 'phone_id';
    const CUSTOMER_ID = 'customer_id';
    const PHONE = 'phone';
    const COUNTRY_CODE = 'country_code';
    const VERIFIED = 'verified';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get phone_id
     * @return int|null
     */
    public function getPhoneId();

    /**
     * Set phone_id
     * @param int $phone_id
     * @return \Lof\SmsNotification\PhoneInterface
     */
    public function setPhoneId($phone_id);

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param int $customer_id
     * @return \Lof\SmsNotification\PhoneInterface
     */
    public function setCustomerId($customer_id);

    /**
     * Get phone
     * @return string|null
     */
    public function getPhone();

    /**
     * Set phone
     * @param string $phone
     * @return \Lof\SmsNotification\PhoneInterface
     */
    public function setPhone($phone);

    /**
     * Get country_code
     * @return string|null
     */
    public function getCountryCode();

    /**
     * Set country_code
     * @param string $country_code
     * @return \Lof\SmsNotification\PhoneInterface
     */
    public function setCountryCode($country_code);

    /**
     * Get verified
     * @return int|null
     */
    public function getVerified();

    /**
     * Set verified
     * @param int $verified
     * @return \Lof\SmsNotification\PhoneInterface
     */
    public function setVerified($verified);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $created_at
     * @return \Lof\SmsNotification\PhoneInterface
     */
    public function setCreatedAt($created_at);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updated_at
     * @return \Lof\SmsNotification\PhoneInterface
     */
    public function setUpdatedAt($updated_at);
    
    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Lof\SmsNotification\Api\Data\PhoneExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Lof\SmsNotification\Api\Data\PhoneExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Lof\SmsNotification\Api\Data\PhoneExtensionInterface $extensionAttributes
    );
}