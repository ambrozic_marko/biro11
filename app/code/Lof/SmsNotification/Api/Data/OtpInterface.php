<?php
/**
 * Copyright © Landofcoder All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Lof\SmsNotification\Api\Data;

interface OtpInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const OTP_ID = 'otp_id';
    const CUSTOMER_ID = 'customer_id';
    const DIGIT_CODE = 'digit_code';
    const PHONE = 'phone';
    const COUNTRY_CODE = 'country_code';
    const RESEND = 'resend';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get otp_id
     * @return int|null
     */
    public function getOtpId();

    /**
     * Set otp_id
     * @param int $otp_id
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setOtpId($otp_id);


    /**
     * Get customer_id
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param int $customer_id
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setCustomerId($customer_id);

    /**
     * Get digit_code
     * @return string|null
     */
    public function getDigitCode();

    /**
     * Set digit_code
     * @param int $digit_code
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setDigitCode($digit_code);

    /**
     * Get phone
     * @return string|null
     */
    public function getPhone();

    /**
     * Set phone
     * @param string $phone
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setPhone($phone);

    /**
     * Get country_code
     * @return string|null
     */
    public function getCountryCode();

    /**
     * Set country_code
     * @param string $country_code
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setCountryCode($country_code);

    /**
     * Get resend
     * @return int|null
     */
    public function getResend();

    /**
     * Set resend
     * @param int $resend
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setResend($resend);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $created_at
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setCreatedAt($created_at);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updated_at
     * @return \Lof\SmsNotification\OtpInterface
     */
    public function setUpdatedAt($updated_at);
    
    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Lof\SmsNotification\Api\Data\OtpExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Lof\SmsNotification\Api\Data\OtpExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Lof\SmsNotification\Api\Data\OtpExtensionInterface $extensionAttributes
    );
}