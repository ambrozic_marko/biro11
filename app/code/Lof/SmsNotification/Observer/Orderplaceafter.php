<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */


namespace Lof\SmsNotification\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;

class Orderplaceafter implements ObserverInterface
{
	protected $sendsms;
	protected $orderFactory;
	protected $phone;
	protected $customerFactory;
	protected $helperData;

	public function __construct(
	  \Lof\SmsNotification\Model\SendSms $sendsms,
	  \Lof\SmsNotification\Model\Phone $phone,
	  \Magento\Sales\Model\OrderFactory $orderFactory,
	  \Magento\Customer\Model\CustomerFactory $customerFactory,
	  \Lof\SmsNotification\Helper\Data $helperData
   )
   {
	   $this->orderFactory = $orderFactory;
	   $this->sendsms = $sendsms;
	   $this->phone = $phone;
	   $this->customerFactory = $customerFactory;
	   $this->helperData = $helperData;
   }

   public function execute(\Magento\Framework\Event\Observer $observer)
   {
      	$order_id = $observer->getData('order_ids');
        $order = $this->orderFactory->create()->load($order_id[0]);
		$customerid = $order->getCustomerId();
		//$customer = $this->customerFactory->create()->load($customerid);
		$helperData = $this->helperData;

		$order_information = $order->loadByIncrementId($order_id[0]);
		$orderIncrementId = $order_information->getIncrementId();
		$orderTotal =  $order_information->getGrandTotal();
		$orderCustomerFirstName = $order_information->getCustomerFirstname();
		$orderCustomerLastName = $order_information->getCustomerLastname();
	 	$orderCustomerEmail =  $order_information->getCustomerEmail();

	 	$storeName = $helperData->getStoreName($order->getStoreId());
        $phone = $this->phone->load($customerid,'customer_id');
        if($phone->getData() && count($phone->getData()) > 0) {
        	$mobile = $phone->getPhone();
        } elseif($shippingAddress = $order_information->getShippingAddress()) {
        	$mobile = $shippingAddress->getData('telephone');
			$country_id = $shippingAddress->getData('country_id');
			$country_phone_code = $helperData->getCountryPhoneCode($country_id);
			if($country_phone_code){
				$mobile = $country_phone_code.$mobile;
			}
        } else {
			$mobile = "";
		}

		if($helperData->getConfig('sms_customer/enable_sms_new_order') && $mobile){
	   		$message = $helperData->getOrderPlaceMessageForUser($orderIncrementId,$mobile,$orderTotal,$orderCustomerFirstName,$orderCustomerLastName,$orderCustomerEmail,$storeName);
	   		$send_to = 'customer';
			$temp = $this->sendsms->send($mobile,$message,$send_to);
		}
		if($helperData->getConfig('sms_admin/enable_sms_new_order')){
			$adminMobile = $helperData->getConfig('sms_settings/admin_phone');
			$send_to = 'admin';
	   		$message = $helperData->getOrderPlaceMessageForAdmin($orderIncrementId,$adminMobile,$orderTotal,$orderCustomerFirstName,$orderCustomerLastName,$orderCustomerEmail,$storeName) ;
			$temp = $this->sendsms->send($adminMobile,$message,$send_to);
		}
	 return true;
   }
}
