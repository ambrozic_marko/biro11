<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

 
namespace Lof\SmsNotification\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Data\Customer;
//use Lof\SmsNotification\Model\OtpFactory;

class CustomerRegister implements ObserverInterface
{
	protected $helperData;
	protected $repository;
	//protected $_modelOtpFactory;
	protected $customerAddressFactory;
	protected $dataObjectProcessor;
	protected $sendsms;
	protected $phoneFactory;
	  
	  
	public function __construct(
	  \Lof\SmsNotification\Model\SendSms $sendsms,
	  \Lof\SmsNotification\Helper\Data $helperData,
	  \Lof\SmsNotification\Model\PhoneFactory $phoneFactory
	  //OtpFactory $modelOtpFactory
   )
   {
	   $this->helperData = $helperData;
	   $this->phoneFactory = $phoneFactory;
	   //$this->_modelOtpFactory = $modelOtpFactory;
	   $this->sendsms = $sendsms;
	     
   }
 
   public function execute(\Magento\Framework\Event\Observer $observer)
   {
		$helperData = $this->helperData;
		if($helperData->getConfig('general_settings/enable_module') != 1) {
			return;
		}
	  	$model = $this->phoneFactory->create();
	  	$accountController = $observer->getData('account_controller');
	  	$customer = $observer->getData('customer');
	  	$mobilenumber = $accountController->getRequest()->getParam('mobile_number');
	  	$country_code = $accountController->getRequest()->getParam('country_code');
	  	$firstname = $customer->getFirstName();
	  	$lastname = $customer->getLastName();
	  	$email = $customer->getEmail();
	  	$storeName = $helperData->getStoreName();
	  	$storeUrl = $helperData->getStoreUrl();	  
  
		$model->setPhone($mobilenumber)->setCustomerId($customer->getId())->setCountryCode($country_code)->save();
		if($helperData->getConfig('sms_customer/enable_sms_register_customer')){
			$message = $helperData->getSignupMessageForUser($firstname,$lastname,$email,$storeName,$mobilenumber,$storeUrl); 	
			$send_to = 'customer';
			//call api for send SMS from Helper Data		
			$temp = $this->sendsms->send($mobilenumber,$message,$send_to);	
		}
		if($helperData->getConfig('sms_admin/enable_sms_register_customer')){
			$adminMobile = $helperData->getConfig('sms_settings/admin_phone');
			$message = $helperData->getSignupMessageForAdmin($firstname,$lastname,$email,$storeName,$adminMobile,$storeUrl); 
			$send_to = 'admin';
			//call api for send SMS from Helper Data
			$temp = $this->sendsms->send($mobilenumber,$message,$send_to);	
		}
	 return true;
   }
}