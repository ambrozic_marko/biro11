<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer\Adminhtml;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;

class Invoicecreate implements ObserverInterface
{
	protected $helperData;
	protected $sendsms;
	protected $customerFactory;
	protected $phoneFactory;

	public function __construct(
	\Lof\SmsNotification\Model\SendSms $sendsms,
	\Lof\SmsNotification\Helper\Data $helperData,
	\Magento\Customer\Model\CustomerFactory $customerFactory,
	\Lof\SmsNotification\Model\PhoneFactory $phoneFactory
	)
	{
		$this->helperData = $helperData;
		$this->sendsms = $sendsms;
		$this->customerFactory = $customerFactory;
		$this->phoneFactory = $phoneFactory;
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$helperData = $this->helperData;
		if($helperData->isEnabled()){
			$invoice = $observer->getEvent()->getInvoice();
			$order = $invoice->getOrder();
			if ($invoice->getState() == \Magento\Sales\Model\Order\Invoice::STATE_CANCELED) {
                return;
            }
			$customerid = $order->getCustomerId();
			//$customer = $this->customerFactory->create()>load($customerid);

			$storeName = $helperData->getStoreName($order->getStoreId());
			$orderCretaedAt = date("F j, Y",strtotime($order->getCreatedAt()));
			$orderTotal = number_format($order->getGrandTotal(), 2, '.', '');
			//$orderEmail = $order->getCustomerEmail();
			$orderId = $order->getIncrementId();
			$orderOldStatus = $order->getStatus();
			$orderNewStatus = "Processing";
			$phone = $this->phoneFactory->create()->load($customerid, 'customer_id');
			if($phone->getData() && count($phone->getData()) > 0) {
				$mobilenumber = $phone->getPhone();
			}elseif($shippingAddress = $order->getShippingAddress()) {
				$mobilenumber = $shippingAddress->getTelephone();
				$country_id = $shippingAddress->getData('country_id');
				$country_phone_code = $helperData->getCountryPhoneCode($country_id);
				if($country_phone_code){
					$mobilenumber = $country_phone_code.$mobilenumber;
				}
			} else {
				$mobilenumber = '';
			}
			$firstName = $order->getCustomerFirstname();
			$lastName = $order->getCustomerLastname();
			$send_to = 'customer';

			if($helperData->getConfig('sms_customer/enable_new_invoice') && $mobilenumber) {
				$message = $helperData->getInvoiceMessageForUser($orderCretaedAt,$orderTotal,$orderId,$orderOldStatus,$orderNewStatus,$storeName,$firstName,$lastName) ;
				$temp = $this->sendsms->send($mobilenumber,$message,$send_to);
			}

			if($helperData->getConfig('sms_admin/enable_new_invoice')){
				$adminMobile = $helperData->getConfig('sms_settings/admin_phone');
				$send_to = 'admin';
				$message = $helperData->getInvoiceMessageForAdmin($orderCretaedAt,$orderTotal,$orderId,$orderOldStatus,$orderNewStatus,$storeName,$firstName,$lastName) ;
				$temp = $this->sendsms->send($adminMobile,$message,$send_to);
			}

			return true;
		}
	}
}
