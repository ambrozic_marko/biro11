<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\SmsNotification\Observer\Adminhtml;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;

class CreditmemoCommitAfter implements ObserverInterface
{
	protected $helperData;
	protected $sendsms;
	protected $orderFactory;
	protected $customerFactory;
	protected $phoneFactory;
	/**
     * @var \Magento\Sales\Model\Order\Creditmemo
     */
    protected $creditmemo;

	public function __construct(
	  	\Lof\SmsNotification\Model\SendSms $sendsms,
		\Lof\SmsNotification\Helper\Data $helperData,
		\Magento\Customer\Model\CustomerFactory $customerFactory,
		\Magento\Sales\Model\OrderFactory $orderFactory,
		\Lof\SmsNotification\Model\PhoneFactory $phoneFactory,
		\Magento\Sales\Model\Order\Creditmemo $creditmemo
	){
		$this->helperData = $helperData;
		$this->sendsms = $sendsms;
		$this->customerFactory = $customerFactory;
		$this->orderFactory = $orderFactory;
		$this->phoneFactory = $phoneFactory;
		$this->creditmemo = $creditmemo;
	}
	 
   public function execute(\Magento\Framework\Event\Observer $observer)
   {
		$helperData = $this->helperData;
		if($helperData->isEnabled()){
			$creditmemo = $observer->getEvent()->getCreditmemo();
            //$creditmemo = $this->creditmemo->getCollection()->addFieldToFilter('order_id', $orderId)->getFirstItem();
			if ($creditmemo) {
                $order = $creditmemo->getOrder();
                $orderId = $order->getIncrementId();
				//$order = $this->orderFactory->create()->load($orderId);
				$order_information = $order->loadByIncrementId($orderId);
				$customerid = $creditmemo->getCustomerId();
				//$customer = $this->customerFactory->create()->load($customerid);
				$creditMemoCreatedAt = $creditmemo->getCreatedAt();
				$creditMemoId = $creditmemo->getIncrementId();
				
				$orderTotal = $order_information->getGrandTotal();
				$orderIncrementId = $order_information->getIncrementId();
				$email = $order_information->getCustomerEmail();
				$orderCreatedAt = $order_information->getCreatedAt();
				$phone = $this->phoneFactory->create()->load($customerid, 'customer_id');
				if($phone->getData() && count($phone->getData()) > 0) {
					$mobilenumber = $phone->getPhone();
				}elseif($shippingAddress = $order->getShippingAddress()) {
					$mobilenumber = $shippingAddress->getTelephone();
					$country_id = $shippingAddress->getData('country_id');
					$country_phone_code = $helperData->getCountryPhoneCode($country_id);
					if($country_phone_code){
						$mobilenumber = $country_phone_code.$mobilenumber;
					}
				} else {
					$mobilenumber = '';
				}	
				$firstName = $order_information->getCustomerFirstname();
				$lastName = $order_information->getCustomerLastname();
				$send_to = 'customer';
			
				if($helperData->getConfig('sms_customer/enable_new_credit_memo') && $mobilenumber){
					$message = $helperData->getCreditMemoMessageForUser($creditMemoId,$creditMemoCreatedAt,$orderTotal,$orderIncrementId,$email,$orderCreatedAt,$firstName,$lastName);	
					$temp = $this->sendsms->send($mobilenumber,$message,$send_to); 
				}
				if($helperData->getConfig('sms_admin/enable_new_credit_memo')){
					$adminMobile = $helperData->getConfig('sms_settings/admin_phone');
					$send_to = 'admin';
					$message = $helperData->getCreditMemoMessageForAdmin($creditMemoId,$creditMemoCreatedAt,$orderTotal,$orderIncrementId,$email,$orderCreatedAt,$firstName,$lastName);	
					$temp = $this->sendsms->send($adminMobile,$message,$send_to);
				}
				return true;
			}
		}
   }
}