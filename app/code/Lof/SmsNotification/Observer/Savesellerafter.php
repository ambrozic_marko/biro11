<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SmsNotification
 * @copyright  Copyright (c) 2017 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

 
namespace Lof\SmsNotification\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;

class Savesellerafter implements ObserverInterface
{
	protected $helperData;
	protected $_modelOtpFactory;
	protected $sendsms;

	public function __construct(
	\Lof\SmsNotification\Model\SendSms $sendsms,
	\Lof\SmsNotification\Helper\Data $helperData
	)
	{
		$this->helperData = $helperData;
		$this->sendsms = $sendsms;
	}
	 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		/** @var Http $request */
		$helperData = $this->helperData;
		$accountController = $observer->getData('account_controller');
		$customer = $observer->getSeller();
		$mobilenumber = $accountController->getRequest()->getParam('mobile_number');
		$firstname = $customer->getFirstName();
		$lastname = $customer->getLastName();
		$email = $customer->getEmail();
		$storeName = $helperData->getStoreName();
		$storeUrl = $helperData->getStoreUrl();

		if($helperData->getConfig('sms_seller/enable_sms_register_seller')){
			$message = $helperData->getSignupMessageForSeller($firstname,$lastname,$email,$storeName,$mobilenumber,$storeUrl);
			$send_to = 'seller';
			//call api for send SMS from Helper Data		
			$temp = $this->sendsms->send($mobilenumber,$message,$send_to);
		}
		if($helperData->getConfig('sms_admin/enable_sms_register_seller')){
			$adminMobile = $helperData->getConfig('sms_settings/admin_phone');
			$message = $helperData->getSignupSellerMessageForAdmin($firstname,$lastname,$email,$storeName,$mobilenumber,$storeUrl);
			$send_to = 'admin';
			//call api for send SMS from Helper Data
			$temp = $this->sendsms->send($adminMobile,$message,$send_to);
		}
	 	return true;
    }
}