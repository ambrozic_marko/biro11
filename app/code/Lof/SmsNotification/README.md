# module-sms-notification-magento2
Magento 2 Sms notification https://landofcoder.com/magento-2-sms-notification-extension.html/

# SETUP GUIDE

## Step 1. The extension require library extension, please setup the library before
### How to setup library?

- Please setup the library via composer:
```
composer require landofcoder/smsnotification-lib
```

- The repo of library at here: https://github.com/landofcoder/smsnotification-lib
- Packagist url: https://packagist.org/packages/landofcoder/smsnotification-lib

## Step 2. Setup the module Sms Notification
- Upload module file to webroot folder in your server via FTP client.
- Run Magento 2 Command in SSH terminal:

```
php bin/magento setup:upgrade --keep-generated

php bin/magento setup:static-content:deploy -f

php bin/magento cache:clean
```