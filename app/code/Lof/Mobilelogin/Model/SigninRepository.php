<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_Mobilelogin
 * @copyright  Copyright (c) 2021 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\Mobilelogin\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Integration\Api\CustomerTokenServiceInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Customer\Model\Config\Share as ConfigShare;
use Lof\Mobilelogin\Api\SigninRepositoryInterface;
use Lof\SmsNotification\Api\SmsRepositoryInterface;
use Lof\Mobilelogin\Helper\Data as HelperData;

/**
 * Class SigninRepository
 * Handle login using the phone number instead of the email as default.
 */
class SigninRepository implements SigninRepositoryInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Data
     */
    private $helperData;

    /**
     * @var SmsRepositoryInterface
     */
    protected $smsRepositoryInterface;

    /**
     * @var CustomerTokenServiceInterface
     */
    protected $customerTokenServiceInterface;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param StoreManagerInterface $storeManager
     * @param HelperData $helperData
     * @param SmsRepositoryInterface $smsRepositoryInterface
     * @param CustomerTokenServiceInterface $customerTokenServiceInterface
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        StoreManagerInterface $storeManager,
        HelperData $helperData,
        SmsRepositoryInterface $smsRepositoryInterface,
        CustomerTokenServiceInterface $customerTokenServiceInterface
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->storeManager = $storeManager;
        $this->helperData = $helperData;
        $this->smsRepositoryInterface = $smsRepositoryInterface;
        $this->customerTokenServiceInterface = $customerTokenServiceInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function loginWithOtp(string $phone, string $otp)
    {
        if (!$this->helperData->isLoginWithMobilePhone()) {
            throw new NoSuchEntityException(__('Dont support login with phone number. Please try to login with email and password.'));
        }
        $result = $this->smsRepositoryInterface->verifyOtp($phone, $otp, true);

        if (!$result || ($result && !$result->getVerified())) {
            throw new NoSuchEntityException(__('Phone number or OTP is invalid.'));
        }
        $websiteIdFilter[] = $this->filterWebsiteShare();

        // Add customer attribute filter
        $customerFilter[] = $this->filterBuilder
            ->setField(HelperData::PHONE_NUMBER)
            ->setConditionType('eq')
            ->setValue($phone)
            ->create();

        // Build search criteria
        $searchCriteriaBuilder = $this->searchCriteriaBuilder->addFilters($customerFilter);
        if (!empty($websiteIdFilter)) {
            $searchCriteriaBuilder->addFilters($websiteIdFilter);
        }
        $searchCriteria = $searchCriteriaBuilder->create();

        try {
            // Retrieve customer collection.
            $customerData = null;
            $collection = $this->customerRepository->getList($searchCriteria);
            if ($collection->getTotalCount() == 1) {
                // Return first occurrence.
                $accounts = $collection->getItems();
                $customerData = reset($accounts);
                // process login get token
                $otpPassword = $this->helperData->generateOtpPassword($otp);
                return $this->customerTokenServiceInterface->createCustomerAccessToken($customerData->getEmail(), $otpPassword);
            }
        }catch (NoSuchEntityException $e) {
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }
        return "";
    }


    /**
     * Add website filter if customer accounts are shared per website.
     *
     * @return FilterBuilder|boolean
     */
    private function filterWebsiteShare()
    {
        if ($this->helperData->getCustomerShareScope() == ConfigShare::SHARE_WEBSITE) {
            return $this->filterBuilder
                ->setField('website_id')
                ->setConditionType('eq')
                ->setValue($this->storeManager->getStore()->getWebsiteId())
                ->create();
        }
        return false;
    }
}
