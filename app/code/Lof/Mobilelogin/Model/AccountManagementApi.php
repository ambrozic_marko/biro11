<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_Mobilelogin
 * @copyright  Copyright (c) 2021 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\Mobilelogin\Model;

use Magento\Customer\Api\Data\CustomerInterface;

/**
 * Account Management service implementation for external API access.
 * Handle various customer account actions.
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class AccountManagementApi extends AccountManagement
{
    /**
     * @inheritDoc
     *
     * Override createAccount method to unset confirmation attribute for security purposes.
     */
    public function createAccount(CustomerInterface $customer, $password = null, $redirectUrl = '')
    {
        $customer = parent::createAccount($customer, $password, $redirectUrl);
        $customer->setConfirmation(null);

        return $customer;
    }
}
