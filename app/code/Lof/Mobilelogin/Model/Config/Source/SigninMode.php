<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_Mobilelogin
 * @copyright  Copyright (c) 2019 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\Mobilelogin\Model\Config\Source;

/**
 * Class SigninMode
 * Define options to the login process.
 */
class SigninMode
{
    /**
     * @var int Value using phone to sign in.
     */
    const TYPE_PHONE = 1;

    /**
     * @var int Value using phone or email to sign in.
     */
    const TYPE_BOTH_OR = 2;

    /**
     * Retrieve possible sign in options.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            self::TYPE_PHONE => __('Sign in Only With Phone and Password'),
            self::TYPE_BOTH_OR => __('Sign in With Phone/Email and Password')
        ];
    }
}
