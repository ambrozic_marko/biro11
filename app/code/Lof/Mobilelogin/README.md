# module-mobilelogin-magento2

Magento 2 Mobile Login, SMS notification: https://landofcoder.com/magento-2-sms-notification-extension.html/

## Features
- Support login with mobile phone number and password
- Support login with mobile phone number and OTP code
- Support REST API get customer token with Mobile phone number and OTP

## Module settings

Go to admin > stores > configuration > Landofcoder > SMS notification > OTP Verify Phone Number, and Mobile Log Settings

![module_settings](./assets/mobile_login_with_otp_settings.png)

## Frontend

1. Login form with mobile phone or email and password:

![login_phone_pass](./assets/login_with_phone_and_pass.png)

2. Login with phone number and OTP:

- Input Phone Number and get OTP:
![input_phone_get_otp](./assets/login_with_phone_and_otp.png)

- Input OTP and Login:
![input_otp_and_login](./assets/login_with_otp_code.png)

## REST Api
```
https://yoursitedomain/rest/V1/lofmobilelogin/customer/token
```
Body Data:
```
{
	"otp": "699666",
    "phone": "+18123456789"
}
```

![REST_API](./assets/rest_api_get_customer_token.png)