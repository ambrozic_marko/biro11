<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_Mobilelogin
 * @copyright  Copyright (c) 2021 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Mobilelogin\Api;

/**
 * @api
 */
interface SigninRepositoryInterface
{
    /**
     * Login with phone number and OTP.
     *
     * @param string $phone
     * @param string $otp
     * @return string Token created
     * @throws \Magento\Framework\Exception\AuthenticationException|\Magento\Framework\Exception\LocalizedException
     */
    public function loginWithOtp(string $phone, string $otp);
}
