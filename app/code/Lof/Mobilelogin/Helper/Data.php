<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_Mobilelogin
 * @copyright  Copyright (c) 2019 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\Mobilelogin\Helper;

use Magento\Store\Model\ScopeInterface;
use Lof\Mobilelogin\Model\Config\Source\SigninMode;

class Data extends \Lof\SmsNotification\Helper\Data
{
  const PHONE_NUMBER = 'mobilenumber';
  const OTP_PREFIX = "OTP";
  const OTP_SUFFIX = "LOF";
  /**
   * is active
   * 
   * @return string|int|bool
   */
  public function isActive()
  {
    return $this->getConfig("mobile_login/enabled");
  }

  /**
   * get signin mode
   * 
   * @return string|int
   */
  public function getSigninMode()
  {
    return $this->getConfig("mobile_login/mode");
  }

  /**
   * get enabled otp
   * 
   * @return bool|int
   */
  public function getEnableOtp()
  {
    return $this->getConfig("mobile_login/enabled_otp");
  }

  /**
   * check is login with mobile phone
   * 
   * @return boolean|int
   */
  public function isLoginWithMobilePhone()
  {
      switch ($this->getSigninMode()) {
        case SigninMode::TYPE_PHONE:
            return true;
        case SigninMode::TYPE_BOTH_OR:
            return true;
        default:
            return false;
      }
  }

  /**
   * Get if customer accounts are shared per website.
   *
   * @see \Magento\Customer\Model\Config\Share
   * @param string|null $scopeCode
   * @return string
   */
  public function getCustomerShareScope($scopeCode = null)
  {
      return $this->scopeConfig->getValue(
          \Magento\Customer\Model\Config\Share::XML_PATH_CUSTOMER_ACCOUNT_SHARE,
          ScopeInterface::SCOPE_STORE,
          $scopeCode
      );
  }

  /**
   * Generate OTP password
   * 
   * @param string
   * @return string
   */
  public function generateOtpPassword($otp) 
  {
    return self::OTP_PREFIX.$otp.self::OTP_SUFFIX;
  }

  /**
   * Check Is OTP password
   * 
   * @param string
   * @return boolean|int
   */
  public function isOtpPassword($password) 
  {
    return (strpos($password, self::OTP_PREFIX) == 0 && strpos($password, self::OTP_SUFFIX) > 0)?true:false;
  }
}