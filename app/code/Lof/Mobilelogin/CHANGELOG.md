# Version 1.0.1 - 09/05/2021
- Fix bugs
- Refactor coding standard
- Support login with mobile phone and OTP
- Support login with mobile phone and password
- Support REST API login mobile phone and OTP