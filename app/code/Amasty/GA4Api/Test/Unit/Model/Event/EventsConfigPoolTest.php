<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Test\Unit\Model\Event;

use Amasty\GA4Api\Api\Event\EventConfigInterface;
use Amasty\GA4Api\Model\DataLayer\DataLayerStorageInterface;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use PHPUnit\Framework\TestCase;

class EventsConfigPoolTest extends TestCase
{
    public function testInitialization(): void
    {
        $configMock1 = $this->createConfiguredMock(EventConfigInterface::class, ['getName' => 'first']);
        $configMock2 = $this->createConfiguredMock(EventConfigInterface::class, ['getName' => 'second']);

        $pool = new EventsConfigPool(['test2' => $configMock2, 'test1' => $configMock1]);
        $configs = $pool->getAll();

        $this->assertEquals(['first', 'second'], array_keys($configs));
        $this->assertEquals($configMock1, $configs['first']);
        $this->assertEquals($configMock2, $configs['second']);
    }

    public function testWrongClassInitialization(): void
    {
        $wrongClassMock = $this->createMock(DataLayerStorageInterface::class);

        $this->expectException(\LogicException::class);
        new EventsConfigPool(['wrong' => $wrongClassMock]);
    }

    public function testGetByUsage(): void
    {
        $pool = new EventsConfigPool($this->getTestConfigsMocks());

        $datalayerPool = $pool->getByUsage(EventsConfigPool::USAGE_HEADER_DATALAYER);
        $configsNames = array_map(static fn($config) => $config->getName(), $datalayerPool);
        $this->assertEquals(['first', 'third'], $configsNames);
    }

    public function testGetByName(): void
    {
        $configs = $this->getTestConfigsMocks();

        $pool = new EventsConfigPool($configs);
        $this->assertEquals($configs[0], $pool->getByName('first'));
        $this->assertEquals($configs[1], $pool->getByName('second'));
        $this->assertEquals($configs[2], $pool->getByName('third'));
    }

    private function getTestConfigsMocks(): array
    {
        $configMock1 = $this->createConfiguredMock(
            EventConfigInterface::class,
            ['getName' => 'first', 'getUsageType' => EventsConfigPool::USAGE_HEADER_DATALAYER]
        );
        $configMock2 = $this->createConfiguredMock(
            EventConfigInterface::class,
            ['getName' => 'second', 'getUsageType' => EventsConfigPool::USAGE_REQUEST]
        );
        $configMock3 = $this->createConfiguredMock(
            EventConfigInterface::class,
            ['getName' => 'third', 'getUsageType' => EventsConfigPool::USAGE_HEADER_DATALAYER]
        );

        return [$configMock1, $configMock2, $configMock3];
    }
}
