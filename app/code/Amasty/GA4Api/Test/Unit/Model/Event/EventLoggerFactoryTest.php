<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Test\Unit\Model\Event;

use Amasty\GA4Api\Api\Event\EventConfigInterface;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Magento\Framework\ObjectManagerInterface;
use PHPUnit\Framework\TestCase;

class EventLoggerFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $loggerMock = $this->createMock(EventLoggerInterface::class);
        $class = get_class($loggerMock);
        $omMock = $this->createMock(ObjectManagerInterface::class);
        $omMock->expects($this->once())->method('create')->with($class)->willReturn($loggerMock);

        $factory = new EventLoggerFactory($omMock);
        $result = $factory->create($class);
        $this->assertEquals($loggerMock, $result);
    }

    public function testCreateWrongClass(): void
    {
        $wrongClass = $this->createMock(EventConfigInterface::class);
        $omMock = $this->createMock(ObjectManagerInterface::class);

        $factory = new EventLoggerFactory($omMock);
        $this->expectException(\LogicException::class);
        $factory->create(get_class($wrongClass));
    }
}
