<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Test\Unit\Model\Event\DataLayer;

use Amasty\GA4Api\Model\DataLayer\DataObjectStorage;
use PHPUnit\Framework\TestCase;

class DataObjectStorageTest extends TestCase
{
    public function testAddDataLayerOption(): void
    {
        $storage = new DataObjectStorage();
        $storage->addDataLayerOption('test_event', ['test_data']);

        $result = $storage->getData();
        $this->assertNull($result[0]['ecommerce']);
        $this->assertEquals('test_event', $result[1]['event']);
        $this->assertEquals(['test_data'], $result[1]['ecommerce']);
    }

    public function testAddDataLayerOptionMultiple(): void
    {
        $storage = new DataObjectStorage();
        $storage->addDataLayerOption('test_event1', ['test_data1']);
        $storage->addDataLayerOption('test_event2', ['test_data2']);

        $result = $storage->getData();

        $this->assertNull($result[2]['ecommerce']);
        $this->assertEquals('test_event2', $result[3]['event']);
        $this->assertEquals(['test_data2'], $result[3]['ecommerce']);
    }

    public function testAddDataLayerOptionAdditionalData(): void
    {
        $storage = new DataObjectStorage();
        $storage->addDataLayerOption('test_event', [], ['add_data_key' => 'add_data_value']);

        $result = $storage->getData();
        $this->assertEquals('add_data_value', $result[0]['add_data_key']);
    }

    public function testGetDataLayerOptionsByEvent(): void
    {
        $storage = new DataObjectStorage();
        $storage->addDataLayerOption('test_event1', ['test_data1']);
        $storage->addDataLayerOption('test_event2', ['test_data2']);

        $expectedArray = [
            'event' => 'test_event2',
            'ecommerce' => ['test_data2']
        ];
        $this->assertEquals($expectedArray, $storage->getDataLayerOptions('test_event2'));
    }
}
