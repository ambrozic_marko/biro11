<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Test\Unit\Model\Event\Meta;

use PHPUnit\Framework\TestCase;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;

class IncrementIdStorageTest extends TestCase
{
    private const INCREMENTATION_STEP = 1;

    public function testIncrementation(): void
    {
        $incrementIdStorage = new IncrementIdStorage();

        $this->assertEquals(
            $incrementIdStorage->getTagId() + self::INCREMENTATION_STEP,
            $this->getProperty($incrementIdStorage, 'tagId')
        );
        $this->assertEquals(
            $incrementIdStorage->getVariableId() + self::INCREMENTATION_STEP,
            $this->getProperty($incrementIdStorage, 'variableId')
        );
        $this->assertEquals(
            $incrementIdStorage->getTriggerId() + self::INCREMENTATION_STEP,
            $this->getProperty($incrementIdStorage, 'triggerId')
        );
    }

    private function getProperty(object $object, string $propertyName)
    {
        $reflection = new \ReflectionClass(IncrementIdStorage::class);
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);

        return $property->getValue($object);
    }
}
