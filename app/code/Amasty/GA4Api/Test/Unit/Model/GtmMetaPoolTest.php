<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Test\Unit\Model;

use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;
use Amasty\GA4Api\Model\GtmMetaPool;
use PHPUnit\Framework\TestCase;

class GtmMetaPoolTest extends TestCase
{
    /**
     * @covers GtmMetaPool::initCollectors
     * @dataProvider initializationDataProvider
     */
    public function testInitialization(array $collectorsConfig, array $collectorsOrder): void
    {
        $pool = new GtmMetaPool($collectorsConfig);
        $collectors = $pool->getAll();

        $this->assertEquals($collectorsOrder, array_keys($collectors));
    }

    public function testWrongClassInitialization(): void
    {
        $collectorsConfig = [
            ['collector' => 'Wrong/Class']
        ];

        $this->expectException(\LogicException::class);
        new GtmMetaPool($collectorsConfig);
    }

    private function initializationDataProvider(): array
    {
        $collectorsMock = $this->createMock(GtmMetaInterface::class);

        $collector1 = ['collector' => $collectorsMock, 'sortOrder' => 10];
        $collector2 = ['collector' => $collectorsMock, 'sortOrder' => 20];
        $modifierNoOrder = ['collector' => $collectorsMock];

        return [
            'no modifiers' => [[], []],
            'correct order' => [
                ['first' => $collector1, 'second' => $collector2],
                ['first', 'second']
            ],
            'wrong order' => [
                ['first' => $collector2, 'second' => $collector1],
                ['second', 'first']
            ],
            'correct order without sortOrder' => [
                ['first' => $modifierNoOrder, 'second' => $collector1, 'third' => $collector2],
                ['first', 'second', 'third']
            ],
            'wrong order without sortOrder' => [
                ['first' => $collector2, 'second' => $modifierNoOrder, 'third' => $collector1],
                ['second', 'third', 'first']
            ]
        ];
    }
}
