<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model\Event;

use Amasty\GA4Api\Api\Event\EventConfigInterface;

class EventsConfigPool
{
    public const USAGE_HEADER_DATALAYER = 'header';
    public const USAGE_FOOTER_DATALAYER = 'footer';
    public const USAGE_REQUEST = 'request';

    /**
     * @var EventConfigInterface[]
     */
    private array $eventConfigs;

    public function __construct(
        array $eventConfigs = []
    ) {
        $this->initConfigs($eventConfigs);
    }

    /**
     * @return EventConfigInterface[]
     */
    public function getAll(): array
    {
        return $this->eventConfigs;
    }

    /**
     * @return EventConfigInterface[]
     */
    public function getByUsage(string $usage): array
    {
        $result = [];
        foreach ($this->getAll() as $config) {
            if ($config->getUsageType() === $usage) {
                $result[] = $config;
            }
        }

        return $result;
    }

    public function getByName(string $name): ?EventConfigInterface
    {
        return $this->eventConfigs[$name] ?? null;
    }

    private function initConfigs(array $eventConfigs): void
    {
        foreach ($eventConfigs as $config) {
            if (!$config instanceof EventConfigInterface) {
                throw new \LogicException(
                    sprintf('Event Config must implement %s', EventConfigInterface::class)
                );
            }
            $this->eventConfigs[$config->getName()] = $config;
        }
        ksort($this->eventConfigs);
    }
}
