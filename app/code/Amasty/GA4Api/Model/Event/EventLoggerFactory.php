<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model\Event;

use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Magento\Framework\ObjectManagerInterface;

class EventLoggerFactory
{
    /**
     * @var ObjectManagerInterface
     */
    private ObjectManagerInterface $objectManager;

    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    public function create(string $type, array $args = []): EventLoggerInterface
    {
        if (!is_subclass_of($type, EventLoggerInterface::class)) {
            throw new \InvalidArgumentException(
                'The Event Logger instance "' . $type . '" must implement '
                . EventLoggerInterface::class
            );
        }

        return $this->objectManager->create($type, $args);
    }
}
