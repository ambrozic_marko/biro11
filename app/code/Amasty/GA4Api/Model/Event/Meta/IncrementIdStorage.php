<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model\Event\Meta;

class IncrementIdStorage
{
    /**
     * @var int
     */
    private int $variableId = 1;

    /**
     * @var int
     */
    private int $triggerId = 1;

    /**
     * @var int
     */
    private int $tagId = 1;

    public function getVariableId(): int
    {
        return $this->variableId++;
    }

    public function getTriggerId(): int
    {
        return $this->triggerId++;
    }

    public function getTagId(): int
    {
        return $this->tagId++;
    }
}
