<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model\Event;

use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;

class ItemListProcessorsPool
{
    /**
     * @var ItemListProcessorInterface[]
     */
    private array $itemListProcessors;

    public function __construct(
        array $itemListProcessors = []
    ) {
        $this->initProcessors($itemListProcessors);
    }

    public function getAll(): array
    {
        return $this->itemListProcessors;
    }

    public function getAvailable(): array
    {
        $result = [];

        $allProcessors = $this->getAll();
        foreach ($allProcessors as $processor) {
            if ($processor->isAvailable()) {
                $result[] = $processor;
            }
        }

        return $result;
    }

    private function initProcessors(array $itemListProcessors): void
    {
        foreach ($itemListProcessors as $processor) {
            if (!$processor instanceof ItemListProcessorInterface) {
                throw new \LogicException(
                    sprintf('Item List Processor must implement %s', ItemListProcessorInterface::class)
                );
            }
        }
        $this->itemListProcessors = array_values($itemListProcessors);
    }
}
