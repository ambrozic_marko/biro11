<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model\DataLayer;

interface DataLayerStorageInterface
{
    /**
     * @param string|null $event
     *
     * @return array
     */
    public function getDataLayerOptions(?string $event = null): array;

    /**
     * @param string $event
     * @param array $ecommerceData
     * @param array $additionalData
     *
     * @return void
     */
    public function addDataLayerOption(string $event, array $ecommerceData, array $additionalData = []): void;

    /**
     * @param string $key
     * @param mixed $value
     *
     * @return void
     */
    public function setUserProperty(string $key, $value): void;

    /**
     * @param bool $resetIndex
     *
     * @return void
     */
    public function clear(bool $resetIndex = false): void;
}
