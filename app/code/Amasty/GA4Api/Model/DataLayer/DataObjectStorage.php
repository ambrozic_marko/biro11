<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model\DataLayer;

use Magento\Framework\DataObject;

class DataObjectStorage extends DataObject implements DataLayerStorageInterface
{
    public const EVENT_KEY = 'event';
    public const ECOMMERCE_KEY = 'ecommerce';

    /**
     * @var int
     */
    private int $innerIndex = 0;

    public function getDataLayerOptions(?string $event = null): array
    {
        if ($event) {
            foreach ($this->getData() as $element) {
                if ($event === ($element[self::EVENT_KEY] ?? null)) {
                    return $element;
                }
            }

            return [];
        }

        return $this->getData();
    }

    public function addDataLayerOption(string $event, array $ecommerceData, array $additionalData = []): void
    {
        if (!empty($ecommerceData)) {
            $this->setData(
                $this->innerIndex++,
                [self::ECOMMERCE_KEY => null] // Clear the previous ecommerce object.
            );
        }
        $dataArray = array_merge(
            [self::EVENT_KEY => $event, self::ECOMMERCE_KEY => $ecommerceData],
            $additionalData
        );

        $this->setData(
            $this->innerIndex++,
            array_filter($dataArray)
        );
    }

    public function setUserProperty(string $key, $value): void
    {
        $this->setData($this->innerIndex++, [$key => $value]);
    }

    public function clear(bool $resetIndex = false): void
    {
        $this->setData([]);
        if ($resetIndex) {
            $this->innerIndex = 0;
        }
    }
}
