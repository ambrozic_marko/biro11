<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model;

use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;

class GtmMetaPool
{
    private const DEFAULT_SORT_ORDER = 0;

    /**
     * @var GtmMetaInterface[]
     */
    private array $metaCollectors = [];

    /**
     * @param array $metaCollectors ['name' => ['class, 'sortOrder']]
     */
    public function __construct(
        array $metaCollectors = []
    ) {
        $this->initCollectors($metaCollectors);
    }

    /**
     * @return GtmMetaInterface[]
     */
    public function getAll(): array
    {
        return $this->metaCollectors;
    }

    private function initCollectors(array $metaCollectors): void
    {
        foreach ($metaCollectors as &$collector) {
            if (!isset($collector['collector']) || !$collector['collector'] instanceof GtmMetaInterface) {
                throw new \LogicException(
                    sprintf('Gtm Meta Collector must implement %s', GtmMetaInterface::class)
                );
            }
            $collector['sortOrder'] ??= self::DEFAULT_SORT_ORDER;
        }
        unset($collector);

        uasort($metaCollectors, static function ($first, $second) {
            return $first['sortOrder'] <=> $second['sortOrder'];
        });

        foreach ($metaCollectors as $key => $collector) {
            $this->metaCollectors[$key] = $collector['collector'];
        }
    }
}
