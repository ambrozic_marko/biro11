<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model\GtmMeta;

interface GtmMetaInterface
{
    /**
     * Collects specific meta part for Json Export file
     *
     * @param array $data processed for the moment data.
     * @param int|null $fingerprint execution time of the collector
     */
    public function collect(int $fingerprint = null, array &$data = []): void;
}
