<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Model;

use Amasty\Base\Model\Serializer;
use Amasty\GA4Api\Model\DataLayer\DataLayerStorageInterface;

class DataLayerCollector
{
    /**
     * @var DataLayerStorageInterface
     */
    private DataLayerStorageInterface $storage;

    /**
     * @var Serializer
     */
    private Serializer $serializer;

    public function __construct(
        DataLayerStorageInterface $storage,
        Serializer $serializer
    ) {
        $this->storage = $storage;
        $this->serializer = $serializer;
    }

    public function collect(): string
    {
        return $this->serializer->serialize(array_values($this->storage->getDataLayerOptions()));
    }

    public function addDataLayerOption(string $event, array $ecommerceData, array $additionalData = []): void
    {
        $this->storage->addDataLayerOption($event, $ecommerceData, $additionalData);
    }

    public function addUserProperty(string $key, $value): void
    {
        $this->storage->setUserProperty($key, $value);
    }

    public function clearStorage(): void
    {
        $this->storage->clear();
    }
}
