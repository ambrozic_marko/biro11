<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Api\Event;

interface EventLoggerInterface
{
    /**
     * Performs logging action to process GA4 event data
     */
    public function execute(): void;
}
