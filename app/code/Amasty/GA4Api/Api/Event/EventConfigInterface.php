<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */
namespace Amasty\GA4Api\Api\Event;

/**
 * Representation of GA4 event.
 */
interface EventConfigInterface
{
    public function getName(): string;

    public function getMeta(?int $fingerprint = null): array;

    public function isAvailable(): bool;

    public function getUsageType(): string;

    public function getLogger(array $eventData = []): EventLoggerInterface;
}
