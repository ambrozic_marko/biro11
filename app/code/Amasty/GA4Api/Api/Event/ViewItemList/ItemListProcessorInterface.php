<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 API by Amasty
 */

namespace Amasty\GA4Api\Api\Event\ViewItemList;

interface ItemListProcessorInterface
{
    /**
     * @return array list data
     */
    public function processViewList(): array;

    /**
     * Adds product items data of each element to the block
     * for further processing in plugins
     */
    public function processItemClick(): void;

    /**
     * indicates if listing exists on the page
     */
    public function isAvailable(): bool;
}
