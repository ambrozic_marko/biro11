<?php

namespace Amasty\PreOrderMFTF3\Test\Mftf\Helper;

use Magento\FunctionalTestingFramework\Helper\Helper;

class CompareStatistics extends Helper
{
    /**
     * Compare statistics after placing preorder
     *
     * @param int $value1
     * @param int $value2
     * @param int $value3
     * @return void
     */
    public function compareStatisticCount(int $value1, int $value2, int $value3)
    {
        try {
            if ($value1 + $value2 != $value3)
                throw new ErrorException("Coutn isn't correct");
            }
         catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    /**
     * Compare statistics after placing preorder
     *
     * @param string $value1
     * @param string $value2
     * @param string $value3
     * @return void
     */
    public function compareStatisticRevenue(string $value1, string $value2, string $value3)
    {
    $value1 = (float) str_replace(',','',ltrim($value1,'$'));
    $value2 = (float) str_replace(',','',ltrim($value2,'$'));
    $value3 = (float) str_replace(',','',ltrim($value3,'$'));
        try {
            if ($value1 + $value2 != $value3)
                throw new ErrorException("Revenue isn't correct");
            }
         catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
}
