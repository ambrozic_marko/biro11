var config = {
    config: {
        mixins: {
            'Magento_Catalog/js/category-checkbox-tree': {
                'Amasty_VisualMerch/js/category-checkbox-tree-mixin': true
            },
            'Amasty_Xlanding/js/form/element/amlanding_is_dynamic': {
                'Amasty_VisualMerch/js/form/element/amlanding_is_dynamic-mixin': true
            },
            'Amasty_VisualMerchUi/js/products': {
                'Amasty_VisualMerch/js/products/check-before-preview': true
            }
        }
    }
};
