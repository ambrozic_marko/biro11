<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Visual Merchandiser for Magento 2
 */

namespace Amasty\VisualMerch\Model\Indexer\DynamicCategory;

use Magento\Framework\Indexer\AbstractProcessor;

class SourceItemProcessor extends AbstractProcessor
{
    public const INDEXER_ID = 'amasty_merch_product_dynamic_category_msi';
}
