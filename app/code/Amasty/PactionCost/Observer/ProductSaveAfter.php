<?php

namespace Amasty\PactionCost\Observer;

use Amasty\Paction\Model\Command\Addspecialbycost;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

class ProductSaveAfter implements ObserverInterface
{
    private const PERCENT = '+10%';

    /**
     * @var Addspecialbycost
     */
    private $addSpecialByCost;

    public function __construct(
        Addspecialbycost $addSpecialByCost
    ) {
        $this->addSpecialByCost = $addSpecialByCost;
    }

    public function execute(Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();

        $origCost = (float) $product->getOrigData('cost');
        $cost = (float) $product->getData('cost');

        if ($origCost && $cost && $cost !== $origCost) {
            try {
                $this->addSpecialByCost->execute(
                    [$product->getId()],
                    (int) $product->getStoreId(),
                    self::PERCENT
                );
            } catch (LocalizedException $e) {
            }
        }
    }
}
