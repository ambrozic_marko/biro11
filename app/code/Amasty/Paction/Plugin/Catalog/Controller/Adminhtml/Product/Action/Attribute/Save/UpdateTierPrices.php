<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Plugin\Catalog\Controller\Adminhtml\Product\Action\Attribute\Save;

use Amasty\Paction\Block\Adminhtml\Product\Edit\Action\Attribute\Tab\TierPrice as TierPriceBlock;
use Amasty\Paction\Model\Product\Attribute\TierPrice\ConverterInterface as TierPriceConverterInterface;
use Amasty\Paction\Model\Product\Attribute\TierPrice\UpdaterInterface as TierPriceUpdaterInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Controller\Adminhtml\Product\Action\Attribute\Save as AttributeSave;
use Magento\Catalog\Helper\Product\Edit\Action\Attribute;
use Magento\Catalog\Pricing\Price\TierPrice;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Psr\Log\LoggerInterface;

class UpdateTierPrices
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Attribute
     */
    private $attributeHelper;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var TierPriceUpdaterInterface
     */
    private $tierPriceUpdater;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TierPriceConverterInterface
     */
    private $tierPriceConverter;

    public function __construct(
        RequestInterface $request,
        ProductRepositoryInterface $productRepository,
        Attribute $attributeHelper,
        ArrayManager $arrayManager,
        TierPriceUpdaterInterface $tierPriceUpdater,
        LoggerInterface $logger,
        TierPriceConverterInterface $tierPriceConverter
    ) {
        $this->request = $request;
        $this->productRepository = $productRepository;
        $this->attributeHelper = $attributeHelper;
        $this->arrayManager = $arrayManager;
        $this->tierPriceUpdater = $tierPriceUpdater;
        $this->logger = $logger;
        $this->tierPriceConverter = $tierPriceConverter;
    }

    public function beforeExecute(AttributeSave $subject): void
    {
        $productIds = $this->attributeHelper->getProductIds();
        $requestParams = $this->request->getParams();
        $attrData = $requestParams['attributes'] ?? [];
        $isNeedDeletePrices = $this->request->getParam(TierPriceBlock::TIER_PRICE_CHANGE_CHECKBOX_NAME);
        $newTierPrices = $this->arrayManager->get(TierPrice::PRICE_CODE, $attrData)
            ? $this->tierPriceConverter->convert($attrData[TierPrice::PRICE_CODE])
            : [];

        if ($newTierPrices || $isNeedDeletePrices) {
            foreach ($productIds as $productId) {
                try {
                    $product = $this->productRepository->getById($productId);
                    $product->setMediaGalleryEntries($product->getMediaGalleryEntries());

                    if ($isNeedDeletePrices) {
                        $product->setTierPrices($newTierPrices);
                        $this->productRepository->save($product);
                    } else {
                        $this->tierPriceUpdater->update($product->getSku(), $newTierPrices);
                    }
                } catch (\Exception $e) {
                    $this->logger->critical($e->getMessage());
                }
            }

            unset($attrData[TierPrice::PRICE_CODE]);
        }

        $requestParams['attributes'] = $attrData;
        $this->request->setParams($requestParams);
    }
}
