<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Source;

use Amasty\Base\Model\Serializer;
use Amasty\Paction\Model\Collection\BatchLoader;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\Category as CatalogCategory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;

class Category implements OptionSourceInterface
{
    private const CATEGORY_OPTIONS_CACHE_ID_FORMAT = 'amasty_paction_category_options_%d';
    private const STORE_PARAM = 'store';
    private const CACHE_LIFETIME = 3600;

    private const ROOT_CATEGORY_LEVEL = '1';

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var BatchLoader
     */
    private $batchLoader;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        CategoryCollectionFactory $categoryCollectionFactory,
        RequestInterface $request,
        LoggerInterface $logger,
        BatchLoader $batchLoader,
        CacheInterface $cache,
        Serializer $serializer
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->request = $request;
        $this->logger = $logger;
        $this->batchLoader = $batchLoader;
        $this->cache = $cache;
        $this->serializer = $serializer;
    }

    public function toOptionArray(): array
    {
        $result = [];
        $storeId = (int) $this->request->getParam(self::STORE_PARAM, Store::DEFAULT_STORE_ID);

        $cacheId = sprintf(self::CATEGORY_OPTIONS_CACHE_ID_FORMAT, $storeId);
        $cache = $this->cache->load($cacheId);

        if ($cache) {
            $result = $this->serializer->unserialize($cache);
        } else {
            try {
                $categoryCollection = $this->categoryCollectionFactory->create()
                    ->addAttributeToSelect([CategoryInterface::KEY_NAME])
                    ->addFieldToFilter(CategoryInterface::KEY_PATH, ['neq' => self::ROOT_CATEGORY_LEVEL])
                    ->setStoreId($storeId);

                /** @var CategoryInterface $category */
                foreach ($this->batchLoader->load($categoryCollection) as $category) {
                    $result[$category->getId()] = $category->getName();
                }

                $this->cache->save(
                    $this->serializer->serialize($result),
                    $cacheId,
                    [CatalogCategory::CACHE_TAG],
                    self::CACHE_LIFETIME
                );
            } catch (LocalizedException $e) {
                $this->logger->critical($e->getMessage());
            }
        }

        return $result;
    }
}
