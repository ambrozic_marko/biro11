<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CategorySelectMode implements OptionSourceInterface
{
    public const MULTISELECT = 1;
    public const ENTERING_IDS = 2;

    public function toOptionArray(): array
    {
        $result = [];

        foreach ($this->toArray() as $value => $label) {
            $result[] = [
                'value' => $value,
                'label' => $label,
            ];
        }

        return $result;
    }

    public function toArray(): array
    {
        return [
            self::MULTISELECT => __('Name Multiselect'),
            self::ENTERING_IDS => __('Enter Category IDs'),
        ];
    }
}
