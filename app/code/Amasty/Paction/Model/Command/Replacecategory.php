<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Command;

use Amasty\Paction\Model\ConfigProvider;
use Amasty\Paction\Model\EntityResolver;
use Magento\Catalog\Model\ResourceModel\Category;
use Magento\Framework\App\ResourceConnection;

class Replacecategory extends Addcategory
{
    public const TYPE = 'replacecategory';

    public function __construct(
        ResourceConnection $resource,
        Category\CollectionFactory $categoryCollectionFactory,
        EntityResolver $entityResolver,
        ConfigProvider $configProvider
    ) {
        parent::__construct(
            $resource,
            $categoryCollectionFactory,
            $entityResolver,
            $configProvider
        );

        $this->type = self::TYPE;
        $this->info = [
            'confirm_title' => __('Replace Category')->render(),
            'confirm_message' => __('Are you sure you want to replace category?')->render(),
            'type' => $this->type,
            'label' => __('Replace Category')->render(),
            'fieldLabel' => $this->getFieldLabel(),
            'placeholder' => __('id1,id2,id3')->render()
        ];
    }
}
