<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Command;

use Amasty\Paction\Model\ConfigProvider;
use Amasty\Paction\Model\EntityResolver;
use Magento\Catalog\Model\ResourceModel\Category;
use Magento\Framework\App\ResourceConnection;

class Removecategory extends Addcategory
{
    public const TYPE = 'removecategory';

    public function __construct(
        ResourceConnection $resource,
        Category\CollectionFactory $categoryCollectionFactory,
        EntityResolver $entityResolver,
        ConfigProvider $configProvider
    ) {
        parent::__construct(
            $resource,
            $categoryCollectionFactory,
            $entityResolver,
            $configProvider
        );

        $this->type = self::TYPE;
        $this->info = [
            'confirm_title' => __('Remove Category')->render(),
            'confirm_message' => __('Are you sure you want to remove category?')->render(),
            'type' => $this->type,
            'label' => __('Remove Category')->render(),
            'fieldLabel' => $this->getFieldLabel(),
            'placeholder' => __('id1,id2,id3')->render()
        ];
    }
}
