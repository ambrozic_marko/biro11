<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Command;

use Amasty\Paction\Model\LinkActionsManagement;
use Magento\Framework\App\ResourceConnection;

class Unupsell extends Unrelate
{
    public const TYPE = 'unupsell';

    public function __construct(ResourceConnection $resource, LinkActionsManagement $linkActionsManagement)
    {
        parent::__construct($resource, $linkActionsManagement);

        $this->type = self::TYPE;
        $this->info = [
            'confirm_title' => __('Remove Up-sells')->render(),
            'confirm_message' => __('Are you sure you want to remove up-sells?')->render(),
            'type' => $this->type,
            'label' => __('Remove Up-sells')->render(),
            'fieldLabel' => __('Select Algorithm')->render()
        ];
    }
}
