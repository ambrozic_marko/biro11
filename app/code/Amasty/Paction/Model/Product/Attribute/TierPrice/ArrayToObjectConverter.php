<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Product\Attribute\TierPrice;

use Magento\Catalog\Api\Data\ProductTierPriceExtensionFactory;
use Magento\Catalog\Api\Data\ProductTierPriceInterface;
use Magento\Catalog\Api\Data\ProductTierPriceInterfaceFactory;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Catalog\Model\Config\Source\ProductPriceOptionsInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class ArrayToObjectConverter implements ConverterInterface
{
    public const PRICE_QTY_KEY = 'price_qty';
    public const WEBSITE_ID_KEY = 'website_id';
    public const VALUE_TYPE_KEY = 'value_type';
    public const PRICE_KEY = 'price';
    public const CUSTOMER_GROUP_KEY = 'cust_group';
    public const SEPARATOR = '-';
    public const DEFAULT_WEBSITE_ID = 0;

    /**
     * @var ProductTierPriceExtensionFactory
     */
    private $productTierPriceExtensionFactory;

    /**
     * @var ProductTierPriceInterfaceFactory
     */
    private $productTierPriceInterfaceFactory;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        ProductTierPriceExtensionFactory $productTierPriceExtensionFactory,
        ProductTierPriceInterfaceFactory $productTierPriceInterfaceFactory,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->productTierPriceExtensionFactory = $productTierPriceExtensionFactory;
        $this->productTierPriceInterfaceFactory = $productTierPriceInterfaceFactory;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @param array $tierPrices
     *
     * @return ProductTierPriceInterface[]
     */
    public function convert(array $tierPrices): array
    {
        $result = [];
        $websiteId = $this->getWebsiteId();

        foreach ($tierPrices as $item) {
            if (!$item[self::PRICE_QTY_KEY]) {
                continue;
            }

            $tierPriceExtensionAttribute = $this->productTierPriceExtensionFactory->create()
                ->setWebsiteId($item[self::WEBSITE_ID_KEY] ?: $websiteId);

            $isPercentValue = $item[self::VALUE_TYPE_KEY] === ProductPriceOptionsInterface::VALUE_PERCENT;
            if ($isPercentValue) {
                $tierPriceExtensionAttribute->setPercentageValue($item[self::PRICE_KEY]);
            }

            $key = implode(
                self::SEPARATOR,
                [
                    $item[self::WEBSITE_ID_KEY],
                    $item[self::CUSTOMER_GROUP_KEY],
                    (int) $item[self::PRICE_QTY_KEY]
                ]
            );

            $result[$key] = $this->productTierPriceInterfaceFactory
                ->create()
                ->setCustomerGroupId($item[self::CUSTOMER_GROUP_KEY])
                ->setQty((float) $item[self::PRICE_QTY_KEY])
                ->setValue(!$isPercentValue ? (float) $item[self::PRICE_KEY] : '')
                ->setExtensionAttributes($tierPriceExtensionAttribute);
        }

        return array_values($result);
    }

    private function getWebsiteId(): int
    {
        $websiteIdentifier = self::DEFAULT_WEBSITE_ID;

        $value = $this->scopeConfig->getValue(
            CatalogHelper::XML_PATH_PRICE_SCOPE,
            ScopeInterface::SCOPE_WEBSITE
        );
        if ($value) {
            try {
                $websiteIdentifier = (int) $this->storeManager->getWebsite()->getId();
            } catch (LocalizedException $e) {
                return self::DEFAULT_WEBSITE_ID;
            }
        }

        return $websiteIdentifier;
    }
}
