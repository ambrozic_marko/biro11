<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Product\Attribute\TierPrice;

use Magento\Catalog\Api\Data\ProductTierPriceInterface;
use Magento\Catalog\Api\ScopedProductTierPriceManagementInterface;
use Psr\Log\LoggerInterface;

class DefaultUpdater implements UpdaterInterface
{
    /**
     * @var ScopedProductTierPriceManagementInterface
     */
    private $tierPriceManagement;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ScopedProductTierPriceManagementInterface $tierPriceManagement,
        LoggerInterface $logger
    ) {
        $this->tierPriceManagement = $tierPriceManagement;
        $this->logger = $logger;
    }

    public function update(string $sku, array $newTierPrices): void
    {
        /** @var ProductTierPriceInterface $newTierPrice */
        foreach ($newTierPrices as $newTierPrice) {
            try {
                $this->tierPriceManagement->add($sku, $newTierPrice);
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
    }
}
