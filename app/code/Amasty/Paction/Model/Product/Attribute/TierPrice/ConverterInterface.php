<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Mass Product Actions for Magento 2
 */

namespace Amasty\Paction\Model\Product\Attribute\TierPrice;

interface ConverterInterface
{
    public function convert(array $tierPrices): array;
}
