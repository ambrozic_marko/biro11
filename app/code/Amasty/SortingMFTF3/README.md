# Improved Sorting MFTF3

### Current version of tests is for Magento EE 2.4.5 only.

### Release version consists of 23 smoke tests. 
 
### The tests are divided into following groups:
- ImpSort (is used for running of all tests. E.g. vendor/bin/mftf run:group ImpSort -r)
- ImpSort_CPTS (is used for running of all critical tests. E.g. vendor/bin/mftf run:group ImpSort_CPTS -r)

