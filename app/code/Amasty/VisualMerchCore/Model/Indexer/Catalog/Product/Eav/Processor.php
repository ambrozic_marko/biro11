<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Visual Merchandiser Core by Amasty for Magento 2 (System)
 */

namespace Amasty\VisualMerchCore\Model\Indexer\Catalog\Product\Eav;

class Processor extends \Magento\Framework\Indexer\AbstractProcessor
{
    public const INDEXER_ID = 'merchandiser_product_attribute';
}
