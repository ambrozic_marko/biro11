<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Model\ResourceModel;

use Amasty\Preorder\Api\Data\OrderItemInformationInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OrderItemPreorder extends AbstractDb
{
    protected function _construct()
    {
        $this->_setResource('sales');
        $this->_init(OrderItemInformationInterface::MAIN_TABLE, OrderItemInformationInterface::ID);
    }
}
