<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Model\OrderPreorder\Query;

interface IsExistForOrderInterface
{
    /**
     * @param int $orderId
     * @return bool
     */
    public function execute(int $orderId): bool;

    /**
     * @param int $orderId
     * @return void
     */
    public function setAsProcessed(int $orderId): void;
}
