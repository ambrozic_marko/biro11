<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Model\Indexer\Product\Msi;

use Magento\Framework\Indexer\AbstractProcessor;

class PreorderProcessor extends AbstractProcessor
{
    public const INDEXER_ID = 'amasty_preorder_product_preorder_msi';
}
