<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Model\OrderItemPreorder\Command;

use Amasty\Preorder\Api\Data\OrderItemInformationInterface;

interface SaveInterface
{
    public function execute(OrderItemInformationInterface $orderItemInformation): void;
}
