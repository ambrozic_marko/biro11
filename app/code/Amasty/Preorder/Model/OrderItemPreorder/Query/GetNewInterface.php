<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Model\OrderItemPreorder\Query;

use Amasty\Preorder\Api\Data\OrderItemInformationInterface;

interface GetNewInterface
{
    public function execute(array $data = []): OrderItemInformationInterface;
}
