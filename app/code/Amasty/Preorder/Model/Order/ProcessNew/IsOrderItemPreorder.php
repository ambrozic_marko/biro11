<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Model\Order\ProcessNew;

use Amasty\Preorder\Model\Product\Detect\IsProductPreorderInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Sales\Api\Data\OrderItemInterface;

class IsOrderItemPreorder implements IsOrderItemPreorderInterface
{
    /**
     * @var IsProductPreorderInterface
     */
    private $isProductPreorder;

    public function __construct(IsProductPreorderInterface $isProductPreorder)
    {
        $this->isProductPreorder = $isProductPreorder;
    }

    public function execute(OrderItemInterface $orderItem): bool
    {
        $product = $orderItem->getProduct();
        $result = false;

        if ($product === null) {
            return $result;
        }

        if ($product->getTypeId() != Configurable::TYPE_CODE) {
            $result = $this->isProductPreorder->execute($product);
        }

        if (!$result) {
            foreach ($orderItem->getChildrenItems() as $childItem) {
                $result = $this->execute($childItem);
                if ($result) {
                    break;
                }
            }
        }

        return $result;
    }
}
