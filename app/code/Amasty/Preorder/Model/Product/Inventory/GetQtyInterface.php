<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Model\Product\Inventory;

interface GetQtyInterface
{
    public function execute(string $productSku, string $websiteCode): float;
}
