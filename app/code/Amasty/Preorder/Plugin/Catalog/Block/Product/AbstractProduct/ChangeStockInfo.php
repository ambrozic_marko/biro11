<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Plugin\Catalog\Block\Product\AbstractProduct;

use Amasty\Preorder\Api\Data\ProductInformationInterface;
use Amasty\Preorder\Model\ConfigProvider;
use Amasty\Preorder\Plugin\ProductList\GetPreorderHtmlForList;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Block\Product\AbstractProduct as NativeAbstractProduct;

/**
 * Class ChangeStockInfo
 *
 * Change stock info for preorder note on server.
 */
class ChangeStockInfo
{
    /**
     * @var array
     */
    private $applicableBlocks = [
        'product.info.configurable',
        'product.info.simple',
        'product.info.bundle',
        'product.info.virtual',
        'product.info.downloadable',
        'product.info.grouped.stock',
        'product.info.type.giftcard'
    ];

    /**
     * @var array
     */
    private $listBlocks = [
        'checkout.cart.crosssell'
    ];

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var GetPreorderHtmlForList
     */
    private $getPreorderHtmlForList;

    public function __construct(
        ConfigProvider $configProvider,
        GetPreorderHtmlForList $getPreorderHtmlForList
    ) {
        $this->configProvider = $configProvider;
        $this->getPreorderHtmlForList = $getPreorderHtmlForList;
    }

    public function afterToHtml(NativeAbstractProduct $subject, string $html): string
    {
        if ($this->configProvider->isEnabled()) {
            $nameInLayout = $subject->getNameInLayout();
            $products = $subject->getItems() ?? $subject->getItemCollection();

            if (in_array($nameInLayout, $this->applicableBlocks)
                && $this->isPreorder($subject->getProduct())
            ) {
                $preorderNote = $this->getPreorderNote($subject->getProduct());

                if ($preorderNote) {
                    $html = sprintf('<div class="stock available"><span>%s</span></div>', $preorderNote);
                }
            } elseif (in_array($nameInLayout, $this->listBlocks)
                && $products
            ) {
                $html .= $this->getPreorderHtmlForList->get($subject, $products);
            }
        }

        return $html;
    }

    private function getPreorderNote(ProductInterface $product): ?string
    {
        $preorderNote = null;

        if ($preorderInfo = $this->getPreorderInfo($product)) {
            $preorderNote = $preorderInfo->getNote();
        }

        return $preorderNote;
    }

    private function isPreorder(ProductInterface $product): bool
    {
        $isPreorder = false;

        if ($preorderInfo = $this->getPreorderInfo($product)) {
            $isPreorder = (bool)$preorderInfo->isPreorder();
        }

        return $isPreorder;
    }

    private function getPreorderInfo(
        ProductInterface $product
    ): ?ProductInformationInterface {
        return $product->getExtensionAttributes()->getPreorderInfo();
    }
}
