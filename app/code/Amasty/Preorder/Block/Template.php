<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Pre Order Base for Magento 2
 */

namespace Amasty\Preorder\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Module\Manager;

class Template extends \Magento\Backend\Block\Template
{
    private const MODULE_NAME = 'Amasty_PreOrderRelease';

    /**
     * @var Manager
     */
    private $moduleManager;

    public function __construct(
        Context $context,
        Manager $moduleManager
    ) {
        parent::__construct($context);
        $this->moduleManager = $moduleManager;
    }

    private function isEnableModule(string $moduleName = self::MODULE_NAME): bool
    {
        return $this->moduleManager->isEnabled($moduleName);
    }

    public function isCanShowReleaseDateField(): bool
    {
        return $this->isEnableModule();
    }
}
