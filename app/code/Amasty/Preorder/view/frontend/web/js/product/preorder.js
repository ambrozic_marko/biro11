define([
    'jquery',
    'Magento_Catalog/js/catalog-add-to-cart'
], function($) {
    'use strict';

    $.widget('mage.amastyPreorder', {
        options: {
            addToCartButton: $("#product-addtocart-button span"),
            parentSelector: '.product-item',
            priceSelector: '.price-box',
            customStockStatusSelector: '.amstockstatus-category',
            nameSelector: '.product-item-name',
            preorderNoteSelector: '.ampreorder-note',
            noteTemplate: '<div class="ampreorder-note" data-preorder-product-id="$productId">$note</div>',
            availabilityElement: '',
            preOrderNote: '',
            addToCartLabel: '',
            originalNote: ''
        },

        _original: {
            availabilityText: '',
            addToCartLabel: ''
        },

        _enabled: false,

        _create: function() {
            this._saveOriginal();
        },

        _saveOriginal: function () {
            this.options.availabilityElement = this.options.availabilityElement
                ? $(this.options.availabilityElement)
                : $(".product-info-main").find('.stock');
            if (this.options.availabilityElement) {
                this._original.availabilityText = this.options.originalNote
                    ? this.options.originalNote
                    : this.options.availabilityElement.text();
            }

            if (this.options.addToCartButton.length) {
                var originalButtonElement = $('.original-add-to-cart-text');
                if (originalButtonElement.length > 0) {
                    this._original.addToCartLabelText = originalButtonElement.data('text');
                    originalButtonElement.remove();
                } else {
                    this._original.addToCartLabelText = this.options.addToCartButton.text();
                }
            }
        },

        _changeLabels: function() {
            this._changeAvailability();
            this.options.addToCartButton.html(this.options.addToCartLabel);
        },

        _changeAvailability: function () {
            if (this.options.availabilityElement && this.options.preOrderNote) {
                var additionalAvailability = '';
                if (this.options.availabilityElement.find('.amstockstatus')) {
                    additionalAvailability = '<br>' + $('<div>').append(
                            this.options.availabilityElement.find('.amstockstatus').clone()
                        ).html();
                }
                this.options.availabilityElement.html(this.options.preOrderNote + additionalAvailability);
                this.options.availabilityElement.addClass('ampreorder-observed');
            }
        },

        _changeConfPreorderNote: function () {
            var self = this,
                selector = self.getPriceSelector(this.options.entity),
                productId = self.options.entity,
                productConfig = {
                    cart_label: self.options.addToCartLabel,
                    note: self.options.preOrderNote.length
                        ? self.options.preOrderNote
                        : self.options.configurablePreorderNote
                };

            $(selector).each(function (i, element) {
                var parent = $(element).parents(self.options.parentSelector);
                if (parent.length) {
                    self.insertPreOrderNote(parent, productId, productConfig);
                }
            });
        },

        getPriceSelector: function (productId) {
            return '[data-product-id="' + productId + '"]';
        },

        insertPreOrderNote: function (parent, productId, productConfig) {
            var self = this,
                elementToInsert = $(parent).find(self.options.priceSelector),
                customStockStatus = $(parent).find(self.options.customStockStatusSelector),
                preorderNote = $(parent).find(self.options.preorderNoteSelector);

            if (!productConfig.note) {
                preorderNote.replaceWith('');
                return;
            }

            if (!elementToInsert.length && !customStockStatus.length) {
                elementToInsert = $(parent).find(self.options.nameSelector);
            }

            var content = self.options.noteTemplate;
            content = content.replace('$productId', productId).replace('$note', productConfig.note);
            if (customStockStatus.length) {
                customStockStatus.replaceWith(content);
            } else if (elementToInsert.length) {
                preorderNote.length ? preorderNote.replaceWith(content) : $(content).insertAfter(elementToInsert.first());
            }
        },

        enable: function() {
            /*if(this._enabled) {
                return;
            }*/
            this._enabled = true;
            this._changeLabels();
            this._changeConfPreorderNote();
        },

        disable: function() {
            /*if(!this._enabled) {
                return;
            }*/
            this._enabled = false;
            if (this.options.availabilityElement) {
                this.options.availabilityElement.text(this._original.availabilityText);
                this.options.availabilityElement.removeClass('ampreorder-observed');
            }
            this.options.addToCartButton.text(this._original.addToCartLabelText);
            this._changeConfPreorderNote();
        }
    });

    return $.mage.amastyPreorder;
});
