<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Observer\Quote\Model;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class Payment implements ObserverInterface
{
    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($quote = $observer->getPayment()->getQuote()) {
            $quote->setAppliedAmastyFeeFlag(true);
        }
    }
}
