<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api\Data;

interface TotalsInformationInterface
{
    public const OPTIONS_IDS = 'options_ids';
    public const FEE_ID = 'fee_id';

    /**
     * @return mixed
     */
    public function getOptionsIds();

    /**
     * @param array $optionIds
     * @return mixed
     */
    public function setOptionsIds($optionIds);

    /**
     * @return int
     */
    public function getFeeId();

    /**
     * @param int $feeId
     * @return mixed
     */
    public function setFeeId($feeId);
}
