<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface FeeSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Amasty\Extrafee\Api\Data\FeeInterface[]
     */
    public function getItems();

    /**
     * @param \Amasty\Extrafee\Api\Data\FeeInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
