<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api\Data;

interface ExtrafeeInvoiceInterface
{
    /**
     * Constants defined for keys of data array
     */
    public const ENTITY_ID = 'entity_id';
    public const ORDER_ID = 'order_id';
    public const INVOICE_ID = 'invoice_id';
    public const FEE_ID = 'fee_id';
    public const OPTION_ID = 'option_id';
    public const BASE_TOTAL = 'base_total_amount';
    public const TOTAL = 'total_amount';
    public const BASE_TAX = 'base_tax_amount';
    public const TAX = 'tax_amount';
    public const LABEL = 'fee_label';
    public const OPTION_LABEL = 'fee_option_label';

    /**
     * @return int
     */
    public function getEntityId(): int;

    /**
     * @param int $entityId
     *
     * @return void
     */
    public function setEntityId($entityId);

    /**
     * @return int
     */
    public function getOrderId(): int;

    /**
     * @param int $orderId
     *
     * @return void
     */
    public function setOrderId(int $orderId);

    /**
     * @return int
     */
    public function getInvoiceId(): int;

    /**
     * @param int $invoiceId
     *
     * @return void
     */
    public function setInvoiceId(int $invoiceId);

    /**
     * @return int
     */
    public function getFeeId(): int;

    /**
     * @param int $feeId
     *
     * @return void
     */
    public function setFeeId(int $feeId);

    /**
     * @return int
     */
    public function getOptionId(): int;

    /**
     * @param int $optionId
     *
     * @return void
     */
    public function setOptionId(int $optionId);

    /**
     * @return float
     */
    public function getBaseTotalAmount(): float;

    /**
     * @param float $total
     *
     * @return void
     */
    public function setBaseTotalAmount($total);

    /**
     * @return float
     */
    public function getTotalAmount(): float;

    /**
     * @param float $total
     *
     * @return void
     */
    public function setTotalAmount($total);

    /**
     * @return float
     */
    public function getBaseTaxAmount(): float;

    /**
     * @param float $tax
     *
     * @return void
     */
    public function setBaseTaxAmount($tax);

    /**
     * @return float
     */
    public function getTaxAmount(): float;

    /**
     * @param float $tax
     *
     * @return void
     */
    public function setTaxAmount($tax);

    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @param string $label
     *
     * @return void
     */
    public function setFeeLabel($label);

    /**
     * @return string
     */
    public function getOptionLabel(): string;

    /**
     * @param string $label
     *
     * @return void
     */
    public function setFeeOptionLabel($label);
}
