<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api\Data;

use Amasty\Extrafee\Model\Config\Source\Excludeinclude;

interface FeeInterface
{
    public const ENTITY_ID = 'entity_id';
    public const ENABLED = 'enabled';
    public const NAME = 'name';
    public const DESCRIPTION = 'description';
    public const OPTIONS = 'options';
    public const BASE_OPTIONS = 'base_options';
    public const CURRENT_VALUE = 'current_value';
    public const FRONTEND_TYPE = 'frontend_type';
    public const DISCOUNT_IN_SUBTOTAL = 'discount_in_subtotal';
    public const TAX_IN_SUBTOTAL = 'tax_in_subtotal';
    public const SHIPPING_IN_SUBTOTAL = 'shipping_in_subtotal';
    public const SORT_ORDER = 'sort_order';
    public const CONDITIONS_SERIALIZED = 'conditions_serialized';
    public const CUSTOMER_GROUP_ID = 'customer_group_id';
    public const STORE_ID = 'store_id';
    public const IS_REQUIRED = 'is_required';
    public const IS_ELIGIBLE_REFUND = 'is_eligible_refund';
    public const IS_PER_PRODUCT = 'is_per_product';
    public const PRODUCT_CONDITIONS_SERIALIZED = 'product_conditions_serialized';

    /**
     * Get ID
     * @return int|null
     */
    public function getId();

    /**
     * Get enabled
     * @return bool
     */
    public function getEnabled();

    /**
     * Get name
     * @return string
     */
    public function getName();

    /**
     * Get description
     * @return string
     */
    public function getDescription();

    /**
     * Get fees options
     * @return mixed|array Format: array(array(
     *  'entity_id' => 0,
     *  'fee_id' => 0,
     *  'price' => 0,
     *  'order' => 0,
     *  'price_type' => 'fixed',
     *  'default' => 0,
     *  'admin' => '',
     *  'options' => array()
     * ))
     */
    public function getOptions();

    /**
     * Get current value
     * @return string
     */
    public function getCurrentValue();

    /**
     * Get fees base options
     * @return string
     */
    public function getBaseOptions();

    /**
     * Get $frontendType
     * @return string
     */
    public function getFrontendType();

    /**
     * @return mixed
     */
    public function getDiscountInSubtotal();

    /**
     * @return mixed
     */
    public function getShippingInSubtotal();

    /**
     * @return string
     */
    public function getConditionsSerialized();

    /**
     * @return int
     */
    public function getSortOrder();

    /**
     * @return string[]
     */
    public function getGroupId();

    /**
     * @return string[]
     */
    public function getStoreId();

    /**
     * @return bool
     */
    public function isRequired();

    /**
     * @return bool
     */
    public function isPerProduct(): bool;

    /**
     * @return null|string
     */
    public function getProductConditionsSerialized(): ?string;

    /**
     * @param bool $enabled
     * @return FeeInterface
     */
    public function setEnabled($enabled);

    /**
     * @param string $name
     * @return FeeInterface
     */
    public function setName($name);

    /**
     * @param string $description
     * @return FeeInterface
     */
    public function setDescription($description);

    /**
     * @param string[] $options
     * @return FeeInterface
     */
    public function setOptions($options);

    /**
     * @param mixed $currentValue
     * @return FeeInterface
     */
    public function setCurrentValue($currentValue);

    /**
     * @param string $frontendType
     * @return FeeInterface
     */
    public function setFrontendType($frontendType);

    /**
     * @param mixed $discountInSubtotal
     * @return FeeInterface
     */
    public function setDiscountInSubtotal($discountInSubtotal);

    /**
     * @param mixed $taxInSubtotal
     * @return FeeInterface
     */
    public function setTaxInSubtotal($taxInSubtotal = Excludeinclude::VAR_DEFAULT);

    /**
     * @param mixed $shippingInSubtotal
     * @return FeeInterface
     */
    public function setShippingInSubtotal($shippingInSubtotal);

    /**
     * @param @param string|null $conditionsSerialized
     * @return FeeInterface
     */
    public function setConditionsSerialized($conditionsSerialized);

    /**
     * @param int $sortOrder
     * @return FeeInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * @param int $entityId
     * @return FeeInterface
     */
    public function setId($entityId);

    /**
     * @param mixed $groupId
     * @return FeeInterface
     */
    public function setGroupId($groupId);

    /**
     * @param mixed $storeId
     * @return FeeInterface
     */
    public function setStoreId($storeId);

    /**
     * @param mixed $baseOptions
     * @return FeeInterface
     */
    public function setBaseOptions($baseOptions);

    /**
     * @param bool $flag
     * @return FeeInterface
     */
    public function setIsRequired($flag);

    /**
     * @param bool $flag
     * @return FeeInterface
     */
    public function setIsEligibleForRefund($flag);

    /**
     * @param bool $flag
     * @return FeeInterface
     */
    public function setIsPerProduct(bool $flag);

    /**
     * @param string|null $conditionsSerialized
     * @return FeeInterface
     */
    public function setProductConditionsSerialized(?string $conditionsSerialized);
}
