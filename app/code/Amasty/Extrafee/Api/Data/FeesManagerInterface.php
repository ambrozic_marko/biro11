<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api\Data;

interface FeesManagerInterface
{
    public const TOTALS = 'totals';
    public const FEES = 'fee';

    /**
     * @param \Amasty\Extrafee\Api\Data\FeeInterface[] $fees
     * @return \Amasty\Extrafee\Api\Data\FeesManagerInterface
     */
    public function setFees($fees);

    /**
     * @param \Magento\Quote\Api\Data\TotalsInterface $totals
     * @return \Amasty\Extrafee\Api\Data\FeesManagerInterface
     */
    public function setTotals($totals);

    /**
     * @return \Amasty\Extrafee\Api\Data\FeeInterface[]
     */
    public function getFees();

    /**
     * @return \Magento\Quote\Api\Data\TotalsInterface
     */
    public function getTotals();
}
