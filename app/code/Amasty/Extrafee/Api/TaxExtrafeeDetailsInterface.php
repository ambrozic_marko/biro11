<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api;

interface TaxExtrafeeDetailsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    public const VALUE_INCL_TAX ='value_incl_tax';
    public const VALUE_EXCL_TAX = 'value_excl_tax';
    public const ITEMS = 'items';

    /**
     * @return float
     */
    public function getValueInclTax();

    /**
     * @param $amoutInclTax
     * @return TaxExtrafeeDetailsInterface
     */
    public function setValueInclTax($amoutInclTax);

    /**
     * @return float
     */
    public function getValueExclTax();

    /**
     * @param $amountExclTax
     * @return TaxExtrafeeDetailsInterface
     */
    public function setValueExclTax($amountExclTax);

    /**
     * @return mixed
     */
    public function getItems();

    /**
     * @param string $items
     * @return TaxExtrafeeDetailsInterface
     */
    public function setItems($items);
}
