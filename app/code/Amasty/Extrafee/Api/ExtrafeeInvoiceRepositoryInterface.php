<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api;

use Amasty\Extrafee\Api\Data\ExtrafeeInvoiceInterface;

interface ExtrafeeInvoiceRepositoryInterface
{
    /**
     * Save
     *
     * @param ExtrafeeInvoiceInterface $invoiceFee
     *
     * @return ExtrafeeInvoiceInterface
     */
    public function save(ExtrafeeInvoiceInterface $invoiceFee): ExtrafeeInvoiceInterface;

    /**
     * Get by id
     *
     * @param int $entityId
     *
     * @return ExtrafeeInvoiceInterface
     */
    public function getById(int $entityId): ExtrafeeInvoiceInterface;

    /**
     * Delete
     *
     * @param ExtrafeeInvoiceInterface $invoiceFee
     *
     * @return bool true on success
     */
    public function delete(ExtrafeeInvoiceInterface $invoiceFee): bool;
}
