<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Api;

use Magento\Checkout\Api\Data\TotalsInformationInterface;

interface GuestFeesInformationManagementInterface
{
    /**
     * @param string $cartId
     * @param TotalsInformationInterface $addressInformation
     *
     * @return \Amasty\Extrafee\Api\Data\FeesManagerInterface
     */
    public function collect(
        $cartId,
        TotalsInformationInterface $addressInformation
    );
}
