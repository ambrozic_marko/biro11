<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Setup;

use Amasty\Extrafee\Model\ResourceModel\ExtrafeeCreditmemo;
use Amasty\Extrafee\Model\ResourceModel\ExtrafeeInvoice;
use Amasty\Extrafee\Model\ResourceModel\ExtrafeeOrder;
use Amasty\Extrafee\Model\ResourceModel\ExtrafeeQuote;
use Amasty\Extrafee\Model\ResourceModel\Fee;
use Amasty\Extrafee\Model\ResourceModel\Option;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

class Uninstall implements UninstallInterface
{
    /**
     * @param SchemaSetupInterface $installer
     * @param ModuleContextInterface $context
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function uninstall(SchemaSetupInterface $installer, ModuleContextInterface $context): void
    {
        $installer->startSetup();
        
        $this->dropTables($installer);
        $this->clearConfigData($installer);

        $installer->endSetup();
    }

    private function dropTables(SchemaSetupInterface $installer): void
    {
        $tablesToDrop = [
            Fee::TABLE_NAME,
            ExtrafeeCreditmemo::TABLE_NAME,
            Fee::GROUP_TABLE_NAME,
            Fee::STORE_TABLE_NAME,
            ExtrafeeInvoice::TABLE_NAME,
            Option::TABLE_NAME,
            ExtrafeeOrder::TABLE_NAME,
            ExtrafeeQuote::TABLE_NAME
        ];

        foreach ($tablesToDrop as $table) {
            $installer->getConnection()->dropTable(
                $installer->getTable($table)
            );
        }
    }

    private function clearConfigData(SchemaSetupInterface $installer): void
    {
        $configTable = $installer->getTable('core_config_data');
        $installer->getConnection()->delete($configTable, "`path` LIKE 'amasty_affiliate%'");
    }
}
