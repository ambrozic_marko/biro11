<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Setup;

use Amasty\Base\Setup\SerializedFieldDataConverter;
use Amasty\Extrafee\Api\Data\FeeInterface;
use Amasty\Extrafee\Model\ResourceModel\Fee;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\ProductMetadataInterface;

/**
 * Recurring Post-Updates Data script
 */
class RecurringData implements InstallDataInterface
{
    /**
     * @var SerializedFieldDataConverter
     */
    private $fieldDataConverter;

    /**
     * @var ProductMetadataInterface
     */
    private $productMetadata;

    public function __construct(
        ProductMetadataInterface $productMetadata,
        SerializedFieldDataConverter $fieldDataConverter
    ) {
        $this->productMetadata = $productMetadata;
        $this->fieldDataConverter = $fieldDataConverter;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context): void
    {
        if (version_compare($this->productMetadata->getVersion(), '2.2', '>=')) {
            $this->convertSerializedDataToJson($setup);
        }
    }

    public function convertSerializedDataToJson(ModuleDataSetupInterface $setup): void
    {
        $this->fieldDataConverter->convertSerializedDataToJson(
            $setup->getTable(Fee::TABLE_NAME),
            FeeInterface::ENTITY_ID,
            [FeeInterface::CONDITIONS_SERIALIZED]
        );
    }
}
