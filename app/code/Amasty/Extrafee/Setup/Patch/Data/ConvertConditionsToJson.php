<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Setup\Patch\Data;

use Amasty\Base\Setup\SerializedFieldDataConverter;
use Amasty\Extrafee\Api\Data\FeeInterface;
use Amasty\Extrafee\Model\ResourceModel\Fee;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class ConvertConditionsToJson implements DataPatchInterface
{
    /**
     * @var SerializedFieldDataConverter
     */
    private $fieldDataConverter;

    public function __construct(SerializedFieldDataConverter $fieldDataConverter)
    {
        $this->fieldDataConverter = $fieldDataConverter;
    }

    public function apply(): void
    {
        $this->fieldDataConverter->convertSerializedDataToJson(
            Fee::TABLE_NAME,
            FeeInterface::ENTITY_ID,
            [FeeInterface::CONDITIONS_SERIALIZED]
        );
    }

    public function getAliases(): array
    {
        return [];
    }

    public static function getDependencies(): array
    {
        return [];
    }
}
