<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Setup\Patch\Data;

use Amasty\Extrafee\Api\Data\ExtrafeeOrderInterface;
use Amasty\Extrafee\Api\Data\ExtrafeeQuoteInterface;
use Amasty\Extrafee\Api\FeeRepositoryInterface;
use Amasty\Extrafee\Model\Fee as ModelFee;
use Amasty\Extrafee\Model\ResourceModel\ExtrafeeCreditmemo;
use Amasty\Extrafee\Model\ResourceModel\ExtrafeeInvoice;
use Amasty\Extrafee\Model\ResourceModel\ExtrafeeOrder;
use Amasty\Extrafee\Model\ResourceModel\ExtrafeeQuote;
use Amasty\Extrafee\Model\ResourceModel\Fee;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Module\ResourceInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class QuoteToOrderDataMigration implements DataPatchInterface
{
    public const ORDER_ID = 'order_id';
    public const INVOICE_ID = 'invoice_id';
    public const CREDITMEMO_ID = 'creditmemo_id';
    public const LABEL = 'fee_label';
    
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var ResourceInterface
     */
    private $moduleResource;

    /**
     * @var FeeRepositoryInterface
     */
    private $feeRepository;

    public function __construct(
        ResourceConnection $resourceConnection,
        ResourceInterface $moduleResource,
        FeeRepositoryInterface $feeRepository
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->moduleResource = $moduleResource;
        $this->feeRepository = $feeRepository;
    }
    public function apply(): void
    {
        $setupVersion = $this->moduleResource->getDBVersion('Amasty_Extrafee');

        //we need to migrate data only if upgrade module from version less than 1.6.0
        if ($setupVersion && version_compare($setupVersion, '1.6.0', '<')) {
            $this->insertDataToExtrafee($this->getExtrafeeQuotesWithOrders());
            $this->addToExistFeesEligibleForRefund();
        }
    }

    public function getAliases(): array
    {
        return [];
    }

    public static function getDependencies(): array
    {
        return [];
    }

    private function getExtrafeeQuotesWithOrders(): array
    {
        $connection = $this->resourceConnection->getConnection();
        $salesOrderTable = $this->resourceConnection->getTableName('sales_order');
        $salesInvoiceTable = $this->resourceConnection->getTableName('sales_invoice');
        $salesCreditmemoTable = $this->resourceConnection->getTableName('sales_creditmemo');
        $extrafeeTable = $this->resourceConnection->getTableName(Fee::TABLE_NAME);
        $extrafeeQuoteTable = $this->resourceConnection->getTableName(ExtrafeeQuote::TABLE_NAME);

        $select = $connection->select()
            ->from($extrafeeQuoteTable)
            ->where('option_id !=(?)', 0)
            ->joinInner(
                ['salesorder' => $salesOrderTable],
                'salesorder.quote_id = ' . $extrafeeQuoteTable . '.quote_id',
                [
                    'salesorder.entity_id as ' . self::ORDER_ID,
                ]
            )
            ->joinLeft(
                ['salesinvoice' => $salesInvoiceTable],
                'salesinvoice.order_id = salesorder.entity_id',
                [
                    'salesinvoice.entity_id as ' . self::INVOICE_ID
                ]
            )
            ->joinLeft(
                ['salescreditmemo' => $salesCreditmemoTable],
                'salescreditmemo.order_id = salesorder.entity_id',
                [
                    'salescreditmemo.entity_id as ' . self::CREDITMEMO_ID
                ]
            )
            ->joinInner(
                ['extrafee' => $extrafeeTable],
                'extrafee.entity_id = ' . $extrafeeQuoteTable . '.fee_id',
                [
                    'extrafee.name as ' . self::LABEL,
                ]
            );

        return $connection->fetchAll($select);
    }

    private function insertDataToExtrafee(array $extrafeeQuotesWithOrders): void
    {
        foreach ($extrafeeQuotesWithOrders as $extrafeeQuote) {
            $isRefunded = false;
            $baseTotalAmountInvoiced = $totalAmountInvoiced = $baseTaxAmountInvoiced = $taxAmountInvoiced = 0.00;
            $baseTotalAmountRefunded = $totalAmountRefunded = $baseTaxAmountRefunded = $taxAmountRefunded = 0.00;

            if (empty($extrafeeQuote[self::LABEL])) {
                $extrafeeQuote[self::LABEL] = 'Surcharge';
            }

            if (!empty($extrafeeQuote[self::INVOICE_ID])) {
                $baseTotalAmountInvoiced = (float)$extrafeeQuote[ExtrafeeQuoteInterface::BASE_FEE_AMOUNT];
                $totalAmountInvoiced = (float)$extrafeeQuote[ExtrafeeQuoteInterface::FEE_AMOUNT];
                $baseTaxAmountInvoiced = (float)$extrafeeQuote[ExtrafeeQuoteInterface::BASE_TAX_AMOUNT];
                $taxAmountInvoiced = (float)$extrafeeQuote[ExtrafeeQuoteInterface::TAX_AMOUNT];

                $table = $this->resourceConnection->getTableName(ExtrafeeInvoice::TABLE_NAME);
                $this->insertEntityToExtrafee($table, $extrafeeQuote, self::INVOICE_ID);
            }

            if (!empty($extrafeeQuote[self::CREDITMEMO_ID])) {
                $baseTotalAmountRefunded = (float)$extrafeeQuote[ExtrafeeQuoteInterface::BASE_FEE_AMOUNT];
                $totalAmountRefunded = (float)$extrafeeQuote[ExtrafeeQuoteInterface::FEE_AMOUNT];
                $baseTaxAmountRefunded = (float)$extrafeeQuote[ExtrafeeQuoteInterface::BASE_TAX_AMOUNT];
                $taxAmountRefunded = (float)$extrafeeQuote[ExtrafeeQuoteInterface::TAX_AMOUNT];
                $isRefunded = true;

                $table = $this->resourceConnection->getTableName(ExtrafeeCreditmemo::TABLE_NAME);
                $this->insertEntityToExtrafee($table, $extrafeeQuote, self::CREDITMEMO_ID);
            }

            $this->insertOrderToExtrafee(
                $extrafeeQuote,
                $baseTotalAmountInvoiced,
                $baseTotalAmountRefunded,
                $totalAmountInvoiced,
                $totalAmountRefunded,
                $baseTaxAmountInvoiced,
                $baseTaxAmountRefunded,
                $taxAmountInvoiced,
                $taxAmountRefunded,
                $isRefunded
            );
        }
    }

    private function insertOrderToExtrafee(
        array $extrafeeQuote,
        float $baseTotalAmountInvoiced,
        float $baseTotalAmountRefunded,
        float $totalAmountInvoiced,
        float $totalAmountRefunded,
        float $baseTaxAmountInvoiced,
        float $baseTaxAmountRefunded,
        float $taxAmountInvoiced,
        float $taxAmountRefunded,
        bool $isRefunded
    ): void {
        $this->resourceConnection->getConnection()->insert(
            $this->resourceConnection->getTableName(ExtrafeeOrder::TABLE_NAME),
            [
                ExtrafeeOrderInterface::BASE_TOTAL => $extrafeeQuote[ExtrafeeQuoteInterface::BASE_FEE_AMOUNT],
                ExtrafeeOrderInterface::BASE_TOTAL_INVOICED => $baseTotalAmountInvoiced,
                ExtrafeeOrderInterface::BASE_TOTAL_REFUNDED => $baseTotalAmountRefunded,
                ExtrafeeOrderInterface::TOTAL => $extrafeeQuote[ExtrafeeQuoteInterface::FEE_AMOUNT],
                ExtrafeeOrderInterface::TOTAL_INVOICED => $totalAmountInvoiced,
                ExtrafeeOrderInterface::TOTAL_REFUNDED => $totalAmountRefunded,
                ExtrafeeOrderInterface::BASE_TAX => $extrafeeQuote[ExtrafeeQuoteInterface::BASE_TAX_AMOUNT],
                ExtrafeeOrderInterface::BASE_TAX_INVOICED => $baseTaxAmountInvoiced,
                ExtrafeeOrderInterface::BASE_TAX_REFUNDED => $baseTaxAmountRefunded,
                ExtrafeeOrderInterface::TAX => $extrafeeQuote[ExtrafeeQuoteInterface::TAX_AMOUNT],
                ExtrafeeOrderInterface::TAX_INVOICED => $taxAmountInvoiced,
                ExtrafeeOrderInterface::TAX_REFUNDED => $taxAmountRefunded,
                ExtrafeeOrderInterface::LABEL => $extrafeeQuote[self::LABEL],
                ExtrafeeOrderInterface::OPTION_LABEL => $extrafeeQuote[ExtrafeeQuoteInterface::LABEL],
                ExtrafeeOrderInterface::IS_REFUNDED => $isRefunded,
                ExtrafeeOrderInterface::ORDER_ID => $extrafeeQuote[self::ORDER_ID],
                ExtrafeeOrderInterface::FEE_ID => $extrafeeQuote[ExtrafeeQuoteInterface::FEE_ID],
                ExtrafeeOrderInterface::OPTION_ID => $extrafeeQuote[ExtrafeeQuoteInterface::OPTION_ID]
            ]
        );
    }

    private function insertEntityToExtrafee(string $table, array $extrafeeQuote, string $entityIdentifier): void
    {
        $this->resourceConnection->getConnection()->insert(
            $table,
            [
                'base_total_amount' => $extrafeeQuote[ExtrafeeQuoteInterface::BASE_FEE_AMOUNT],
                'total_amount' => $extrafeeQuote[ExtrafeeQuoteInterface::FEE_AMOUNT],
                'base_tax_amount' => $extrafeeQuote[ExtrafeeQuoteInterface::BASE_TAX_AMOUNT],
                'tax_amount' => $extrafeeQuote[ExtrafeeQuoteInterface::TAX_AMOUNT],
                'fee_label' => $extrafeeQuote[self::LABEL],
                'fee_option_label' => $extrafeeQuote[ExtrafeeQuoteInterface::LABEL],
                'order_id' => $extrafeeQuote[self::ORDER_ID],
                $entityIdentifier => $extrafeeQuote[$entityIdentifier],
                'fee_id' => $extrafeeQuote[ExtrafeeQuoteInterface::FEE_ID],
                'option_id' => $extrafeeQuote[ExtrafeeQuoteInterface::OPTION_ID]
            ]
        );
    }
    
    private function addToExistFeesEligibleForRefund(): void
    {
        $feeList = $this->feeRepository->getList();
        /** @var ModelFee $fee */
        foreach ($feeList->getItems() as $fee) {
            $fee->setIsEligibleForRefund(true);
            $this->feeRepository->save($fee, []);
        }
    }
}
