<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Controller\Adminhtml\Index;

use Magento\Backend\Model\View\Result\Forward;
use Magento\Framework\Controller\ResultFactory;

class NewAction extends Index
{
    /**
     * @return Forward
     */
    public function execute()
    {
        $resultForward = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
        $resultForward->forward('edit');

        return $resultForward;
    }
}
