<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;

abstract class Index extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Amasty_Extrafee::manage';

    /**
     * @param Page $resultPage
     */
    protected function prepareDefaultTitle(Page $resultPage)
    {
        $resultPage->getConfig()->getTitle()->prepend(__('Fees'));
    }
}
