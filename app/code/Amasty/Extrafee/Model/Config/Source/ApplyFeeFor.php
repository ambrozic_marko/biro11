<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ApplyFeeFor implements OptionSourceInterface
{
    public const FOR_CART = 0;
    public const PER_PRODUCT = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::FOR_CART,
                'label' => __('Whole Cart')
            ],
            [
                'value' => self::PER_PRODUCT,
                'label' => __('Each Product in the Cart')
            ]
        ];
    }
}
