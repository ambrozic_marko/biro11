<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Model\ResourceModel;

use Amasty\Extrafee\Api\Data\ExtrafeeCreditmemoInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ExtrafeeCreditmemo extends AbstractDb
{
    public const TABLE_NAME = 'amasty_extrafee_creditmemo';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, ExtrafeeCreditmemoInterface::ENTITY_ID);
    }
}
