<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Model\ResourceModel;

use Amasty\Extrafee\Api\Data\ExtrafeeInvoiceInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ExtrafeeInvoice extends AbstractDb
{
    public const TABLE_NAME = 'amasty_extrafee_invoice';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, ExtrafeeInvoiceInterface::ENTITY_ID);
    }
}
