<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Model\Fee\Source;

use Amasty\Extrafee\Model\Fee;
use Magento\Framework\Data\OptionSourceInterface;

class FrontendType implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Checkbox'),
                'value' => Fee::FRONTEND_TYPE_CHECKBOX
            ],
            [
                'label' => __('Dropdown'),
                'value' => Fee::FRONTEND_TYPE_DROPDOWN
            ],
            [
                'label' => __('Radio Button'),
                'value' => Fee::FRONTEND_TYPE_RADIO
            ]
        ];
    }
}
