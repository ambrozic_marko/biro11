<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Extra Fee for Magento 2
 */

namespace Amasty\Extrafee\Model\Fee\Source;

use Amasty\Extrafee\Model\Fee;
use Magento\Framework\Data\OptionSourceInterface;

class PriceType implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Fixed'),
                'value' => Fee::PRICE_TYPE_FIXED
            ],
            [
                'label' => __('Percent'),
                'value' => Fee::PRICE_TYPE_PERCENT
            ]
        ];
    }
}
