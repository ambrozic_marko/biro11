<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Setup\Patch\Data;

use Amasty\Base\Model\Serializer;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddConsents implements DataPatchInterface
{
    /**
     * @var Config
     */
    private Config $configResource;

    /**
     * @var Serializer
     */
    private Serializer $serializer;

    public function __construct(
        Config $configResource,
        Serializer $serializer
    ) {
        $this->configResource = $configResource;
        $this->serializer = $serializer;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function getAliases(): array
    {
        return [];
    }

    public function apply(): self
    {
        $data = $this->getConfigValues();
        foreach ($data as &$record) {
            $value = $this->serializer->unserialize($record['value']);
            $value['item3'] = [
                'consent_type' => 'ad_user_data',
                'default_status' => '0',
                'cookie_group' => '2'
            ];
            $value['item4'] = [
                'consent_type' => 'ad_personalization',
                'default_status' => '0',
                'cookie_group' => '2'
            ];
            $record['value'] = $this->serializer->serialize($value);
        }
        if ($data) {
            $this->configResource->getConnection()->insertOnDuplicate(
                $this->configResource->getMainTable(),
                $data
            );
        }

        return $this;
    }

    private function getConfigValues(): array
    {
        $connection = $this->configResource->getConnection();
        $select = $connection->select()->from(
            $this->configResource->getMainTable()
        )->where(
            'path = ?',
            'am_ga4/consent_mode/consent_types'
        );

        return $connection->fetchAll($select);
    }
}
