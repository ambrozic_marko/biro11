<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Block;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;

class DataLayer extends Template implements IdentityInterface
{
    public const IDENTITY_KEY = 'am_ga4_data_layer';

    /**
     * @var string
     */
    protected $_template = 'Amasty_GA4::datalayer/init-datalayer.phtml';

    public function getIdentities(): array
    {
        return [self::IDENTITY_KEY];
    }
}
