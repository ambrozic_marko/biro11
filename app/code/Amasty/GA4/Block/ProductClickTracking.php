<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Block;

use Amasty\Base\Model\Serializer;
use Magento\Framework\View\Element\Template;

class ProductClickTracking extends Template
{
    public const PRODUCTS_EVENT_DATA = 'products_event_data';

    /**
     * @var string
     */
    protected $_template = 'Amasty_GA4::product/click_tracking.phtml';

    /**
     * @var Serializer
     */
    private Serializer $serializer;

    public function __construct(
        Serializer $serializer,
        Template\Context $context,
        array $data = []
    ) {
        $this->serializer = $serializer;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getProductEventData(): string
    {
        return $this->serializer->serialize($this->getData(self::PRODUCTS_EVENT_DATA) ?? []);
    }

    protected function _toHtml()
    {
        if (!empty($this->getData(self::PRODUCTS_EVENT_DATA))) {
            return parent::_toHtml();
        }

        return '';
    }
}
