<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Block\Adminhtml\System\Config\InformationBlocks;

use Amasty\GA4\Model\ThirdParty\AdditionalExtensionsChecker;
use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;

class MeasurementProtocolLibNotification extends Template
{
    /**
     * @var AdditionalExtensionsChecker
     */
    private AdditionalExtensionsChecker $additionalExtensionsChecker;

    public function __construct(
        Template\Context $context,
        AdditionalExtensionsChecker $additionalExtensionsChecker,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->additionalExtensionsChecker = $additionalExtensionsChecker;
    }

    /**
     * @var string
     */
    protected $_template = 'Amasty_GA4::config/information/mp_lib_notification.phtml';

    public function getNotificationText(): Phrase
    {
        return __(
            'To work with the Measurement Protocol, the Google Analytics 4 Measurement Protocol PHP library '
            . 'is required. Please, run the following command in the SSH: composer require br33f/php-ga4-mp'
        );
    }

    protected function _toHtml(): string
    {
        if ($this->additionalExtensionsChecker->isGa4MPInstalled()) {
            return '';
        }

        return parent::_toHtml();
    }
}
