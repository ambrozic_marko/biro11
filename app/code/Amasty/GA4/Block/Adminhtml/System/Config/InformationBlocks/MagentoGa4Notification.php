<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Block\Adminhtml\System\Config\InformationBlocks;

use Amasty\GA4\Model\Config\ConfigScopeResolver;
use Amasty\GA4\Model\ConfigProvider;
use Magento\Framework\View\Element\Template;

class MagentoGa4Notification extends Template
{
    private const GA_XML_PATH_ACTIVE = 'google/analytics/active';
    private const GTAG_XML_PATH_ACTIVE = 'google/gtag/analytics4/active';

    /**
     * @var string
     */
    protected $_template = 'Amasty_GA4::config/information/magento_ga4_notification.phtml';

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var ConfigScopeResolver
     */
    private ConfigScopeResolver $configScopeResolver;

    public function __construct(
        Template\Context $context,
        ConfigProvider $configProvider,
        ConfigScopeResolver $configScopeResolver,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->configProvider = $configProvider;
        $this->configScopeResolver = $configScopeResolver;
    }

    public function getNotificationText(): string
    {
        $url = $this->_urlBuilder->getUrl('adminhtml/system_config/edit/section/google/');

        return __(
            'For the correct extension work please disable the '
            . '<a href="%1" target="_blank">Adobe Commerce Google Analytics</a> feature.',
            $this->_escaper->escapeUrl($url)
        )->render();
    }

    protected function _toHtml()
    {
        if (!$this->isDisplayMessage()) {
            return '';
        }

        return parent::_toHtml();
    }

    private function isDisplayMessage(): bool
    {
        [$scopeType, $scopeCode] = $this->configScopeResolver->resolve();
        $gtagEnabled = $this->_scopeConfig->isSetFlag(self::GTAG_XML_PATH_ACTIVE, $scopeType, $scopeCode);
        $gaEnabled = $this->_scopeConfig->isSetFlag(self::GA_XML_PATH_ACTIVE, $scopeType, $scopeCode);

        return $this->configProvider->isEnabled((int)$scopeCode, (string)$scopeType) && ($gtagEnabled || $gaEnabled);
    }
}
