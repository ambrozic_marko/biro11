<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Block\Adminhtml\System\Config\InformationBlocks;

use Amasty\GA4\Model\ThirdParty\AdditionalExtensionsChecker;
use Detection\MobileDetect;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;

class MobileDetectNotification extends Template
{
    /**
     * @var AdditionalExtensionsChecker|null
     */
    private ?AdditionalExtensionsChecker $additionalExtensionsChecker;

    public function __construct(
        Template\Context $context,
        array $data = [],
        AdditionalExtensionsChecker $additionalExtensionsChecker = null
    ) {
        parent::__construct($context, $data);
        $this->additionalExtensionsChecker = $additionalExtensionsChecker
            ?: ObjectManager::getInstance()->get(AdditionalExtensionsChecker::class);
    }

    /**
     * @var string
     */
    protected $_template = 'Amasty_GA4::config/information/mobile_detect_notification.phtml';

    public function getNotificationText(): Phrase
    {
        return __(
            'We recommend using the 3rd-party library to improve the mobile detection algorithm. '
            . 'Please, run the following command in the SSH: composer require mobiledetect/mobiledetectlib'
        );
    }

    protected function _toHtml(): string
    {
        if ($this->additionalExtensionsChecker->isMobileDetectInstalled()) {
            return '';
        }

        return parent::_toHtml();
    }
}
