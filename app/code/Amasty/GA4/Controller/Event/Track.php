<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Controller\Event;

use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface;

class Track implements HttpPostActionInterface
{
    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * @var ResultFactory
     */
    private ResultFactory $resultFactory;

    /**
     * @var EventsProcessor
     */
    private EventsProcessor $eventsProcessor;

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(
        RequestInterface $request,
        ResultFactory $resultFactory,
        EventsProcessor $eventsProcessor,
        DataLayerCollector $dataLayerCollector,
        LoggerInterface $logger
    ) {
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->eventsProcessor = $eventsProcessor;
        $this->dataLayerCollector = $dataLayerCollector;
        $this->logger = $logger;
    }

    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        // Contains all parameters that was passed to tracked ajax request
        $data = $this->request->getParams();
        if (!isset($data['event'])) {
            return $resultJson;
        }

        try {
            $this->eventsProcessor->processEvent($data['event'], $data);
            $resultJson->setJsonData($this->dataLayerCollector->collect());
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return $resultJson;
    }
}
