<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Controller\Adminhtml\Config;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Model\Config\ConfigScopeResolver;
use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\EventsMetaCollector;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class GenerateJson extends Action implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Amasty_GA4::config';

    /**
     * @var EventsMetaCollector
     */
    private EventsMetaCollector $eventsMetaCollector;

    /**
     * @var Serializer
     */
    private Serializer $serializer;

    /**
     * @var ConfigScopeResolver
     */
    private ConfigScopeResolver $configScopeResolver;

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    public function __construct(
        Context $context,
        EventsMetaCollector $eventsMetaCollector,
        Serializer $serializer,
        ConfigScopeResolver $configScopeResolver,
        ConfigProvider $configProvider
    ) {
        parent::__construct($context);
        $this->eventsMetaCollector = $eventsMetaCollector;
        $this->serializer = $serializer;
        $this->configScopeResolver = $configScopeResolver;
        $this->configProvider = $configProvider;
    }

    public function execute()
    {
        $this->resolveConfigScope();
        $this->prepareResponse($this->eventsMetaCollector->collect());

        return $this->getResponse();
    }

    private function resolveConfigScope(): void
    {
        [$scopeType, $scopeCode] = $this->configScopeResolver->resolve();
        $this->configProvider->overrideScope($scopeType, (int)$scopeCode);
    }

    private function prepareResponse(array $meta): void
    {
        if ($this->getResponse()->getHeaders()) {
            $this->getResponse()->getHeaders()->clearHeaders();
            $this->getResponse()->setHeader('Content-Type', 'application/json')
                ->setHeader("Content-Disposition", "attachment; filename=ga4_export.json")
                ->setBody($this->serializer->serialize($meta));
        }
    }
}
