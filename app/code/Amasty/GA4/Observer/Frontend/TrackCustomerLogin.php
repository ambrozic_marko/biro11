<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Observer\Frontend;

use Amasty\GA4\Block\DataLayer;
use Amasty\GA4\Model\Event\Customer\Login as LoginEvent;
use Amasty\GA4\Model\Event\Customer\SignUp;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4\Model\Frontend\GA4Session;
use Magento\Customer\Model\Session;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * customer_login
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class TrackCustomerLogin implements ObserverInterface
{
    private const DEFAULT_TYPE = 'email';

    /**
     * @var Session
     */
    private Session $customerSession;

    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;

    /**
     * @var GA4Session
     */
    private GA4Session $ga4Session;

    /**
     * @var EventsProcessor
     */
    private EventsProcessor $eventsProcessor;

    public function __construct(
        Session $customerSession,
        CacheInterface $cache,
        GA4Session $ga4Session,
        EventsProcessor $eventsProcessor
    ) {
        $this->customerSession = $customerSession;
        $this->cache = $cache;
        $this->ga4Session = $ga4Session;
        $this->eventsProcessor = $eventsProcessor;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(Observer $observer)
    {
        if (!empty($this->ga4Session->getEventByName(SignUp::NAME))) {
            return; //login after registration, must track signup
        }

        $type = $this->customerSession->getType(); //social login
        if (!$type) {
            $type = self::DEFAULT_TYPE;
        }
        $this->eventsProcessor->processEvent(LoginEvent::NAME, ['type' => $type]);

        if ($this->customerSession->getCustomerGroupId()) {
            $this->ga4Session->setCustomerGroupId((int)$this->customerSession->getCustomerGroupId());
        }
        if ($this->customerSession->getCustomerId()) {
            $this->ga4Session->setCustomerId((int)$this->customerSession->getCustomerId());
        }

        $this->cache->clean([DataLayer::IDENTITY_KEY]);
    }
}
