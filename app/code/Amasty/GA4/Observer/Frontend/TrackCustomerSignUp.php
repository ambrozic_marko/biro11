<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Observer\Frontend;

use Amasty\GA4\Block\DataLayer;
use Amasty\GA4\Model\Event\Customer\SignUp;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4\Model\Frontend\GA4Session;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\State;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Area;

class TrackCustomerSignUp implements ObserverInterface
{
    private const DEFAULT_TYPE = 'email';
    private const OSC_SIGNUP_TYPE = 'amasty_checkout_register';

    /**
     * @var GA4Session
     */
    private GA4Session $ga4Session;

    /**
     * @var CustomerSession
     */
    private CustomerSession $customerSession;

    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;

    /**
     * @var State
     */
    private State $appState;

    /**
     * @var EventsProcessor
     */
    private EventsProcessor $eventsProcessor;

    public function __construct(
        GA4Session $ga4Session,
        CustomerSession $customerSession,
        CacheInterface $cache,
        State $appState,
        EventsProcessor $eventsProcessor
    ) {
        $this->ga4Session = $ga4Session;
        $this->customerSession = $customerSession;
        $this->cache = $cache;
        $this->appState = $appState;
        $this->eventsProcessor = $eventsProcessor;
    }

    public function execute(Observer $observer)
    {
        if ($this->appState->getAreaCode() === Area::AREA_WEBAPI_REST
            && $observer->getData(self::OSC_SIGNUP_TYPE) !== true
        ) {
            return;
        }

        $type = $this->customerSession->getType(); // social login
        if (!$type) {
            $type = self::DEFAULT_TYPE;
        }
        $this->eventsProcessor->processEvent(SignUp::NAME, ['type' => $type]);

        /** @var CustomerInterface $customer */
        $customer = $observer->getData('customer');
        if ($customer->getGroupId() !== null) {
            $this->ga4Session->setCustomerGroupId((int)$customer->getGroupId());
        }
        if ($customer->getId() !== null) {
            $this->ga4Session->setCustomerId((int)$customer->getId());
        }

        $this->cache->clean([DataLayer::IDENTITY_KEY]);
    }
}
