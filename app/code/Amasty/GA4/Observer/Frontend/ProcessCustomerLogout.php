<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Observer\Frontend;

use Amasty\GA4\Model\Frontend\GA4Session;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * customer_logout
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class ProcessCustomerLogout implements ObserverInterface
{
    /**
     * @var GA4Session
     */
    private GA4Session $gA4Session;

    public function __construct(
        GA4Session $gA4Session
    ) {
        $this->gA4Session = $gA4Session;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(Observer $observer)
    {
        $this->gA4Session->setCustomerGroupId(GroupInterface::NOT_LOGGED_IN_ID);
        $this->gA4Session->setCustomerId(null);
    }
}
