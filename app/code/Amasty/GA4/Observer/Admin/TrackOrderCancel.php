<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Observer\Admin;

use Amasty\GA4\Model\Event\Order\Refund;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4\Model\ResourceModel\Order\Handlers\ReadHandler;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class TrackOrderCancel implements ObserverInterface
{
    /**
     * @var EventsProcessor
     */
    private EventsProcessor $eventsProcessor;

    /**
     * @var ReadHandler
     */
    private ReadHandler $readHandler;

    public function __construct(
        EventsProcessor $eventsProcessor,
        ReadHandler $readHandler
    ) {
        $this->eventsProcessor = $eventsProcessor;
        $this->readHandler = $readHandler;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getData('order');
        $this->readHandler->loadAttribute($order);

        $this->eventsProcessor->processEvent(
            Refund::NAME,
            [
                'type' => Refund::NAME,
                'order' => $order
            ]
        );
    }
}
