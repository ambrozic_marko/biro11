<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Observer\Order;

use Amasty\GA4\Model\ResourceModel\Order\Handlers\SaveHandler;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SaveClientData implements ObserverInterface
{
    public const CLIENT_ID_COOKIE = '_ga';

    /**
     * @var SaveHandler
     */
    private SaveHandler $saveHandler;

    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    public function __construct(
        SaveHandler $saveHandler,
        RequestInterface $request
    ) {
        $this->saveHandler = $saveHandler;
        $this->request = $request;
    }

    public function execute(Observer $observer): void
    {
        $ga = $this->request->getCookie(self::CLIENT_ID_COOKIE);
        if (!isset($ga)) {
            return;
        }

        $order = $observer->getEvent()->getOrder();
        if ($orderId = $order->getId()) {
            $this->saveHandler->saveClientData($orderId, $ga);
        }
    }
}
