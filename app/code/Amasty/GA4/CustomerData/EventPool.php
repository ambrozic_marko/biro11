<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\CustomerData;

use Amasty\GA4\Model\Frontend\GA4Session;
use Magento\Customer\CustomerData\SectionSourceInterface;

class EventPool implements SectionSourceInterface
{
    public const EVENTS_DATA = 'eventsData';

    /**
     * @var GA4Session
     */
    private GA4Session $ga4Session;

    public function __construct(
        GA4Session $ga4Session
    ) {
        $this->ga4Session = $ga4Session;
    }

    public function getSectionData(): array
    {
        $result = [self::EVENTS_DATA => []];

        if ($this->ga4Session->hasEvents()) {
            $result[self::EVENTS_DATA] = array_values($this->ga4Session->getEvents(true));
        }

        return $result;
    }
}
