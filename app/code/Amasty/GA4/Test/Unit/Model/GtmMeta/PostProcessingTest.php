<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Unit\Model\GtmMeta;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\GtmMeta\PostProcessing;
use PHPUnit\Framework\TestCase;

class PostProcessingTest extends TestCase
{
    /**
     * @covers PostProcessing::collect
     * @dataProvider collectDataProvider
     */
    public function testCollect(
        int $fingerprint,
        array $data,
        string $accountId,
        string $containerId,
        array $expectedData
    ): void {
        $configProviderMock = $this->createMock(ConfigProvider::class);
        $configProviderMock->expects($this->any())->method('getAccountId')->willReturn($accountId);
        $configProviderMock->expects($this->any())->method('getContainerId')->willReturn($containerId);

        $postProcessing = new PostProcessing($configProviderMock);
        $postProcessing->collect($fingerprint, $data);

        $this->assertEquals($expectedData, $data);
    }

    private function collectDataProvider(): array
    {
        return [
            'keys exist' => [
                1700829112,
                ['containerVersion' => [
                    'variable' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ],
                    'trigger' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ],
                    'tag' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ]
                ]],
                '700374935',
                '2293555',
                ['containerVersion' => [
                    'variable' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ],
                    'trigger' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ],
                    'tag' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ]
                ]]
            ],
            'keys do not exist' => [
                1700829112,
                ['containerVersion' => [
                    'variable' => [[]],
                    'trigger' => [[]],
                    'tag' => [[]],
                ]],
                '700374935',
                '2293555',
                ['containerVersion' => [
                    'variable' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ],
                    'trigger' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ],
                    'tag' => [
                        [
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ]
                ]]
            ]
        ];
    }
}
