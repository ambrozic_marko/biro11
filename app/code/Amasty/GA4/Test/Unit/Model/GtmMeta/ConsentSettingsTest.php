<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Unit\Model\GtmMeta;

use Amasty\GA4\Model\Consent\ConsentModeAdapter;
use Amasty\GA4\Model\GtmMeta\ConsentSettings;
use PHPUnit\Framework\TestCase;

class ConsentSettingsTest extends TestCase
{
    /**
     * @covers ConsentSettings::collect
     * @dataProvider collectDataProvider
     */
    public function testCollect(
        ?int $fingerprint,
        array $data,
        bool $isConsentModeEnabled,
        array $expectedData
    ): void {
        $consentModeMock = $this->createMock(ConsentModeAdapter::class);
        $consentModeMock->expects($this->any())->method('isModuleValid')->willReturn($isConsentModeEnabled);

        $consentSettings = new ConsentSettings($consentModeMock);
        $consentSettings->collect($fingerprint, $data);

        $this->assertEquals($expectedData, $data);
    }

    private function collectDataProvider(): array
    {
        return [
            'tag exists' => [
                null,
                ['containerVersion' => [
                    'tag' => [[]]
                ]],
                true,
                ['containerVersion' => [
                    'tag' => [
                        [
                            'consentSetting' => [
                                'consentStatus' => 'NOT_NEEDED'
                            ]
                        ]
                    ]
                ]]
            ],
            'tag does not exist' => [
                null,
                ['containerVersion' => []],
                true,
                ['containerVersion' => []]
            ],
            'consent mode is disabled' => [
                null,
                ['containerVersion' => [
                    'tag' => [[]]
                ]],
                false,
                ['containerVersion' => [
                    'tag' => [[]]
                ]]
            ]
        ];
    }
}
