<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Unit\Model\GtmMeta;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\GtmMeta\CommonData;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Magento\Framework\Stdlib\DateTime;
use PHPUnit\Framework\TestCase;

class CommonDataTest extends TestCase
{
    public const MEASUREMENT_ID = 'Amasty - MEASUREMENT ID';
    public const MEASUREMENT_TAG_NAME = 'Amasty GA4';
    public const ALL_PAGES_TRIGGER_ID = '2147479553';

    /**
     * @covers CommonData::collect
     * @dataProvider collectDataProvider
     */
    public function testCollect(
        int $fingerprint,
        array $data,
        array $configMockedData,
        array $variables,
        array $basicVariable,
        int $variableId,
        int $triggerId,
        int $tagId,
        array $expectedData
    ): void {
        $configProviderMock = $this->createConfiguredMock(
            ConfigProvider::class,
            $configMockedData
        );
        $variablesProviderMock = $this->createMock(VariablesProvider::class);
        $incrementIdStorageMock = $this->createMock(IncrementIdStorage::class);

        $variablesProviderMock->method('get')->willReturn($variables);
        $variablesProviderMock->method('prepareBasicVariable')->willReturn($basicVariable);
        $incrementIdStorageMock->method('getVariableId')->willReturn($variableId);
        $incrementIdStorageMock->method('getTriggerId')->willReturn($triggerId);
        $incrementIdStorageMock->method('getTagId')->willReturn($tagId);

        $commonData = new CommonData(
            $configProviderMock,
            $variablesProviderMock,
            $incrementIdStorageMock
        );
        $commonData->collect($fingerprint, $data);

        $this->assertEquals($expectedData, $data);
    }

    private function collectDataProvider(): array
    {
        return [
            'collected data' => [
                1700829112,
                ['containerVersion' => [
                    'variable' => [],
                    'trigger' => [],
                    'tag' => []
                ]],
                [
                    'getAccountId' => '700374935',
                    'getContainerId' => '2293555',
                    'getMeasurementId' => 'G-KVS22NJKUY',
                    'getPublicId' => '0987654321'
                ],
                [
                    'name' => self::MEASUREMENT_ID,
                    'type' => 'c',
                    'parameter' => [
                        [
                            'type' => 'TEMPLATE',
                            'key' => 'value',
                            'value' => 'G-KVS22NJKUY'
                        ]
                    ],
                    'accountId' => '700374935',
                    'containerId' => '2293555',
                    'variableId' => 1,
                    'fingerprint' => 1700829112,
                    'formatValue' => new \stdClass()
                ],
                [
                    'name' => self::MEASUREMENT_ID,
                    'type' => 'c',
                    'parameter' => [
                        [
                            'type' => 'TEMPLATE',
                            'key' => 'value',
                            'value' => 'G-KVS22NJKUY'
                        ]
                    ],
                    'accountId' => '700374935',
                    'containerId' => '2293555',
                    'variableId' => 1,
                    'fingerprint' => 1700829112,
                    'formatValue' => new \stdClass()
                ],
                1,
                1,
                1,
                // $expectedData
                [
                    'exportFormatVersion' => 2,
                    'exportTime' => (new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT),
                    'containerVersion' => [
                        'path' => 'accounts/700374935/containers/2293555/versions/0',
                        'accountId' => '700374935',
                        'containerId' => '2293555',
                        'containerVersionId' => '0',
                        'container' => [
                            'path' => 'accounts/700374935/containers/2293555',
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'name' => 'Amasty_GA4 JsonExport',
                            'publicId' => '0987654321',
                            'usageContext' => [0 => 'WEB'],
                            'fingerprint' => 1700829112,
                            'tagManagerUrl' => 'https://tagmanager.google.com/#/container/accounts/700374935/'
                                . 'containers/2293555/workspaces?apiLink=container',
                        ],
                        'builtInVariable' => [
                            0 => [
                                'accountId' => '700374935',
                                'containerId' => '2293555',
                                'type' => 'PAGE_URL',
                                'name' => 'Page URL',
                            ],
                            1 => [
                                'accountId' => '700374935',
                                'containerId' => '2293555',
                                'type' => 'PAGE_HOSTNAME',
                                'name' => 'Page Hostname',
                            ],
                            2 => [
                                'accountId' => '700374935',
                                'containerId' => '2293555',
                                'type' => 'PAGE_PATH',
                                'name' => 'Page Path',
                            ],
                            3 => [
                                'accountId' => '700374935',
                                'containerId' => '2293555',
                                'type' => 'REFERRER',
                                'name' => 'Referrer',
                            ],
                            4 => [
                                'accountId' => '700374935',
                                'containerId' => '2293555',
                                'type' => 'EVENT',
                                'name' => 'Event',
                            ],
                        ],
                        'variable' => [
                            'name' => self::MEASUREMENT_ID,
                            'type' => 'c',
                            'parameter' => [
                                0 => [
                                    'type' => 'TEMPLATE',
                                    'key' => 'value',
                                    'value' => 'G-KVS22NJKUY',
                                ],
                            ],
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'variableId' => 1,
                            'fingerprint' => 1700829112,
                            'formatValue' => new \stdClass(),
                        ],
                        'trigger' => [
                            0 => [
                                'name' => 'Amasty GA4 - gtm.dom',
                                'type' => 'CUSTOM_EVENT',
                                'customEventFilter' => [
                                    0 => [
                                        'type' => 'EQUALS',
                                        'parameter' => [
                                            0 => [
                                                'type' => 'TEMPLATE',
                                                'key' => 'arg0',
                                                'value' => '{{_event}}',
                                            ],
                                            1 => [
                                                'type' => 'TEMPLATE',
                                                'key' => 'arg1',
                                                'value' => 'gtm.dom',
                                            ],
                                        ],
                                    ],
                                ],
                                'filter' => [
                                    0 => [
                                        'type' => 'EQUALS',
                                        'parameter' => [
                                            0 => [
                                                'type' => 'TEMPLATE',
                                                'key' => 'arg0',
                                                'value' => '{{Event}}',
                                            ],
                                            1 => [
                                                'type' => 'TEMPLATE',
                                                'key' => 'arg1',
                                                'value' => 'gtm.dom',
                                            ],
                                        ],
                                    ],
                                ],
                                'accountId' => '700374935',
                                'containerId' => '2293555',
                                'triggerId' => 1,
                                'fingerprint' => 1700829112,
                            ],
                        ],
                        'tag' => [
                            0 => [
                                'name' => self::MEASUREMENT_TAG_NAME,
                                'firingTriggerId' => [0 => self::ALL_PAGES_TRIGGER_ID],
                                'tagFiringOption' => 'ONCE_PER_EVENT',
                                'type' => 'gaawc',
                                'parameter' => [
                                    0 => [
                                        'type' => 'BOOLEAN',
                                        'key' => 'sendPageView',
                                        'value' => 'true',
                                    ],
                                    1 => [
                                        'type' => 'TEMPLATE',
                                        'key' => 'measurementId',
                                        'value' => '{{' . self::MEASUREMENT_ID . '}}',
                                    ],
                                ],
                                'monitoringMetadata' => [
                                    'type' => 'MAP',
                                ],
                                'accountId' => '700374935',
                                'containerId' => '2293555',
                                'tagId' => 1,
                                'fingerprint' => 1700829112,
                                ],
                            ],
                        'fingerprint' => 1700829112,
                        ],
                    'fingerprint' => 1700829112,
                        'tagManagerUrl' => 'https://tagmanager.google.com/#/versions/accounts/700374935/containers/'
                            . '2293555/versions/0?apiLink=version',
                ]
            ]
        ];
    }
}
