<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Unit\Model\GtmMeta;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\GtmMeta\AdWordsConversions;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use PHPUnit\Framework\TestCase;

class AdWordsConversionsTest extends TestCase
{
    public const VAR_CONVERSION_TRACKING_CONVERSION_VALUE = 'Amasty - Conversion Value';
    public const TEMPLATE_CONVERSION_TRACKING_CONVERSION_VALUE = 'am_conversion_value';
    private const DEFAULT_SUCCESS_PAGE = 'checkout/onepage/success';
    public const TAG_CONVERSION_TRACKING = 'Amasty - AdWords Conversion Tracking';
    public const ALL_PAGES_TRIGGER_ID = '2147479553';
    public const VAR_CONVERSION_TRACKING_ORDER_ID = 'Amasty - Conversion Order ID';

    /**
     * @covers AdWordsConversions::collect
     * @dataProvider collectDataProvider
     */
    public function testCollect(
        int $fingerprint,
        array $data,
        array $configMockedData,
        array $prepareVariables,
        int $triggerId,
        int $tagId,
        array $expectedData
    ): void {
        $configProviderMock = $this->createConfiguredMock(
            ConfigProvider::class,
            $configMockedData
        );
        $variablesProviderMock = $this->createMock(VariablesProvider::class);
        $incrementIdStorageMock = $this->createMock(IncrementIdStorage::class);

        $variablesProviderMock->method('prepareBasicVariable')->willReturn($prepareVariables);
        $incrementIdStorageMock->method('getTriggerId')->willReturn($triggerId);
        $incrementIdStorageMock->method('getTagId')->willReturn($tagId);

        $adWordsConversions = new AdWordsConversions(
            $configProviderMock,
            $variablesProviderMock,
            $incrementIdStorageMock
        );
        $adWordsConversions->collect($fingerprint, $data);

        $this->assertEquals($expectedData, $data);
    }

    private function collectDataProvider(): array
    {
        return [
            'adWords conversion is enabled' => [
                1700829112,
                ['containerVersion' => [
                    'variable' => [],
                    'trigger' => [],
                    'tag' => []
                ]],
                [
                    'isAdWordsConversionEnabled' => true,
                    'getAccountId' => '700374935',
                    'getContainerId' => '2293555',
                    'getSuccessPages' => [],
                    'getConversionId' => '1234567890',
                    'getConversionCurrency' => 'USD',
                    'getConversionLabel' => 'conversionLabel'
                ],
                [
                    'name' => self::VAR_CONVERSION_TRACKING_CONVERSION_VALUE,
                    'type' => 'v',
                    'parameter' => [
                        [
                            'type' => 'INTEGER',
                            'key' => 'dataLayerVersion',
                            'value' => '2'
                        ],
                        [
                            'type' => 'BOOLEAN',
                            'key' => 'setDefaultValue',
                            'value' => 'false'
                        ],
                        [
                            'type' => 'TEMPLATE',
                            'key' => 'name',
                            'value' => self::TEMPLATE_CONVERSION_TRACKING_CONVERSION_VALUE
                        ]
                    ],
                    'accountId' => '700374935',
                    'containerId' => '2293555',
                    'variableId' => 1,
                    'fingerprint' => 1700829112,
                    'formatValue' => new \stdClass()
                ],
                1,
                1,
                // $expectedData
                ['containerVersion' => [
                    'variable' => [
                        0 => [
                            'name' => self::VAR_CONVERSION_TRACKING_CONVERSION_VALUE,
                            'type' => 'v',
                            'parameter' => [
                                0 => [
                                    'type' => 'INTEGER',
                                    'key' => 'dataLayerVersion',
                                    'value' => '2',
                                ],
                                1 => [
                                    'type' => 'BOOLEAN',
                                    'key' => 'setDefaultValue',
                                    'value' => 'false',
                                ],
                                2 => [
                                    'type' => 'TEMPLATE',
                                    'key' => 'name',
                                    'value' => self::TEMPLATE_CONVERSION_TRACKING_CONVERSION_VALUE,
                                ],
                            ],
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'variableId' => 1,
                            'fingerprint' => 1700829112,
                            'formatValue' => new \stdClass(),
                        ],
                        1 => [
                            'name' => self::VAR_CONVERSION_TRACKING_CONVERSION_VALUE,
                            'type' => 'v',
                            'parameter' => [
                                0 => [
                                    'type' => 'INTEGER',
                                    'key' => 'dataLayerVersion',
                                    'value' => '2',
                                ],
                                1 => [
                                    'type' => 'BOOLEAN',
                                    'key' => 'setDefaultValue',
                                    'value' => 'false',
                                ],
                                2 => [
                                    'type' => 'TEMPLATE',
                                    'key' => 'name',
                                    'value' => self::TEMPLATE_CONVERSION_TRACKING_CONVERSION_VALUE,
                                ],
                            ],
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'variableId' => 1,
                            'fingerprint' => 1700829112,
                            'formatValue' => new \stdClass(),
                        ],
                    ],
                    'trigger' => [
                        0 => [
                            'name' => 'Amasty - Checkout Success Pages',
                            'type' => 'PAGEVIEW',
                            'triggerId' => 1,
                            'filter' => [
                                'type' => 'MATCH_REGEX',
                                'parameter' => [
                                    0 => [
                                        'type' => 'TEMPLATE',
                                        'key' => 'arg0',
                                        'value' => '{{Page URL}}',
                                    ],
                                    1 => [
                                        'type' => 'TEMPLATE',
                                        'key' => 'arg1',
                                        'value' => '(' . self::DEFAULT_SUCCESS_PAGE . ')',
                                    ],
                                ],
                            ],
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112,
                        ],
                    ],
                    'tag' => [
                        0 => [
                            'name' => self::TAG_CONVERSION_TRACKING,
                            'type' => 'awct',
                            'firingTriggerId' => [
                                1
                            ],
                            'tagFiringOption' => 'ONCE_PER_EVENT',
                            'tagId' => 1,
                            'parameter' => [
                                [
                                    'type' => 'BOOLEAN',
                                    'key' => 'enableConversionLinker',
                                    'value' => 'true'
                                ],
                                [
                                    'type' => 'TEMPLATE',
                                    'key' => 'conversionCookiePrefix',
                                    'value' => '_gcl'
                                ],
                                [
                                    'type' => 'TEMPLATE',
                                    'key' => 'conversionValue',
                                    'value' => '{{' . self::VAR_CONVERSION_TRACKING_CONVERSION_VALUE . '}}'
                                ],
                                [
                                    'type' => 'TEMPLATE',
                                    'key' => 'orderId',
                                    'value' => '{{' . self::VAR_CONVERSION_TRACKING_ORDER_ID . '}}'
                                ],
                                [
                                    'type' => 'TEMPLATE',
                                    'key' => 'conversionId',
                                    'value' => '1234567890'
                                ],
                                [
                                    'type' => 'TEMPLATE',
                                    'key' => 'currencyCode',
                                    'value' => 'USD'
                                ],
                                [
                                    'type' => 'TEMPLATE',
                                    'key' => 'conversionLabel',
                                    'value' => 'conversionLabel'
                                ]
                            ],
                            'monitoringMetadata' => [
                                'type' => 'MAP'
                            ],
                            'accountId' => '700374935',
                            'containerId' => '2293555',
                            'fingerprint' => 1700829112
                        ]
                    ]
                ]],
            ],
            'adWords conversion is disabled' => [
                1700829112,
                ['containerVersion' => [
                    'variable' => [],
                    'trigger' => [],
                    'tag' => []
                ]],
                [
                    'isAdWordsConversionEnabled' => false,
                    'getAccountId' => '700374935',
                    'getContainerId' => '2293555',
                    'getSuccessPages' => [],
                    'getConversionId' => '1234567890',
                    'getConversionCurrency' => 'USD',
                    'getConversionLabel' => 'conversionLabel'
                ],
                [],
                1,
                1,
                // $expectedData
                ['containerVersion' => [
                    'variable' => [],
                    'trigger' => [],
                    'tag' => []
                ]],
            ]
        ];
    }
}
