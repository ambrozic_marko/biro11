<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Unit\Model\GtmMeta;

use Amasty\GA4\Model\GtmMeta\Events;
use Amasty\GA4Api\Api\Event\EventConfigInterface;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use PHPUnit\Framework\TestCase;

class EventsTest extends TestCase
{
    /**
     * @covers Events::collect
     * @dataProvider collectDataProvider
     */
    public function testCollect(
        int $fingerprint,
        array $data,
        ?array $meta,
        array $expectedData
    ): void {
        $eventConfigMock = null;
        if ($meta) {
            $eventConfigMock = $this->createMock(EventConfigInterface::class);
            $eventConfigMock->expects($this->once())->method('getMeta')->with($fingerprint)->willReturn($meta);
        }
        $eventsConfigPoolMock = $this->createMock(EventsConfigPool::class);
        $eventsConfigPoolMock->expects($this->any())->method('getAll')
            ->willReturn($eventConfigMock ? [$eventConfigMock] : []);

        $events = new Events($eventsConfigPoolMock);
        $events->collect($fingerprint, $data);

        $this->assertEquals($expectedData, $data);
    }

    private function collectDataProvider(): array
    {
        return [
            'merge data' => [
                1700829112,
                ['containerVersion' => [
                    'variable' => [[]],
                ]],
                ['containerVersion' => [
                    'trigger' => [[]],
                    'tag' => [[]],
                ]],
                ['containerVersion' => [
                    'variable' => [[]],
                    'trigger' => [[]],
                    'tag' => [[]],
                ]],
            ],
            'empty data' => [
                1700829112,
                ['containerVersion' => [
                    'variable' => [[]],
                ]],
                null,
                ['containerVersion' => [
                    'variable' => [[]],
                ]],
            ]
        ];
    }
}
