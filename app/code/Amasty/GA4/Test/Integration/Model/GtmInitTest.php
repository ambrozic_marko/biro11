<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model;

use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

class GtmInitTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/enable 0
     * @magentoAppArea frontend
     */
    public function testModuleDisabled(): void
    {
        $layout = $this->prepareLayout();
        $this->assertFalse($layout->hasElement('amasty.ga4.gtm'));
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/enable 1
     * @magentoAppArea frontend
     */
    public function testModuleEnabled(): void
    {
        $layout = $this->prepareLayout();
        $this->assertTrue($layout->hasElement('amasty.ga4.gtm'));
    }

    private function prepareLayout(): LayoutInterface
    {
        $this->objectManager->get(DesignInterface::class)->setDesignTheme('Magento/luma', 'frontend');
        $layout = $this->objectManager->get(LayoutInterface::class);
        $layout->getUpdate()->load(['default', 'empty']);
        $layout->generateXml();
        $layout->generateElements();

        return $layout;
    }
}
