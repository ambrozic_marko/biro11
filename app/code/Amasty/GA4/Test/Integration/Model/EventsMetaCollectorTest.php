<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4\Model\EventsMetaCollector;
use Amasty\GA4Api\Api\Event\EventConfigInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

class EventsMetaCollectorTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
    }

    /**
     * @magentoConfigFixture current_store am_ga4/gtm_api/account_id 123
     * @magentoConfigFixture current_store am_ga4/gtm_api/container_id 123
     * @magentoConfigFixture current_store am_ga4/gtm_api/measurement_id G-123
     * @magentoConfigFixture current_store am_ga4/general/public_id GTM-123
     */
    public function testCollectCustomized(): void
    {
        $customEvent = $this->createCustomEvent();
        $this->objectManager->configure(
            [
                EventsConfigPool::class => [
                    'arguments' => [
                        'eventConfigs' => [
                            ['instance' => get_class($customEvent)]
                        ],
                    ],
                ]
            ]
        );

        $collector = $this->objectManager->create(
            EventsMetaCollector::class
        );

        $result = $collector->collect();
        $this->assertApiCredentials($result);
        $this->assertEquals('test', $result['containerVersion']['trigger'][1]['name']);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/gtm_api/account_id 123
     * @magentoConfigFixture current_store am_ga4/gtm_api/container_id 123
     * @magentoConfigFixture current_store am_ga4/gtm_api/measurement_id G-123
     * @magentoConfigFixture current_store am_ga4/general/public_id GTM-123
     */
    public function testCollectDefault(): void
    {
        $collector = $this->objectManager->create(
            EventsMetaCollector::class
        );

        $result = $collector->collect();
        $this->assertApiCredentials($result);
    }

    private function assertApiCredentials(array $result): void
    {
        $container = $result['containerVersion']['container'];
        $this->assertEquals(123, $container['accountId']);
        $this->assertEquals(123, $container['containerId']);
        $this->assertEquals('GTM-123', $container['publicId']);

        $measurementVariable = $result['containerVersion']['variable'][0]; //always first
        $this->assertEquals('G-123', $measurementVariable['parameter'][0]['value']);
    }

    private function createCustomEvent(): EventConfigInterface
    {
        $configProvider = $this->objectManager->get(ConfigProvider::class);
        $eventLoggerFactory = $this->objectManager->get(EventLoggerFactory::class);
        $incrementIdStorage = $this->objectManager->get(IncrementIdStorage::class);

        return new class (
            $configProvider,
            $eventLoggerFactory,
            $incrementIdStorage
        ) extends CommonEvent {
            public const NAME = 'test';

            public function isAvailable(): bool
            {
                return true;
            }

            public function getUsageType(): string
            {
                return EventsConfigPool::USAGE_HEADER_DATALAYER;
            }

            public function getMeta(?int $fingerprint = null): array
            {
                $fingerprint = $fingerprint ?? time();

                return $this->buildCommonMeta(self::NAME, self::NAME, $fingerprint);
            }
        };
    }
}
