<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model\Event\Cart;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Model\Event\Cart\AddToCart;
use Amasty\GA4\Model\Event\Cart\AddToWishlist;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Request\Http as HttpRequest;
use Magento\TestFramework\TestCase\AbstractController;
use Magento\Wishlist\Model\Wishlist;

/**
 * @magentoDbIsolation disabled
 * @magentoAppIsolation enabled
 */
class WishlistActionsTest extends AbstractController
{
    /**
     * @var EventsProcessor
     */
    private $eventsProcessor;

    /**
     * @var DataLayerCollector
     */
    private $dataLayerCollector;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->productRepository = $this->_objectManager->get(ProductRepositoryInterface::class);
        $this->eventsProcessor = $this->_objectManager->create(EventsProcessor::class);
        $this->dataLayerCollector = $this->_objectManager->create(DataLayerCollector::class);
        $this->serializer = $this->_objectManager->get(Serializer::class);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Wishlist/_files/wishlist_with_simple_product.php
     */
    public function testAddToWishlist(): void
    {
        $customer = $this->_objectManager->get(CustomerRepositoryInterface::class)->get('customer@example.com');
        $this->_objectManager->get(Session::class)->setCustomerId($customer->getId());
        $wishlist = $this->_objectManager->create(Wishlist::class)->loadByCustomerId($customer->getId());

        $this->eventsProcessor->processEvent(AddToWishlist::NAME, ['wishlist_id' => $wishlist->getId()]);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(AddToWishlist::NAME, $result[1]['event']);
        $product = $this->productRepository->get('simple-1');
        $this->assertEquals($product->getSku(), $result[1]['ecommerce']['items'][0]['item_id']);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Wishlist/_files/wishlist_with_simple_product.php
     */
    public function testAddToCartFromWishlist(): void
    {
        $customer = $this->_objectManager->get(CustomerRepositoryInterface::class)->get('customer@example.com');
        $this->_objectManager->get(Session::class)->setCustomerId($customer->getId());
        $wishlist = $this->_objectManager->create(Wishlist::class)->loadByCustomerId($customer->getId());
        $wishlistItem = $wishlist->getItemCollection()->getLastItem();
        $product = $wishlistItem->getProduct();

        $this->performAddToCartRequest(['item' => $wishlistItem->getId(), 'qty' => 1]);
        $this->eventsProcessor->processEvent(AddToCart::NAME, ['product' => $product->getId()]);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(AddToCart::NAME, $result[1]['event']);
        $this->assertEquals($product->getSku(), $result[1]['ecommerce']['items'][0]['item_id']);
    }

    private function performAddToCartRequest(array $params): void
    {
        $this->getRequest()->setParams($params)->setMethod(HttpRequest::METHOD_POST);
        $this->dispatch('wishlist/index/cart');
    }
}
