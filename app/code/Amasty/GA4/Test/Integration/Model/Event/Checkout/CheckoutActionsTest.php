<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model\Event\Checkout;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Model\Event\Checkout\AddPaymentInfo;
use Amasty\GA4\Model\Event\Checkout\AddShippingInfo;
use Amasty\GA4\Model\Event\Checkout\BeginCheckout;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Framework\App\Request\Http;
use Magento\Quote\Model\Quote;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * @magentoDbIsolation disabled
 * @magentoAppIsolation enabled
 */
class CheckoutActionsTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var EventsProcessor
     */
    private $eventsProcessor;

    /**
     * @var DataLayerCollector
     */
    private $dataLayerCollector;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->productRepository = $this->objectManager->get(ProductRepositoryInterface::class);
        $this->eventsProcessor = $this->objectManager->create(EventsProcessor::class);
        $this->dataLayerCollector = $this->objectManager->create(DataLayerCollector::class);
        $this->serializer = $this->objectManager->get(Serializer::class);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Checkout/_files/quote_with_simple_product.php
     */
    public function testBeginCheckoutLogging(): void
    {
        $request = $this->objectManager->get(Http::class);
        $request->setRouteName('checkout')->setControllerName('index')->setActionName('index');

        $this->eventsProcessor->processEvent(BeginCheckout::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(BeginCheckout::NAME, $result[1]['event']);

        $product = $this->productRepository->get('simple');
        $this->assertEquals($product->getSku(), $result[1]['ecommerce']['items'][0]['item_id']);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Checkout/_files/quote_with_shipping_method_and_items_categories.php
     */
    public function testAddShippingInfo(): void
    {
        $quote = $this->objectManager->create(Quote::class);
        $quote->load('test_order_item_with_items', 'reserved_order_id');
        $this->objectManager->get(\Magento\Customer\Model\Session::class)->setCustomerId($quote->getCustomerId());
        $this->objectManager->get(Session::class)->setQuoteId($quote->getId());

        $this->eventsProcessor->processEvent(AddShippingInfo::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(AddShippingInfo::NAME, $result[1]['event']);
        $this->assertEquals('Flat Rate - Fixed', $result[1]['ecommerce'][AddShippingInfo::SHIPPING_TIER_KEY]);
        $this->assertNotEmpty($result[1]['ecommerce']['items']);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Checkout/_files/quote_with_check_payment.php
     */
    public function testAddPaymentInfo(): void
    {
        $quote = $this->objectManager->create(Quote::class);
        $quote->load('test_order_1', 'reserved_order_id');
        $this->objectManager->get(\Magento\Customer\Model\Session::class)->setCustomerId($quote->getCustomerId());
        $this->objectManager->get(Session::class)->setQuoteId($quote->getId());

        $this->eventsProcessor->processEvent(AddPaymentInfo::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(AddPaymentInfo::NAME, $result[1]['event']);
        $this->assertEquals('Check / Money order', $result[1]['ecommerce'][AddPaymentInfo::PAYMENT_TYPE_KEY]);
        $this->assertNotEmpty($result[1]['ecommerce']['items']);
    }
}
