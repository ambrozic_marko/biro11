<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model\Event\Checkout;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Model\Event\Checkout\Purchase;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Model\Quote;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * @magentoDbIsolation disabled
 * @magentoAppIsolation enabled
 */
class PurchaseTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var EventsProcessor
     */
    private $eventsProcessor;

    /**
     * @var DataLayerCollector
     */
    private $dataLayerCollector;

    /**
     * @var Serializer
     */
    private $serializer;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->eventsProcessor = $this->objectManager->create(EventsProcessor::class);
        $this->dataLayerCollector = $this->objectManager->create(DataLayerCollector::class);
        $this->serializer = $this->objectManager->get(Serializer::class);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoConfigFixture current_store am_ga4/general/success_pages test/success/page
     * @magentoDataFixture Magento/Checkout/_files/quote_with_check_payment.php
     */
    public function testPurchaseLogging(): void
    {
        $request = $this->objectManager->get(Http::class);
        $request->setPathInfo('test/success/page');

        $quote = $this->placeOrder();

        $this->eventsProcessor->processEvent(Purchase::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(Purchase::NAME, $result[1]['event']);
        $this->assertEquals((float)$quote->getGrandTotal(), $result[1]['ecommerce'][Purchase::VALUE_KEY]);
        $this->assertNotEmpty($result[1]['ecommerce']['items']);
    }

    private function placeOrder(): Quote
    {
        $quote = $this->objectManager->create(Quote::class);
        $quote->load('test_order_1', 'reserved_order_id');
        $this->objectManager->get(\Magento\Customer\Model\Session::class)->setCustomerId($quote->getCustomerId());
        $this->objectManager->get(Session::class)->setQuoteId($quote->getId());

        $cartManagement = $this->objectManager->get(CartManagementInterface::class);
        $cartManagement->placeOrder($quote->getId());

        return $quote;
    }
}
