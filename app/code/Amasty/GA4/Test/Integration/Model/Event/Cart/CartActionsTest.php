<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model\Event\Cart;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Model\Event\Cart\AddToCart;
use Amasty\GA4\Model\Event\Cart\RemoveFromCart;
use Amasty\GA4\Model\Event\Cart\ViewCart;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * @magentoAppIsolation enabled
 */
class CartActionsTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var EventsProcessor
     */
    private $eventsProcessor;

    /**
     * @var DataLayerCollector
     */
    private $dataLayerCollector;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->productRepository = $this->objectManager->get(ProductRepositoryInterface::class);
        $this->eventsProcessor = $this->objectManager->create(EventsProcessor::class);
        $this->dataLayerCollector = $this->objectManager->create(DataLayerCollector::class);
        $this->serializer = $this->objectManager->get(Serializer::class);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Checkout/_files/quote_with_simple_product.php
     */
    public function testAddToCartLogging(): void
    {
        $product = $this->productRepository->get('simple');
        $eventData = ['product' => $product->getId()];

        $this->eventsProcessor->processEvent(AddToCart::NAME, $eventData);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(AddToCart::NAME, $result[1]['event']);
        $this->assertEquals($product->getSku(), $result[1]['ecommerce']['items'][0]['item_id']);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Checkout/_files/quote_with_simple_product.php
     */
    public function testRemoveFromCartLogging(): void
    {
        $currentQuote = $this->objectManager->get(Session::class)->getQuote();
        $item = $currentQuote->getItemsCollection()->getLastItem();
        $eventData = ['item_id' => $item->getId()];

        $this->eventsProcessor->processEvent(RemoveFromCart::NAME, $eventData);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(RemoveFromCart::NAME, $result[1]['event']);
        $this->assertEquals($item->getProduct()->getSku(), $result[1]['ecommerce']['items'][0]['item_id']);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoDataFixture Magento/Checkout/_files/quote_with_simple_product.php
     */
    public function testViewCartLogging(): void
    {
        $request = $this->objectManager->get(Http::class);
        $request->setRouteName('checkout')->setControllerName('cart')->setActionName('index');

        $this->eventsProcessor->processEvent(ViewCart::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(ViewCart::NAME, $result[1]['event']);

        $currentQuote = $this->objectManager->get(Session::class)->getQuote();
        $this->assertEquals($currentQuote->getGrandTotal(), $result[1]['ecommerce']['value']);

        $product = $this->productRepository->get('simple');
        $this->assertEquals($product->getSku(), $result[1]['ecommerce']['items'][0]['item_id']);
    }

    public function testViewCartLoggingWrongPage(): void
    {
        $request = $this->objectManager->get(Http::class);
        $request->setRouteName('another')->setControllerName('page')->setActionName('index');

        $this->eventsProcessor->processEvent(ViewCart::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEmpty($result);
    }
}
