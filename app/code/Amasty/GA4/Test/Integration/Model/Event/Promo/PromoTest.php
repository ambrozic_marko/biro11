<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model\Event\Promo;

use Amasty\GA4\Model\Event\Promo\Logger\Promotion;
use Amasty\GA4\Model\Event\Promo\SelectPromotion;
use Amasty\GA4\Model\EventsProcessor;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

class PromoTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var EventsProcessor
     */
    private $eventsProcessor;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->eventsProcessor = $this->objectManager->create(EventsProcessor::class);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/promotion_tracking 1
     * @magentoAppArea frontend
     */
    public function testPromoBlockCreation(): void
    {
        $this->objectManager->get(DesignInterface::class)->setDesignTheme('Magento/luma', 'frontend');
        $layout = $this->objectManager->get(LayoutInterface::class);
        $layout->getUpdate()->load(['empty']);
        $layout->generateXml();
        $layout->generateElements();

        $this->eventsProcessor->processEvent(SelectPromotion::NAME);
        $this->assertTrue($layout->hasElement(Promotion::BLOCK_NAME));
    }
}
