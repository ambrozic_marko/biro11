<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model\Event\Catalog;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Model\Event\Catalog\ViewItem;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * @magentoDbIsolation disabled
 * @magentoAppIsolation enabled
 */
class ViewItemTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var EventsProcessor
     */
    private $eventsProcessor;

    /**
     * @var DataLayerCollector
     */
    private $dataLayerCollector;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Registry
     */
    private $registry;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->productRepository = $this->objectManager->get(ProductRepositoryInterface::class);
        $this->registry = $this->objectManager->get(Registry::class);

        $this->eventsProcessor = $this->objectManager->create(EventsProcessor::class);
        $this->dataLayerCollector = $this->objectManager->create(DataLayerCollector::class);
        $this->serializer = $this->objectManager->get(Serializer::class);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoConfigFixture current_store am_ga4/general/success_pages test/success/page
     * @magentoDataFixture Magento/Catalog/_files/product_simple.php
     */
    public function testLogging(): void
    {
        $request = $this->objectManager->get(Http::class);
        $request->setRouteName('catalog')->setControllerName('product')->setActionName('view');

        $product = $this->productRepository->get('simple');
        $this->registry->register('current_product', $product);

        $this->eventsProcessor->processEvent(ViewItem::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEquals(ViewItem::NAME, $result[1]['event']);
        $this->assertEquals($product->getSku(), $result[1]['ecommerce']['items'][0]['item_id']);
    }

    public function testLoggingOnWrongPage(): void
    {
        $request = $this->objectManager->get(Http::class);
        $request->setRouteName('another')->setControllerName('page')->setActionName('index');

        $this->eventsProcessor->processEvent(ViewItem::NAME);
        $result = $this->serializer->unserialize($this->dataLayerCollector->collect());

        $this->assertEmpty($result);
    }
}
