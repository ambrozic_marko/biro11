<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Test\Integration\Model\Event\Catalog;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Block\ProductClickTracking;
use Amasty\GA4\Model\Event\Catalog\SelectItem;
use Amasty\GA4\Model\Event\Catalog\ViewItemList;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * @magentoDbIsolation disabled
 * @magentoAppIsolation enabled
 */
class ListsActionsTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var EventsProcessor
     */
    private $eventsProcessor;

    /**
     * @var DataLayerCollector
     */
    private $dataLayerCollector;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var CategoryRepositoryInterface|mixed
     */
    private $categoryRepository;

    /**
     * @var Registry|mixed
     */
    private $registry;

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->categoryRepository = $this->objectManager->get(CategoryRepositoryInterface::class);
        $this->registry = $this->objectManager->get(Registry::class);

        $this->eventsProcessor = $this->objectManager->create(EventsProcessor::class);
        $this->dataLayerCollector = $this->objectManager->create(DataLayerCollector::class);
        $this->serializer = $this->objectManager->get(Serializer::class);
    }

    /**
     * @magentoConfigFixture current_store am_ga4/general/product_identifier 1
     * @magentoConfigFixture current_store am_ga4/general/measure_product_clicks 1
     * @magentoConfigFixture current_store am_ga4/general/enable
     * @magentoDataFixture Magento/Catalog/_files/product_with_category.php
     * @magentoAppArea frontend
     */
    public function testCategoryLogging(): void
    {
        $request = $this->objectManager->get(Http::class);
        $request->setRouteName('catalog')->setControllerName('category')->setActionName('view');

        $category = $this->categoryRepository->get(333);
        $this->registry->register('current_category', $category);

        $layout = $this->prepareLayout('catalog_category_view');

        $categoryBlock = $layout->getBlock('category.products.list');
        $productCollection = $this->objectManager->create(Collection::class);
        $productCollection->addCategoryFilter($category);
        $categoryBlock->setCollection($productCollection);

        //ViewItemList assertions
        $this->eventsProcessor->processEvent(ViewItemList::NAME);
        $viewListResult = $this->serializer->unserialize($this->dataLayerCollector->collect());
        $this->assertEquals(ViewItemList::NAME, $viewListResult[1]['event']);

        $productItem = $viewListResult[1]['ecommerce']['items'][0];
        $this->assertEquals($category->getId(), $productItem['item_list_id']);
        $this->assertEquals($productCollection->getFirstItem()->getSku(), $productItem['item_id']);

        //SelectItem assertions
        $this->eventsProcessor->processEvent(SelectItem::NAME);
        $clickBlock = $layout->getBlock('amasty.ga4.click-tracking');
        $itemClickData = $clickBlock->getData(ProductClickTracking::PRODUCTS_EVENT_DATA);
        $this->assertNotNull($itemClickData['category']);

        $itemClickResult = array_pop($itemClickData['category']);
        $this->assertEquals(SelectItem::NAME, $itemClickResult['event']);
        $this->assertEquals(
            $productCollection->getFirstItem()->getSku(),
            $itemClickResult['ecommerce']['items'][0]['item_id']
        );
    }

    private function prepareLayout(string $layoutName): LayoutInterface
    {
        $this->objectManager->get(DesignInterface::class)->setDesignTheme('Magento/luma', 'frontend');
        $layout = $this->objectManager->get(LayoutInterface::class);
        $layout->getUpdate()->load(['default', 'empty', $layoutName]);
        $layout->generateXml();
        $layout->generateElements();
        $layout->createBlock(
            ProductClickTracking::class,
            'amasty.ga4.click-tracking'
        );
        $layout->setChild('before.body.end', 'amasty.ga4.click-tracking', 'amasty.ga4.click-tracking');

        return $layout;
    }
}
