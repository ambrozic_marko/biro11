define([
    'jquery',
    'mage/url',
    'Amasty_GA4/js/datalayer-storage',
    'domReady!'
], function ($, urlBuilder, datalayerStorage) {
    'use strict';

    $.widget('amGa4.wishlist', {
        options: {
            eventDataEndpoint: 'amga4/event/track',
            gaEvent: {
                addToWishlist: 'add_to_wishlist',
                addToCart: 'add_to_cart'
            },
            storageKey: {
                wishlist: 'amga4-wishlist',
                addToCart: 'amga4-add-to-cart'
            }
        },
        selectors: {
            addToWishlistButton: '[data-action="add-to-wishlist"]',
            addToCartButton: '#wishlist-sidebar button.tocart, .wishlist-index-index .product-items button.tocart'
        },

        /**
         * @private
         * @return {void}
         */
        _create: function () {
            this.bindClickEvents();
            this.processAddToWishlistStorage();
            this.processAddToCartStorage();
        },

        /**
         * @return {void}
         */
        bindClickEvents: function () {
            $(this.selectors.addToWishlistButton).on('click', (event) => {
                this.storeNodeData(
                    $(event.target).closest(this.selectors.addToWishlistButton).data(),
                    this.options.gaEvent.addToWishlist,
                    this.options.storageKey.wishlist
                );
            });
            $(this.selectors.addToCartButton).on('click', (event) => {
                const data = $(event.target).closest(this.selectors.addToCartButton).data();
                const productId = this.getProductId(event.target);
                productId && !!data?.post?.data && (data.post.data.product = productId);

                this.storeNodeData(data, this.options.gaEvent.addToCart, this.options.storageKey.addToCart);
            });
        },

        /**
         * @param {Object} data
         * @param {string} gaEventName
         * @param {string} storageKey
         * @return {void}
         */
        storeNodeData: function (data, gaEventName, storageKey) {
            let dataToCollect = {...data?.post?.data ?? data?.postNewWishlist?.data ?? {}};
            if (Object.keys(dataToCollect).length === 0) {
                return;
            }

            dataToCollect.event = gaEventName;
            datalayerStorage.pushItem(storageKey, dataToCollect);
        },

        /**
         * @return {void}
         */
        processAddToWishlistStorage: function () {
            const shouldPerformRequest = (dataToCollect) => {
                return dataToCollect?.wishlist_id !== undefined ||
                    (!!dataToCollect && window.location.href === document.referrer);
            };

            this.processStorageData(
                this.options.storageKey.wishlist,
                this.setAdditionalWishlistData.bind(this),
                shouldPerformRequest
            );
        },

        /**
         * @return {void}
         */
        processAddToCartStorage: function () {
            // check if there was redirect back to the current page
            const shouldPerformRequest = (dataToCollect) => {
                return !!dataToCollect && window.location.href === document.referrer;
            }

            this.processStorageData(
                this.options.storageKey.addToCart,
                (dataToCollect) => {},
                shouldPerformRequest
            );
        },

        /**
         * @param {string} storageKey
         * @param {function} additionalDataSetter
         * @param {function} shouldPerformRequest
         * @return {void}
         */
        processStorageData: function (storageKey, additionalDataSetter, shouldPerformRequest) {
            const dataToCollect = datalayerStorage.getItem(storageKey);
            dataToCollect && additionalDataSetter(dataToCollect);
            if (!shouldPerformRequest(dataToCollect)) {
                return;
            }

            $.ajax({
                method: 'POST',
                url: urlBuilder.build(this.options.eventDataEndpoint),
                data: dataToCollect,
                beforeSend: () => {
                    datalayerStorage.removeItem(storageKey);
                },
                success: (observedData) => {
                    observedData.forEach(data => this.pushToDataLayer(data));
                }
            });
        },

        /**
         * @param {Object} eventData
         * @return {void}
         */
        pushToDataLayer: function (eventData) {
            window.dataLayer.push(eventData);
        },

        /**
         * @return {string|undefined}
         */
        getWishlistId: function () {
            return window.location.pathname.match(/wishlist_id\/(\d+)/)?.[1] ?? undefined;
        },

        /**
         * @param {Object} dataToCollect
         * @return {void}
         */
        setAdditionalWishlistData: function (dataToCollect) {
            dataToCollect.wishlist_id = dataToCollect.wishlist_id ?? this.getWishlistId();
        },

        /**
         * @param {jQuery|Element} addToCartButton
         * @return {number|undefined}
         */
        getProductId: function (addToCartButton) {
            return $(addToCartButton).closest('.product-item')?.find('[data-product-id]')?.data('product-id');
        }
    });

    return $.amGa4.wishlist;
});
