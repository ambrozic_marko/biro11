define([
    'jquery',
    'Amasty_GA4/js/event/product/wishlist'
], function ($, amGa4Wishlist) {
    'use strict';

    const widgetMixin = {
        _buildWishlistDropdown: function () {
            this._super();
            amGa4Wishlist();
        }
    };

    return function (targetWidget) {
        $.widget('mage.multipleWishlist', targetWidget, widgetMixin);
        return $.mage.multipleWishlist;
    };
});
