define([
    'jquery',
    'jquery/jquery-storageapi',
], function ($) {
    'use strict';

    return {
        storage: $.localStorage,

        /**
         * @param {string} key
         * @param {Object} value
         * @return {void}
         */
        pushItem: function (key, value) {
            try {
                this.storage.set(key, JSON.stringify(value));
            } catch (e) {
                console.error(e);
            }
        },

        /**
         * @param {string} key
         * @return {Object|null}
         */
        getItem: function (key) {
            return this.storage.get(key);
        },

        /**
         * @param {string} key
         * @return {void}
         */
        removeItem: function (key) {
            this.storage.remove(key);
        }
    }
});
