define([
    'jquery',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'underscore',
    'jquery/jquery-storageapi'
], function ($, Component, customerData, _) {
    'use strict';

    return Component.extend({
        sectionName: 'amga4_event_pool',
        initialize: function () {
            this._super();
            this.bindEventPush();
            this.bindSectionReload();
        },
        bindEventPush: function () {
            this.eventPool = customerData.get(this.sectionName);
            this.eventPool.subscribe((eventPool) => {
                const eventsToPush = eventPool?.eventsData ?? [];

                eventsToPush.forEach((eventData) => {
                    this.pushToDataLayer(eventData);
                });

                // prevent event duplication
                if (!_.isEmpty(eventsToPush)) {
                    customerData.set(this.sectionName, { data_id: eventPool?.data_id ?? undefined });
                }
            });
        },
        bindSectionReload: function () {
           if (document.referrer === window.checkout?.checkoutUrl) {
               customerData.reload([this.sectionName], true);
           }
        },
        pushToDataLayer: function (eventData) {
            window.dataLayer.push(eventData);
        }
    });
});
