define([
    'jquery',
    'mage/url',
    'Amasty_GA4/js/datalayer-storage',
    'domReady!'
], function ($, urlBuilder, datalayerStorage) {
    'use strict';

    $.widget('amGa4.removeFromCart', {
        options: {
            eventDataEndpoint: 'amga4/event/track',
            gaEvent: 'remove_from_cart',
            storageKey: 'amga4-remove-from-cart'
        },
        selectors: {
            removeItemAction: '.cart .action-delete',
        },

        /**
         * @private
         * @return {void}
         */
        _create: function () {
            this.bindClickEvents();
            this.processStorageData();
        },

        /**
         * @return {void}
         */
        bindClickEvents: function () {
            $(this.selectors.removeItemAction).on('click', (event) => {
                const itemData = $(event.target).closest(this.selectors.removeItemAction).data();
                const dataToCollect = {...itemData?.post?.data ?? {}};
                if (Object.keys(dataToCollect).length === 0) {
                    return;
                }

                dataToCollect.item_id = dataToCollect.id;
                dataToCollect.event = this.options.gaEvent;

                $.ajax({
                    method: 'POST',
                    url: urlBuilder.build(this.options.eventDataEndpoint),
                    async: false,
                    data: dataToCollect,
                    success: (observedData) => {
                        datalayerStorage.pushItem(this.options.storageKey, observedData);
                    }
                });
            });
        },


        /**
         * @return {void}
         */
        processStorageData: function () {
            const dataToCollect = datalayerStorage.getItem(this.options.storageKey);
            if (!dataToCollect || window.location.href !== document.referrer) {
                return;
            }

            dataToCollect.forEach(data => this.pushToDataLayer(data));
            datalayerStorage.removeItem(this.options.storageKey);
        },

        /**
         * @param {Object} eventData
         * @return {void}
         */
        pushToDataLayer: function (eventData) {
            window.dataLayer.push(eventData);
        },
    });

    return $.amGa4.removeFromCart;
});
