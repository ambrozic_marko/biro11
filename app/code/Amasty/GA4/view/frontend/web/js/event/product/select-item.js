define([
    'jquery',
    'domReady!'
], function ($) {
    'use strict';

    $.widget('amGa4.selectItem', {
        options: {
            productEventData: {},
            typeSelectorMapping: [
                {
                    type: 'category',
                    selector: '.products-grid'
                },
                {
                    type: 'search_results',
                    selector: '.products-grid'
                },
                {
                    type: 'related',
                    selector: '.block.related'
                },
                {
                    type: 'upsell',
                    selector: '.block.upsell'
                },
                {
                    type: 'crosssell',
                    selector: '.block.crosssell'
                },
                {
                    type: 'widget_product_list',
                    selector: '.block.widget.block-products-list'
                }
            ],
            clickEventFlag: 'data-am-ga-observed',
            traverseParentLevel: 3,
            eventBindVariations: [
                {
                    productIdContainer: '#product-item-info_%productId%',
                    containerToBind: 'a.product-item-photo',
                },
                {
                    productIdContainer: '[data-product-id="%productId%"]',
                    containerToBind: 'a.product-item-photo'
                }
            ]
        },

        /**
         * @private
         * @return {void}
         */
        _create: function () {
            this.options.typeSelectorMapping.forEach((mapping) => {
                this.getProductIds(mapping.type).forEach((productId) => {
                    const eventData = this.getEventData(mapping.type, productId)
                    eventData && this.setProductClickEvent.bind(this, mapping.selector, productId, eventData)();
                });
            });
        },

        /**
         * @param {string} listContainerSelector
         * @param {string} productId
         * @param {Object} eventData
         * @return {void}
         */
        setProductClickEvent: function (listContainerSelector, productId, eventData) {
            for (const eventBindVariant of this.options.eventBindVariations) {
                const containerToBind = this.findProductContainer(listContainerSelector, productId, eventBindVariant);
                if (!containerToBind || containerToBind.length === 0) {
                    continue;
                }

                containerToBind.on('click', (event) => {
                    this.triggerSelectItem(eventData);
                });

                break;
            }
        },

        /**
         * @param {Object} eventData
         * @return {void}
         */
        triggerSelectItem: function (eventData) {
            window.dataLayer.push({ecommerce: null});
            window.dataLayer.push(eventData);
        },

        /**
         * @param {string} listContainerSelector
         * @param {string} productId
         * @param {Object} eventBindVariant
         * @return {jQuery|null}
         */
        findProductContainer: function (listContainerSelector, productId, eventBindVariant) {
            const productIdContainerSelector = listContainerSelector
                + ' ' + eventBindVariant.productIdContainer.replace('%productId%', productId);

            const productIdContainer = $(productIdContainerSelector);

            if (productIdContainer.length === 0) {
                return null;
            }

            const firstContainer = $(productIdContainer.first());
            const parentNodes = firstContainer.parents().slice(0, this.options.traverseParentLevel);
            for (const parent of parentNodes) {
                const containerToBind = $(parent).find(eventBindVariant.containerToBind);

                if (containerToBind.length !== 0) {
                    return containerToBind;
                }
            }

            return null;
        },

        /**
         * @return {string[]}
         */
        getProductIds: function (type) {
            return Object.keys(this.options.productEventData?.[type] ?? {});
        },

        /**
         * @param {string} type
         * @param {string} productId
         * @return {Object|null}
         */
        getEventData: function (type, productId) {
            return this.options.productEventData?.[type]?.[productId] ?? null;
        }
    });

    return $.amGa4.selectItem;
});
