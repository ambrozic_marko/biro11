define([
    'jquery',
    'domReady!'
], function ($) {
    'use strict';

    $.widget('amGa4.promotions', {
        options: {
            observerConfig: {
                root: null,
                threshold: 1
            },
            selectPromoEvent: 'select_promotion',
            viewPromoEvent: 'view_promotion'
        },
        selectors: {
            container: '[data-promotion-event]',
        },
        dataAttributes: {
            creativeName: 'promotion-creative-name',
            creativeSlot: 'promotion-creative-slot',
            promotionId: 'promotion-id',
            promotionName: 'promotion-name'
        },
        /**
         * @private
         * @return {void}
         */
        _create: function () {
            $(this.selectors.container).on('click', (event) => {
                this.pushEventInDataLayer(event.target, this.options.selectPromoEvent);
            });

            $(this.selectors.container).each((index, element) => {
                this.createIntersectionObserver(element);
            });

            $(document).on('amga4-view-promotion', (event, element) => {
                this.pushEventInDataLayer(element, this.options.viewPromoEvent);
            });
        },
        /**
         * @param {Element|jQuery} target
         * @return {void}
         */
        createIntersectionObserver: function (target) {
            const observer = new IntersectionObserver((elements) => {
                elements.forEach((element) => {
                    if (!element.isIntersecting) {
                        return;
                    }

                    this.pushEventInDataLayer(element.target, this.options.viewPromoEvent);
                    observer.disconnect();
                });
            }, this.options.observerConfig);

            observer.observe(target);
        },
        /**
         * @param {Element|jQuery} element
         * @param {string} eventName
         * @return {void}
         */
        pushEventInDataLayer: function (element, eventName) {
            const $container = $(element);
            const promotionData = {
                creative_name: $container.data(this.dataAttributes.creativeName),
                creative_slot: $container.data(this.dataAttributes.creativeSlot),
                promotion_id: $container.data(this.dataAttributes.promotionId),
                promotion_name: $container.data(this.dataAttributes.promotionName)
            };

            window.dataLayer.push({ ecommerce: null });
            window.dataLayer.push(this.getPromotionObject(promotionData, eventName));
        },
        /**
         * @param {Object} promotionData
         * @param {string} eventName
         * @return {Object}
         */
        getPromotionObject: function (promotionData, eventName) {
            return {
                event: eventName,
                ecommerce: {
                    ...promotionData
                }
            }
        }
    });

    return $.amGa4.promotions;
});
