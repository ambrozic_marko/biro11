define([
    'jquery',
    'Magento_Ui/js/modal/confirm'
], function ($, confirmAlert) {
    'use strict';

    $.widget('amga4.exportJson', {
        options: {
            exportUrl: '',
            website: null,
            store: null
        },

        _create: function () {
            this._on({
                'click': this.runExport.bind(this)
            });
        },

        runExport: function () {
            confirmAlert({
                title: $.mage.__('Notification'),
                content: $.mage.__(
                    'Make sure that the settings updated have been saved in order to download the refreshed file'
                ),
                actions: {
                    confirm: function () {
                        window.location = this.getExportUrl();
                    }.bind(this)
                }
            });
        },

        getExportUrl: function () {
            const url = new URL(this.options.exportUrl);
            this.options.website && url.searchParams.append('website', this.options.website);
            this.options.store && url.searchParams.append('store', this.options.store);

            return url.toString();
        }
    });

    return $.amga4.exportJson;
});
