<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\ViewModel;

use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4\Model\Frontend\CustomerPropertiesProcessor;
use Amasty\GA4Api\Model\DataLayerCollector;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class DataLayer implements ArgumentInterface
{
    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var EventsProcessor
     */
    private EventsProcessor $eventsProcessor;

    /**
     * @var CustomerPropertiesProcessor
     */
    private CustomerPropertiesProcessor $customerPropertiesProcessor;

    public function __construct(
        DataLayerCollector $dataLayerCollector,
        EventsProcessor $eventsProcessor,
        CustomerPropertiesProcessor $customerPropertiesProcessor
    ) {
        $this->dataLayerCollector = $dataLayerCollector;
        $this->eventsProcessor = $eventsProcessor;
        $this->customerPropertiesProcessor = $customerPropertiesProcessor;
    }

    public function getJson(string $usage): string
    {
        if ($usage === EventsConfigPool::USAGE_HEADER_DATALAYER) {
            $this->customerPropertiesProcessor->execute();
        }
        $this->eventsProcessor->processDataLayerData($usage);

        return $this->dataLayerCollector->collect();
    }

    public function clearStorage(): void
    {
        $this->dataLayerCollector->clearStorage();
    }
}
