<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\ViewModel;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Cookie\GdprCookieAdapter;
use Amasty\GA4\Model\Frontend\HeaderScriptPersist;
use Amasty\GA4\Model\Frontend\MobileDetect;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManager;

class GtmViewModel implements ArgumentInterface
{
    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var HeaderScriptPersist
     */
    private HeaderScriptPersist $headerScriptPersist;

    /**
     * @var MobileDetect
     */
    private MobileDetect $mobileDetect;

    /**
     * @var GdprCookieAdapter
     */
    private GdprCookieAdapter $gdprCookieAdapter;

    /**
     * @var StoreManager
     */
    private StoreManager $storeManager;

    public function __construct(
        ConfigProvider $configProvider,
        HeaderScriptPersist $headerScriptPersist,
        MobileDetect $mobileDetect,
        GdprCookieAdapter $gdprCookieAdapter,
        StoreManager $storeManager
    ) {
        $this->configProvider = $configProvider;
        $this->headerScriptPersist = $headerScriptPersist;
        $this->mobileDetect = $mobileDetect;
        $this->gdprCookieAdapter = $gdprCookieAdapter;
        $this->storeManager = $storeManager;
    }

    public function isConsentModeEnabled(): bool
    {
        /* extension point for GoogleConsentMode */
        return false;
    }

    public function getStoreId(): int
    {
        return (int)$this->storeManager->getStore()->getId();
    }

    public function isGdprModuleValid(): bool
    {
        return $this->gdprCookieAdapter->isModuleValid();
    }

    public function getPublicId(): string
    {
        return $this->configProvider->getPublicId();
    }

    public function getOpenTag(): string
    {
        return $this->headerScriptPersist->getOpenTag();
    }

    public function getCloseTag(): string
    {
        return $this->headerScriptPersist->getCloseTag();
    }

    public function isMobile(): bool
    {
        return $this->mobileDetect->isMobile();
    }

    public function getEndpoint(): string
    {
        return $this->configProvider->getGtmEndpoint();
    }
}
