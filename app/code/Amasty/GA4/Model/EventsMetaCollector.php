<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model;

use Amasty\GA4Api\Model\GtmMetaPool;

class EventsMetaCollector
{
    /**
     * @var GtmMetaPool
     */
    private GtmMetaPool $gtmMetaPool;

    public function __construct(
        GtmMetaPool $gtmMetaPool
    ) {
        $this->gtmMetaPool = $gtmMetaPool;
    }

    public function collect(): array
    {
        $result = [];
        $fingerprint = time();
        foreach ($this->gtmMetaPool->getAll() as $metaCollector) {
            $metaCollector->collect($fingerprint, $result);
        }

        return $result;
    }
}
