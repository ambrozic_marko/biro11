<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ThirdParty;

use Magento\Framework\Module\Manager;

class ModuleChecker
{
    public const COOKIE_MODULE = 'Amasty_GdprCookie';
    public const CONSENT_MODULE = 'Amasty_GoogleConsentMode';

    /**
     * @var Manager
     */
    private Manager $moduleManager;

    public function __construct(Manager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }

    public function isGdprCookieEnabled(): bool
    {
        return $this->moduleManager->isEnabled(self::COOKIE_MODULE);
    }

    public function isGoogleConsentModeEnabled(): bool
    {
        return $this->moduleManager->isEnabled(self::CONSENT_MODULE);
    }
}
