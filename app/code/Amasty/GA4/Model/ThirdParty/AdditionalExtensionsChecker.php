<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ThirdParty;

use Br33f\Ga4\MeasurementProtocol\Service;
use Detection\MobileDetect;

class AdditionalExtensionsChecker
{
    public function isGa4MPInstalled(): bool
    {
        return class_exists(Service::class);
    }

    public function isMobileDetectInstalled(): bool
    {
        return class_exists(MobileDetect::class);
    }
}
