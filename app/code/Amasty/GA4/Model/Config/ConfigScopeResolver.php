<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface as StoreScopeInterface;

class ConfigScopeResolver
{
    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * @return array [scopeType, scopeCode]
     */
    public function resolve(): array
    {
        if ($this->request->getParam('store')) {
            return [StoreScopeInterface::SCOPE_STORE, $this->request->getParam('store')];
        }

        if ($this->request->getParam('website')) {
            return [StoreScopeInterface::SCOPE_WEBSITE, $this->request->getParam('website')];
        }

        return [ScopeConfigInterface::SCOPE_TYPE_DEFAULT, null];
    }
}
