<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Config\Backend;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\Validator\Url as UrlValidator;
use Magento\Framework\Exception\ValidatorException;
use Magento\Store\Model\StoreManagerInterface;

class ServerContainerUrl extends Value
{
    /**
     * @var UrlValidator
     */
    private UrlValidator $urlValidator;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        UrlValidator $urlValidator,
        StoreManagerInterface $storeManager,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->urlValidator = $urlValidator;
        $this->storeManager = $storeManager;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * @return void
     * @throws ValidatorException
     */
    public function beforeSave(): void
    {
        if (empty($this->getValue())) {
            return;
        }

        $this->validateUrl();
        $this->validateDomain();

        $this->setValue(rtrim($this->getValue(), '/'));
    }

    /**
     * @return void
     * @throws ValidatorException
     */
    private function validateUrl(): void
    {
        $urlValue = $this->getValue();

        if (!$this->urlValidator->isValid($urlValue)) {
            throw new ValidatorException(
                __(' Invalid GTM Server Container URL.'
                . ' Please enter the valid URL according to the format https://example.com or leave this field blank.')
            );
        }
    }

    /**
     * @return void
     * @throws ValidatorException
     */
    private function validateDomain(): void
    {
        $urlValueHost = $this->getHost($this->getValue());
        $store = $this->storeManager->getStore($this->getScopeId());
        $baseSecureUrlHost = $this->getHost($store->getBaseUrl(UrlInterface::URL_TYPE_WEB, true));
        $baseUnsecureUrlHost = $this->getHost($store->getBaseUrl(UrlInterface::URL_TYPE_WEB, false));

        if (!$this->endsWithSameDomain($urlValueHost, $baseSecureUrlHost)
            && !$this->endsWithSameDomain($urlValueHost, $baseUnsecureUrlHost)
        ) {
            throw new ValidatorException(
                __('The subdomain specified in the GTM Server Container URL field'
                . ' does not correspond to the Base URL defined for the store view. '
                . 'Please review and update the setting to ensure proper functionality.')
            );
        }
    }

    private function endsWithSameDomain(string $haystack, string $needle): bool
    {
        $needleLength = strlen($needle);

        return ($needleLength === 0 || 0 === substr_compare($haystack, $needle, - $needleLength));
    }

    private function getHost(string $url): string
    {
        // phpcs:disable Magento2.Functions.DiscouragedFunction.Discouraged
        return parse_url($url)['host'] ?? '';
    }
}
