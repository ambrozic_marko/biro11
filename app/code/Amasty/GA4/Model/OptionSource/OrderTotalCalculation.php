<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class OrderTotalCalculation implements OptionSourceInterface
{
    public const GRAND_TOTAL = 0;
    public const SUBTOTAL = 1;

    public function toOptionArray(): array
    {
        $result = [];

        foreach ($this->toArray() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }

        return $result;
    }

    public function toArray(): array
    {
        return [
            self::GRAND_TOTAL => __('Grand Total'),
            self::SUBTOTAL => __('Subtotal')
        ];
    }
}
