<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class IdentifierResolution implements OptionSourceInterface
{
    public const CHILD = 0;
    public const PARENT = 1;

    public function toOptionArray(): array
    {
        $result = [];

        foreach ($this->toArray() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }

        return $result;
    }

    public function toArray(): array
    {
        return [
            self::CHILD => __('Child'),
            self::PARENT => __('Parent')
        ];
    }
}
