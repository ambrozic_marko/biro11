<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\OptionSource;

use Magento\Framework\Data\OptionSourceInterface;

class ProductIdentifier implements OptionSourceInterface
{
    public const ID = 0;
    public const SKU = 1;

    public function toOptionArray(): array
    {
        $result = [];

        foreach ($this->toArray() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }

        return $result;
    }

    public function toArray(): array
    {
        return [
            self::ID => __('ID'),
            self::SKU => __('SKU')
        ];
    }
}
