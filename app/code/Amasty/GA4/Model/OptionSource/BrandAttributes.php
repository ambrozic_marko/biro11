<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\OptionSource;

use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class BrandAttributes implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function toOptionArray(): array
    {
        $result = [['value' => '', 'label' => __('Select brand attribute')]];
        foreach ($this->toArray() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }

        return $result;
    }

    public function toArray(): array
    {
        $attributesCollection = $this->collectionFactory->create()
            ->addFieldToFilter('used_in_product_listing', 1)
            ->addFieldToFilter('is_visible', 1);
        foreach ($attributesCollection->getItems() as $attribute) {
            if ($attribute->getFrontendLabel() !== null) {
                $result[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
            }
        }
        natcasesort($result);

        return $result;
    }
}
