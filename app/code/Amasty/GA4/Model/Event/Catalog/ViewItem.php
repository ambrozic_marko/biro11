<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Magento\Framework\App\Request\Http;

class ViewItem extends CommonEvent
{
    public const NAME = 'view_item';
    public const LOGGER_CLASS = Logger\ViewItem::class;

    private const FULL_ACTION_NAME = 'catalog_product_view';

    /**
     * @var Http
     */
    private Http $request;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        Http $request,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
        $this->request = $request;
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        return $this->buildCommonMeta('Amasty GA4 - ' . self::NAME, 'Amasty GA4 - Item Views', $fingerprint);
    }

    public function isAvailable(): bool
    {
        return $this->request->getFullActionName() === self::FULL_ACTION_NAME;
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_HEADER_DATALAYER;
    }
}
