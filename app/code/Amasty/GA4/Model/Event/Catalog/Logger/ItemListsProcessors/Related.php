<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger\ItemListsProcessors;

use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;
use Magento\Catalog\Block\Product\ProductList\Related as RelatedBlock;
use Magento\TargetRule\Block\Catalog\Product\ProductList\Related as TargetRuleRelatedBlock;
use Magento\Framework\Escaper;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutInterface;

class Related extends Common
{
    public const PROCESSOR_TYPE = 'related';
    public const ITEM_LIST_ID = 'related_products';

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var LayoutInterface
     */
    private LayoutInterface $layout;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * @var Escaper
     */
    private Escaper $escaper;

    public function __construct(
        ProductDetailsFormatter $productDetailsFormatter,
        LayoutInterface $layout,
        Registry $registry,
        Escaper $escaper
    ) {
        parent::__construct($layout);
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->layout = $layout;
        $this->registry = $registry;
        $this->escaper = $escaper;
    }

    public function isAvailable(): bool
    {
        return $this->layout->hasElement('catalog.product.related');
    }

    protected function prepareProductsEventData(): array
    {
        $products = $this->getProducts();
        if (empty($products)) {
            return [];
        }

        $listName = __('Related Products');
        if ($product = $this->registry->registry('current_product')) {
            $listName = __('Related Products from %1', $this->escaper->escapeHtml($product->getName()));
        }

        $result = [];
        foreach (array_values($products) as $index => $product) {
            $result[$product->getId()] = $this->productDetailsFormatter->formatFromProduct(
                $product,
                self::ITEM_LIST_ID,
                $listName->render(),
                $index + 1
            );
        }

        return $result;
    }

    private function getProducts(): array
    {
        /** @var RelatedBlock|TargetRuleRelatedBlock $relatedBlock */
        $relatedBlock = $this->layout->getBlock('catalog.product.related');
        $blockType = $relatedBlock->getData('type');
        if ($blockType == 'related-rule') {
            $collection = $relatedBlock->getAllItems();
        } else {
            $collection = $relatedBlock->getItems()->getItems();
        }

        return $collection;
    }
}
