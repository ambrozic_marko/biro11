<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger;

use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;

class SelectItem implements EventLoggerInterface
{
    /**
     * @var ItemListProcessorInterface[]
     */
    private array $itemListProcessors;

    public function __construct(
        array $itemListProcessors = []
    ) {
        $this->itemListProcessors = $itemListProcessors;
    }

    public function execute(): void
    {
        foreach ($this->itemListProcessors as $processor) {
            $processor->processItemClick();
        }
    }
}
