<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger\ItemListsProcessors;

use Amasty\GA4\Model\Registry\CatalogWidget\Block as BlockRegistry;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Magento\Framework\View\LayoutInterface;

class Widget extends Common
{
    public const PROCESSOR_TYPE = 'widget_product_list';

    /**
     * @var BlockRegistry
     */
    private BlockRegistry $registry;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    public function __construct(
        LayoutInterface $layout,
        BlockRegistry $registry,
        ProductDetailsFormatter $productDetailsFormatter
    ) {
        parent::__construct($layout);
        $this->registry = $registry;
        $this->productDetailsFormatter = $productDetailsFormatter;
    }

    public function isAvailable(): bool
    {
        return !empty($this->registry->getBlocksData());
    }

    protected function prepareProductsEventData(): array
    {
        $result = [];
        foreach ($this->registry->getBlocksData() as $blockName => $productCollection) {
            foreach (array_values($productCollection->getItems()) as $index => $product) {
                if (isset($result[$product->getId()])) {
                    continue;
                }
                $result[$product->getId()] = $this->productDetailsFormatter->formatFromProduct(
                    $product,
                    $blockName,
                    __('Product List Widget')->render(),
                    $index + 1
                );
            }
        }

        return $result;
    }
}
