<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger\ItemListsProcessors;

use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\App\Request\Http;
use Magento\Framework\View\LayoutInterface;

class SearchResults extends Common
{
    public const PROCESSOR_TYPE = 'search_results';
    public const ITEM_LIST_ID = 'search_results';

    private const FULL_ACTION_NAME = 'catalogsearch_result_index';

    /**
     * @var Collection|null
     */
    private ?Collection $productCollection = null;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var LayoutInterface
     */
    private LayoutInterface $layout;

    /**
     * @var Http
     */
    private Http $request;

    public function __construct(
        ProductDetailsFormatter $productDetailsFormatter,
        LayoutInterface $layout,
        Http $request
    ) {
        parent::__construct($layout);
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->request = $request;
        $this->layout = $layout;
    }

    public function isAvailable(): bool
    {
        return $this->request->getFullActionName() === self::FULL_ACTION_NAME
            && $this->layout->hasElement('search_result_list');
    }

    protected function prepareProductsEventData(): array
    {
        if (!($productCollection = $this->getProductCollection())) {
            return [];
        }

        $result = [];
        foreach (array_values($productCollection->getItems()) as $index => $product) {
            $result[$product->getId()] = $this->productDetailsFormatter->formatFromProduct(
                $product,
                self::ITEM_LIST_ID,
                __('Search Results')->render(),
                $index + 1
            );
        }

        return $result;
    }

    private function getProductCollection(): ?Collection
    {
        if ($this->productCollection === null) {
            /** @var ListProduct $categoryProductListBlock */
            $searchResultListBlock = $this->layout->getBlock('search_result_list');
            if (empty($searchResultListBlock)) {
                return null;
            }
            $this->productCollection = $searchResultListBlock->getLoadedProductCollection();
        }

        return $this->productCollection;
    }
}
