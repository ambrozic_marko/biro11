<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger\ItemListsProcessors;

use Amasty\GA4\Block\ProductClickTracking;
use Amasty\GA4\Model\Utils\CategoriesResolver;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutInterface;
use Magento\Store\Model\StoreManagerInterface;

class Category extends Common
{
    public const PROCESSOR_TYPE = 'category';
    private const FULL_ACTION_NAME = 'catalog_category_view';

    /**
     * @var Collection|null
     */
    private ?Collection $productCollection = null;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var CategoriesResolver
     */
    private CategoriesResolver $categoriesResolver;

    /**
     * @var LayoutInterface
     */
    private LayoutInterface $layout;

    /**
     * @var Http
     */
    private Http $request;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    public function __construct(
        ProductDetailsFormatter $productDetailsFormatter,
        CategoriesResolver $categoriesResolver,
        LayoutInterface $layout,
        Http $request,
        Registry $registry,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($layout);
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->request = $request;
        $this->layout = $layout;
        $this->registry = $registry;
        $this->categoriesResolver = $categoriesResolver;
        $this->storeManager = $storeManager;
    }

    public function isAvailable(): bool
    {
        return $this->request->getFullActionName() === self::FULL_ACTION_NAME
            && $this->layout->hasElement('category.products.list');
    }

    protected function prepareProductsEventData(): array
    {
        if (!($productCollection = $this->getProductCollection()) || !($category = $this->getCurrentCategory())) {
            return [];
        }
        $listId = (int)$category->getId();
        $categoryNames = $this->categoriesResolver->getCategoryNamePathFromId($listId, $this->storeManager->getStore());
        $listName = implode('/', $categoryNames);

        $result = [];
        foreach (array_values($productCollection->getItems()) as $index => $product) {
            $result[$product->getId()] = $this->productDetailsFormatter->formatFromProduct(
                $product,
                (string)$listId,
                $listName,
                $index + 1
            );
        }

        return $result;
    }

    private function getCurrentCategory(): ?CategoryInterface
    {
        return $this->registry->registry('current_category');
    }

    private function getProductCollection(): ?Collection
    {
        if ($this->productCollection === null) {
            /** @var ListProduct $categoryProductListBlock */
            $categoryProductListBlock = $this->layout->getBlock('category.products.list');
            if (!$categoryProductListBlock) {
                return null;
            }
            $this->productCollection = $categoryProductListBlock->getLoadedProductCollection();
        }

        return $this->productCollection;
    }
}
