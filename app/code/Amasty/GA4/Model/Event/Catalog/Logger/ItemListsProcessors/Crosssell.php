<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger\ItemListsProcessors;

use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Checkout\Block\Cart\Crosssell as CrosssellBlock;
use Magento\TargetRule\Block\Checkout\Cart\Crosssell as TargetRuleCrosssellBlock;
use Magento\Framework\View\LayoutInterface;

class Crosssell extends Common
{
    public const PROCESSOR_TYPE = 'crosssell';
    public const ITEM_LIST_ID = 'crosssell_products';

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var LayoutInterface
     */
    private LayoutInterface $layout;

    public function __construct(
        ProductDetailsFormatter $productDetailsFormatter,
        LayoutInterface $layout
    ) {
        parent::__construct($layout);
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->layout = $layout;
    }

    public function isAvailable(): bool
    {
        return $this->layout->hasElement('checkout.cart.crosssell');
    }

    protected function prepareProductsEventData(): array
    {
        $products = $this->getProducts();
        if (empty($products)) {
            return [];
        }

        $result = [];
        foreach (array_values($products) as $index => $product) {
            $result[$product->getId()] = $this->productDetailsFormatter->formatFromProduct(
                $product,
                self::ITEM_LIST_ID,
                __('Cross-Sell Products')->render(),
                $index + 1
            );
        }

        return $result;
    }

    /**
     * @return ProductInterface[]
     */
    private function getProducts(): array
    {
        /** @var CrosssellBlock|TargetRuleCrosssellBlock $crosssellBlock */
        $crosssellBlock = $this->layout->getBlock('checkout.cart.crosssell');
        $blockType = $crosssellBlock->getData('type');
        if ($blockType == 'crosssell-rule') {
            $items = $crosssellBlock->getItemCollection();
        } else {
            $items = $crosssellBlock->getItems();
        }

        return $items;
    }
}
