<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger\ItemListsProcessors;

use Amasty\Base\Model\Serializer;
use Amasty\GA4\Block\ProductClickTracking;
use Amasty\GA4\Model\Event\Catalog\SelectItem;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;
use Amasty\GA4Api\Model\DataLayer\DataObjectStorage;
use Magento\Framework\View\LayoutInterface;

abstract class Common implements ItemListProcessorInterface
{
    public const PROCESSOR_TYPE = '';

    /**
     * @var LayoutInterface
     */
    private LayoutInterface $layout;

    public function __construct(
        LayoutInterface $layout
    ) {
        $this->layout = $layout;
    }

    abstract protected function prepareProductsEventData(): array;

    public function processItemClick(): void
    {
        $productsEventData = $this->getItemClickEventData();
        if ($productsEventData) {
            /** @var ProductClickTracking $clickTrackingBlock */
            $clickTrackingBlock = $this->layout->getBlock('amasty.ga4.click-tracking');
            if ($clickTrackingBlock) {
                $eventsData = $clickTrackingBlock->getData(ProductClickTracking::PRODUCTS_EVENT_DATA);
                $eventsData[static::PROCESSOR_TYPE] = $productsEventData;
                $clickTrackingBlock->setData(ProductClickTracking::PRODUCTS_EVENT_DATA, $eventsData);
            }
        }
    }

    public function processViewList(): array
    {
        $result = $this->prepareProductsEventData();

        return array_values($result);
    }

    protected function getItemClickEventData(): array
    {
        $productsEventData = $this->prepareProductsEventData();

        $callback = static function ($productData) {
            return [
                DataObjectStorage::EVENT_KEY => SelectItem::NAME,
                DataObjectStorage::ECOMMERCE_KEY => [
                    'items' => [$productData]
                ]
            ];
        };

        return array_map($callback, $productsEventData);
    }
}
