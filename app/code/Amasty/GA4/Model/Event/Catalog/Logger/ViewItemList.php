<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger;

use Amasty\GA4\Model\Event\Catalog\ViewItemList as ViewItemListEvent;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;
use Amasty\GA4Api\Model\DataLayerCollector;

class ViewItemList implements EventLoggerInterface
{
    /**
     * @var ItemListProcessorInterface[]
     */
    private array $itemListProcessors;

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @param ItemListProcessorInterface[] $itemListProcessors
     */
    public function __construct(
        DataLayerCollector $dataLayerCollector,
        array $itemListProcessors = []
    ) {
        $this->itemListProcessors = $itemListProcessors;
        $this->dataLayerCollector = $dataLayerCollector;
    }

    public function execute(): void
    {
        foreach ($this->itemListProcessors as $processor) {
            if ($items = $processor->processViewList()) {
                $this->dataLayerCollector->addDataLayerOption(
                    ViewItemListEvent::NAME,
                    [ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => $items]
                );
            }
        }
    }
}
