<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger\ItemListsProcessors;

use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\ViewItemList\ItemListProcessorInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Block\Product\ProductList\Upsell as UpsellBlock;
use Magento\TargetRule\Block\Catalog\Product\ProductList\Upsell as TargetRuleUpsellBlock;
use Magento\Framework\Escaper;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutInterface;

class Upsell extends Common
{
    public const PROCESSOR_TYPE = 'upsell';
    public const ITEM_LIST_ID = 'upsell_products';

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var LayoutInterface
     */
    private LayoutInterface $layout;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * @var Escaper
     */
    private Escaper $escaper;

    public function __construct(
        ProductDetailsFormatter $productDetailsFormatter,
        LayoutInterface $layout,
        Registry $registry,
        Escaper $escaper
    ) {
        parent::__construct($layout);
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->layout = $layout;
        $this->registry = $registry;
        $this->escaper = $escaper;
    }

    public function isAvailable(): bool
    {
        return $this->layout->hasElement('product.info.upsell');
    }

    protected function prepareProductsEventData(): array
    {
        $products = $this->getProducts();
        if (empty($products)) {
            return [];
        }

        $listName = __('Up-Sell Products');
        if ($product = $this->registry->registry('current_product')) {
            $listName = __('Upsell Products from %1', $this->escaper->escapeHtml($product->getName()));
        }

        $result = [];
        foreach (array_values($products) as $index => $product) {
            $result[$product->getId()] = $this->productDetailsFormatter->formatFromProduct(
                $product,
                self::ITEM_LIST_ID,
                $listName->render(),
                $index + 1
            );
        }

        return $result;
    }

    /**
     * @return ProductInterface[]
     */
    private function getProducts(): array
    {
        /** @var UpsellBlock|TargetRuleUpsellBlock $upsellBlock */
        $upsellBlock = $this->layout->getBlock('product.info.upsell');
        $blockType = $upsellBlock->getData('type');
        if ($blockType == 'upsell-rule') {
            $items = $upsellBlock->getAllItems();
        } else {
            $items = $upsellBlock->getItemCollection()->getItems();
        }

        return $items;
    }
}
