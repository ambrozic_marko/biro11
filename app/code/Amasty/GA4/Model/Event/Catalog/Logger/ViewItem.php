<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog\Logger;

use Amasty\GA4\Model\Event\Catalog\ViewItem as ViewItemEvent;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Framework\Registry;

class ViewItem implements EventLoggerInterface
{
    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $detailsFormatter;

    /**
     * @var Registry
     */
    private Registry $registry;

    public function __construct(
        DataLayerCollector $dataLayerCollector,
        ProductDetailsFormatter $detailsFormatter,
        Registry $registry
    ) {
        $this->dataLayerCollector = $dataLayerCollector;
        $this->detailsFormatter = $detailsFormatter;
        $this->registry = $registry;
    }

    public function execute(): void
    {
        $product = $this->registry->registry('current_product');
        if ($product) {
            $productData = $this->detailsFormatter->formatFromProduct($product);
            $this->dataLayerCollector->addDataLayerOption(
                ViewItemEvent::NAME,
                [ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => [$productData]]
            );
        }
    }
}
