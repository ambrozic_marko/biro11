<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Catalog;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\ItemListProcessorsPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;

class SelectItem extends CommonEvent
{
    public const NAME = 'select_item';
    public const LOGGER_CLASS = Logger\SelectItem::class;

    /**
     * @var ItemListProcessorsPool
     */
    private ItemListProcessorsPool $itemListProcessorsPool;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        ItemListProcessorsPool $itemListProcessorsPool,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
        $this->itemListProcessorsPool = $itemListProcessorsPool;
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        return $this->buildCommonMeta('Amasty GA4 - ' . self::NAME, 'Amasty GA4 - Item List Clicks', $fingerprint);
    }

    public function isAvailable(): bool
    {
        return $this->configProvider->isMeasureProductClicks() && !empty($this->getAvailableListTypes());
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_FOOTER_DATALAYER;
    }

    public function getLogger(array $eventData = []): EventLoggerInterface
    {
        $eventData['itemListProcessors'] = $this->getAvailableListTypes();

        return parent::getLogger($eventData);
    }

    private function getAvailableListTypes(): array
    {
        return $this->itemListProcessorsPool->getAvailable();
    }
}
