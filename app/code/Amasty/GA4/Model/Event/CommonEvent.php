<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\GtmMeta\CommonData;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Api\Event\EventConfigInterface;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class CommonEvent implements EventConfigInterface
{
    public const NAME = '';

    /**
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;

    /**
     * @var EventLoggerFactory
     */
    private EventLoggerFactory $eventLoggerFactory;

    /**
     * @var IncrementIdStorage
     */
    private IncrementIdStorage $incrementIdStorage;

    /**
     * @var string
     */
    private string $loggerClass;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        string $loggerClass = ''
    ) {
        $this->configProvider = $configProvider;
        $this->eventLoggerFactory = $eventLoggerFactory;
        $this->incrementIdStorage = $incrementIdStorage;
        $this->loggerClass = $loggerClass;
    }

    public function getName(): string
    {
        return static::NAME;
    }

    public function getLogger(array $eventData = []): EventLoggerInterface
    {
        return $this->eventLoggerFactory->create($this->loggerClass, $eventData);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function buildCommonMeta(string $triggerName, string $tagName, int $fingerprint): array
    {
        $triggerId = $this->getTriggerId();
        $accountId = $this->getAccountId();
        $containerId = $this->getContainerId();

        return [
            'containerVersion' => [
                'trigger' => [
                    [
                        'name' => $triggerName,
                        'type' => 'CUSTOM_EVENT',
                        'customEventFilter' => [
                            [
                                'type' => 'EQUALS',
                                'parameter' => [
                                    [
                                        'type' => 'TEMPLATE',
                                        'key' => 'arg0',
                                        'value' => '{{_event}}'
                                    ],
                                    [
                                        'type' => 'TEMPLATE',
                                        'key' => 'arg1',
                                        'value' => static::NAME
                                    ]
                                ]
                            ]
                        ],
                        'accountId' => $accountId,
                        'containerId' => $containerId,
                        'triggerId' => $triggerId,
                        'fingerprint' => $fingerprint
                    ]
                ],
                'tag' => [
                    [
                        'name' => $tagName,
                        'firingTriggerId' => [
                            $triggerId
                        ],
                        'tagFiringOption' => 'ONCE_PER_EVENT',
                        'type' => 'gaawe',
                        'parameter' => [
                            [
                                'type' => 'LIST',
                                'key' => 'userProperties',
                                'list' => [
                                    [
                                        'type' => 'MAP',
                                        'map' => [
                                            [
                                                'type' => 'TEMPLATE',
                                                'key' => 'name',
                                                'value' => 'customerGroup'
                                            ],
                                            [
                                                'type' => 'TEMPLATE',
                                                'key' => 'value',
                                                'value' => '{{' . VariablesProvider::GA4_CUSTOMER_GROUP . '}}'
                                            ]
                                        ]
                                    ],
                                    [
                                        'type' => 'MAP',
                                        'map' => [
                                            [
                                                'type' => 'TEMPLATE',
                                                'key' => 'name',
                                                'value' => 'customerId'
                                            ],
                                            [
                                                'type' => 'TEMPLATE',
                                                'key' => 'value',
                                                'value' => '{{' . VariablesProvider::GA4_CUSTOMER_ID . '}}'
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'type' => 'TEMPLATE',
                                'key' => 'eventName',
                                'value' => static::NAME
                            ],
                            [
                                'type' => 'LIST',
                                'key' => 'eventParameters',
                                'list' => [
                                    [
                                        'type' => 'MAP',
                                        'map' => [
                                            [
                                                'type' => 'TEMPLATE',
                                                'key' => 'name',
                                                'value' => 'items'
                                            ],
                                            [
                                                'type' => 'TEMPLATE',
                                                'key' => 'value',
                                                'value' => '{{' . VariablesProvider::GA4_ECOMMERCE_ITEMS . '}}'
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'type' => 'TAG_REFERENCE',
                                'key' => 'measurementId',
                                'value' => CommonData::MEASUREMENT_TAG_NAME
                            ],
                        ],
                        'monitoringMetadata' => [
                            'type' => 'MAP'
                        ],
                        'accountId' => $accountId,
                        'containerId' => $containerId,
                        'tagId' => $this->getTagId(),
                        'fingerprint' => $fingerprint
                    ]
                ]
            ]
        ];
    }

    protected function buildEventParameter(string $code, string $valueName): array
    {
        return [
            'type' => 'MAP',
            'map' => [
                [
                    'type' => 'TEMPLATE',
                    'key' => 'name',
                    'value' => $code
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'value',
                    'value' => '{{' . $valueName . '}}'
                ]
            ]
        ];
    }

    protected function getAccountId(): string
    {
        return $this->configProvider->getAccountId();
    }

    protected function getContainerId(): string
    {
        return $this->configProvider->getContainerId();
    }

    protected function getVariableId(): int
    {
        return $this->incrementIdStorage->getVariableId();
    }

    protected function getTriggerId(): int
    {
        return $this->incrementIdStorage->getTriggerId();
    }

    protected function getTagId(): int
    {
        return $this->incrementIdStorage->getTagId();
    }
}
