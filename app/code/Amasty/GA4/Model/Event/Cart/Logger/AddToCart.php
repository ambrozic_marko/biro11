<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Cart\Logger;

use Amasty\GA4\Model\Event\Cart\AddToCart as AddToCartEvent;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Data\Collection;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\CartItemInterface;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class AddToCart implements EventLoggerInterface
{
    public const REQUEST_QTY_KEY = 'qty';

    /**
     * @var Http
     */
    private Http $request;

    /**
     * @var ProductInterface
     */
    private ProductInterface $product;

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var Session
     */
    private Session $checkoutSession;

    public function __construct(
        Http $request,
        ProductInterface $product,
        DataLayerCollector $dataLayerCollector,
        ProductDetailsFormatter $productDetailsFormatter,
        Session $checkoutSession
    ) {
        $this->request = $request;
        $this->product = $product;
        $this->dataLayerCollector = $dataLayerCollector;
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->checkoutSession = $checkoutSession;
    }

    public function execute(): void
    {
        if (!$this->product->getId()) {
            return;
        }

        if ($this->product->getTypeId() === Grouped::TYPE_CODE) {
            $itemsData = $this->processGroupedProduct();
        } else {
            $itemsData = [$this->processCommonProduct()];
        }
        if ($itemsData) {
            $this->dataLayerCollector->addDataLayerOption(
                AddToCartEvent::NAME,
                [ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => $itemsData]
            );
        }
    }

    private function processCommonProduct(): array
    {
        $quote = $this->checkoutSession->getQuote();
        if (!$quote->getId()) {
            return [];
        }
        $cartItem = $this->retrieveLastAddedItem($quote);

        return $this->productDetailsFormatter->formatFromCartItem($cartItem);
    }

    private function processGroupedProduct(): array
    {
        $quote = $this->checkoutSession->getQuote();
        if (!$quote) {
            return [];
        }

        $itemsData = [];
        $superGroup = (array)$this->request->getParam('super_group', []);
        foreach ($quote->getAllVisibleItems() as $cartItem) {
            $productId = $cartItem->getProductId();
            if (isset($superGroup[$productId]) && ($superGroup[$productId] > 0)) {
                $cartItem->setQty($superGroup[$productId]);
                $itemsData[] = $this->productDetailsFormatter->formatFromCartItem($cartItem);
            }
        }

        return $itemsData;
    }

    private function retrieveLastAddedItem(CartInterface $quote): CartItemInterface
    {
        $quote->getItemsCollection()->setOrder('updated_at', Collection::SORT_ORDER_DESC);
        $visibleItems = $quote->getAllVisibleItems();
        $cartItem = end($visibleItems);

        $qty = (float)$this->request->getParam(self::REQUEST_QTY_KEY, 1);
        $cartItem->setQty($qty);

        return $cartItem;
    }
}
