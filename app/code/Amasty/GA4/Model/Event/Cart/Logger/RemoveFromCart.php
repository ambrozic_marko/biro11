<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Cart\Logger;

use Amasty\GA4\Model\Event\Cart\RemoveFromCart as RemoveFromCartEvent;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Quote\Api\Data\CartItemInterface;

class RemoveFromCart implements EventLoggerInterface
{
    /**
     * @var CartItemInterface
     */
    private CartItemInterface $cartItem;

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    public function __construct(
        CartItemInterface $cartItem,
        DataLayerCollector $dataLayerCollector,
        ProductDetailsFormatter $productDetailsFormatter
    ) {
        $this->dataLayerCollector = $dataLayerCollector;
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->cartItem = $cartItem;
    }

    public function execute(): void
    {
        $itemData = $this->productDetailsFormatter->formatFromCartItem($this->cartItem);
        if ($itemData) {
            $this->dataLayerCollector->addDataLayerOption(
                RemoveFromCartEvent::NAME,
                [ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => [$itemData]]
            );
        }
    }
}
