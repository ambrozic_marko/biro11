<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Cart\Logger;

use Amasty\GA4\Model\Event\Cart\ViewCart as ViewCartEvent;
use Amasty\GA4\Model\Utils\Formatter\Price;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Checkout\Model\Session;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class ViewCart implements EventLoggerInterface
{
    /**
     * @var Session
     */
    private Session $checkoutSession;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Price
     */
    private Price $priceFormatter;

    public function __construct(
        Session $checkoutSession,
        ProductDetailsFormatter $productDetailsFormatter,
        DataLayerCollector $dataLayerCollector,
        StoreManagerInterface $storeManager,
        Price $priceFormatter
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->dataLayerCollector = $dataLayerCollector;
        $this->storeManager = $storeManager;
        $this->priceFormatter = $priceFormatter;
    }

    public function execute(): void
    {
        $quote = $this->checkoutSession->getQuote();
        if (!$quote->getId()) {
            return;
        }

        $productItems = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $productItems[] = $this->productDetailsFormatter->formatFromCartItem($item);
        }

        if ($productItems) {
            $this->dataLayerCollector->addDataLayerOption(
                ViewCartEvent::NAME,
                [
                    ViewCartEvent::CURRENCY_KEY => $this->storeManager->getStore()->getCurrentCurrencyCode(),
                    ViewCartEvent::VALUE_KEY => $this->priceFormatter->format((float)$quote->getGrandTotal()),
                    ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => $productItems
                ]
            );
        }
    }
}
