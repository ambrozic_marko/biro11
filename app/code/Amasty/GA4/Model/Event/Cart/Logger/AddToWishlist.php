<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Cart\Logger;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\Cart\AddToWishlist as AddToWishlistEvent;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4\Model\Utils\ProductVariantResolver;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Catalog\Model\Product\Configuration\Item\ItemInterface;
use Magento\Framework\Data\Collection;
use Magento\Wishlist\Model\Item;
use Magento\Wishlist\Model\Wishlist;

class AddToWishlist implements EventLoggerInterface
{
    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var ProductVariantResolver
     */
    private ProductVariantResolver $productVariantResolver;

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var Wishlist
     */
    private Wishlist $wishlist;

    /**
     * @var array
     */
    private array $buyRequest;

    public function __construct(
        Wishlist $wishlist,
        ProductDetailsFormatter $productDetailsFormatter,
        ProductVariantResolver $productVariantResolver,
        DataLayerCollector $dataLayerCollector,
        ConfigProvider $configProvider,
        array $buyRequest = []
    ) {
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->productVariantResolver = $productVariantResolver;
        $this->dataLayerCollector = $dataLayerCollector;
        $this->buyRequest = $buyRequest;
        $this->configProvider = $configProvider;
        $this->wishlist = $wishlist;
    }

    public function execute(): void
    {
        $itemData = $this->prepareItemData();
        if ($itemData) {
            $this->dataLayerCollector->addDataLayerOption(
                AddToWishlistEvent::NAME,
                [ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => [$itemData]]
            );
        }
    }

    private function prepareItemData(): array
    {
        /** @var Item $wishlistItem */
        $wishlistItem = $this->wishlist->getItemCollection()
            ->setOrder('added_at', Collection::SORT_ORDER_ASC)
            ->getLastItem();
        $product = $wishlistItem->getProduct();
        $productData = $this->productDetailsFormatter->formatFromProduct($product);
        if (!empty($productData) && $this->configProvider->isEnableVariant()) {
            $variants = $this->productVariantResolver->getWishlistProductVariants(
                $wishlistItem,
                $this->buyRequest
            );
            if ($variants) {
                $productData[ProductDetailsFormatter::ITEM_VARIANT] = $variants;
            }
        }

        return $productData;
    }
}
