<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Cart;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Quote\Model\ResourceModel\Quote\Item\CollectionFactory;

class RemoveFromCart extends CommonEvent
{
    public const NAME = 'remove_from_cart';
    public const LOGGER_CLASS = Logger\RemoveFromCart::class;

    public const CURRENCY_KEY = 'currency';
    public const VALUE_KEY = 'value';

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $quoteItemCollectionFactory;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        CollectionFactory $quoteItemCollectionFactory,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
        $this->quoteItemCollectionFactory = $quoteItemCollectionFactory;
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        $meta = $this->buildCommonMeta(
            'Amasty GA4 - ' . self::NAME,
            'Amasty GA4 - Remove Item(s) from Cart',
            $fingerprint
        );
        if (isset($meta['containerVersion']['tag'][0]['parameter'])) {
            foreach ($meta['containerVersion']['tag'][0]['parameter'] as &$param) {
                if ($param['key'] === 'eventParameters') {
                    $param['list'][] = $this->buildEventParameter(self::CURRENCY_KEY, VariablesProvider::GA4_CURRENCY);
                    $param['list'][] = $this->buildEventParameter(
                        self::VALUE_KEY,
                        VariablesProvider::GA4_PURCHASE_VALUE
                    );
                }
            }
        }

        return $meta;
    }

    public function isAvailable(): bool
    {
        return true;
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_REQUEST;
    }

    public function getLogger(array $eventData = []): EventLoggerInterface
    {
        $loggerData = [];
        if (isset($eventData['item_id'])) {
            $cartItem = $this->retrieveCartItem((int)$eventData['item_id']);
            if ($cartItem->getId()) {
                $loggerData['cartItem'] = $cartItem;
            }
        }

        return parent::getLogger($loggerData);
    }

    /**
     * @param int $itemId
     *
     * @return CartItemInterface
     */
    private function retrieveCartItem(int $itemId): CartItemInterface
    {
        $collection = $this->quoteItemCollectionFactory->create();
        $collection->addFieldToFilter('item_id', $itemId)->setPageSize(1);

        return $collection->getFirstItem();
    }
}
