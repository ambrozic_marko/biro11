<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Cart;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Magento\Wishlist\Controller\WishlistProviderInterface;

class AddToWishlist extends CommonEvent
{
    public const NAME = 'add_to_wishlist';
    public const LOGGER_CLASS = Logger\AddToWishlist::class;

    public const CURRENCY_KEY = 'currency';
    public const VALUE_KEY = 'value';

    /**
     * @var WishlistProviderInterface
     */
    private WishlistProviderInterface $wishlistProvider;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        WishlistProviderInterface $wishlistProvider,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
        $this->wishlistProvider = $wishlistProvider;
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        $meta = $this->buildCommonMeta(
            'Amasty GA4 - ' . self::NAME,
            'Amasty GA4 - Add Item(s) to Wishlist',
            $fingerprint
        );
        if (isset($meta['containerVersion']['tag'][0]['parameter'])) {
            foreach ($meta['containerVersion']['tag'][0]['parameter'] as &$param) {
                if ($param['key'] === 'eventParameters') {
                    $param['list'][] = $this->buildEventParameter(self::VALUE_KEY, VariablesProvider::GA4_CURRENCY);
                    $param['list'][] = $this->buildEventParameter(
                        self::CURRENCY_KEY,
                        VariablesProvider::GA4_PURCHASE_VALUE
                    );
                }
            }
        }

        return $meta;
    }

    public function isAvailable(): bool
    {
        return true;
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_REQUEST;
    }

    public function getLogger(array $eventData = []): EventLoggerInterface
    {
        $loggerData['wishlist'] = $this->wishlistProvider->getWishlist($eventData['wishlist_id'] ?? null);
        $loggerData['buyRequest'] = $eventData;

        return parent::getLogger($loggerData);
    }
}
