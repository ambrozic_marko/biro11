<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Cart;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4\Model\ResourceModel\WishlistItemResource;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Magento\Catalog\Api\ProductRepositoryInterface;

class AddToCart extends CommonEvent
{
    public const NAME = 'add_to_cart';
    public const LOGGER_CLASS = Logger\AddToCart::class;

    public const CURRENCY_KEY = 'currency';
    public const VALUE_KEY = 'value';

    /**
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * @var WishlistItemResource
     */
    private WishlistItemResource $wishlistItemResource;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        ProductRepositoryInterface $productRepository,
        WishlistItemResource $wishlistItemResource,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
        $this->productRepository = $productRepository;
        $this->wishlistItemResource = $wishlistItemResource;
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        $meta = $this->buildCommonMeta('Amasty GA4 - ' . self::NAME, 'Amasty GA4 - Add Item(s) to Cart', $fingerprint);
        if (isset($meta['containerVersion']['tag'][0]['parameter'])) {
            foreach ($meta['containerVersion']['tag'][0]['parameter'] as &$param) {
                if ($param['key'] === 'eventParameters') {
                    $param['list'][] = $this->buildEventParameter(self::CURRENCY_KEY, VariablesProvider::GA4_CURRENCY);
                    $param['list'][] = $this->buildEventParameter(
                        self::VALUE_KEY,
                        VariablesProvider::GA4_PURCHASE_VALUE
                    );
                }
            }
        }

        return $meta;
    }

    public function isAvailable(): bool
    {
        return true;
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_REQUEST;
    }

    public function getLogger(array $eventData = []): EventLoggerInterface
    {
        $loggerData = [];
        if (isset($eventData['product'])) {
            $productId = (int)$eventData['product'];
        } elseif (isset($eventData['item'])) { //add_to_cart from wishlist page
            $productId = $this->wishlistItemResource->getProductIdByWishlistId((int)$eventData['item']);
        }
        if (isset($productId)) {
            $product = $this->productRepository->getById($productId);
            $loggerData['product'] = $product;
        }

        return parent::getLogger($loggerData);
    }
}
