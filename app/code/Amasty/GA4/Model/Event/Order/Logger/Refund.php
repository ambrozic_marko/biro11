<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Order\Logger;

use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\OrderInterface;

class Refund implements EventLoggerInterface
{
    /**
     * @var MeasurementProtocolAdapterInterface
     */
    private MeasurementProtocolAdapterInterface $measurementProtocolAdapter;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var OrderInterface
     */
    private OrderInterface $order;

    /**
     * @var CreditmemoInterface|null
     */
    private ?CreditmemoInterface $creditMemo;

    public function __construct(
        MeasurementProtocolAdapterInterface $measurementProtocolAdapter,
        ProductDetailsFormatter $productDetailsFormatter,
        OrderInterface $order,
        ?CreditmemoInterface $creditMemo = null
    ) {
        $this->measurementProtocolAdapter = $measurementProtocolAdapter;
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->order = $order;
        $this->creditMemo = $creditMemo;
    }

    public function execute(): void
    {
        $order = $this->order;
        $extensions = $order->getExtensionAttributes();
        if (!$extensions || !$extensions->getAmGa4ClientId()) {
            return;
        }

        $creditMemo = $this->creditMemo;
        $productItems = [];
        if (null !== $creditMemo) {
            foreach ($creditMemo->getAllItems() as $item) {
                $productItems[] = $this->productDetailsFormatter->formatFromOrderItem($item->getOrderItem());
            }
        } else {
            foreach ($order->getAllVisibleItems() as $item) {
                $productItems[] = $this->productDetailsFormatter->formatFromOrderItem($item);
            }
        }

        $data = [
            'client_id' => $order->getExtensionAttributes()->getAmGa4ClientId(),
            'currency' => $creditMemo ? $creditMemo->getOrderCurrencyCode() : $order->getOrderCurrencyCode(),
            'transaction_id' => $order->getIncrementId(),
            'value' => $creditMemo ? $creditMemo->getGrandTotal() : $order->getGrandTotal(),
            'shipping' => $creditMemo ? $creditMemo->getShippingAmount() : $order->getShippingAmount(),
            'tax' => $creditMemo ? $creditMemo->getTaxAmount() : $order->getTaxAmount(),
            'items' => $productItems
        ];

        $this->measurementProtocolAdapter->sendData($data);
    }
}
