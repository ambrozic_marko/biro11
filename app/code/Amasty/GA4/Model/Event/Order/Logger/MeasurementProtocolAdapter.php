<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Order\Logger;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\ThirdParty\AdditionalExtensionsChecker;
use Br33f\Ga4\MeasurementProtocol\Dto\Event\RefundEvent;
use Br33f\Ga4\MeasurementProtocol\Dto\Parameter\ItemParameter;
use Br33f\Ga4\MeasurementProtocol\Dto\Request\BaseRequest;
use Br33f\Ga4\MeasurementProtocol\Dto\Response\BaseResponse;
use Br33f\Ga4\MeasurementProtocol\Service;
use Magento\Framework\ObjectManagerInterface;
use Psr\Log\LoggerInterface;

class MeasurementProtocolAdapter implements MeasurementProtocolAdapterInterface
{
    public const SUCCESS_CODE = "/^2[0-9][0-9]*$/";
    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var ObjectManagerInterface
     */
    private ObjectManagerInterface $objectManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ConfigProvider $configProvider,
        ObjectManagerInterface $objectManager,
        LoggerInterface $logger,
        AdditionalExtensionsChecker $additionalExtensionsChecker
    ) {
        $this->configProvider = $configProvider;
        $this->objectManager = $objectManager;
        $this->logger = $logger;
        if (!$additionalExtensionsChecker->isGa4MPInstalled()) {
            throw new \RuntimeException(__('PHP library br33f/php-ga4-mp is required. '
                . 'Please, run the following command in the SSH: composer require br33f/php-ga4-mp')->getText());
        }
    }

    /**
     * @param array $data [client_id => _ga_cookie, currency => 'CurrencyCode', transaction_id => 'order_id',
     * value => 'total', shipping => 'shippingAmount', tax => 'taxAmount',
     * items => [ item_name => 'Product Name', item_id => 'SKU', price => 'price', currency => 'CurrencyCode',
     * item_category => 'Product Category', quantity => 'qty']]
     */
    public function sendData(array $data): void
    {
        $service = $this->objectManager->create(
            Service::class,
            [
                'apiSecret' => $this->configProvider->getSecretKey(),
                'measurementId' => $this->configProvider->getMeasurementId()
            ]
        );

        $baseRequest = $this->objectManager->create(BaseRequest::class);
        $baseRequest->setClientId($data['client_id']);

        $refund = $this->prepareRefund($data);
        $baseRequest->addEvent($refund);

        $response = $service->send($baseRequest);
        $this->logResponse($response);
    }

    /**
     * @param array $data [client_id => _ga_cookie, currency => 'CurrencyCode', transaction_id => 'order_id',
     * value => 'total', shipping => 'shippingAmount', tax => 'taxAmount',
     * items => [ item_name => 'Product Name', item_id => 'SKU', price => 'price', currency => 'CurrencyCode',
     * item_category => 'Product Category', quantity => 'qty']]
     */
    private function prepareRefund(array $data): RefundEvent
    {
        $refund = $this->objectManager->create(RefundEvent::class);
        $refund->setCurrency($data['currency'])
            ->setTransactionId($data['transaction_id'])
            ->setValue((float)$data['value'])
            ->setShipping((float)$data['shipping'])
            ->setTax((float)$data['tax']);

        foreach ($data['items'] as $item) {
            $itemParameter = $this->objectManager->create(ItemParameter::class);
            foreach ($item as $key => $param) {
                $method = 'set' . str_replace('_', '', ucwords($key, '_'));
                $itemParameter->$method($param);
            }

            $refund->addItem($itemParameter);
        }

        return $refund;
    }

    private function logResponse(BaseResponse $response): void
    {
        $statusCode = (string)$response->getStatusCode();
        $body = $response->getBody();
        if (preg_match(self::SUCCESS_CODE, $statusCode)) {
            return;
        }

        if ($body) {
            $this->logger->error(
                __("Measurement Protocol API request error occurred. Error: '%1'. Code: '%2'", $statusCode, $body)
            );
        } else {
            $this->logger->error(
                __("Measurement Protocol API request error occurred. Code: '%1'", $statusCode)
            );
        }
    }
}
