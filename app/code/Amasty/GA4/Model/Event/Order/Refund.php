<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Order;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\ThirdParty\AdditionalExtensionsChecker;
use Amasty\GA4Api\Api\Event\EventConfigInterface;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;

class Refund implements EventConfigInterface
{
    public const NAME = 'refund';
    public const LOGGER_CLASS = Logger\Refund::class;

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var EventLoggerFactory
     */
    private EventLoggerFactory $eventLoggerFactory;

    /**
     * @var AdditionalExtensionsChecker
     */
    private AdditionalExtensionsChecker $additionalExtensionsChecker;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        AdditionalExtensionsChecker $additionalExtensionsChecker
    ) {
        $this->configProvider = $configProvider;
        $this->eventLoggerFactory = $eventLoggerFactory;
        $this->additionalExtensionsChecker = $additionalExtensionsChecker;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getMeta(?int $fingerprint = null): array
    {
        return [];
    }

    public function isAvailable(): bool
    {
        return $this->additionalExtensionsChecker->isGa4MPInstalled()
            && $this->configProvider->isTrackRefund();
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_REQUEST;
    }

    public function getLogger(array $eventData = []): EventLoggerInterface
    {
        return $this->eventLoggerFactory->create(self::LOGGER_CLASS, $eventData);
    }
}
