<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Promo\Logger;

use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\LayoutInterface;

class Promotion implements EventLoggerInterface
{
    public const BLOCK_NAME = 'amasty.ga4.promotions';

    /**
     * @var LayoutInterface
     */
    private LayoutInterface $layout;

    public function __construct(
        LayoutInterface $layout
    ) {
        $this->layout = $layout;
    }

    public function execute(): void
    {
        if (!$this->layout->getBlock(self::BLOCK_NAME)) {
            $promotionsBlock = $this->layout->createBlock(
                Template::class,
                self::BLOCK_NAME
            );
            $promotionsBlock->setTemplate('Amasty_GA4::event/promotions.phtml');
            $this->layout->setChild('before.body.end', self::BLOCK_NAME, self::BLOCK_NAME);
        }
    }
}
