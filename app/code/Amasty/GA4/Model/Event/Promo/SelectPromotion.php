<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Promo;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;

class SelectPromotion extends CommonEvent
{
    public const NAME = 'select_promotion';
    public const LOGGER_CLASS = Logger\Promotion::class;

    //phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        return $this->buildCommonMeta('Amasty GA4 - ' . self::NAME, 'Amasty GA4 - Select Promotion', $fingerprint);
    }

    public function isAvailable(): bool
    {
        return $this->configProvider->isPromotionTrackingEnabled();
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_HEADER_DATALAYER;
    }
}
