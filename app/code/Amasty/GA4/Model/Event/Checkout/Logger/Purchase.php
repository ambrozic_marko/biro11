<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Checkout\Logger;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\Checkout\Purchase as PurchaseEvent;
use Amasty\GA4\Model\GtmMeta\AdWordsConversions;
use Amasty\GA4\Model\OptionSource\OrderTotalCalculation;
use Amasty\GA4\Model\Utils\Formatter\Price;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Checkout\Model\Session;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class Purchase implements EventLoggerInterface
{
    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var Session
     */
    private Session $checkoutSession;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var Price
     */
    private Price $priceFormatter;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $ordersCollectionFactory;

    public function __construct(
        DataLayerCollector $dataLayerCollector,
        ProductDetailsFormatter $productDetailsFormatter,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        ConfigProvider $configProvider,
        Price $priceFormatter,
        CollectionFactory $ordersCollectionFactory
    ) {
        $this->dataLayerCollector = $dataLayerCollector;
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->configProvider = $configProvider;
        $this->priceFormatter = $priceFormatter;
        $this->ordersCollectionFactory = $ordersCollectionFactory;
    }

    public function execute(): void
    {
        $orders = $this->retrieveLastOrders();
        $trackAdWords = count($orders) === 1;

        foreach ($orders as $order) {
            $this->processPurchaseEvent($order);
            if ($trackAdWords) {
                $this->processAdwordsConversion($order);
            }
        }
    }

    private function processPurchaseEvent(OrderInterface $order): void
    {
        if ((float)$order->getGrandtotal() <= .0 && $this->configProvider->isExcludeFreeOrders()) {
            return;
        }

        $productItems = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $productItems[] = $this->productDetailsFormatter->formatFromOrderItem($item);
        }

        if ($productItems) {
            $this->dataLayerCollector->addDataLayerOption(
                PurchaseEvent::NAME,
                [
                    PurchaseEvent::CURRENCY_KEY => $this->storeManager->getStore()->getCurrentCurrencyCode(),
                    PurchaseEvent::VALUE_KEY => $this->getOrderTotal($order),
                    PurchaseEvent::COUPON_KEY => $order->getCouponCode(),
                    PurchaseEvent::TAX_KEY => $this->priceFormatter->format((float)$order->getTaxAmount()),
                    PurchaseEvent::SHIPPING_KEY => $this->priceFormatter->format((float)$order->getShippingAmount()),
                    PurchaseEvent::TRANSACTION_ID_KEY => $order->getIncrementId(),
                    ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => $productItems
                ]
            );
        }
    }

    private function processAdwordsConversion(OrderInterface $order): void
    {
        if (!$this->configProvider->isAdWordsConversionEnabled()) {
            return;
        }

        if (((float)$order->getGrandtotal() <= .0
            && $this->configProvider->isExcludeConversionFreeOrders())
        ) {
            return;
        }

        $this->dataLayerCollector->addUserProperty(
            AdWordsConversions::TEMPLATE_CONVERSION_TRACKING_CONVERSION_VALUE,
            $this->getOrderTotal($order)
        );
        $this->dataLayerCollector->addUserProperty(
            AdWordsConversions::TEMPLATE_CONVERSION_TRACKING_ORDER_ID,
            $order->getIncrementId()
        );
    }

    /**
     * @return OrderInterface[]
     */
    private function retrieveLastOrders(): array
    {
        $quoteId = (int)$this->checkoutSession->getLastQuoteId();
        if (!$quoteId) {
            return [];
        }

        $collection = $this->ordersCollectionFactory->create();
        $collection->addFieldToFilter('quote_id', $quoteId);

        $result = [];
        foreach ($collection->getItems() as $order) {
            if (!$order->getId()) {
                continue;
            }
            $result[] = $order;
        }

        return $result;
    }

    private function getOrderTotal(OrderInterface $order): float
    {
        switch ($this->configProvider->getOrderTotalCalculation()) {
            case OrderTotalCalculation::GRAND_TOTAL:
                $orderTotal = $this->calculateAvailableGrandTotal($order);
                break;
            case OrderTotalCalculation::SUBTOTAL:
            default:
                $orderTotal = $order->getSubtotal();
                break;
        }

        return $this->priceFormatter->format((float)$orderTotal);
    }

    private function calculateAvailableGrandTotal(OrderInterface $order): float
    {
        $isExcludeTax = $this->configProvider->isExcludeTax();
        $isExcludeShipping = $this->configProvider->isExcludeShipping();

        $price = (float)$order->getGrandTotal();
        if ($isExcludeShipping) {
            $price -= $order->getShippingAmount();
        }

        if ($isExcludeTax) {
            $price -= $order->getTaxAmount();
            if ($isExcludeShipping) {
                $price -= $order->getShippingTaxAmount();
            }
        }

        return $price;
    }
}
