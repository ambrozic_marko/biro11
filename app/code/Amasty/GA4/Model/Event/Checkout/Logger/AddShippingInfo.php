<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Checkout\Logger;

use Amasty\GA4\Model\Event\Checkout\AddShippingInfo as AddShippingInfoEvent;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Checkout\Model\Session;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class AddShippingInfo implements EventLoggerInterface
{
    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var Session
     */
    private Session $checkoutSession;

    public function __construct(
        ProductDetailsFormatter $productDetailsFormatter,
        DataLayerCollector $dataLayerCollector,
        Session $checkoutSession
    ) {
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->dataLayerCollector = $dataLayerCollector;
        $this->checkoutSession = $checkoutSession;
    }

    public function execute(): void
    {
        $quote = $this->checkoutSession->getQuote();
        if (!$quote->getId() || !$quote->getShippingAddress()) {
            return;
        }

        $productItems = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $productItems[] = $this->productDetailsFormatter->formatFromCartItem($item);
        }

        if ($productItems) {
            $this->dataLayerCollector->addDataLayerOption(
                AddShippingInfoEvent::NAME,
                [
                    AddShippingInfoEvent::SHIPPING_TIER_KEY => $quote->getShippingAddress()->getShippingDescription(),
                    ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => $productItems
                ]
            );
        }
    }
}
