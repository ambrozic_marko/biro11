<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Checkout\Logger;

use Amasty\GA4\Model\Event\Checkout\BeginCheckout as BeginCheckoutEvent;
use Amasty\GA4\Model\Utils\Formatter\Price;
use Amasty\GA4\Model\Utils\ProductDetailsFormatter;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;
use Amasty\GA4Api\Model\DataLayerCollector;
use Magento\Checkout\Model\Session;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class BeginCheckout implements EventLoggerInterface
{
    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var ProductDetailsFormatter
     */
    private ProductDetailsFormatter $productDetailsFormatter;

    /**
     * @var Session
     */
    private Session $checkoutSession;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Price
     */
    private Price $priceFormatter;

    public function __construct(
        DataLayerCollector $dataLayerCollector,
        ProductDetailsFormatter $productDetailsFormatter,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Price $priceFormatter
    ) {
        $this->dataLayerCollector = $dataLayerCollector;
        $this->productDetailsFormatter = $productDetailsFormatter;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->priceFormatter = $priceFormatter;
    }

    public function execute(): void
    {
        $quote = $this->checkoutSession->getQuote();
        if (!$quote->getId()) {
            return;
        }

        $productItems = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $productItems[] = $this->productDetailsFormatter->formatFromCartItem($item);
        }

        if ($productItems) {
            $this->dataLayerCollector->addDataLayerOption(
                BeginCheckoutEvent::NAME,
                [
                    BeginCheckoutEvent::CURRENCY_KEY => $this->storeManager->getStore()->getCurrentCurrencyCode(),
                    BeginCheckoutEvent::VALUE_KEY => $this->priceFormatter->format((float)$quote->getGrandTotal()),
                    BeginCheckoutEvent::COUPON_KEY => $quote->getCouponCode(),
                    ProductDetailsFormatter::ECOMMERCE_ITEMS_KEY => $productItems
                ]
            );
        }
    }
}
