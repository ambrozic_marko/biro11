<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Checkout;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Magento\Framework\App\Request\Http;

class Purchase extends CommonEvent
{
    public const NAME = 'purchase';
    public const LOGGER_CLASS = Logger\Purchase::class;

    public const CURRENCY_KEY = 'currency';
    public const VALUE_KEY = 'value';
    public const COUPON_KEY = 'coupon';
    public const TAX_KEY = 'tax';
    public const SHIPPING_KEY = 'shipping';
    public const TRANSACTION_ID_KEY = 'transaction_id';

    private const DEFAULT_SUCCESS_PAGES = [
        'checkout/onepage/success',
        'multishipping/checkout/success'
    ];

    /**
     * @var Http
     */
    private Http $request;

    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        Http $request,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
        $this->configProvider = $configProvider;
        $this->request = $request;
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        $meta = $this->buildCommonMeta('Amasty GA4 - ' . self::NAME, 'Amasty GA4 - Purchase', $fingerprint);
        if (isset($meta['containerVersion']['tag'][0]['parameter'])) {
            foreach ($meta['containerVersion']['tag'][0]['parameter'] as &$param) {
                if ($param['key'] === 'eventParameters') {
                    $param['list'][] = $this->buildEventParameter(self::CURRENCY_KEY, VariablesProvider::GA4_CURRENCY);
                    $param['list'][] = $this->buildEventParameter(
                        self::VALUE_KEY,
                        VariablesProvider::GA4_PURCHASE_VALUE
                    );
                    $param['list'][] = $this->buildEventParameter(self::COUPON_KEY, VariablesProvider::GA4_COUPON);
                    $param['list'][] = $this->buildEventParameter(self::TAX_KEY, VariablesProvider::GA4_TAX);
                    $param['list'][] = $this->buildEventParameter(self::SHIPPING_KEY, VariablesProvider::GA4_SHIPPING);
                    $param['list'][] = $this->buildEventParameter(
                        self::TRANSACTION_ID_KEY,
                        VariablesProvider::GA4_TRANSACTION_ID
                    );

                }
            }
        }

        return $meta;
    }

    public function isAvailable(): bool
    {
        $requestPath = trim($this->request->getPathInfo(), '/');
        $successPages = [...self::DEFAULT_SUCCESS_PAGES, ...$this->configProvider->getSuccessPages()];

        return in_array($requestPath, $successPages);
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_HEADER_DATALAYER;
    }
}
