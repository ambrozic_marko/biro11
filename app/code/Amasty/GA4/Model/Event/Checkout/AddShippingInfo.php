<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Checkout;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Event\CommonEvent;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Model\Event\EventLoggerFactory;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;

class AddShippingInfo extends CommonEvent
{
    public const NAME = 'add_shipping_info';
    public const LOGGER_CLASS = Logger\AddShippingInfo::class;

    public const SHIPPING_TIER_KEY = 'shipping_tier';

    //phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
    public function __construct(
        ConfigProvider $configProvider,
        EventLoggerFactory $eventLoggerFactory,
        IncrementIdStorage $incrementIdStorage,
        string $loggerClass = self::LOGGER_CLASS
    ) {
        parent::__construct($configProvider, $eventLoggerFactory, $incrementIdStorage, $loggerClass);
    }

    public function getMeta(?int $fingerprint = null): array
    {
        $fingerprint = $fingerprint ?? time();

        $meta = $this->buildCommonMeta('Amasty GA4 - ' . self::NAME, 'Amasty GA4 - Shipping Info', $fingerprint);
        if (isset($meta['containerVersion']['tag'][0]['parameter'])) {
            foreach ($meta['containerVersion']['tag'][0]['parameter'] as &$param) {
                if ($param['key'] === 'eventParameters') {
                    $param['list'][] = $this->buildEventParameter(
                        self::SHIPPING_TIER_KEY,
                        VariablesProvider::GA4_SHIPPING_TYPE
                    );
                }
            }
        }

        return $meta;
    }

    public function isAvailable(): bool
    {
        return true;
    }

    public function getUsageType(): string
    {
        return EventsConfigPool::USAGE_REQUEST;
    }
}
