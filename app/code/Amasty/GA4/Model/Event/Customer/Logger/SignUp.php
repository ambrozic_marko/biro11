<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Customer\Logger;

use Amasty\GA4\Model\Event\Customer\SignUp as SignUpEvent;
use Amasty\GA4\Model\Frontend\GA4Session;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class SignUp implements EventLoggerInterface
{
    /**
     * @var GA4Session
     */
    private GA4Session $ga4Session;

    /**
     * @var string
     */
    private string $type;

    public function __construct(
        GA4Session $ga4Session,
        string $type
    ) {
        $this->ga4Session = $ga4Session;
        $this->type = $type;
    }

    public function execute(): void
    {
        $this->ga4Session->addEvent(
            SignUpEvent::NAME,
            [
                'event' => SignUpEvent::NAME,
                'method' => $this->type
            ]
        );
    }
}
