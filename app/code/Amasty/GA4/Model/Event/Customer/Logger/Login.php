<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Event\Customer\Logger;

use Amasty\GA4\Model\Event\Customer\Login as LoginEvent;
use Amasty\GA4\Model\Frontend\GA4Session;
use Amasty\GA4Api\Api\Event\EventLoggerInterface;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class Login implements EventLoggerInterface
{
    /**
     * @var GA4Session
     */
    private GA4Session $ga4Session;

    /**
     * @var string
     */
    private string $type;

    public function __construct(
        GA4Session $ga4Session,
        string $type
    ) {
        $this->ga4Session = $ga4Session;
        $this->type = $type;
    }

    public function execute(): void
    {
        $this->ga4Session->addEvent(
            LoginEvent::NAME,
            [
                'event' => LoginEvent::NAME,
                'method' => $this->type
            ]
        );
    }
}
