<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Cookie;

use Amasty\Base\Model\ModuleInfoProvider;
use Amasty\GA4\Model\ThirdParty\ModuleChecker;

class GdprCookieAdapter
{
    /**
     * @var bool
     */
    private bool $isAllowed;

    public function __construct(
        ModuleChecker $moduleChecker,
        ModuleInfoProvider $moduleInfoProvider
    ) {
        $this->isAllowed = $this->checkAvailability($moduleChecker, $moduleInfoProvider);
    }

    public function isModuleValid(): bool
    {
        return $this->isAllowed;
    }

    private function checkAvailability(ModuleChecker $moduleChecker, ModuleInfoProvider $moduleInfoProvider): bool
    {
        if (!$moduleChecker->isGdprCookieEnabled()) {
            return false;
        }

        $requiredVersion = '2.10.1';
        $originalVersion = $moduleInfoProvider->getModuleInfo(ModuleChecker::COOKIE_MODULE)['version'] ?? '0.0.1';

        return version_compare($originalVersion, $requiredVersion) >= 0;
    }
}
