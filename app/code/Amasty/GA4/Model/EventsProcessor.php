<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model;

use Amasty\GA4\Model\Frontend\CustomerPropertiesProcessor;
use Amasty\GA4Api\Api\Event\EventConfigInterface;
use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Psr\Log\LoggerInterface;

class EventsProcessor
{
    /**
     * @var EventsConfigPool
     */
    private EventsConfigPool $eventsConfigPool;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(
        EventsConfigPool $eventsConfigPool,
        LoggerInterface $logger
    ) {
        $this->eventsConfigPool = $eventsConfigPool;
        $this->logger = $logger;
    }

    public function processEvent(string $eventName, array $eventData = []): void
    {
        $eventConfig = $this->eventsConfigPool->getByName($eventName);
        if ($eventConfig === null || !$eventConfig->isAvailable()) {
            return;
        }
        $this->performLogging($eventConfig, $eventData);
    }

    public function processDataLayerData(
        string $usage = EventsConfigPool::USAGE_HEADER_DATALAYER,
        array $eventData = []
    ): void {
        foreach ($this->eventsConfigPool->getByUsage($usage) as $eventConfig) {
            if ($eventConfig->isAvailable()) {
                $this->performLogging($eventConfig, $eventData);
            }
        }
    }

    private function performLogging(EventConfigInterface $eventConfig, array $eventData): void
    {
        try {
            $eventConfig->getLogger($eventData)->execute();
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
    }
}
