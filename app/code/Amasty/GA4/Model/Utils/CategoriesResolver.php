<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Utils;

use Amasty\GA4\Model\ResourceModel\CategoriesPathResource;
use Magento\Store\Api\Data\StoreInterface;

class CategoriesResolver
{
    public const GA4_CATEGORY_KEY = 'item_category';

    /**
     * @var CategoriesPathResource
     */
    private CategoriesPathResource $categoriesPathResource;

    public function __construct(
        CategoriesPathResource $categoriesPathResource
    ) {
        $this->categoriesPathResource = $categoriesPathResource;
    }

    public function getGa4CategoriesFromId(int $categoryId, StoreInterface $store): array
    {
        $categoriesNamesMap = $this->getCategoryNamePathFromId($categoryId, $store);
        $result = [];
        foreach ($categoriesNamesMap as $i => $name) {
            $key = $i === 0 ? self::GA4_CATEGORY_KEY : self::GA4_CATEGORY_KEY . ($i + 1);
            $result[$key] = $name;
        }

        return $result;
    }

    public function getCategoryNamePathFromId(int $categoryId, StoreInterface $store): array
    {
        $categoryPath = $this->categoriesPathResource->getCategoryPath($categoryId);
        $rootCategoryId = (int)$store->getRootCategoryId();
        $categoryPath = array_filter($categoryPath, static fn ($categoryId) => $categoryId !== $rootCategoryId);

        return array_values($this->categoriesPathResource->getCategoriesNames($categoryPath, (int)$store->getId()));
    }
}
