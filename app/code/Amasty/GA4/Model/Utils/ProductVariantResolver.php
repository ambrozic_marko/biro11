<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Utils;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductCustomOptionRepositoryInterface;
use Magento\Catalog\Helper\Product\Configuration;
use Magento\Catalog\Model\Product\Configuration\Item\ItemInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Sales\Api\Data\OrderItemInterface;

class ProductVariantResolver
{
    private const VARIANTS_SEPARATOR = ';';

    /**
     * @var Configuration
     */
    private Configuration $configurationHelper;

    /**
     * @var ProductCustomOptionRepositoryInterface
     */
    private ProductCustomOptionRepositoryInterface $productCustomOptionRepository;

    public function __construct(
        Configuration $configurationHelper,
        ProductCustomOptionRepositoryInterface $productCustomOptionRepository
    ) {
        $this->configurationHelper = $configurationHelper;
        $this->productCustomOptionRepository = $productCustomOptionRepository;
    }

    public function getProductVariants(ProductInterface $product, array $buyRequest = []): string
    {
        $variants = [];
        if ((string)$product->getTypeId() === Configurable::TYPE_CODE) {
            $variants = [...$variants, ...$this->getConfigurableProductVariants($product, $buyRequest)];
        }

        array_push($variants, ...$this->getProductOrderOptionsVariants($product));

        if (isset($buyRequest['options'])) {
            array_push($variants, ...$this->getProductOptionsVariants($product, $buyRequest));
        }

        return implode(self::VARIANTS_SEPARATOR . ' ', array_unique($variants));
    }

    public function getWishlistProductVariants(ItemInterface $wishlistItem, array $buyRequest = []): string
    {
        $product = $wishlistItem->getProduct();
        if (!$product) {
            return '';
        }

        $variants = [];
        if ((string)$product->getTypeId() === Configurable::TYPE_CODE) {
            $variants = [...$variants, ...$this->getConfigurableProductVariants($product, $buyRequest)];
        }

        if ($customOptions = $this->getProductOrderOptionsVariants($product)) {
            array_push($variants, ...$customOptions);
        } else {
            $options = $this->configurationHelper->getOptions($wishlistItem);
            foreach ($options as $customOption) {
                if (isset($customOption['print_value'])) {
                    $variants[] = $customOption['label'] . ': ' . $customOption['print_value'];
                }
            }
        }

        if (isset($buyRequest['options'])) {
            array_push($variants, ...$this->getProductOptionsVariants($product, $buyRequest));
        }

        return implode(self::VARIANTS_SEPARATOR . ' ', array_unique($variants));
    }

    public function getOrderItemProductVariants(OrderItemInterface $orderItem): string
    {
        $variants = [];
        $productOptions = $orderItem->getProductOptions();
        if ($orderItem->getProductType() == Configurable::TYPE_CODE) {
            foreach (($productOptions['attributes_info'] ?? []) as $attribute) {
                $variants[] = $attribute['label'] . ': ' . $attribute['value'];
            }
        }

        foreach (($productOptions['options'] ?? []) as $option) {
            $variants[] = $option['label'] . ': ' . $option['print_value'];
        }

        return implode(self::VARIANTS_SEPARATOR . ' ', array_unique($variants));
    }

    private function getConfigurableProductVariants(ProductInterface $product, array $buyRequest): array
    {
        $variants = [];

        foreach ($product->getTypeInstance()->getSelectedAttributesInfo($product) as $option) {
            $variants[] = $option['label'] . ': ' . $option['value'];
        }

        if (empty($variants) && isset($buyRequest['super_attribute'])) {
            $superAttrOptions = [];
            $attributes = $product->getTypeInstance()->getConfigurableAttributes($product);
            foreach ($attributes as $attr) {
                $attrId = $attr['attribute_id'];
                $superAttrOptions[$attrId]['label'] = $attr['label'];
                foreach ($attr->getOptions() as $option) {
                    $superAttrOptions[$attrId]['options'][$option['value_index']] = $option['store_label'];
                }
            }

            foreach ($buyRequest['super_attribute'] as $id => $value) {
                $variants[] = $superAttrOptions[$id]['label'] . ': ' . $superAttrOptions[$id]['options'][$value];
            }
        }

        return $variants;
    }

    private function getProductOrderOptionsVariants(ProductInterface $product): array
    {
        $variants = [];
        $customOptions = $product->getTypeInstance()->getOrderOptions($product);
        if (array_key_exists('options', $customOptions)) {
            foreach ($customOptions['options'] as $option) {
                $variants[] = $option['label'] . ': ' . $option['print_value'];
            }
        }

        return $variants;
    }

    private function getProductOptionsVariants(ProductInterface $product, array $buyRequest): array
    {
        $variants = $productCustomOptions = [];
        $productOptions = $this->productCustomOptionRepository->getProductOptions($product);
        foreach ($productOptions as $option) {
            $productCustomOptions[$option['option_id']]['label'] = $option['title'];
            if ($option->hasValues()) {
                $values = $option->getValues();
                foreach ($values as $value) {
                    $productCustomOptions[$option['option_id']]['values'][$value->getOptionTypeId()]
                        = $value->getTitle();
                }
            }
        }

        if (!$productCustomOptions) {
            return [];
        }

        foreach (array_filter($buyRequest['options']) as $optionId => $optionValues) {
            if (is_array($optionValues)) {
                $optValue = [];
                foreach ($optionValues as $value) {
                    $optValue[] = $productCustomOptions[$optionId]['values'][$value];
                }
                $variants[] = $productCustomOptions[$optionId]['label'] . ': ' . implode(',', $optValue);
            } elseif (isset($productCustomOptions[$optionId]['values'])) {
                $variants[] = $productCustomOptions[$optionId]['label']
                    . ': ' . $productCustomOptions[$optionId]['values'][$optionValues];
            } else {
                $variants[] = $productCustomOptions[$optionId]['label'] . ': ' . $optionValues;
            }
        }

        return $variants;
    }
}
