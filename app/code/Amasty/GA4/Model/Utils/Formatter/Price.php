<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Utils\Formatter;

class Price
{
    public function format(float $price): float
    {
        return round($price, 2);
    }
}
