<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Utils;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\OptionSource\IdentifierResolution;
use Amasty\GA4\Model\OptionSource\ProductIdentifier;
use Amasty\GA4\Model\Utils\Formatter\Price;
use Magento\Bundle\Model\Product\Type as BundleProductType;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Pricing\Price\FinalPrice as CatalogFinalPrice;
use Magento\Catalog\Pricing\Price\RegularPrice as CatalogRegularPrice;
use Magento\Catalog\Pricing\Price\SpecialPrice as CatalogSpecialPrice;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Escaper;
use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Store\Model\StoreManagerInterface;

class ProductDetailsFormatter
{
    public const ECOMMERCE_ITEMS_KEY = 'items';

    /**
     * Common info
     */
    public const ITEM_ID = 'item_id';
    public const ITEM_NAME = 'item_name';
    public const PRICE = 'price';
    public const ITEM_BRAND = 'item_brand';
    public const CURRENCY = 'currency';

    /**
     * List specific info
     */
    public const ITEM_LIST_ID = 'item_list_id';
    public const ITEM_LIST_NAME = 'item_list_name';
    public const INDEX = 'index';

    /**
     * Cart/checkout specific info
     */
    public const ITEM_VARIANT = 'item_variant';
    public const QUANTITY = 'quantity';

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var Escaper
     */
    private Escaper $escaper;

    /**
     * @var CategoriesResolver
     */
    private CategoriesResolver $categoriesResolver;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var ProductVariantResolver
     */
    private ProductVariantResolver $productVariantResolver;

    /**
     * @var Price
     */
    private Price $priceFormatter;

    public function __construct(
        ConfigProvider $configProvider,
        Escaper $escaper,
        CategoriesResolver $categoriesResolver,
        StoreManagerInterface $storeManager,
        ProductVariantResolver $productVariantResolver,
        Price $priceFormatter
    ) {
        $this->configProvider = $configProvider;
        $this->escaper = $escaper;
        $this->categoriesResolver = $categoriesResolver;
        $this->storeManager = $storeManager;
        $this->productVariantResolver = $productVariantResolver;
        $this->priceFormatter = $priceFormatter;
    }

    public function formatFromProduct(
        ProductInterface $product,
        string $listId = '',
        string $listName = '',
        ?int $index = null
    ): array {
        $productData[self::ITEM_NAME] = $this->escaper->escapeHtml($product->getName());
        $productData[self::ITEM_ID] = $this->getProductId($product);
        $productData[self::PRICE] = $this->getProductPrice($product);
        if ($this->configProvider->isEnableBrand()) {
            if ($brand = $this->getBrand($product)) {
                $productData[self::ITEM_BRAND] = $brand;
            }
        }
        $productData[self::CURRENCY] = $this->storeManager->getStore()->getCurrentCurrencyCode();

        if (!empty($listId) && !empty($listName) && !empty($index)) {
            $productData[self::ITEM_LIST_ID] = $listId;
            $productData[self::ITEM_LIST_NAME] = $listName;
            $productData[self::INDEX] = $index;
        }
        $productCategory = (int)($product->getCategoryId() ?: $product->getCategoryIds()[0] ?? null);
        if ($productCategory) {
            $ga4Categories = $this->categoriesResolver->getGa4CategoriesFromId(
                $productCategory,
                $this->storeManager->getStore()
            );
            $productData = array_merge($productData, $ga4Categories);
        }

        return $productData;
    }

    public function formatFromCartItem(
        CartItemInterface $cartItem,
        string $listId = '',
        string $listName = '',
        ?int $index = null
    ): array {
        $product = $cartItem->getProduct();
        $productData = $this->formatFromProduct($product, $listId, $listName, $index);
        if ($this->isChildIdentifier($cartItem)) {
            foreach ($cartItem->getChildren() as $child) {
                $productData[self::ITEM_ID] = $this->getProductId($child->getProduct());
            }
        }
        if ($this->configProvider->isEnableVariant()) {
            $variants = $this->productVariantResolver->getProductVariants($product);
            if ($variants) {
                $productData[self::ITEM_VARIANT] = $variants;
            }
        }
        $productData[self::ITEM_NAME] = $this->escaper->escapeHtml($cartItem->getName());
        $productData[self::PRICE] = $this->priceFormatter->format((float)$cartItem->getPriceInclTax());
        $productData[self::QUANTITY] = (float)$cartItem->getQty();

        return $productData;
    }

    public function formatFromOrderItem(
        OrderItemInterface $orderItem,
        string $listId = '',
        string $listName = '',
        ?int $index = null
    ): array {
        $product = $orderItem->getProduct();
        $productData = $this->formatFromProduct($product, $listId, $listName, $index);
        if ($this->isChildIdentifier($orderItem)) {
            foreach ($orderItem->getChildrenItems() as $child) {
                $productData[self::ITEM_ID] = $this->getProductId($child->getProduct());
            }
        }

        if ($this->configProvider->isEnableVariant()) {
            $variants = $this->productVariantResolver->getOrderItemProductVariants($orderItem);
            if ($variants) {
                $productData[self::ITEM_VARIANT] = $variants;
            }
        }
        $productData[self::ITEM_NAME] = $this->escaper->escapeHtml($orderItem->getName());
        $productData[self::PRICE] = $this->priceFormatter->format((float)$orderItem->getPrice());
        $productData[self::QUANTITY] = (float)$orderItem->getQtyOrdered();

        return $productData;
    }

    private function getProductId(ProductInterface $product): string
    {
        $productIdentifier = $this->configProvider->getProductIdentifier();
        switch ($productIdentifier) {
            case ProductIdentifier::SKU:
                $productId = $product->getData('sku');
                break;
            case ProductIdentifier::ID:
            default:
                $productId = (string)$product->getId();
                break;
        }

        return $productId;
    }

    private function getProductPrice(ProductInterface $product): float
    {
        $priceInfo = $product->getPriceInfo();
        $finalPrice = $priceInfo->getPrice(CatalogFinalPrice::PRICE_CODE)
            ->getAmount()
            ->getValue(['tax', 'weee']);
        if ($product->getTypeId() === BundleProductType::TYPE_CODE) {
            if ($product->getPrice()) {//fixed price
                $percentage = $priceInfo->getPrice(CatalogSpecialPrice::PRICE_CODE)->getDiscountPercent();
                if ($percentage) { //special price
                    $finalPrice = $priceInfo->getPrice(CatalogRegularPrice::PRICE_CODE)->getAmount()->getBaseAmount()
                        * $percentage / 100;
                }
            } else {//dynamic price
                $finalPrice = $priceInfo->getPrice(CatalogFinalPrice::PRICE_CODE)->getMinimalPrice()->getBaseAmount();
            }
        }

        return $this->priceFormatter->format((float)$finalPrice);
    }

    private function getBrand(ProductInterface $product): string
    {
        $brandAttr = $this->configProvider->getBrandAttribute();
        if (!$brandAttr) {
            return '';
        }

        $attrValue = $product->getData($brandAttr);
        if ($selectAttr = $product->getAttributeText($brandAttr)) {
            $attrValue = $selectAttr;
        }

        if (is_array($attrValue)) {
            $attrValue = implode(',', $attrValue);
        }

        return (string)$attrValue;
    }

    /**
     * @param CartItemInterface|OrderItemInterface $item
     *
     * @return bool
     */
    private function isChildIdentifier($item): bool
    {
        return $item->getProductType() === Configurable::TYPE_CODE
            && $this->configProvider->getIdentifierResolution() === IdentifierResolution::CHILD;
    }
}
