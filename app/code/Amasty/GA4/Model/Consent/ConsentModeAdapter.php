<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Consent;

use Amasty\GA4\Model\ThirdParty\ModuleChecker;
use Amasty\GoogleConsentMode\Model\ConfigProvider;
use Magento\Framework\ObjectManagerInterface;

class ConsentModeAdapter
{
    /**
     * @var bool
     */
    private bool $isAllowed;

    /**
     * @var ObjectManagerInterface
     */
    private ObjectManagerInterface $objectManager;

    public function __construct(
        ModuleChecker $moduleConsentChecker,
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
        $this->isAllowed = $this->checkAvailability($moduleConsentChecker);
    }

    public function isModuleValid(): bool
    {
        return $this->isAllowed;
    }

    private function checkAvailability(ModuleChecker $moduleConsentChecker): bool
    {
        if (!$moduleConsentChecker->isGoogleConsentModeEnabled()
            || ($moduleConsentChecker->isGoogleConsentModeEnabled()
                && !$this->objectManager->create(ConfigProvider::class)->isConsentModeEnabled())
        ) {
            return false;
        }

        return true;
    }
}
