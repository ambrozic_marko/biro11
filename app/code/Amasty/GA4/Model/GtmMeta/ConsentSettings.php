<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\GtmMeta;

use Amasty\GA4\Model\Consent\ConsentModeAdapter;
use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;

class ConsentSettings implements GtmMetaInterface
{
    /**
     * @var ConsentModeAdapter
     */
    private ConsentModeAdapter $consentModeAdapter;

    public function __construct(
        ConsentModeAdapter $consentModeAdapter
    ) {
        $this->consentModeAdapter = $consentModeAdapter;
    }

    public function collect(int $fingerprint = null, array &$data = []): void
    {
        if (!isset($data['containerVersion']['tag']) || !$this->consentModeAdapter->isModuleValid()) {
            return;
        }

        foreach ($data['containerVersion']['tag'] as &$tag) {
            $tag['consentSetting'] = [
                'consentStatus' => 'NOT_NEEDED'
            ];
        }
    }
}
