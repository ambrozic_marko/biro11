<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\GtmMeta;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;

class AdWordsRemarketing implements GtmMetaInterface
{
    public const TAG_REMARKETING_TRACKING = 'Amasty - AdWords Remarketing Tracking';

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var IncrementIdStorage
     */
    private IncrementIdStorage $incrementIdStorage;

    public function __construct(
        ConfigProvider $configProvider,
        IncrementIdStorage $incrementIdStorage
    ) {
        $this->configProvider = $configProvider;
        $this->incrementIdStorage = $incrementIdStorage;
    }

    public function collect(int $fingerprint = null, array &$data = []): void
    {
        $fingerprint ??= time();
        if (!$this->configProvider->isAdWordsRemarketingEnabled()) {
            return;
        }
        $data['containerVersion']['tag'][] = $this->getTag($fingerprint);
    }

    private function getTag(int $fingerprint): array
    {
        $accountId = $this->configProvider->getAccountId();
        $containerId = $this->configProvider->getContainerId();

        return [
            'name' => self::TAG_REMARKETING_TRACKING,
            'type' => 'sp',
            'firingTriggerId' => [
                CommonData::ALL_PAGES_TRIGGER_ID
            ],
            'tagFiringOption' => 'ONCE_PER_EVENT',
            'tagId' => $this->incrementIdStorage->getTagId(),
            'parameter' => [
                [
                    'type' => 'BOOLEAN',
                    'key' => 'enableConversionLinker',
                    'value' => 'true'
                ],
                [
                    'type' => 'BOOLEAN',
                    'key' => 'enableDynamicRemarketing',
                    'value' => 'false'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'conversionCookiePrefix',
                    'value' => '_gcl'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'conversionId',
                    'value' => $this->configProvider->getRemarketingConversionId()
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'customParamsFormat',
                    'value' => 'NONE'
                ],
                [
                    'type' => 'BOOLEAN',
                    'key' => 'rdp',
                    'value' => 'false'
                ]
            ],
            'monitoringMetadata' => [
                'type' => 'MAP'
            ],
            'accountId' => $accountId,
            'containerId' => $containerId,
            'fingerprint' => $fingerprint
        ];
    }
}
