<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\GtmMeta\Utils;

use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;

class VariablesProvider
{
    public const MEASUREMENT_ID = 'Amasty - MEASUREMENT ID';
    public const GA4_PAGE_TYPE = 'Amasty - GA4 - Page Type';
    public const GA4_CUSTOMER_ID = 'Amasty - GA4 - customerId';
    public const GA4_CUSTOMER_GROUP = 'Amasty - GA4 - customerGroup';
    public const GA4_LOGIN_METHOD = 'Amasty - GA4 - Login Method';
    public const GA4_ECOMMERCE_ITEMS = 'Amasty - GA4 - ecommerce.items';
    public const GA4_TRANSACTION_ID = 'Amasty - GA4 - transaction_id';
    public const GA4_COUPON = 'Amasty - GA4 - coupon';
    public const GA4_TAX = 'Amasty - GA4 - tax';
    public const GA4_SHIPPING = 'Amasty - GA4 - shipping';
    public const GA4_SHIPPING_TYPE = 'Amasty - GA4 - Shipping Type';
    public const GA4_PAYMENT_TYPE = 'Amasty - GA4 - Payment Type';
    public const GA4_CURRENCY = 'Amasty - GA4 - currency';
    public const GA4_PURCHASE_VALUE = 'Amasty - GA4 - Purchase Value';

    public const GA4_NAME_TEMPLATE_NAME_MAP = [
        'pageType' => self::GA4_PAGE_TYPE,
        'customerId' => self::GA4_CUSTOMER_ID,
        'customerGroup' => self::GA4_CUSTOMER_GROUP,
        'method' => self::GA4_LOGIN_METHOD,
        'ecommerce.items' => self::GA4_ECOMMERCE_ITEMS,
        'ecommerce.transaction_id' => self::GA4_TRANSACTION_ID,
        'ecommerce.coupon' => self::GA4_COUPON,
        'ecommerce.tax' => self::GA4_TAX,
        'ecommerce.shipping' => self::GA4_SHIPPING,
        'ecommerce.shipping_tier' => self::GA4_SHIPPING_TYPE,
        'ecommerce.payment_type' => self::GA4_PAYMENT_TYPE,
        'ecommerce.currency' => self::GA4_CURRENCY,
        'ecommerce.value' => self::GA4_PURCHASE_VALUE
    ];

    /**
     * @var IncrementIdStorage
     */
    private IncrementIdStorage $incrementIdStorage;

    public function __construct(
        IncrementIdStorage $incrementIdStorage
    ) {
        $this->incrementIdStorage = $incrementIdStorage;
    }

    public function get(string $accountId, string $containerId, string $measurementId, int $fingerprint): array
    {
        $variables = [
            [
                'name' => self::MEASUREMENT_ID,
                'type' => 'c',
                'parameter' => [
                    [
                        'type' => 'TEMPLATE',
                        'key' => 'value',
                        'value' => $measurementId
                    ]
                ],
                'accountId' => $accountId,
                'containerId' => $containerId,
                'variableId' => $this->getVariableId(),
                'fingerprint' => $fingerprint,
                'formatValue' => new \stdClass()
            ]
        ];

        foreach (self::GA4_NAME_TEMPLATE_NAME_MAP as $templateName => $name) {
            $variables[] = $this->prepareBasicVariable($name, $templateName, $accountId, $containerId, $fingerprint);
        }

        return $variables;
    }

    public function prepareBasicVariable(
        string $variableName,
        string $templateVariableName,
        string $accountId,
        string $containerId,
        int $fingerprint
    ): array {
        return [
            'name' => $variableName,
            'type' => 'v',
            'parameter' => [
                [
                    'type' => 'INTEGER',
                    'key' => 'dataLayerVersion',
                    'value' => '2'
                ],
                [
                    'type' => 'BOOLEAN',
                    'key' => 'setDefaultValue',
                    'value' => 'false'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'name',
                    'value' => $templateVariableName
                ]
            ],
            'accountId' => $accountId,
            'containerId' => $containerId,
            'variableId' => $this->getVariableId(),
            'fingerprint' => $fingerprint,
            'formatValue' => new \stdClass()
        ];
    }

    protected function getVariableId(): int
    {
        return $this->incrementIdStorage->getVariableId();
    }
}
