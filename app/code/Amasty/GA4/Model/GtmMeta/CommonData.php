<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\GtmMeta;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;
use Magento\Framework\Stdlib\DateTime;

class CommonData implements GtmMetaInterface
{
    public const MEASUREMENT_TAG_NAME = 'Amasty GA4';
    public const ALL_PAGES_TRIGGER_ID = '2147479553';

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var VariablesProvider
     */
    private VariablesProvider $variablesProvider;

    /**
     * @var IncrementIdStorage
     */
    private IncrementIdStorage $incrementIdStorage;

    public function __construct(
        ConfigProvider $configProvider,
        VariablesProvider $variablesProvider,
        IncrementIdStorage $incrementIdStorage
    ) {
        $this->configProvider = $configProvider;
        $this->variablesProvider = $variablesProvider;
        $this->incrementIdStorage = $incrementIdStorage;
    }

    public function collect(int $fingerprint = null, array &$data = []): void
    {
        $fingerprint ??= time();
        $accountId = $this->configProvider->getAccountId();
        $containerId = $this->configProvider->getContainerId();
        $containerVersionOptions = $this->getContainerVersionOptions($accountId, $containerId, $fingerprint);

        $data = [
            'exportFormatVersion' => 2,
            'exportTime' => (new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT),
            'containerVersion' => $containerVersionOptions,
            'fingerprint' => $fingerprint,
            'tagManagerUrl' => 'https://tagmanager.google.com/#/versions/accounts/' . $accountId
                . '/containers/' . $containerId . '/versions/0?apiLink=version'
        ];
    }

    private function getContainerVersionOptions(string $accountId, string $containerId, int $fingerprint): array
    {
        $measurementId = $this->configProvider->getMeasurementId();

        return [
            'path' => 'accounts/' . $accountId . '/containers/' . $containerId . '/versions/0',
            'accountId' => $accountId,
            'containerId' => $containerId,
            'containerVersionId' => '0',
            'container' => [
                'path' => 'accounts/' . $accountId . '/containers/' . $containerId,
                'accountId' => $accountId,
                'containerId' => $containerId,
                'name' => 'Amasty_GA4 JsonExport',
                'publicId' => $this->configProvider->getPublicId(),
                'usageContext' => [
                    'WEB'
                ],
                'fingerprint' => $fingerprint,
                'tagManagerUrl' => 'https://tagmanager.google.com/#/container/accounts/' . $accountId
                    . '/containers/' . $containerId . '/workspaces?apiLink=container'
            ],
            'builtInVariable' => [
                [
                    'accountId' => $accountId,
                    'containerId' => $containerId,
                    'type' => 'PAGE_URL',
                    'name' => 'Page URL'
                ],
                [
                    'accountId' => $accountId,
                    'containerId' => $containerId,
                    'type' => 'PAGE_HOSTNAME',
                    'name' => 'Page Hostname'
                ],
                [
                    'accountId' => $accountId,
                    'containerId' => $containerId,
                    'type' => 'PAGE_PATH',
                    'name' => 'Page Path'
                ],
                [
                    'accountId' => $accountId,
                    'containerId' => $containerId,
                    'type' => 'REFERRER',
                    'name' => 'Referrer'
                ],
                [
                    'accountId' => $accountId,
                    'containerId' => $containerId,
                    'type' => 'EVENT',
                    'name' => 'Event'
                ],
            ],
            'variable' => $this->variablesProvider->get($accountId, $containerId, $measurementId, $fingerprint),
            'trigger' => [$this->getGtmDomTrigger($accountId, $containerId, $fingerprint)],
            'tag' => [$this->getMeasurementTag($accountId, $containerId, $fingerprint)],
            'fingerprint' => $fingerprint
        ];
    }

    private function getGtmDomTrigger(string $accountId, string $containerId, int $fingerprint): array
    {
        return [
            'name' => 'Amasty GA4 - gtm.dom',
            'type' => 'CUSTOM_EVENT',
            'customEventFilter' => [
                [
                    'type' => 'EQUALS',
                    'parameter' => [
                        [
                            'type' => 'TEMPLATE',
                            'key' => 'arg0',
                            'value' => '{{_event}}'
                        ],
                        [
                            'type' => 'TEMPLATE',
                            'key' => 'arg1',
                            'value' => 'gtm.dom'
                        ]
                    ]
                ]
            ],
            'filter' => [
                [
                    'type' => 'EQUALS',
                    'parameter' => [
                        [
                            'type' => 'TEMPLATE',
                            'key' => 'arg0',
                            'value' => '{{Event}}'
                        ],
                        [
                            'type' => 'TEMPLATE',
                            'key' => 'arg1',
                            'value' => 'gtm.dom'
                        ]
                    ]
                ]
            ],
            'accountId' => $accountId,
            'containerId' => $containerId,
            'triggerId' => $this->incrementIdStorage->getTriggerId(),
            'fingerprint' => $fingerprint
        ];
    }

    private function getMeasurementTag(string $accountId, string $containerId, int $fingerprint): array
    {
        return [
            'name' => self::MEASUREMENT_TAG_NAME,
            'firingTriggerId' => [
                self::ALL_PAGES_TRIGGER_ID
            ],
            'tagFiringOption' => 'ONCE_PER_EVENT',
            'type' => 'gaawc',
            'parameter' => [
                [
                    'type' => 'BOOLEAN',
                    'key' => 'sendPageView',
                    'value' => 'true'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'measurementId',
                    'value' => '{{' . VariablesProvider::MEASUREMENT_ID . '}}'
                ]
            ],
            'monitoringMetadata' => [
                'type' => 'MAP'
            ],
            'accountId' => $accountId,
            'containerId' => $containerId,
            'tagId' => $this->incrementIdStorage->getTagId(),
            'fingerprint' => $fingerprint
        ];
    }
}
