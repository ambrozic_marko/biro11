<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\GtmMeta;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;

class PostProcessing implements GtmMetaInterface
{
    private const KEYS_TO_POSTPROCESS = ['variable', 'trigger', 'tag'];

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    public function __construct(
        ConfigProvider $configProvider
    ) {
        $this->configProvider = $configProvider;
    }

    public function collect(int $fingerprint = null, array &$data = []): void
    {
        foreach (self::KEYS_TO_POSTPROCESS as $key) {
            if (isset($data['containerVersion'][$key])) {
                foreach ($data['containerVersion'][$key] as &$element) {
                    $element['accountId'] ??= $this->configProvider->getAccountId();
                    $element['containerId'] ??= $this->configProvider->getContainerId();
                    $element['fingerprint'] ??= $fingerprint;
                }
            }
        }
    }
}
