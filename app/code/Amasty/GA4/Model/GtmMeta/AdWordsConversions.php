<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\GtmMeta;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\GtmMeta\Utils\VariablesProvider;
use Amasty\GA4Api\Model\Event\Meta\IncrementIdStorage;
use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;

class AdWordsConversions implements GtmMetaInterface
{
    public const VAR_CONVERSION_TRACKING_CONVERSION_VALUE = 'Amasty - Conversion Value';
    public const VAR_CONVERSION_TRACKING_ORDER_ID = 'Amasty - Conversion Order ID';
    public const TEMPLATE_CONVERSION_TRACKING_CONVERSION_VALUE = 'am_conversion_value';
    public const TEMPLATE_CONVERSION_TRACKING_ORDER_ID = 'am_conversion_order_id';

    public const TRIGGER_CONVERSION_TRACKING = 'Amasty - Checkout Success Pages';

    public const TAG_CONVERSION_TRACKING = 'Amasty - AdWords Conversion Tracking';

    private const DEFAULT_SUCCESS_PAGE = 'checkout/onepage/success';

    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var VariablesProvider
     */
    private VariablesProvider $variablesProvider;

    /**
     * @var IncrementIdStorage
     */
    private IncrementIdStorage $incrementIdStorage;

    public function __construct(
        ConfigProvider $configProvider,
        VariablesProvider $variablesProvider,
        IncrementIdStorage $incrementIdStorage
    ) {
        $this->configProvider = $configProvider;
        $this->variablesProvider = $variablesProvider;
        $this->incrementIdStorage = $incrementIdStorage;
    }

    public function collect(int $fingerprint = null, array &$data = []): void
    {
        $fingerprint ??= time();
        if (!$this->configProvider->isAdWordsConversionEnabled()) {
            return;
        }

        $variables = $this->prepareVariables($fingerprint);
        $triggers = $this->prepareTriggers($fingerprint);
        $tags = $this->prepareTags($fingerprint, $triggers['triggerId']);

        $data['containerVersion']['trigger'][] = $triggers;
        $data['containerVersion']['tag'][] = $tags;
        array_push($data['containerVersion']['variable'], ...$variables);
    }

    private function prepareVariables(int $fingerprint): array
    {
        $accountId = $this->configProvider->getAccountId();
        $containerId = $this->configProvider->getContainerId();

        return [
            $this->variablesProvider->prepareBasicVariable(
                self::VAR_CONVERSION_TRACKING_CONVERSION_VALUE,
                self::TEMPLATE_CONVERSION_TRACKING_CONVERSION_VALUE,
                $accountId,
                $containerId,
                $fingerprint
            ),
            $this->variablesProvider->prepareBasicVariable(
                self::VAR_CONVERSION_TRACKING_ORDER_ID,
                self::TEMPLATE_CONVERSION_TRACKING_ORDER_ID,
                $accountId,
                $containerId,
                $fingerprint
            )
        ];
    }

    private function prepareTriggers(int $fingerprint): array
    {
        $successPages = [
            self::DEFAULT_SUCCESS_PAGE,
            ...$this->configProvider->getSuccessPages()
        ];

        return [
            'name' => self::TRIGGER_CONVERSION_TRACKING,
            'type' => 'PAGEVIEW',
            'triggerId' => $this->incrementIdStorage->getTriggerId(),
            'filter' => $this->prepareTriggerFilter(array_filter($successPages)),
            'accountId' => $this->configProvider->getAccountId(),
            'containerId' => $this->configProvider->getContainerId(),
            'fingerprint' => $fingerprint
        ];
    }

    private function prepareTags(int $fingerprint, int $triggerId): array
    {
        $accountId = $this->configProvider->getAccountId();
        $containerId = $this->configProvider->getContainerId();

        return [
            'name' => self::TAG_CONVERSION_TRACKING,
            'type' => 'awct',
            'firingTriggerId' => [
                $triggerId
            ],
            'tagFiringOption' => 'ONCE_PER_EVENT',
            'tagId' => $this->incrementIdStorage->getTagId(),
            'parameter' => [
                [
                    'type' => 'BOOLEAN',
                    'key' => 'enableConversionLinker',
                    'value' => 'true'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'conversionCookiePrefix',
                    'value' => '_gcl'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'conversionValue',
                    'value' => '{{' . self::VAR_CONVERSION_TRACKING_CONVERSION_VALUE . '}}'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'orderId',
                    'value' => '{{' . self::VAR_CONVERSION_TRACKING_ORDER_ID . '}}'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'conversionId',
                    'value' => $this->configProvider->getConversionId()
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'currencyCode',
                    'value' => $this->configProvider->getConversionCurrency()
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'conversionLabel',
                    'value' => $this->configProvider->getConversionLabel()
                ]
            ],
            'monitoringMetadata' => [
                'type' => 'MAP'
            ],
            'accountId' => $accountId,
            'containerId' => $containerId,
            'fingerprint' => $fingerprint
        ];
    }

    private function prepareTriggerFilter(array $filters): array
    {
        return [
            'type' => 'MATCH_REGEX',
            'parameter' => [
                [
                    'type' => 'TEMPLATE',
                    'key' => 'arg0',
                    'value' => '{{Page URL}}'
                ],
                [
                    'type' => 'TEMPLATE',
                    'key' => 'arg1',
                    'value' => '(' . implode('|', $filters) . ')'
                ]
            ]
        ];
    }
}
