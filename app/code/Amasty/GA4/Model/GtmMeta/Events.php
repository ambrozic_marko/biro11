<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\GtmMeta;

use Amasty\GA4Api\Model\Event\EventsConfigPool;
use Amasty\GA4Api\Model\GtmMeta\GtmMetaInterface;

class Events implements GtmMetaInterface
{
    /**
     * @var EventsConfigPool
     */
    private EventsConfigPool $eventsConfigPool;

    public function __construct(
        EventsConfigPool $eventsConfigPool
    ) {
        $this->eventsConfigPool = $eventsConfigPool;
    }

    public function collect(int $fingerprint = null, array &$data = []): void
    {
        $result = [];
        $fingerprint ??= time();
        foreach ($this->eventsConfigPool->getAll() as $eventConfig) {
            $result = array_merge_recursive($result, $eventConfig->getMeta($fingerprint));
        }
        $data = array_merge_recursive($data, $result);
    }
}
