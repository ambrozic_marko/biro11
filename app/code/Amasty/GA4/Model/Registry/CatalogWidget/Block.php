<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Registry\CatalogWidget;

use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;

class Block
{
    /**
     * @var array ['block_name' => ProductCollection]
     */
    private array $blocksData = [];

    public function register(string $blockName, ProductCollection $productCollection): void
    {
        $this->blocksData[$blockName] = $productCollection;
    }

    public function getBlocksData(): array
    {
        return $this->blocksData;
    }
}
