<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ResourceModel;

use Magento\Wishlist\Model\ResourceModel\Item;

class WishlistItemResource
{
    /**
     * @var Item
     */
    private Item $itemResource;

    public function __construct(
        Item $itemResource
    ) {
        $this->itemResource = $itemResource;
    }

    public function getProductIdByWishlistId(int $id): int
    {
        $connection = $this->itemResource->getConnection();
        $select = $connection->select()->from(
            $this->itemResource->getMainTable(),
            'product_id'
        )->where($this->itemResource->getIdFieldName() . ' = ?', $id);

        return (int)$connection->fetchOne($select);
    }
}
