<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ResourceModel;

use Magento\Customer\Model\ResourceModel\Group;

class CustomerGroupResource
{
    /**
     * @var Group
     */
    private Group $groupResource;

    public function __construct(
        Group $groupResource
    ) {
        $this->groupResource = $groupResource;
    }

    public function getNameById(int $id): string
    {
        $connection = $this->groupResource->getConnection();
        $select = $connection->select()->from(
            $this->groupResource->getMainTable(),
            'customer_group_code'
        )->where('customer_group_id = ?', $id);

        return (string)$connection->fetchOne($select);
    }
}
