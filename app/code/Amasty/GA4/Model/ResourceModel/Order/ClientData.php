<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ResourceModel\Order;

use Magento\Framework\App\ResourceConnection;

class ClientData
{
    public const GA4_CLIENT_DATA_TABLE = 'amasty_ga4_client_data';
    public const ID = 'id';
    public const ORDER_ID = 'order_id';
    public const CLIENT_ID = 'client_id';

    /**
     * @var ResourceConnection
     */
    private ResourceConnection $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    public function getClientData(int $orderId): string
    {
        $connection = $this->resourceConnection->getConnection();
        $table = $this->resourceConnection->getTableName(self::GA4_CLIENT_DATA_TABLE);
        $select = $connection->select()
            ->from(
                ['a' => $table],
                [self::CLIENT_ID]
            )->where('a.' . self::ORDER_ID . ' = ?', $orderId);

        return $connection->fetchOne($select) ?: '';
    }
}
