<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ResourceModel\Order\Handlers;

use Amasty\GA4\Model\ResourceModel\Order\ClientData;
use Magento\Sales\Api\Data\OrderInterface;

class ReadHandler
{
    /**
     * @var ClientData
     */
    private ClientData $clientData;

    public function __construct(
        ClientData $clientData
    ) {
        $this->clientData = $clientData;
    }

    public function loadAttribute(OrderInterface $order): OrderInterface
    {
        $extension = $order->getExtensionAttributes();
        if (null !== $extension->getAmGa4ClientId()) {
            return $order;
        }
        $orderId = (int)$order->getId();
        $clientId = $this->clientData->getClientData($orderId);
        $extension->setAmGa4ClientId($clientId);
        $order->setExtensionAttributes($extension);

        return $order;
    }
}
