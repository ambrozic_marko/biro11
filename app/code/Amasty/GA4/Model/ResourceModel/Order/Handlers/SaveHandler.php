<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ResourceModel\Order\Handlers;

use Amasty\GA4\Model\ResourceModel\Order\ClientData;
use Magento\Framework\App\ResourceConnection;

class SaveHandler
{
    /**
     * @var ResourceConnection
     */
    private ResourceConnection $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    public function saveClientData(string $orderId, string $ga): void
    {
        $connection = $this->resourceConnection->getConnection();
        $table = $this->resourceConnection->getTableName(ClientData::GA4_CLIENT_DATA_TABLE);

        $connection->insert(
            $table,
            [
                ClientData::ORDER_ID => $orderId,
                ClientData::CLIENT_ID => $ga
            ]
        );
    }
}
