<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\ResourceModel;

use Magento\Catalog\Api\Data\CategoryAttributeInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Select;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Store\Model\Store;

class CategoriesPathResource
{
    /**
     * @var ResourceConnection
     */
    private ResourceConnection $resourceConnection;

    /**
     * @var AttributeRepositoryInterface
     */
    private AttributeRepositoryInterface $attributeRepository;

    /**
     * @var MetadataPool
     */
    private MetadataPool $metadataPool;

    public function __construct(
        ResourceConnection $resourceConnection,
        AttributeRepositoryInterface $attributeRepository,
        MetadataPool $metadataPool
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->attributeRepository = $attributeRepository;
        $this->metadataPool = $metadataPool;
    }

    /**
     * @param int $categoryId
     * @return int[] ids of categories path
     */
    public function getCategoryPath(int $categoryId): array
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()->from(
            ['ent' => $this->resourceConnection->getTableName('catalog_category_entity')],
            ['path']
        )->where('entity_id = ?', $categoryId);

        $path = [];
        if ($categoryPath = $connection->fetchOne($select)) {
            $path = explode('/', $categoryPath);
            $path = array_map('intval', $path);
            $path = array_filter($path, static fn ($categoryId) => $categoryId !== Category::TREE_ROOT_ID);
        }

        return $path;
    }

    /**
     * @param int[] $categoryIds
     * @return array [
     *     (int) => (string), // key = category ID, value = category name
     * ]
     */
    public function getCategoriesNames(array $categoryIds, int $storeId = Store::DEFAULT_STORE_ID): array
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()->from(
            ['e' => $this->resourceConnection->getTableName('catalog_category_entity')],
            [
                'e.entity_id',
                'name' => 'IFNULL(`val`.`value`, `def`.`value`)'
            ]
        );
        if ($storeId !== Store::DEFAULT_STORE_ID) {
            $this->joinAttributeTable($select, 'def', Store::DEFAULT_STORE_ID);
        }
        $this->joinAttributeTable($select, 'val', $storeId);
        $select->where('e.entity_id IN (?)', $categoryIds, \Zend_Db::INT_TYPE);

        return $connection->fetchPairs($select);
    }

    private function joinAttributeTable(
        Select $select,
        string $alias,
        int $storeId
    ): void {
        $nameAttribute = $this->attributeRepository->get(CategoryAttributeInterface::ENTITY_TYPE_CODE, 'name');
        $connection = $this->resourceConnection->getConnection();
        $metadata = $this->metadataPool->getMetadata(CategoryInterface::class);
        $linkField = $metadata->getLinkField();
        $joinConditions = [
            'e.' . $linkField . ' = ' . $alias . '.' . $linkField,
            $alias . '.attribute_id = ' . $nameAttribute->getId(),
            $connection->quoteInto($alias . '.store_id = ?', $storeId),
        ];
        $select->joinLeft(
            [$alias => $nameAttribute->getBackendTable()],
            implode(' AND ', $joinConditions),
            []
        );
    }
}
