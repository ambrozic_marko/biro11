<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Frontend;

use Amasty\GA4\Model\ResourceModel\CustomerGroupResource;
use Amasty\GA4Api\Model\DataLayerCollector;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class CustomerPropertiesProcessor
{
    public const CUSTOMER_GROUP_KEY = 'customerGroup';
    public const CUSTOMER_ID_KEY = 'customerId';

    /**
     * @var DataLayerCollector
     */
    private DataLayerCollector $dataLayerCollector;

    /**
     * @var CustomerGroupResource
     */
    private CustomerGroupResource $customerGroupResource;

    /**
     * @var GA4Session
     */
    private GA4Session $ga4Session;

    public function __construct(
        DataLayerCollector $dataLayerCollector,
        CustomerGroupResource $customerGroupResource,
        GA4Session $ga4Session
    ) {
        $this->dataLayerCollector = $dataLayerCollector;
        $this->customerGroupResource = $customerGroupResource;
        $this->ga4Session = $ga4Session;
    }

    /**
     * adds user properties to common datalayer
     */
    public function execute(): void
    {
        $groupId = (int)$this->ga4Session->getCustomerGroupId();
        $groupName = $this->customerGroupResource->getNameById($groupId);
        if ($groupName) {
            $this->dataLayerCollector->addUserProperty(self::CUSTOMER_GROUP_KEY, $groupName);
        }

        if ($customerId = (int)$this->ga4Session->getCustomerId()) {
            $this->dataLayerCollector->addUserProperty(self::CUSTOMER_ID_KEY, $customerId);
        }
    }
}
