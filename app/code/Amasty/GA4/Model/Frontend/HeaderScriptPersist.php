<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Frontend;

use Amasty\GA4\Model\ConfigProvider;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class HeaderScriptPersist implements ArgumentInterface
{
    public const OPEN_SCRIPT_TAG = '<script>';
    public const CLOSE_SCRIPT_TAG = '</script>';
    public const OPEN_REPLACER_TAG = '<am-ga4>';
    public const CLOSE_REPLACER_TAG = '</am-ga4>';

    /**
     * @var ConfigProvider $configProvider
     */
    private ConfigProvider $configProvider;

    public function __construct(
        ConfigProvider $configProvider
    ) {
        $this->configProvider = $configProvider;
    }

    public function getOpenTag(): string
    {
        return $this->configProvider->isMoveScriptEnabled()
            ? self::OPEN_REPLACER_TAG
            : self::OPEN_SCRIPT_TAG;
    }

    public function getCloseTag(): string
    {
        return $this->configProvider->isMoveScriptEnabled()
            ? self::CLOSE_REPLACER_TAG
            : self::CLOSE_SCRIPT_TAG;
    }

    public function changeReplacerTags(string $content): string
    {
        return str_replace(
            [self::OPEN_REPLACER_TAG, self::CLOSE_REPLACER_TAG],
            [self::OPEN_SCRIPT_TAG, self::CLOSE_SCRIPT_TAG],
            $content
        );
    }
}
