<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Frontend;

use Detection\MobileDetect as MobileDetectLib;
use Magento\Framework\HTTP\Header;
use Magento\Framework\ObjectManagerInterface;

class MobileDetect
{
    /**
     * @var MobileDetectLib|null
     */
    private ?MobileDetectLib $mobileDetector = null;

    /**
     * @var Header
     */
    private Header $header;

    public function __construct(
        Header $header,
        ObjectManagerInterface $objectManager
    ) {
        // We are using object manager to create 3rd-party packages' class
        if (class_exists(MobileDetectLib::class)) {
            $this->mobileDetector = $objectManager->get(MobileDetectLib::class);
        }

        $this->header = $header;
    }

    public function isMobile(): bool
    {
        return $this->mobileDetector === null
            ? stripos($this->header->getHttpUserAgent(), 'mobi') !== false
            : $this->mobileDetector->isMobile();
    }
}
