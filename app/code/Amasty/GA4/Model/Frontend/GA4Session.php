<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model\Frontend;

use Magento\Framework\Session\SessionManager;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class GA4Session extends SessionManager
{
    public const CUSTOMER_GROUP_ID_KEY = 'customer_group_id';
    public const CUSTOMER_ID_KEY = 'customer_id';
    public const EVENT_DATA = 'event_data';

    public function getEvents(bool $clear = false): array
    {
        return $this->getData(self::EVENT_DATA, $clear) ?? [];
    }

    public function addEvent(string $eventName, array $eventData): void
    {
        $events = $this->getEvents();
        $events[$eventName] = $eventData;

        $this->setData(self::EVENT_DATA, $events);
    }

    public function getEventByName(string $eventName): ?array
    {
        return $this->getData(self::EVENT_DATA)[$eventName] ?? null;
    }

    public function hasEvents(?string $eventName = null): bool
    {
        if ($eventName !== null) {
            return !empty($this->getEventByName($eventName));
        }

        return !empty($this->getEvents());
    }

    public function setCustomerId(?int $id): void
    {
        if ($id === 0) {
            $id = null;
        }
        $this->setData(self::CUSTOMER_ID_KEY, $id);
    }

    public function getCustomerId(bool $clear = false): ?int
    {
        $customerId = $this->getData(self::CUSTOMER_ID_KEY, $clear);

        return $customerId === null ? $customerId : (int)$customerId;
    }

    public function setCustomerGroupId(?int $id): void
    {
        $this->setData(self::CUSTOMER_GROUP_ID_KEY, $id);
    }

    public function getCustomerGroupId(bool $clear = false): ?int
    {
        $groupId = $this->getData(self::CUSTOMER_GROUP_ID_KEY, $clear);

        return $groupId === null ? $groupId : (int)$groupId;
    }
}
