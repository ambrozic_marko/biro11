<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Model;

use Amasty\Base\Model\ConfigProviderAbstract;
use Magento\Store\Model\ScopeInterface;

class ConfigProvider extends ConfigProviderAbstract
{
    /**
     * @var string
     */
    protected $pathPrefix = 'am_ga4/';

    public const ENABLE = 'general/enable';
    public const PUBLIC_ID = 'general/public_id';
    public const SERVER_CONTAINER_URL = 'general/server_container_url';
    public const PRODUCT_IDENTIFIER = 'general/product_identifier';
    public const ENABLE_BRAND = 'general/enable_brand';
    public const BRAND_ATTRIBUTE = 'general/brand_attribute';
    public const ENABLE_VARIANT = 'general/enable_variant';
    public const IDENTIFIER_RESOLUTION = 'general/identifier_resolution';
    public const ORDER_TOTAL_CALCULATION = 'general/order_total_calculation';
    public const EXCLUDE_TAX = 'general/exclude_tax';
    public const EXCLUDE_SHIPPING = 'general/exclude_shipping';
    public const EXCLUDE_FREE_ORDERS = 'general/exclude_free_orders';
    public const SUCCESS_PAGES = 'general/success_pages';
    public const MEASURE_PRODUCT_CLICKS = 'general/measure_product_clicks';
    public const PROMOTION_TRACKING = 'general/promotion_tracking';

    public const ACCOUNT_ID = 'gtm_api/account_id';
    public const CONTAINER_ID = 'gtm_api/container_id';
    public const MEASUREMENT_ID = 'gtm_api/measurement_id';

    public const API_SECRET = 'api_tracking/secret';
    public const REFUND_TRACKING = 'api_tracking/refunds';
    public const ENABLE_ADWORDS_CONVERSION = 'adwords_conversion/enable';
    public const CONVERSION_ID = 'adwords_conversion/conversion_id';
    public const CONVERSION_LABEL = 'adwords_conversion/conversion_label';
    public const CONVERSION_CURRENCY = 'adwords_conversion/conversion_currency';
    public const CONVERSION_EXCLUDE_FREE_ORDERS = 'adwords_conversion/exclude_free_orders';

    public const ENABLE_ADWORDS_REMARKETING = 'adwords_remarketing/enable';
    public const REMARKETING_CONVERSION_ID = 'adwords_remarketing/conversion_id';

    public const DEV_JS_MOVE_SCRIPT_TO_BOTTOM = 'dev/js/move_script_to_bottom';

    public const DEFAULT_GTM_URL = 'https://www.googletagmanager.com';

    /**
     * @var int|null
     */
    private ?int $overrideCode = null;

    /**
     * @var string|null
     */
    private ?string $overrideType = null;

    public function overrideScope(string $scopeType, int $scopeCode): void
    {
        $this->overrideType = $scopeType;
        $this->overrideCode = $scopeCode;
    }

    public function isEnabled(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::ENABLE, $storeId, $scope);
    }

    public function getGtmEndpoint(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)($this->getValue(self::SERVER_CONTAINER_URL, $storeId, $scope) ?? self::DEFAULT_GTM_URL);
    }

    public function getProductIdentifier(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): int
    {
        return (int)$this->getValue(self::PRODUCT_IDENTIFIER, $storeId, $scope);
    }

    public function isEnableBrand(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::ENABLE_BRAND, $storeId, $scope);
    }

    public function getBrandAttribute(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::BRAND_ATTRIBUTE, $storeId, $scope);
    }

    public function isEnableVariant(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::ENABLE_VARIANT, $storeId, $scope);
    }

    public function getIdentifierResolution(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): int
    {
        return (int)$this->getValue(self::IDENTIFIER_RESOLUTION, $storeId, $scope);
    }

    public function getOrderTotalCalculation(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): int
    {
        return (int)$this->getValue(self::ORDER_TOTAL_CALCULATION, $storeId, $scope);
    }

    public function isExcludeTax(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::EXCLUDE_TAX, $storeId, $scope);
    }

    public function isExcludeShipping(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::EXCLUDE_SHIPPING, $storeId, $scope);
    }

    public function isExcludeFreeOrders(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::EXCLUDE_FREE_ORDERS, $storeId, $scope);
    }

    public function getSuccessPages(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): array
    {
        $pages = (string)$this->getValue(self::SUCCESS_PAGES, $storeId, $scope);

        return array_map(static fn ($page) => trim(trim($page, '/')), explode(',', $pages));
    }

    public function isMeasureProductClicks(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::MEASURE_PRODUCT_CLICKS, $storeId, $scope);
    }

    public function isPromotionTrackingEnabled(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::PROMOTION_TRACKING, $storeId, $scope);
    }

    public function getAccountId(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::ACCOUNT_ID, $storeId, $scope);
    }

    public function getContainerId(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::CONTAINER_ID, $storeId, $scope);
    }

    public function getMeasurementId(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::MEASUREMENT_ID, $storeId, $scope);
    }

    public function getSecretKey(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::API_SECRET, $storeId, $scope);
    }

    public function isTrackRefund(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::REFUND_TRACKING, $storeId, $scope);
    }

    public function getPublicId(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::PUBLIC_ID, $storeId, $scope);
    }

    public function isAdWordsConversionEnabled(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(self::ENABLE_ADWORDS_CONVERSION, $storeId, $scope);
    }

    public function getConversionId(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::CONVERSION_ID, $storeId, $scope);
    }

    public function getConversionLabel(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::CONVERSION_LABEL, $storeId, $scope);
    }

    public function getConversionCurrency(?int $storeId = null, string $scope = ScopeInterface::SCOPE_STORE): string
    {
        return (string)$this->getValue(self::CONVERSION_CURRENCY, $storeId, $scope);
    }

    public function isExcludeConversionFreeOrders(
        ?int $storeId = null,
        string $scope = ScopeInterface::SCOPE_STORE
    ): bool {
        return $this->isSetFlag(self::CONVERSION_EXCLUDE_FREE_ORDERS, $storeId, $scope);
    }

    public function isAdWordsRemarketingEnabled(
        ?int $storeId = null,
        string $scope = ScopeInterface::SCOPE_STORE
    ): bool {
        return $this->isSetFlag(self::ENABLE_ADWORDS_REMARKETING, $storeId, $scope);
    }

    public function getRemarketingConversionId(
        ?int $storeId = null,
        string $scope = ScopeInterface::SCOPE_STORE
    ): string {
        return (string)$this->getValue(self::REMARKETING_CONVERSION_ID, $storeId, $scope);
    }

    public function isMoveScriptEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::DEV_JS_MOVE_SCRIPT_TO_BOTTOM);
    }

    protected function getValue($path, $storeId = null, $scope = ScopeInterface::SCOPE_STORE)
    {
        if ($this->overrideType !== null) {
            $scope = $this->overrideType;
        }
        if ($this->overrideCode !== null) {
            $storeId = $this->overrideCode;
        }

        return parent::getValue($path, $storeId, $scope);
    }
}
