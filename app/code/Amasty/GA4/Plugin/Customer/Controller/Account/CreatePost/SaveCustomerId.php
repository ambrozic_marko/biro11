<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Plugin\Customer\Controller\Account\CreatePost;

use Amasty\GA4\Block\DataLayer;
use Amasty\GA4\Model\Frontend\GA4Session;
use Magento\Customer\Controller\Account\CreatePost;
use Magento\Customer\Model\Session;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Controller\ResultInterface;

class SaveCustomerId
{
    /**
     * @var Session
     */
    private Session $customerSession;

    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;

    /**
     * @var GA4Session
     */
    private GA4Session $ga4Session;

    public function __construct(
        Session $customerSession,
        CacheInterface $cache,
        GA4Session $ga4Session
    ) {
        $this->customerSession = $customerSession;
        $this->cache = $cache;
        $this->ga4Session = $ga4Session;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(CreatePost $subject, ResultInterface $result): ResultInterface
    {
        if ($this->customerSession->getCustomerGroupId()) {
            $this->ga4Session->setCustomerGroupId((int)$this->customerSession->getCustomerGroupId());
        }
        if ($this->customerSession->getCustomerId()) {
            $this->ga4Session->setCustomerId((int)$this->customerSession->getCustomerId());
        }

        $this->cache->clean([DataLayer::IDENTITY_KEY]);

        return $result;
    }
}
