<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Plugin\Framework\App\Response\Http;

use Amasty\GA4\Model\ConfigProvider;
use Amasty\GA4\Model\Frontend\HeaderScriptPersist;
use Magento\Framework\App\Response\Http;

class UpdateScriptTag
{
    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;

    /**
     * @var HeaderScriptPersist $headerScriptPersist
     */
    private HeaderScriptPersist $headerScriptPersist;

    public function __construct(
        ConfigProvider $configProvider,
        HeaderScriptPersist $headerScriptPersist
    ) {
        $this->configProvider = $configProvider;
        $this->headerScriptPersist = $headerScriptPersist;
    }

    public function beforeSendResponse(Http $subject): void
    {
        $content = $subject->getContent();

        if (is_string($content)
            && $this->configProvider->isEnabled()
            && $this->configProvider->isMoveScriptEnabled()
            && $this->shouldUpdateScriptTag($content)
        ) {
            $subject->setContent($this->headerScriptPersist->changeReplacerTags($content));
        }
    }

    /**
     * @param string $content
     * @return bool
     */
    private function shouldUpdateScriptTag(string $content): bool
    {
        return strpos($content, HeaderScriptPersist::OPEN_REPLACER_TAG) !== false;
    }
}
