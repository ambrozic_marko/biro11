<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Plugin\Sales\Model\Order\RefundAdapterInterface;

use Amasty\GA4\Model\Event\Order\Refund;
use Amasty\GA4\Model\EventsProcessor;
use Amasty\GA4\Model\ResourceModel\Order\Handlers\ReadHandler;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\RefundAdapterInterface;

class TrackCreditMemo
{
    /**
     * @var EventsProcessor
     */
    private EventsProcessor $eventsProcessor;

    /**
     * @var ReadHandler
     */
    private ReadHandler $readHandler;

    public function __construct(
        ReadHandler $readHandler,
        EventsProcessor $eventsProcessor
    ) {
        $this->readHandler = $readHandler;
        $this->eventsProcessor = $eventsProcessor;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterRefund(
        RefundAdapterInterface $subject,
        $result,
        CreditmemoInterface $creditmemo,
        OrderInterface $order
    ) {
        $this->readHandler->loadAttribute($order);
        $this->eventsProcessor->processEvent(
            Refund::NAME,
            [
                'type' => Refund::NAME,
                'creditMemo' => $creditmemo,
                'order' => $order,
            ]
        );

        return $order;
    }
}
