<?php

declare(strict_types=1);

/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (https://www.amasty.com)
 * @package Google Analytics GA4 by Amasty
 */

namespace Amasty\GA4\Plugin\CatalogWidget\Block\Product\ProductsList;

use Amasty\GA4\Model\Registry\CatalogWidget\Block as BlockNameRegistry;
use Magento\CatalogWidget\Block\Product\ProductsList;

class BlockRegistry
{
    /**
     * @var BlockNameRegistry
     */
    private BlockNameRegistry $registry;

    public function __construct(
        BlockNameRegistry $registry
    ) {
        $this->registry = $registry;
    }

    public function afterToHtml(ProductsList $subject, string $result): string
    {
        if ($productCollection = $subject->getProductCollection()) {
            $this->registry->register($subject->getNameInLayout(), $productCollection);
        }

        return $result;
    }
}
