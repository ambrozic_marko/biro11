<?php


namespace Pgc\Pgc\Model\Payment;

use Magento\Sales\Api\Data\TransactionInterface;
use Pgc\Pgc\Model\Ui\ConfigProvider;
use Magento\Payment\Model\InfoInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class Bankart extends \Magento\Payment\Model\Method\AbstractMethod {

	protected $_code = ConfigProvider::CREDITCARD_CODE;
	protected $_infoBlockType = \Magento\Payment\Block\Info\Cc::class;
	
	protected $_isGateway = true;
	protected $_canCapture = true;
	protected $_canCapturePartial = true;
	protected $_canVoid = true;
	protected $_canRefund = false;
    
    

    /**
     * 
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
      public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
      	\Pgc\Pgc\Helper\Data $pgcHelper,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
		\Magento\Checkout\Model\Session $checkoutSession      
              
    ) {
        $this->pgcHelper = $pgcHelper;
        $this->orderSender = $orderSender;
        $this->httpClientFactory = $httpClientFactory;
        $this->checkoutSession = $checkoutSession;
        

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger
        );

    }

    /*public function canUseForCurrency($currencyCode) {
        if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
            return false;
        }
        return true;
    }*/

    public function capture(InfoInterface $payment, $amount)
    {
    	if ($amount > 0)
    	{
    		$paymentMethod = 'pgc_creditcard';
    
    		\Pgc\Client\Client::setApiUrl($this->pgcHelper->getGeneralConfigData('host'));
    		$client = new \Pgc\Client\Client(
    				$this->pgcHelper->getGeneralConfigData('username'),
    				$this->pgcHelper->getGeneralConfigData('password'),
    				$this->pgcHelper->getPaymentConfigData('api_key', $paymentMethod, null),
    				$this->pgcHelper->getPaymentConfigData('shared_secret', $paymentMethod, null)
    				);
    
    		$capture = new \Pgc\Client\Transaction\Capture();
    		$capture->setTransactionId($payment->getOrder()->getIncrementId() . '-capture');
    		$capture->setReferenceTransactionId($payment->getLastTransId());
    
    		$capture->setAmount(\number_format($amount, 2, '.', ''));
    		$capture->setCurrency($payment->getOrder()->getOrderCurrency()->getCode());;
    		try
    		{
    			$captureResult = $client->capture($capture);
    			if (!$captureResult->isSuccess()) {
    				$msg = __('Something went wrong performing the capture.');
    				throw new LocalizedException($msg);
    			}
    		}
    		catch (\Exception $e)
    		{
    			throw new CouldNotSaveException(__('Could not capture the payment'));
    		}
    	}
    
    	$payment->setParentTransactionId($payment->getLastTransId());
    	$payment->setTransactionId($captureResult->getReferenceId());
    	$payment->setLastTransId($captureResult->getReferenceId());
    
    	return $this;
    }
    
    public function void(InfoInterface $payment, $amount = null)
    {
    	$paymentMethod = 'pgc_creditcard';
    
    	\Pgc\Client\Client::setApiUrl($this->pgcHelper->getGeneralConfigData('host'));
    	$client = new \Pgc\Client\Client(
    			$this->pgcHelper->getGeneralConfigData('username'),
    			$this->pgcHelper->getGeneralConfigData('password'),
    			$this->pgcHelper->getPaymentConfigData('api_key', $paymentMethod, null),
    			$this->pgcHelper->getPaymentConfigData('shared_secret', $paymentMethod, null)
    			);
    
    	$void = new \Pgc\Client\Transaction\VoidTransaction();
    	$void->setTransactionId($payment->getOrder()->getIncrementId() . '-void');
    	$void->setReferenceTransactionId($payment->getLastTransId());
    
    	try
    	{
    		$voidResult = $client->void($void);
    		if (!$voidResult->isSuccess()) {
    			$msg = __('Something went wrong performing the void.');
    			throw new LocalizedException($msg);
    		}
    	}
    	catch (\Exception $e)
    	{
    		throw new \Exception(__('Could not void payment'));
    	}
    
    	$payment->setParentTransactionId($payment->getLastTransId());
    	$payment->setTransactionId($voidResult->getReferenceId());
    	$payment->setLastTransId($voidResult->getReferenceId());
    
    	return $this;
    }
    
    public function cancel(InfoInterface $payment, $amount = null)
    {
    	$this->void($payment);
    
    	return $this;
    }
}
